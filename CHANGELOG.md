###########
Change Log
###########

All notable changes to this project will be documented in this file.
This project adheres to `Semantic Versioning <http://semver.org/>`_.

Latest pre-release
------------------
- Updated the Helm charts to deploy PSS.LMC ver 0.2.0-rc1.

1.0.0-rc2
---------
- Update ska-csp-lmc-common to 1.0.1
- Update ska-low-cbf chart to v0.12.0
- Move get_device_proxy() method to common device
- Added integration tests for bugs  SKB-734, SKB-736, SKB-739
- Parametric configuration of helm chart for deploying multiple beams (up to 16) and subarrays (up to 4)


1.0.0-rc1
---------
- Update charts: 
  - ska-low-cbf to v0.11.0
  - ska-low-cbf-proc to v0.15.0
  - ska-low-cbf-tango-cnic to v 0.5.0
  - ska-tango-base to v0.4.12
  - ska-tango-util to v0.4.12
- Remove processors and add global env variables to use ska-low-cbf without HW (processors)
- Implemented semantic validation for the following:
  - SDP address when CBF is configured for visibilities.
  - PST beams must be included in the list of assigned beams for configuration.
- Update ska-pst to 1.0.1 (Warning: this version of ska-csp-lmc-low is not compatible with earlier releases of ska-pst than 0.13.0)
- Use of the NewTestHarness for assign/release resources test
- Update pst observationMode attribute to pstProcessingMode (fix SKB-692)
- Rework commands (fix SKB-637, SKB-501, SKB-629, SKB-505, SKB-689)
  - update tests using LRC in the CSP LMC simulators
  - update abort
  - update command timeout
  - implement command map as class
  - update initialisation process and init command
  - include capabilities in the initialisation process
  - update standby
  - update on/off
  - update scan
  - update gotoidle/reset/restart
  - update assign/release/releaseAll resources
  - clean code
  - fix restart command release pst beam (fix SKB-708)
  - update documentation
  - fix assigned beams and obsmode messages
  - update input file and add the possibility to configure multiple PST beams
- Update ska-csp-lmc-common to 1.0.0
- Update ska-csp-simulators to 0.8.0

0.19.1
------
- update ska-csp-lmc-common (fix SKB-591)
  - version (0.27.2)

0.19.0
------
- Implementation of FSP Capability
- helm chart uses image from artefact.skao.int
- Use common package version 0.27.0 supporting TelModel 1.19.2
- update default timeout to 30s (fix SKB-474)
- update ska-csp-simulators 
  - version (0.7.1)
  - registry: artifact.skao.int
- update ska-csp-lmc-common to v 0.27.1 (fix SKB-493)

0.18.0
------
- Add LowCspCapabilityPst class
- Update Makefile to exclude database connection during unit tests (PYTHON_VARS_BEFORE_PYTEST)
- update ska-csp-simulators 
  - version (0.7.0)
  - registry: harbor.skao.int/production
- update ska-csp-lmc-common to v 0.26.0

0.17.1
------
- Use common package version 0.25.1
- Add tests to verify behaviour of obsMode when reconfiguring
- Add LowCspCapability class

0.17.0
------
- Add CspLowJsonValidator class to perform semantic checks on JSON config file
- Use common package version 0.25.0
  - Override the obsMode attribute and change the type to array of obsMode values
  - Specialize obsMode evaluation based on LOW Configure json schema
 
0.16.2
------
- Fix bug SKB-429

0.16.1
------
Deprecated. 
Chart released with old oci image and failure in documentation.

0.16.0
------
- Use common package version 0.24.0
  - installing SKA Base Classes 1.0.0
  - update telescope model to v1.18.1
- Updated tests to use ENGINEERING adminMode instead of MAINTENANCE
- Use CSP simulators helm chart v. 0.4.0
- Update the PST configuration script specifying the key `timing_beam_id`.
- Update documentation
- Update charts:
  - PST v0.12.1
  - CBF v0.9.0
  - CBF_PROC v 0.13.0
  - taranta

0.15.0
------
- Add CHANGELOG to documentation
- Use common package version 0.22.1 supporting JSON strict validation (strictness=2) in 
  CspJsonValidator.
- example files and tests have been adjusted accordingly to adhere to the stricter JSON validation.
- Use TelModel CSP CompatibilityMap to identify CSP subsystems' interface.
- Implemented CI/CD tests (manual triggered) on Low PSI facility.
- Add telescope model and subsystem version pages in the documentation
- Add integration tests to evaluate HealthState aggregation using simulators
- Update csp lmc to use yaml file to configure its devices

0.14.0
------
- Updated common package to 0.22.0
  - pytango 9.5.0
  - SKA BC 0.20.2
- helm chart uses image in harbor.skao.int/staging
- update ska-tango-util and ska-tango-base charts to 0.4.11 and 0.4.10

0.13.1
------
- Fix versionId value (SKB-345)

0.13.0
------
- use ska-pst 0.11.0
- Use common package v0.21.0
- use json_configuration_parser to parse Scan Command
- Update controller and subarray attributes documentation with examples
- Fixed issues in Off command for Controller and Subarray: devices beloging to CBF sub-system are skipped because these devices are always in On state.
- Modified the behavior of the Subarray when Abort is issued with all the CSP sub-systems in EMPTY state: the subarray returns its observing state as ABORTED instead of EMPTY.
- Observing state model implements the aborted flag to force the ABORTED state.
- remove old simulators charts (sim-pss, sim-pst, sim-cbf) and use of the new ska-csp-simulators
- enable tests with new simulators
- enable scanID attribute subscription
- Remove old simulators charts (sim-pss, sim-pst, sim-cbf) and use of the new ska-csp-simulators
- Fix bug in configuring without PST if PST is assigned
- add tests to verify healthstate aggregation

0.12.1
------
- update RTD building procedure according to STS-553
- update RTD documentation

0.12.0
------
- Use common package v0.20.1 (use of telmodel 1.14.0)
- update timing beams destination in the cbf configure input after retrieving info from pst
- Update controller and subarray attributes documentation with examples

0.11.1
------
- Use common package v0.19.2 (fix abort during resource assignment).

0.11.0
------
- Use ska-csp-lmc-common package v. 0.19.1
- Added BDD tests to check the correct detection of archive events on the state, healthState and obsState attributes.
- Report the updated value for the configurationID attribute.

0.10.1
------
- use ska-csp-lmc-common v0.18.4 
- tested compatibility with ska-low-cbf 0.10.0 and ska-low-cbf-proc 0.11.0
- fixes formatting in readthedocs
- added check on processors in PSI-LOW tests
- fixed re-inizialization of PST devices in test-harness

0.10.0
------
- use ska-csp-lmc-common v0.18.3
- reintegration of PST Beam
- Full control of 4 subarrays (broken before)
- Test on PSI-LOW on demand from CI/CD
- First integration of xray for test result

0.9.1
-----
- update ska-csp-lmc-common package to v. 0.17.6
- update documentation
- removed constraints to python package from '3.10.6' to '~3.10.6'
- Command timeout default value set as Device Property of a device.
- Command timeout is configurable via TANGO attribute commandTimeout.
- Added support to report subarray versionId and buildState info
- Update libraries:
  - pytango v 9.4.2
  - ska-tango-base 0.19.1
- Update base containers:
  - ska-tango-images-pytango-builder:9.4.3
  - ska-tango-images-pytango-runtime:9.4.3
- Update charts:
  - ska-tango-util 0.4.7
  - ska-tango-base 0.4.7
  - ska-tango-images-tango-dsconfig 1.5.12
  - tango-itango 9.4.3
  - ska-tango-taranta 2.4.0
  - ska-tango-taranta-auth 0.1.7
  - ska-tango-tangogql 1.3.10
  - ska-pst-lmc 0.6.4 (not tested)
- Update .make subsystem to master
- Update sphinx packages, add them to pyproject.toml and use .readthedocs.yaml to build documentation

0.9.0
-----
- use ska-csp-lmc-common 0.17.3
- compatibility with ska-low-cbf v0.6.1 and new json schemas
- validation of cbf part of json input with telescope model

0.8.2
-----
- Update to ska-csp-lmc-common v0.17.1
- Update test environment
  - Add management of Long Running Command
- update charts. in particular
  - ska-tango-util to v0.4.5
  - ska-tango-base to v0.4.4
  - ska-pst-lmc to v0.5.4 
- Add the charts and json files to exclude PST from deployment
  - Update tests to manage the presence of the PST device
  - update documentation

0.8.1
-----
- Update to ska-csp-lmc-common v0.17.0
- Scan command is completed when all subsystems are scanning
- Examples documentation includes long running command attributes use


0.8.0
-----
[DEPRECATED]
Old version of common used. The code has the same functionalities of 0.7.1.

0.7.1
-----
- Update to ska-csp-lmc-common v0.16.0 (CspJsonvalidator)
- Update BC to v0.18.1
- Specialization of CspJsonValidator

0.7.0
-----
[DEPRECATED]
Release done by mistake. The code has the same functionalities of 0.6.0

0.6.0
-----
- Update ska-csp-lmc-common version to 0.15.0
- Update to BC v0.17.0
- Implement Abort command behavior for Subarrays
- Configure attributes that report subsystem states and modes to push archive and change events
- Add CSP.LMC devices and subsystems software version ID attributes

0.5.0
-----
- Update pytango to 9.3.6 and pytango images (ska-tango-images-pytango-builder:9.3.35 and ska-tango-images-pytango-runtime:9.3.22)
- Use of ska-csp-lmc-common v0.14.0

0.4.0
-----  
- Use ska-csp-lmc-common package version = 0.12.2
- Integration of PST beam 
- refactor of json validation mechanism

0.3.1
-----
- Use ska-csp-lmc-common package version = 0.12.2
- Updated low json parser to agree with the Low CBF proposed JSON schemas.
- Added connection to Low CBF Allocator devices to get infrmation about allocated resources. 

0.3.0
-----
- Use ska-csp-lmc-common v 0.12.0 (Changes in command execution)

0.2.3
-----
- Use ska-csp-lmc-common package version = 0.11.12 with changes in code for unit tests.
- Updated helm charts for Low CSP and simulated devices to 0.2.3
- Refactoring of the unit tests..

0.2.2
-----
- Refactoring of the unit tests.
- Implemented new cicd machinery and poetry.
- Skip helm chart publishing in gitlab.

0.2.1
-----

- use ska-csp-lmc-common 0.11.3
- Off controller command is allowed also from ON state

0.2.0
-----

- use ska-csp-lmc-common 0.11.1
- use json_config_parser based on common class SplitResources

**Released helm chart 0.2.0**

0.1.3
-----
NOTE: released on 23/11/2021

- use ska-csp-lmc-common 0.10.3
- Added support for Low CSP Subarray commands: AssignResources, ReleaseAllResources, Configure.

**Released helm chart 0.1.4**

0.1.2
-----
NOTE: released on 16/06/2021

- use tango images from CAR (Central Artefact Repository)
- update tango-base and tango-util

**Released helm chart 0.1.3**

0.1.1
-----
NOTE: released on 26/05/2021

- patch on initialization of attribute values (call to super().do() in InitCommand)

**Released helm chart 0.1.1**

- changed polling period for csp-cbf state attribute

**Released helm chart 0.1.2** (maintain same image number - changes only on Chart.yaml)

0.1.0
-----
NOTE: released on 17/05/2021

- First implementation of Csp-lmc low Controller
- ska-tango-base v0.10.1

**Released helm chart 0.1.0**


