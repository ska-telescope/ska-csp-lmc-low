#!/bin/bash

PSI_USER="mar902" #change with own username
LOCAL_FOLDER="." #change with own local folder

# Get the list of changed files using git status
changed_files=$(git status -s | awk '{print $2}')

echo $changed_files

# Loop through each changed file and copy it to the destination directory
for file in $changed_files; do
    echo "Copying $file"
    scp -J $PSI_USER@venice.atnf.csiro.au "$LOCAL_FOLDER/$file" $PSI_USER@psi-head.atnf.csiro.au:ska-csp-lmc-low/$file
    echo "Done!"
done

echo "Copy process complete."