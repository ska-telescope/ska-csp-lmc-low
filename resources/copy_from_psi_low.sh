#!/bin/bash

PSI_USER="mar902" #change with own username
LOCAL_FOLDER="/home/toor/ska-code/ska-csp-lmc-low/" #change with own local folder

# Get the list of changed files using git status
changed_files=$(ssh -J $PSI_USER@venice.atnf.csiro.au $PSI_USER@psi-head.atnf.csiro.au "cd ska-csp-lmc-low && git status -s | awk '{print $2}'")

# Loop through each changed file and copy it to the destination directory
for file in $changed_files; do
    # the if remove "M" and "??" from changed_files variable
    if (( "${#file}" > 2 )); then
        echo "Copying $file"
        scp -J $PSI_USER@venice.atnf.csiro.au $PSI_USER@psi-head.atnf.csiro.au:ska-csp-lmc-low/$file "$LOCAL_FOLDER$file"
        echo "Done!"
    fi
done

echo "Copy process complete."