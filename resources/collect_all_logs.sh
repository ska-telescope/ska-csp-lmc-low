#!/bin/bash

NAMESPACE="low-csp"
OUTPUT_FILE="logs.tar.gz"

# Get all pod names in the specified namespace
PODS=$(kubectl get pods -n $NAMESPACE --no-headers -o custom-columns=":metadata.name")

# Create a temporary directory to store log files
TMP_DIR=$(mktemp -d)

# Iterate over each pod and get its logs
for POD in $PODS; do
  echo "Downloading logs for pod: $POD"
  kubectl logs -n $NAMESPACE $POD > "${TMP_DIR}/${POD}_logs.txt"
  echo "Logs downloaded for pod: $POD"
done

# Compress log files into a tar.gz file
echo "Creating tar.gz archive..."
tar -czf "$OUTPUT_FILE" -C "$TMP_DIR" .
echo "Archive created: $OUTPUT_FILE"

# Cleanup temporary directory
rm -rf "$TMP_DIR"

echo "All logs downloaded and archived successfully."
