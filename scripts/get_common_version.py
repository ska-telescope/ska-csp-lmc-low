"""
This script is used to retrieve the version of the common package
from gitlab package repository in ska-csp-lmc-common
The script looks for the required version for the ska-csp-lmc-common package
(in requirements.txt) and then select the first entry satisfying
the version.
Scanning of the list of packages in the Gitlab Package Registry is done in
reversed order to get the newer packages.
"""

import requests
import json
import os

common_pkg_name = 'ska-csp-lmc-common'


def get_last_version():
    with open('requirements.txt', 'r') as req_file:
        Lines = req_file.readlines()
        for line in Lines:
            if common_pkg_name in line:
                # split line to get version
                start = line.find("==") + len("==")
                end = line.find("+")
                common_pkg_version = line[start:end]

    for ver_num in reversed(packages_list):
        gitlab_version = ver_num['version']
        token = gitlab_version.find('+')
        major_version = gitlab_version[:token]
        if major_version == common_pkg_version:
            break
        else:
            #return an invalid package number on error
            major_version = 0
    return(gitlab_version)

try:
    token = os.environ["ACCESS_CSP_COMMON"]
    common_repo_id = os.environ["COMMON_REPO_ID"]
    headers = {"PRIVATE-TOKEN" : token}
    url = f'https://gitlab.com/api/v4/projects/{common_repo_id}/packages'
    res = requests.get(url=url, headers=headers)
    packages_list = res.json()
    common_pkg_version = 0
    # Get the name of mid repository branch
    mid_branch_name = os.environ["CI_COMMIT_REF_NAME"]
    try:
        tag_branch_name = os.environ["CI_COMMIT_TAG"]
    except KeyError:
        tag_branch_name = None

    # if it is a tag pipeline, we look for the common package with
    # the name of the common tag
    if tag_branch_name:
        mid_branch_name = common_pkg_name
    # get the required version for the common package
    for package in reversed(packages_list):
        if mid_branch_name in package['pipeline']['ref']:
            common_pkg_version = package['version']
            break
    # if no common version found, try to recover the last package version
    if not common_pkg_version:
        common_pkg_version = get_last_version()
    print(common_pkg_version)
except Exception as e:
    print(e)
