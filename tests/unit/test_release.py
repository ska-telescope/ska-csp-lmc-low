import ska_csp_lmc_low.release as rls


def test_release_file():
    assert rls.name
    assert rls.version
    assert rls.version_info
    assert rls.description
    assert rls.author
    assert rls.author_email
    assert rls.license
    assert rls.url
    assert rls.copyright


def test_compare_release_with_version():
    """Test that the device version in release.py is the same
    than the one in .release"""

    file_name = ".release"

    with open(file_name, encoding="utf-8") as release_file:
        for line in release_file:
            name, value = line.partition("=")[::2]
            if value and name.lower() == "release":
                value = value.strip()
                assert rls.version == value
                break
