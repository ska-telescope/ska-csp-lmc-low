from ska_control_model import AdminMode, HealthState
from tango import DevState

attributes = {
    "ondurationexpected": 1,
    "offdurationexpected": 3,
    "standbydurationexpected": 1,
    "healthstate": HealthState.OK,
    "adminmode": AdminMode.ONLINE,
    "state": DevState.STANDBY,
}


class AttributeData:
    def __init__(self, attr_name):
        global attributes
        self._attr_name = attr_name
        self._value = attributes

    @property
    def value(self):
        return self._value[self._attr_name]

    @value.setter
    def value(self, new_val):
        print(f"{new_val} current value: {self._value}")
        self._value[self._attr_name] = new_val
        print(f"{new_val} current value: {self._value}")
