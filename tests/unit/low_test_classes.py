from ska_csp_lmc_common.testing.test_classes import TestBase


class TestLowPstCapability(TestBase):
    @classmethod
    def raise_event_on_pst(
        cls, id, attr_name, value, argument=None, callback=None
    ):
        """Helper function to raise a change event on a PST attribute."""
        id_str = str(id).zfill(2)
        dev_id = f"pst-{id_str}"

        cls.raise_event_on_sub_system(
            attr_name, value, cls.devices_list[dev_id], argument, callback
        )
