import logging

import mock
import pytest
from low_mocked_connector import LowMockedConnector
from low_properties import test_properties_low_ctrl
from mock import patch
from ska_csp_lmc_common.testing.poller import probe_poller
from ska_csp_lmc_common.testing.test_classes import TestBaseController
from ska_tango_testing.mock import MockCallableGroup

logger = logging.getLogger(__name__)


@pytest.fixture()
def callbacks() -> MockCallableGroup:
    """
    Return a dictionary of callbacks with asynchrony support.

    :return: a collections.defaultdict that returns callbacks by name.
    """
    return MockCallableGroup(
        "on_task",
        "off_task",
        "standby_task",
        timeout=5.0,
    )


dict_of_online_components = {
    "low-cbf/control/0": mock.MagicMock(),
    "low-pss/control/0": mock.MagicMock(),
    "low-pst/beam/01": mock.MagicMock(),
    "low-pst/beam/02": mock.MagicMock(),
    "low-csp/subarray/01": mock.MagicMock(),
    "low-csp/subarray/02": mock.MagicMock(),
    "low-csp/subarray/03": mock.MagicMock(),
}


def test_controller_cm_with_components_offline(ctrl_component_manager):
    ctrl_component_manager.start_communicating()
    # wait to establish communication
    probe_poller(ctrl_component_manager, "is_communicating", False, time=3)


@patch("ska_csp_lmc_common.component.Connector", new=LowMockedConnector)
class TestLowControllerComponentManager(TestBaseController):
    devices_list = {
        "cbf-ctrl": test_properties_low_ctrl["CspCbf"],
        "pss-ctrl": test_properties_low_ctrl["CspPss"],
        "pst-beam-01": test_properties_low_ctrl["CspPstBeams"][0],
        "pst-beam-02": test_properties_low_ctrl["CspPstBeams"][1],
        "allocator": test_properties_low_ctrl["CbfAllocator"],
        "subarray_01": test_properties_low_ctrl["CspSubarrays"][0],
        "subarray_02": test_properties_low_ctrl["CspSubarrays"][1],
        "subarray_03": test_properties_low_ctrl["CspSubarrays"][2],
    }

    def test_components_created(self, ctrl_component_manager):
        logger.error(f"COMPONENTS: {ctrl_component_manager.components}")
        # assert len(ctrl_component_manager.components) == 8
        cbf = TestLowControllerComponentManager.devices_list["cbf-ctrl"]
        assert ctrl_component_manager.components[cbf].name == "cbf-controller"
        pss = TestLowControllerComponentManager.devices_list["pss-ctrl"]
        assert ctrl_component_manager.components[pss].name == "pss-controller"
        pst1 = TestLowControllerComponentManager.devices_list["pst-beam-01"]
        assert ctrl_component_manager.components[pst1].name == "pst-beam-01"
        pst2 = TestLowControllerComponentManager.devices_list["pst-beam-02"]
        assert ctrl_component_manager.components[pst2].name == "pst-beam-02"
        allocator = TestLowControllerComponentManager.devices_list["allocator"]
        assert (
            ctrl_component_manager.components[allocator].name
            == "cbf-allocator"
        )
        sub1 = TestLowControllerComponentManager.devices_list["subarray_01"]
        assert (
            ctrl_component_manager.components[sub1].name == "csp-subarray-01"
        )
        sub2 = TestLowControllerComponentManager.devices_list["subarray_02"]
        assert (
            ctrl_component_manager.components[sub2].name == "csp-subarray-02"
        )
        sub3 = TestLowControllerComponentManager.devices_list["subarray_03"]
        assert (
            ctrl_component_manager.components[sub3].name == "csp-subarray-03"
        )

    def test_stop_communicating(
        self, ctrl_component_manager, controller_cm_connected
    ):
        assert ctrl_component_manager.is_communicating
        ctrl_component_manager.stop_communicating()
        # wait to establish communication
        probe_poller(ctrl_component_manager, "is_communicating", False, time=5)

    def test_controller_cm_online_components(
        self, ctrl_component_manager, controller_cm_connected
    ):
        assert len(ctrl_component_manager.online_components) == 7
