import mock
import pytest
from low_mocked_connector import LowMockedConnector
from ska_control_model import AdminMode, HealthState
from tango import DevState

from ska_csp_lmc_low.controller.low_ctrl_component import (
    LowCbfControllerComponent,
    LowPssControllerComponent,
)

attributes_default = dict(
    healthstate=HealthState.OK, adminmode=AdminMode.ONLINE, state=DevState.ON
)


class CtrlMockedConnectorException(LowMockedConnector):
    """Class to mock the connection with a sub-system controller.

    Inherits from the base class MockedConnector and overrides the
    kafe_get_Attribute method to specilize for LOw instance.
    """

    @classmethod
    def fake_get_attribute(cls, attr_name):
        raise ValueError("Failure in reading attribute")


@pytest.fixture
def cbf_ctrl_component():
    cbf_ctrl_component = LowCbfControllerComponent(fqdn="low-cbf/controller/0")
    yield cbf_ctrl_component


@pytest.fixture
def pss_ctrl_component():
    pss_ctrl_component = LowPssControllerComponent(fqdn="low-pss/controller/0")
    yield pss_ctrl_component


class TestLowCbfControllerComponent:
    @mock.patch(
        "ska_csp_lmc_common.component.Connector", new=LowMockedConnector
    )
    def test_cbf_ctrl_component_name(self, cbf_ctrl_component):
        assert cbf_ctrl_component.fqdn == "low-cbf/controller/0"
        cbf_ctrl_component.connect()
        assert cbf_ctrl_component.proxy
        assert cbf_ctrl_component.name == "cbf-controller"

    def test_cbf_ctrl_component_proxy_is_none(self, cbf_ctrl_component):
        cbf_ctrl_component.connect()
        assert not cbf_ctrl_component.proxy

    def test_cbf_ctrl_list_of_stations(self, cbf_ctrl_component):
        assert cbf_ctrl_component.list_of_stations == [1, 2, 3, 4]


class TestLowPssControllerComponent:
    @mock.patch(
        "ska_csp_lmc_common.component.Connector", new=LowMockedConnector
    )
    def test_pss_ctrl_component_name(self, pss_ctrl_component):
        assert pss_ctrl_component.fqdn == "low-pss/controller/0"
        pss_ctrl_component.connect()
        assert pss_ctrl_component.name == "pss-controller"

    def test_pss_ctrl_list_of_beams(self, pss_ctrl_component):
        assert pss_ctrl_component.list_of_beams == [1, 2, 3, 4]
