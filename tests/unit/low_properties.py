test_properties_low_ctrl = {
    "ConnectionTimeout": "3",
    "PingConnectionTime": "1",
    "DefaultCommandTimeout": "5",
    "CspCbf": "low-cbf/control/0",
    "CspPss": "low-pss/control/0",
    "CspPstBeams": [
        "low-pst/beam/01",
        "low-pst/beam/02",
    ],
    "CspSubarrays": [
        "low-csp/subarray/01",
        "low-csp/subarray/02",
        "low-csp/subarray/03",
    ],
    "CbfAllocator": "low-cbf/allocator/0",
}

test_properties_low_subarray = {
    "ConnectionTimeout": "3",
    "PingConnectionTime": "1",
    "DefaultCommandTimeout": "5",
    "SubID": "1",
    "CspController": "low-csp/control/0",
    "CbfSubarray": "low-cbf/subarray/01",
    "PssSubarray": "low-pss/subarray/01",
    "PstBeams": [
        "low-pst/beam/01",
        "low-pst/beam/02",
    ],
}


test_properties_low_fsp_capability = {
    "ConnectionTimeout": "3",
    "PingConnectionTime": "1",
    "CbfAllocator": "low-cbf/allocator/0",
}

test_properties_pst_capability = {
    "CspController": "low-csp/control/0",
    "ConnectionTimeout": 3,
    "PingConnectionTime": 1,
    "DefaultCommandTimeout": 5,
}
