import logging

from ska_control_model import AdminMode, HealthState
from ska_csp_lmc_common.testing.mock_attribute import (
    AttributeData,
    AttributeDataFactory,
)
from tango import DevState

module_logger = logging.getLogger(__name__)

# pylint: disable=too-many-instance-attributes


class LowAttributeDataFactory(AttributeDataFactory):
    attributes_ctrl_default = dict(
        AttributeDataFactory.attributes_ctrl_default
    )
    attributes_subarray_default = dict(
        AttributeDataFactory.attributes_subarray_default
    )
    attributes_subarray_default.update({"stations": "1"})
    attributes_subarray_default.update({"stationbeams": "1"})
    attributes_subarray_default.update({"pssbeams": "1"})
    attributes_subarray_default.update({"pstbeams": "1"})
    attributes_allocator_default = {
        "healthstate": HealthState.OK,
        "adminmode": AdminMode.ONLINE,
        "state": DevState.ON,
        "fsps": "{}",
        "procdevfqdn": "{}",
    }
    attributes_processor_default = {
        "healthstate": HealthState.OK,
        "adminmode": AdminMode.ONLINE,
        "state": DevState.ON,
        "stats_mode": '{"ready": "false", "firmware": ""}',
        "stats_io": "{}",
        "subarrayids": [],
    }
    attributes_beam_default = dict(
        AttributeDataFactory.attributes_beam_default
    )
    attributes_beam_default.update({"subarray_id": 0})

    attributes_pstcapability_default = dict(
        AttributeDataFactory.attributes_pstcapability_default
    )

    def create_attribute(self, attr_name):
        if "control" in self._device:
            return AttributeData(
                attr_name, self.__class__.attributes_ctrl_default
            )
        if "subarray" in self._device:
            subarray_id = int(self._device[-2:])
            self.__class__.attributes_subarray_default["subarrayID"] = int(
                subarray_id
            )
            return AttributeData(
                attr_name, self.__class__.attributes_subarray_default
            )
        if "beam" in self._device:
            return AttributeData(
                attr_name, self.__class__.attributes_beam_default
            )

        if "allocator" in self._device:
            return AttributeData(
                attr_name, self.__class__.attributes_allocator_default
            )

        if "processor" in self._device:
            return AttributeData(
                attr_name, self.__class__.attributes_processor_default
            )

        if "pstcapability" in self._device:
            return AttributeData(
                attr_name, self.__class__.attributes_pstcapability_default
            )
        raise ValueError("AttributeFactory: no valid device specified")
