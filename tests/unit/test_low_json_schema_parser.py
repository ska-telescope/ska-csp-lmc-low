import logging

import mock
import pytest
from ska_csp_lmc_common.testing.test_classes import load_json_file
from ska_telmodel import schema

# Local imports
from ska_csp_lmc_low.subarray.low_json_config_parser import (
    LowJsonConfigurationParser,
)

module_logger = logging.getLogger(__name__)


class TestLowJsonConfigurationParser:
    def test_assign_resources_with_lowcbf(self):
        """Test content of json file for assignment of resources."""
        config_dict = load_json_file("AssignResources_CBF_PSS_PST.json")
        cm = mock.MagicMock()
        attrs = {
            "CbfSubarrayFqdn": "low-cbf/subarray/01",
            "PssSubarrayFqdn": "low-pss/subarray/01",
        }
        cm.configure_mock(**attrs)
        json_handler = LowJsonConfigurationParser(cm, config_dict)
        resources = json_handler.assignresources()
        low_cbf_config_dict = resources["low-cbf/subarray/01"]
        assert "lowcbf" in low_cbf_config_dict
        assert "common" not in low_cbf_config_dict

    def test_assign_resources_with_pst_beams(self):
        """Test the 'pst' section is skipped when the 'lowcbf' section od
        the json script is requested."""
        config_dict = load_json_file("Configure_CBF_PSS_PST.json")
        cm = mock.MagicMock()
        attrs = {
            "CbfSubarrayFqdn": "low-cbf/subarray/01",
            "PssSubarrayFqdn": "low-pss/subarray/01",
        }
        cm.configure_mock(**attrs)
        json_handler = LowJsonConfigurationParser(cm, config_dict)
        low_cbf_config_dict = json_handler.assignresources()[
            "low-cbf/subarray/01"
        ]
        assert low_cbf_config_dict
        assert "pst" not in low_cbf_config_dict

    def test_assign_resources_configuration_without_cbf(self):
        """Test the config json script is not loaded into a dictionary if the
        requested section ('lowcbf') is missing inside the json script."""
        config_dict = load_json_file("AssignResources_CBF_PSS_PST.json")
        config_dict.pop("lowcbf")
        cm = mock.MagicMock()
        attrs = {
            "CbfSubarrayFqdn": "low-cbf/subarray/01",
            "PssSubarrayFqdn": "low-pss/subarray/01",
        }
        cm.configure_mock(**attrs)
        json_handler = LowJsonConfigurationParser(cm, config_dict)
        assign_dict = json_handler.assignresources()
        assert "lowcbf" not in assign_dict

    def test_load_scan_configuration_ver2(self):
        """Test the lowcbf section of the config json script is loaded into a
        dictionary."""
        config_dict = load_json_file("Configure_CBF_PSS_PST.json")
        cm = mock.MagicMock()
        json_handler = LowJsonConfigurationParser(cm, config_dict)
        low_cbf_config_dict = json_handler.generate_subsystem_config_dict(
            "cbf"
        )
        assert low_cbf_config_dict

    def test_load_scan_configuration_with_pst_beams(self):
        """Test the 'pst' section is skipped when the 'lowcbf' section od
        the json script is requested."""
        config_dict = load_json_file("Configure_CBF_PSS_PST.json")
        cm = mock.MagicMock()
        json_handler = LowJsonConfigurationParser(cm, config_dict)
        low_cbf_config_dict = json_handler.generate_subsystem_config_dict(
            "cbf"
        )
        assert low_cbf_config_dict
        assert "pst" not in low_cbf_config_dict

    def test_pst_configuration_with_strict_validation(self):
        """Test to ensure that the updated configuration for PST,
        including the timing_beam_id key, is strictly validated
        against the PST schema.
        """
        config_dict = load_json_file("Configure_CBF_PST_VR.json")
        mock_attrs = {
            "sub_id": 1,
            "CbfSubarrayFqdn": "low-cbf/subarray/01",
            "PstBeamsFqdn": ["low-pst/beam/01"],
        }

        cm = mock.MagicMock(**mock_attrs)
        # Add the transaction ID to comply with the real configuration:
        # upon receiving the command, CSP.LMC includes the
        # transaction_id entry.
        config_dict["transaction_id"] = "tnx_111111"
        json_handler = LowJsonConfigurationParser(cm, config_dict)
        low_pst_config_dict = json_handler.configure()["low-pst/beam/01"]
        assert low_pst_config_dict
        uri = low_pst_config_dict["interface"]
        schema.validate(uri, low_pst_config_dict, strictness=2)

    def test_pst_configuration_failure_with_invalid_timing_beam_id(self):
        """Test that the validation fails if an invalid type for
        the timing_beam_id entry is specified.
        """
        config_dict = load_json_file("Configure_CBF_PST_VR.json")
        mock_attrs = {
            "sub_id": 1,
            "CbfSubarrayFqdn": "low-cbf/Subarray/01",
            "PstBeamsFqdn": ["low-pst/beam/01"],
        }

        cm = mock.MagicMock(**mock_attrs)
        config_dict["transaction_id"] = "tnx_111111"
        json_handler = LowJsonConfigurationParser(cm, config_dict)
        low_pst_config_dict = json_handler.configure()["low-pst/beam/01"]
        assert low_pst_config_dict
        # modify type of timing_beam_entry
        low_pst_config_dict["pst"]["scan"]["timing_beam_id"] = 11
        uri = low_pst_config_dict["interface"]
        with pytest.raises(ValueError):
            schema.validate(uri, low_pst_config_dict, strictness=2)

    def test_pst_configuration_failure_without_timing_beam_id(self):
        """Test that the validation fails if no timing_beam_id entry
        is specified in the PST configuration.
        """
        config_dict = load_json_file("Configure_CBF_PST_VR.json")
        mock_attrs = {
            "sub_id": 1,
            "CbfSubarrayFqdn": "low-cbf/Subarray/01",
            "PstBeamsFqdn": ["low-pst/beam/01"],
        }

        cm = mock.MagicMock(**mock_attrs)
        config_dict["transaction_id"] = "tnx_111111"
        json_handler = LowJsonConfigurationParser(cm, config_dict)
        low_pst_config_dict = json_handler.configure()["low-pst/beam/01"]
        assert low_pst_config_dict
        # remove the timing_beam_entry
        low_pst_config_dict["pst"]["scan"].pop("timing_beam_id")
        uri = low_pst_config_dict["interface"]
        with pytest.raises(
            ValueError,
            match=r"Validation 'PST configuration schema 2.5' Key 'pst' error",
        ):
            schema.validate(uri, low_pst_config_dict, strictness=2)

    def test_load_scan_configuration_without_cbf(self):
        """Test the config json script is not loaded into a dictionary if the
        requested section ('lowcbf') is missing inside the json script."""
        config_dict = load_json_file("Configure_CBF_PSS_PST.json")
        config_dict.pop("lowcbf")
        cm = mock.MagicMock()
        json_handler = LowJsonConfigurationParser(cm, config_dict)
        low_cbf_config_dict = json_handler.generate_subsystem_config_dict(
            "cbf"
        )
        assert not low_cbf_config_dict

    def test_csp_configuration_without_subarray_section(self):
        """Test the Low CSP configuration version 3.2 without
        the subarray section to ensure that the SKB_429 bug
        has been resolved.
        """
        config_dict = load_json_file("Configure_CBF_PST_VR.json")
        mock_attrs = {
            "sub_id": 1,
            "CbfSubarrayFqdn": "low-cbf/subarray/01",
            "PstBeamsFqdn": ["low-pst/beam/01"],
        }

        cm = mock.MagicMock(**mock_attrs)
        # Add the transaction ID to comply with the real configuration:
        # upon receiving the command, CSP.LMC includes the
        # transaction_id entry.
        config_dict["transaction_id"] = "tnx_111111"
        # Remove the 'subarray' entry because it's optional
        config_dict.pop("subarray")
        config_dict[
            "interface"
        ] = "https://schema.skao.int/ska-low-csp-configure/3.2"
        json_handler = LowJsonConfigurationParser(cm, config_dict)
        schema.validate(config_dict["interface"], config_dict, strictness=2)
        assert json_handler.configure()
