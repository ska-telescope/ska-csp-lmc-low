import logging
import random
import threading

from low_attribute_factory import LowAttributeDataFactory
from ska_csp_lmc_common.testing.mocked_connector import MockedConnector
from ska_csp_lmc_common.testing.test_utils import create_dummy_event

module_logger = logging.getLogger(__name__)


class LowMockedConnector(MockedConnector):
    """Class to mock the connection with a sub-system controller.

    Inherits from the base class MockedConnector and overrides the
    fake_get_Attribute method to specialize for Low instance.
    """

    @classmethod
    def fake_get_attribute(cls, fqdn, attr_name):
        attr_factory = LowAttributeDataFactory(fqdn)
        attr = attr_factory.create_attribute(attr_name)
        return attr

    @classmethod
    def fake_subscribe_attribute(cls, fqdn, attr_name, evt_type, callback):
        cls.event_subscription_map[fqdn].update({attr_name: callback})
        evt_id = random.randrange(1, 1000)
        if fqdn not in cls.event_id_map:
            cls.event_id_map[fqdn] = {}
        cls.event_id_map[fqdn].update({attr_name: evt_id})
        # create the event with the default value for the attribute
        attr_factory = LowAttributeDataFactory(fqdn)
        attr = attr_factory.create_attribute(attr_name)
        _event = create_dummy_event(fqdn, attr_name, attr.value)
        callback = cls.event_subscription_map[fqdn][attr_name]
        # invoke the registered callback to raise the event in the same thread
        # threading.Thread(target=callback, args=(_event,)).start()
        callback(_event)
        return evt_id

    @classmethod
    def fake_set_attribute(cls, fqdn, attr_name, value):
        attr_factory = LowAttributeDataFactory(fqdn)
        attr = attr_factory.create_attribute(attr_name)
        attr.value = value
        _event = create_dummy_event(fqdn, attr_name, value)
        if attr_name in cls.event_id_map:
            callback = cls.event_subscription_map[fqdn][attr_name]
            threading.Thread(target=callback, args=(_event,)).start()


class LowMockedConnectorException(MockedConnector):
    """Class to mock the connection with a sub-system controller.

    Inherits from the base class MockedConnector and overrides the
    fake_get_attribute method to specialize for Low instance.
    """

    @classmethod
    def fake_get_attribute(cls, attr_name):
        raise ValueError("Failure in reading attribute")
