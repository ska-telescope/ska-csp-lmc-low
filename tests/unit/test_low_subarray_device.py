import json
import logging
import time

import mock
import pytest
import tango
from low_attribute_factory import LowAttributeDataFactory
from low_mocked_connector import LowMockedConnector
from low_properties import test_properties_low_subarray

# SKA imports
from ska_control_model import AdminMode, HealthState, ObsMode, ObsState
from ska_csp_lmc_common.subarray import PstBeamComponent
from ska_csp_lmc_common.testing.poller import probe_poller
from ska_csp_lmc_common.testing.test_classes import (
    TestBaseSubarray,
    load_json_file,
)
from ska_csp_lmc_common.utils.cspcommons import read_release_version
from ska_tango_base.commands import ResultCode
from tango import DevState, EventType

from ska_csp_lmc_low import release

module_logger = logging.getLogger(__name__)

# pylint: disable=invalid-name

# Device test case
device_to_load = {
    "path": "tests/unit/csplmc_dsconfig.json",
    "package": "ska_csp_lmc_low",
    "device": "subarray1",
}


@pytest.fixture
@mock.patch("ska_csp_lmc_common.component.Connector", new=LowMockedConnector)
def low_subarray_init(device_under_test):
    """
    Perform the CSP subarray initialization with no PST beam
    belonging to the subarray (pst-beam subarray_id = 0).
    The Connector class is mocked through the MockedConnector class.
    """
    # at the end of the device intialization, the device is
    # OFFLINE in DISABLE state and UNKNOWN healthState
    assert device_under_test.adminmode == AdminMode.OFFLINE
    probe_poller(device_under_test, "state", DevState.DISABLE, time=5)
    assert device_under_test.healthstate == HealthState.UNKNOWN
    assert device_under_test.obsstate == ObsState.EMPTY
    device_under_test.adminMode = AdminMode.ENGINEERING
    probe_poller(device_under_test, "adminMode", AdminMode.ENGINEERING, time=5)
    # wait to establish connection with the components
    probe_poller(device_under_test, "isCommunicating", True, time=10)
    # TestLowCspSubarray.raise_event("state")
    # TestLowCspSubarray.raise_event("healthstate")
    # TestLowCspSubarray.raise_event("adminmode")
    probe_poller(device_under_test, "State", DevState.OFF, time=5)
    assert device_under_test.healthstate == HealthState.OK


@pytest.fixture
@mock.patch("ska_csp_lmc_common.component.Connector", new=LowMockedConnector)
def low_subarray_init_with_all_subsystems(
    device_under_test, change_event_callbacks
):
    """Perform the CSP subarray initialization with all PST beams already
    belonging to the subarray (re-initialization).

    The Connector class is mocked through the MockedConnector class.
    """
    with mock.patch.object(
        PstBeamComponent, "subarray_id", new_callable=mock.PropertyMock
    ) as mock_subarray_id:
        mock_subarray_id.return_value = 1
        # at the end of the device intialization, the device is
        # OFFLINE in DISABLE state and UNKNOWN healthState
        attrs_list = {
            "state",
            "adminMode",
            "healthState",
            "obsState",
        }
        evt_id = []
        for attr_name in attrs_list:
            evt_id.append(
                device_under_test.subscribe_event(
                    attr_name,
                    EventType.CHANGE_EVENT,
                    change_event_callbacks[attr_name],
                )
            )

        change_event_callbacks.assert_change_event(
            "state", DevState.DISABLE, len(attrs_list)
        )
        change_event_callbacks.assert_change_event(
            "adminMode", AdminMode.OFFLINE, lookahead=len(attrs_list)
        )
        change_event_callbacks.assert_change_event(
            "healthState", HealthState.UNKNOWN, lookahead=len(attrs_list)
        )
        change_event_callbacks.assert_change_event(
            "obsState", ObsState.EMPTY, lookahead=len(attrs_list)
        )
        # enable the device starting the connection with the sub-systems
        device_under_test.adminMode = AdminMode.ENGINEERING
        change_event_callbacks.assert_change_event(
            "adminMode", AdminMode.ENGINEERING, lookahead=len(attrs_list)
        )
        change_event_callbacks.assert_change_event(
            "state", DevState.OFF, lookahead=len(attrs_list)
        )
        change_event_callbacks.assert_change_event(
            "healthState", HealthState.OK, lookahead=len(attrs_list)
        )
        for _id in evt_id:
            device_under_test.unsubscribe_event(_id)


@mock.patch("ska_csp_lmc_common.component.Connector", new=LowMockedConnector)
class TestLowCspSubarray(TestBaseSubarray):

    devices_list = {
        "cbf-subarray": test_properties_low_subarray["CbfSubarray"],
        "pss-subarray": test_properties_low_subarray["PssSubarray"],
        "pst-beam-01": test_properties_low_subarray["PstBeams"][0],
        "pst-beam-02": test_properties_low_subarray["PstBeams"][1],
    }

    def test_low_subarray_first_initialization(
        self, device_under_test, low_subarray_init
    ):
        """Perform the CSP subarray first initialization, with PST Beams not
        assigned:

        the beams return 0 for the subarray_id attribute
        (low_subarray_init fixture).
        The subarray connects to beam at initialization but
        since the beams does not belong to the subarray
        (subarray_id = 0) the pst beam is disconnected. When
        a component is disconnected its state is reported as
        UNKNOWN
        """
        assert device_under_test.State() == DevState.OFF
        pst_beams_state = device_under_test.pstBeamsState
        pst_state_dict = json.loads(pst_beams_state)
        assert "low-pst/beams/01" not in pst_state_dict
        assert "low-pst/beams/02" not in pst_state_dict

    # def test_low_subarray_init_with_all_sub_systems(
    #     self, device_under_test, low_subarray_init_with_all_subsystems
    # ):
    #     """Perform the CSP first initialization, with PST Beams already
    #     belonging to the Low.CSP subarray."""
    #     probe_poller(device_under_test, "state", DevState.OFF, time=5)
    #     pst_state_dict = {}

    #     pst_state_dict["low-pst/beam/01"] = "OFF"
    #     pst_state_dict["low-pst/beam/02"] = "OFF"
    #     pst_state_json = json.dumps(pst_state_dict)
    #     probe_poller(
    #         device_under_test, "pstBeamsState", pst_state_json, time=10
    #     )

    #     pst_health_dict = {}
    #     pst_health_dict["low-pst/beam/01"] = "OK"
    #     pst_health_dict["low-pst/beam/02"] = "OK"
    #     pst_health_json = json.dumps(pst_health_dict)
    #     probe_poller(
    #         device_under_test, "pstBeamsHealthState", pst_health_json, time=5
    #     )

    @mock.patch(
        "ska_csp_lmc_common.connector.Connector",
        new=LowMockedConnector,
    )
    def test_version_id(self, device_under_test):
        """
        Test the software versionId.

        The versionId reported by the subarray corresponds to
        the version of the Helm Chart, the docker image and
        python package.
        The version is defined into the file *.release*.
        """
        version_id, build_state = read_release_version(release)
        assert device_under_test.versionId == version_id
        assert device_under_test.buildState == build_state

    def test_low_assign_resources(self, device_under_test, low_subarray_init):
        """Test assignment of resources."""
        with mock.patch(
            "ska_csp_lmc_low.subarray"
            ".LowCbfSubarrayComponent.assigned_stations",
            new_callable=mock.PropertyMock,
        ) as mock_read:
            mock_read.return_value = (
                LowAttributeDataFactory.attributes_subarray_default["stations"]
            )
            TestLowCspSubarray.set_subsystems_and_go_to_state(
                device_under_test, DevState.ON
            )
            TestLowCspSubarray.set_subsystems_and_go_to_obsstate(
                device_under_test, ObsState.EMPTY
            )

            # load the file with resources
            config_string = load_json_file(
                "AssignResources_CBF_PSS_PST.json", string=True
            )
            _, command_id = device_under_test.AssignResources(config_string)
            probe_poller(
                device_under_test,
                "obsstate",
                ObsState.IDLE,
                time=5,
                command_id=command_id,
            )

            assert (
                device_under_test.stations
                == LowAttributeDataFactory.attributes_subarray_default[
                    "stations"
                ]
            )

    def test_low_assign_resources_with_incoherent_configuration(
        self, device_under_test, low_subarray_init
    ):
        """Test assignment of resources."""
        with mock.patch(
            "ska_csp_lmc_low.subarray"
            ".LowCbfSubarrayComponent.assigned_stations",
            new_callable=mock.PropertyMock,
        ) as mock_read:
            mock_read.return_value = (
                LowAttributeDataFactory.attributes_subarray_default["stations"]
            )
            TestLowCspSubarray.set_subsystems_and_go_to_state(
                device_under_test, DevState.ON
            )
            TestLowCspSubarray.set_subsystems_and_go_to_obsstate(
                device_under_test, ObsState.EMPTY
            )

            # load the file with resources
            config_string = load_json_file(
                "AssignResources_CBF_PST.json", string=True
            )
            config_dict = json.loads(config_string)
            # modify the PST beams to assign: 3 and 4 are not withing
            # the list of deployed
            config_dict["pst"]["beams_id"] = [3, 4]
            with pytest.raises(tango.DevFailed):
                _, command_id = device_under_test.AssignResources(
                    json.dumps(config_dict)
                )

            assert device_under_test.obsstate == ObsState.EMPTY

    @mock.patch(
        "ska_csp_lmc_common.component.Connector",
        new=LowMockedConnector,
    )
    def test_low_assign_release_resources_with_pst_beam(
        self, device_under_test, low_subarray_init
    ):
        """Test release of resources."""
        with mock.patch(
            "ska_csp_lmc_low.subarray"
            ".LowCbfSubarrayComponent.assigned_stations",
            new_callable=mock.PropertyMock,
        ) as mock_read:
            mock_read.return_value = (
                LowAttributeDataFactory.attributes_subarray_default["stations"]
            )
            TestLowCspSubarray.set_subsystems_and_go_to_state(
                device_under_test, DevState.ON
            )
            device_under_test.logginglevel = 5
            input_string = load_json_file(
                "AssignResources_CBF_PST.json", string=True
            )
            _, command_id = device_under_test.AssignResources(input_string)

            probe_poller(device_under_test, "obsstate", ObsState.IDLE, time=5)
            probe_poller(
                device_under_test,
                "longRunningCommandAttributes",
                (0, "COMPLETED"),
                time=3,
                command_id=command_id,
            )

            # force PST Beam state to ON
            TestLowCspSubarray.raise_event_on_pst_beams("state", DevState.ON)
            assert len(device_under_test.assignedTimingBeamIDs) == 1
            _, command_id = device_under_test.ReleaseAllResources()
            probe_poller(device_under_test, "obsstate", ObsState.EMPTY, time=5)

            probe_poller(
                device_under_test,
                "longRunningCommandAttributes",
                (0, "COMPLETED"),
                time=3,
                command_id=command_id,
            )
            assert not device_under_test.assignedTimingBeamIDs

    @mock.patch(
        "ska_csp_lmc_common.component.Connector",
        new=LowMockedConnector,
    )
    def test_low_re_assign_resources(
        self, device_under_test, low_subarray_init
    ):
        """Test re-assignment of resources."""
        with mock.patch(
            "ska_csp_lmc_low.subarray"
            ".LowCbfSubarrayComponent.assigned_stations",
            new_callable=mock.PropertyMock,
        ) as mock_read:
            mock_read.return_value = (
                LowAttributeDataFactory.attributes_subarray_default["stations"]
            )
            # force the subarray to stateON/IDLE
            TestLowCspSubarray.set_subsystems_and_go_to_state(
                device_under_test, DevState.ON
            )
            TestLowCspSubarray.set_subsystems_and_go_to_obsstate(
                device_under_test, ObsState.IDLE
            )
            assert device_under_test.obsstate == ObsState.IDLE
            config_string = load_json_file(
                "AssignResources_CBF_PSS_PST.json", string=True
            )
            _, command_id = device_under_test.AssignResources(config_string)
            probe_poller(device_under_test, "obsstate", ObsState.IDLE, time=5)
            probe_poller(
                device_under_test,
                "longRunningCommandAttributes",
                (0, "COMPLETED"),
                time=3,
                command_id=command_id,
            )

    @mock.patch(
        "ska_csp_lmc_common.connector.Connector", new=LowMockedConnector
    )
    @pytest.mark.skip(
        reason="with telmodel validation, the command is rejected. "
        "Consider to delete the test"
    )
    def test_subarray_idle_with_no_resources_to_assign(
        self, device_under_test, low_subarray_init
    ):
        """Test that the CSP Subarray maintains its obsState to IDLE if the
        input json has no valid resources to allocate to the subarray and an
        exception is raised"""

        TestLowCspSubarray.set_subsystems_and_go_to_state(
            device_under_test, DevState.ON
        )
        TestLowCspSubarray.set_subsystems_and_go_to_obsstate(
            device_under_test, ObsState.IDLE
        )
        assert device_under_test.obsstate == ObsState.IDLE

        config_string = '{"interface": "https://schema.skao.int/ska-low-csp-assignresources/2.0", "common": { "subarray_id": 1 } }'  # noqa E401

        _, command_id = device_under_test.AssignResources(config_string)

        probe_poller(
            device_under_test,
            "commandResult",
            ("assignresources", "3"),
            time=5,
        )
        probe_poller(
            device_under_test,
            "longRunningCommandAttributes",
            (3, "COMPLETED"),
            time=3,
            command_id=command_id,
        )

        probe_poller(device_under_test, "obsstate", ObsState.IDLE, time=5)

    @mock.patch(
        "ska_csp_lmc_common.connector.Connector",
        new=LowMockedConnector,
    )
    def test_low_removeall_resources(
        self, device_under_test, low_subarray_init
    ):
        with mock.patch(
            "ska_csp_lmc_low.subarray"
            ".LowCbfSubarrayComponent.assigned_stations",
            new_callable=mock.PropertyMock,
        ) as mock_read:
            mock_read.return_value = (
                LowAttributeDataFactory.attributes_subarray_default["stations"]
            )
            TestLowCspSubarray.set_subsystems_and_go_to_state(
                device_under_test, DevState.ON
            )
            TestLowCspSubarray.set_subsystems_and_go_to_obsstate(
                device_under_test, ObsState.IDLE
            )
            assert device_under_test.obsstate == ObsState.IDLE
            device_under_test.ReleaseAllResources()
            probe_poller(device_under_test, "obsstate", ObsState.EMPTY, time=5)

    def test_read_cbf_resources(self, device_under_test, low_subarray_init):
        assert (
            device_under_test.stations
            == LowAttributeDataFactory.attributes_subarray_default["stations"]
        )
        assert (
            device_under_test.stationBeams
            == LowAttributeDataFactory.attributes_subarray_default[
                "stationbeams"
            ]  # noqa E503
        )
        assert (
            device_under_test.pssBeams
            == LowAttributeDataFactory.attributes_subarray_default["pssbeams"]
        )
        assert (
            device_under_test.pstBeams
            == LowAttributeDataFactory.attributes_subarray_default["pstbeams"]
        )

    @pytest.mark.parametrize(
        "initial_obsstate", [ObsState.IDLE, ObsState.READY]
    )
    def test_configure_failed(
        self, device_under_test, initial_obsstate, low_subarray_init
    ):
        """Test a the ConfigureCommand with a wrong input interface.

        The ObsState of CSP stays in initial_obsstate, since the
        configuration is failing. An exception is raised immediately
        by the validator
        """
        assert device_under_test.isCommunicating
        TestLowCspSubarray.set_subsystems_and_go_to_state(
            device_under_test, DevState.ON
        )
        TestLowCspSubarray.set_subsystems_and_go_to_obsstate(
            device_under_test, initial_obsstate
        )
        with pytest.raises(tango.DevFailed):
            _, command_id = device_under_test.configure(
                '{"interface":"invalid"}'
            )

        probe_poller(
            device_under_test,
            "obsState",
            initial_obsstate,
            time=1,
        )

    @pytest.mark.parametrize(
        "initial_obsstate", [ObsState.IDLE, ObsState.READY]
    )
    def test_configure_cbf_pss_pst(
        self, device_under_test, initial_obsstate, low_subarray_init
    ):
        """Test if both CSP and CBF are present in configuration file.

        The ObsState of CSP goes to READY The command resultCode is OK
        """
        assert device_under_test.isCommunicating
        TestLowCspSubarray.set_subsystems_and_go_to_state(
            device_under_test, DevState.ON
        )

        config_string = load_json_file(
            "AssignResources_CBF_PSS_PST.json", string=True
        )
        _, command_id = device_under_test.AssignResources(config_string)
        probe_poller(device_under_test, "obsstate", ObsState.IDLE, time=5)
        probe_poller(
            device_under_test,
            "longRunningCommandAttributes",
            (0, "COMPLETED"),
            time=3,
            command_id=command_id,
        )
        TestLowCspSubarray.raise_event_on_pst_beams("state", DevState.ON)
        assert device_under_test.assignedTimingBeamIDs == [1]

        config_input_dict = load_json_file("Configure_CBF_PSS_PST.json")
        _, command_id = device_under_test.configure(
            json.dumps(config_input_dict)
        )

        probe_poller(
            device_under_test,
            "obsState",
            ObsState.READY,
            time=5,
        )
        probe_poller(
            device_under_test,
            "commandResult",
            ("configure", str(ResultCode.OK.value)),
            time=5,
        )

        probe_poller(
            device_under_test,
            "longRunningCommandAttributes",
            (0, "COMPLETED"),
            time=3,
            command_id=command_id,
        )

    @pytest.mark.parametrize(
        "initial_obsstate", [ObsState.IDLE, ObsState.READY]
    )
    def test_configure_only_cbf(
        self, device_under_test, initial_obsstate, low_subarray_init
    ):
        """Test if in the configuration file pss entry is missing.

        The ObsState of CSP goes to READY, since the configuration is
        forwarded to CBF. The command resultCode is OK
        """
        assert device_under_test.isCommunicating
        TestLowCspSubarray.set_subsystems_and_go_to_state(
            device_under_test, DevState.ON
        )
        TestLowCspSubarray.set_subsystems_and_go_to_obsstate(
            device_under_test, initial_obsstate
        )
        config_input_dict = load_json_file("Configure_CBF.json")
        _, command_id = device_under_test.configure(
            json.dumps(config_input_dict)
        )
        probe_poller(
            device_under_test,
            "obsState",
            ObsState.READY,
            time=5,
        )
        probe_poller(
            device_under_test,
            "state",
            DevState.ON,
            time=1,
        )
        probe_poller(
            device_under_test,
            "commandResult",
            ("configure", str(ResultCode.OK.value)),
            time=5,
        )

        probe_poller(
            device_under_test,
            "longRunningCommandAttributes",
            (0, "COMPLETED"),
            time=3,
            command_id=command_id,
        )

        assert (
            device_under_test.configurationID
            == config_input_dict["common"]["config_id"]
        )

    def test_configure_vis_with_wrong_sdp_address_format(
        self, device_under_test, low_subarray_init
    ):
        """Test that the configuration for visibilities is rejected
        when the SDP address is not in numerical dottet format.

        The ObsState of CSP remains in IDLE
        """
        assert device_under_test.isCommunicating
        TestLowCspSubarray.set_subsystems_and_go_to_state(
            device_under_test, DevState.ON
        )
        TestLowCspSubarray.set_subsystems_and_go_to_obsstate(
            device_under_test, ObsState.IDLE
        )
        config_input_dict = load_json_file("Configure_CBF.json")
        config_input_dict["lowcbf"]["vis"]["stn_beams"][0]["host"][0][
            1
        ] = "a.b.c.d"
        with pytest.raises(tango.DevFailed):
            device_under_test.configure(json.dumps(config_input_dict))
        assert device_under_test.obsstate == ObsState.IDLE

    def test_configure_pst_with_incoherent_configuration(
        self, device_under_test, low_subarray_init
    ):
        """Test that the configuration for visibilities is rejected
        when the SDP address is not in numerical dottet format.

        The ObsState of CSP remains in IDLE
        """
        assert device_under_test.isCommunicating
        TestLowCspSubarray.set_subsystems_and_go_to_state(
            device_under_test, DevState.ON
        )
        TestLowCspSubarray.set_subsystems_and_go_to_obsstate(
            device_under_test, ObsState.EMPTY
        )
        config_string = load_json_file(
            "AssignResources_CBF_PST.json", string=True
        )
        _, command_id = device_under_test.AssignResources(config_string)
        probe_poller(device_under_test, "obsstate", ObsState.IDLE, time=5)
        probe_poller(
            device_under_test,
            "longRunningCommandAttributes",
            (0, "COMPLETED"),
            time=3,
            command_id=command_id,
        )
        assert device_under_test.assignedTimingBeamIDs == [1]
        config_input_dict = load_json_file("Configure_CBF_PST.json")
        config_input_dict["pst"]["beams"][0]["beam_id"] = 11
        with pytest.raises(tango.DevFailed):
            device_under_test.configure(json.dumps(config_input_dict))
        assert device_under_test.obsstate == ObsState.IDLE

    @pytest.mark.parametrize(
        "initial_obsstate", [ObsState.IDLE, ObsState.READY]
    )
    @pytest.mark.skip(
        reason="now telmodel validation make the command to be "
        "rejected immediately because of 'lowcbf' missing field."
        "Consider to delete the test"
    )
    def test_configure_only_pss(
        self, device_under_test, initial_obsstate, low_subarray_init
    ):
        """Test if in the configuration file cbf entry is missing.

        The ObsState of CSP won't be updated, while the configuration is
        forwarded to PSS. The command resultCode is OK
        """
        assert device_under_test.isCommunicating
        TestLowCspSubarray.set_subsystems_and_go_to_state(
            device_under_test, DevState.ON
        )
        TestLowCspSubarray.set_subsystems_and_go_to_obsstate(
            device_under_test, initial_obsstate
        )
        config_input_dict = load_json_file("Configure_CBF_PSS_PST.json")
        config_input_dict.pop("lowcbf")
        _, command_id = device_under_test.configure(
            json.dumps(config_input_dict)
        )
        probe_poller(
            device_under_test,
            "obsState",
            initial_obsstate,
            time=5,
        )
        probe_poller(
            device_under_test,
            "state",
            DevState.ON,
            time=1,
        )
        probe_poller(
            device_under_test,
            "commandResult",
            ("configure", str(ResultCode.OK.value)),
            time=10,
        )
        probe_poller(
            device_under_test,
            "longRunningCommandAttributes",
            (0, "COMPLETED"),
            time=3,
            command_id=command_id,
        )

    @pytest.mark.parametrize(
        "initial_obsstate", [ObsState.IDLE, ObsState.READY]
    )
    @pytest.mark.skip(
        reason="now telmodel validation make the command to be"
        "rejected immediately. Consider to delete the test"
    )
    def test_configure_no_sub_id(
        self, device_under_test, initial_obsstate, low_subarray_init
    ):
        """Test if there is no sub_id in the configuration file.

        The ObsState of CSP won't be updated, since the command is
        failing. The command resultCode is FAILED
        """
        assert device_under_test.isCommunicating
        TestLowCspSubarray.set_subsystems_and_go_to_state(
            device_under_test, DevState.ON
        )
        TestLowCspSubarray.set_subsystems_and_go_to_obsstate(
            device_under_test, initial_obsstate
        )
        config_input_dict = load_json_file("Configure_CBF_PSS_PST.json")
        config_input_dict["common"].pop("subarray_id")
        _, command_id = device_under_test.configure(
            json.dumps(config_input_dict)
        )
        probe_poller(
            device_under_test,
            "obsState",
            initial_obsstate,
            time=5,
        )
        probe_poller(
            device_under_test,
            "state",
            DevState.ON,
            time=1,
        )
        probe_poller(
            device_under_test,
            "commandResult",
            ("configure", str(ResultCode.FAILED.value)),
            time=10,
        )
        probe_poller(
            device_under_test,
            "longRunningCommandAttributes",
            (3, "COMPLETED"),
            time=3,
            command_id=command_id,
        )

    def test_obsmode_after_abort_configure(
        self, device_under_test, low_subarray_init
    ):
        """
        Test the value of the obsmode after the subarray has
        been aborted and reconfigured.
        """
        assert device_under_test.isCommunicating
        TestLowCspSubarray.set_subsystems_and_go_to_state(
            device_under_test, DevState.ON
        )
        TestLowCspSubarray.set_subsystems_and_go_to_obsstate(
            device_under_test, ObsState.IDLE
        )

        config_input_dict = load_json_file("Configure_CBF.json")
        _, command_id = device_under_test.configure(
            json.dumps(config_input_dict)
        )

        probe_poller(
            device_under_test,
            "obsState",
            ObsState.READY,
            time=5,
        )
        # probe_poller(
        #     device_under_test,
        #     "commandResult",
        #     ("configure", str(ResultCode.OK.value)),
        #     time=5,
        # )

        probe_poller(
            device_under_test,
            "longRunningCommandAttributes",
            (0, "COMPLETED"),
            time=3,
            command_id=command_id,
        )
        assert device_under_test.obsmode == (ObsMode.IMAGING,)
        time.sleep(0.5)

        _, command_id = device_under_test.Abort()

        probe_poller(
            device_under_test,
            "longRunningCommandAttributes",
            (0, "COMPLETED"),
            time=3,
            command_id=command_id,
        )
        probe_poller(
            device_under_test,
            "obsState",
            ObsState.ABORTED,
            time=5,
        )
        assert device_under_test.obsmode == (ObsMode.IDLE,)
        _, command_id = device_under_test.Restart()

        probe_poller(
            device_under_test,
            "longRunningCommandAttributes",
            (0, "COMPLETED"),
            time=3,
            command_id=command_id,
        )
        probe_poller(device_under_test, "obsState", ObsState.EMPTY, time=3)
        TestLowCspSubarray.set_subsystems_and_go_to_obsstate(
            device_under_test, ObsState.IDLE
        )

        config_input_dict = load_json_file("Configure_CBF.json")
        _, command_id = device_under_test.configure(
            json.dumps(config_input_dict)
        )

        probe_poller(
            device_under_test,
            "obsState",
            ObsState.READY,
            time=5,
        )
        probe_poller(
            device_under_test,
            "commandResult",
            ("configure", str(ResultCode.OK.value)),
            time=5,
        )

        assert device_under_test.obsmode == (ObsMode.IMAGING,)

    def test_scan_cbf(self, device_under_test, low_subarray_init):
        """Test that scan command is accepted
        and Obstate transition to SCANNING"""

        assert device_under_test.isCommunicating
        TestLowCspSubarray.set_subsystems_and_go_to_state(
            device_under_test, DevState.ON
        )
        TestLowCspSubarray.set_subsystems_and_go_to_obsstate(
            device_under_test, ObsState.READY
        )

        config_input_dict = load_json_file("Scan_CBF.json")
        _, command_id = device_under_test.scan(json.dumps(config_input_dict))

        probe_poller(
            device_under_test,
            "obsState",
            ObsState.SCANNING,
            time=5,
        )
        probe_poller(
            device_under_test,
            "commandResult",
            ("scan", str(ResultCode.OK.value)),
            time=5,
        )

        probe_poller(
            device_under_test,
            "longRunningCommandAttributes",
            (0, "COMPLETED"),
            time=3,
            command_id=command_id,
        )

    def test_subarray_cm_configure_no_sub_id(
        self,
        device_under_test,
        low_subarray_init,
    ):
        """
        Test CSP Subarray Configure when the configure script does not specify
        sub_id.
        This script is not validated by the TElmodel schema.

        Expected behavior:
        - the CSP Subarray observing state won't be updated, since the command
          is not executed at all.
        - the command raises an Exception

        """
        TestLowCspSubarray.set_subsystems_and_go_to_state(
            device_under_test, DevState.ON
        )
        TestLowCspSubarray.set_subsystems_and_go_to_obsstate(
            device_under_test, ObsState.IDLE
        )
        config_input = TestLowCspSubarray.configuration_input(
            filename="Configure_CBF.json"
        )
        config_input["common"].pop("subarray_id")
        with pytest.raises(
            tango.DevFailed,
        ) as df:
            device_under_test.Configure(json.dumps(config_input))
        assert "ValueError: Validation 'LOWCSP configure 3.0' Key" in str(df)
        assert device_under_test.obsstate == ObsState.IDLE

    def test_configure_with_wrong_sub_id(
        self, device_under_test, low_subarray_init
    ):
        """
        Test configure is rejected if the subarray ID soes not
        match
        """

        TestLowCspSubarray.set_subsystems_and_go_to_state(
            device_under_test, DevState.ON
        )
        TestLowCspSubarray.set_subsystems_and_go_to_obsstate(
            device_under_test, ObsState.IDLE
        )
        config_dict = load_json_file("Configure_CBF_PSS_PST.json")
        config_dict["common"]["subarray_id"] = 2
        with pytest.raises(tango.DevFailed) as df:
            device_under_test.Configure(json.dumps(config_dict))
        assert "ValueError: Wrong value for the subarray ID: 2" in str(df)
