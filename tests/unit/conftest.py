import logging

import mock
import pytest
from low_mocked_connector import LowMockedConnector
from low_properties import (
    test_properties_low_ctrl,
    test_properties_low_subarray,
)
from ska_csp_lmc_common.manager.manager_configuration import (
    ComponentManagerConfiguration,
)
from ska_csp_lmc_common.testing.poller import probe_poller
from ska_tango_testing.mock.tango import MockTangoEventCallbackGroup

from ska_csp_lmc_low.manager import (
    LowCspControllerComponentManager,
    LowCspSubarrayComponentManager,
)

module_logger = logging.getLogger(__name__)


@pytest.fixture(scope="session")
def ctrl_component_manager(ctrl_op_state_model, ctrl_health_state_model):
    """Instantiate the component manager for the Low CSP Controller."""
    with mock.patch(
        "ska_csp_lmc_common.manager.manager_configuration."
        "ComponentManagerConfiguration.get_device_properties"
    ) as mock_prop:
        mock_prop.return_value = test_properties_low_ctrl
        cm_conf = ComponentManagerConfiguration("", module_logger)
        cm_conf.add_attributes()
        ctrl_component_manager = LowCspControllerComponentManager(
            ctrl_op_state_model,
            ctrl_health_state_model,
            cm_conf,
            logger=module_logger,
        )
        ctrl_component_manager.init()
        yield ctrl_component_manager


@pytest.fixture()
@mock.patch("ska_csp_lmc_common.component.Connector", new=LowMockedConnector)
def controller_cm_connected(ctrl_component_manager):
    """Establish communication between the component manager and the CSP sub-
    systems."""
    ctrl_component_manager.start_communicating()
    probe_poller(ctrl_component_manager, "is_communicating", True, time=3)


@pytest.fixture(scope="session")
def subarray_component_manager(
    subarray_health_state_model,
    subarray_op_state_model,
    subarray_obs_state_model,
):
    """Instantiate the component manager for the Low CSP Subarray."""
    with mock.patch(
        "ska_csp_lmc_common.manager.manager_configuration."
        "ComponentManagerConfiguration.get_device_properties"
    ) as mock_prop:
        mock_prop.return_value = test_properties_low_subarray
        cm_conf = ComponentManagerConfiguration("", module_logger)
        cm_conf.add_attributes()
        subarray_component_manager = LowCspSubarrayComponentManager(
            subarray_health_state_model,
            subarray_op_state_model,
            subarray_obs_state_model,
            cm_conf,
            logger=module_logger,
        )
        subarray_component_manager.init()
        yield subarray_component_manager


@pytest.fixture()
@mock.patch("ska_csp_lmc_common.component.Connector", new=LowMockedConnector)
def subarray_cm_connected(subarray_component_manager):
    """Establish communication between the component manager and the CSP sub-
    systems."""
    subarray_component_manager.start_communicating()
    probe_poller(subarray_component_manager, "is_communicating", True, time=3)


@pytest.fixture
def change_event_callbacks() -> MockTangoEventCallbackGroup:
    """
    Return a dictionary of Tango device change event callbacks with
    asynchrony support.

    :return: a collections.defaultdict that returns change event
        callbacks by name.
    """
    return MockTangoEventCallbackGroup(
        "adminMode",
        "obsState",
        "state",
        "healthState",
        "isCommunicating",
        "longRunningCommandProgress",
        "longRunningCommandResult",
        "longRunningCommandStatus",
        "devicesJson",
        "devicesState",
        "devicesHealthState",
        "devicesAdminMode",
        "devicesJson",
        "devicesPstProcessingMode",
        "devicesDataReceived",
        "devicesDataReceiveRate",
        "devicesDataDropped",
        "devicesDataDropRate",
        "devicesDataDropRateAvg",
        "devicesDataDropRateStddev",
        "devicesDataReceiveRateAvg",
        "devicesDataReceiveRateStddev",
        "devicesDataReceiveRateMonitoring",
        "devicesDataDropRateMonitoring",
        "devicesSerialNumber",
        "devicesReady",
        "devicesFirmware",
        "devicesPktsIn",
        "devicesPktsOut",
        "devicesSubarrayIds",
        timeout=5,
    )
