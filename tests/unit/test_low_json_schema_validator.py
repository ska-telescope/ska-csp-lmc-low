import json
import logging

import pytest
from ska_csp_lmc_common.testing.test_classes import load_json_file

from ska_csp_lmc_low.subarray.low_json_schema_validator import (
    CspLowJsonValidator,
)

module_logger = logging.getLogger(__name__)


# pylint: disable-next=missing-class-docstring
class TestLowJsonSchemaValidator:
    def test_validate_assignresources_config_json_no_lowcbf(self):
        """Test the config json script is not loaded if the
        requested section ('lowcbf') is missing inside the json script."""
        assign_input = load_json_file(
            filename="AssignResources_CBF_PSS_PST.json"
        )
        assign_input.pop("lowcbf")
        json_input = json.dumps(assign_input)
        with pytest.raises(ValueError, match=r"\blowcbf\b"):
            CspLowJsonValidator("Assignresources").validate(json_input)

    def test_validate_configure_config_json_pst_missing(self):
        """Test the config json script raises an error if pst not
        present but timing_beams is"""
        config_input = load_json_file(filename="Configure_CBF_PSS_PST.json")
        config_input.pop("pst")
        json_input = json.dumps(config_input)
        with pytest.raises(
            KeyError,
            match="'timing_beams' key included but 'pst' key is missing",
        ):
            CspLowJsonValidator("Configure", logger=module_logger).validate(
                json_input
            )

    def test_validate_configure_config_json_timingbeams_missing(self):
        """Test the config json script raise an error if timing_beams not
        present but pst is"""
        config_input = load_json_file(filename="Configure_CBF_PSS_PST.json")
        config_input["lowcbf"].pop("timing_beams")
        json_input = json.dumps(config_input)
        with pytest.raises(
            KeyError,
            match="'pst' key included but 'timing_beams' key is missing",
        ):
            CspLowJsonValidator("Configure").validate(json_input)

    def test_validate_configure_config_json_timingbeams_pst_missing(self):
        """Test if the validation pass if both timing_beams
        and pst are missing"""
        config_input = load_json_file(filename="Configure_CBF.json")
        json_input = json.dumps(config_input)
        decoded_dict = CspLowJsonValidator(
            "Configure", logger=module_logger
        ).validate(json_input)
        assert decoded_dict

    def test_validate_configure_config_json_invalid_pst_beam_id(self):
        """Test the config json script is not loaded if the values
        of pst_beam_id are not coherent"""
        config_input = load_json_file(filename="Configure_CBF_PSS_PST.json")
        config_input["pst"]["beams"][0]["beam_id"] = 11
        attrs_dict = {
            "subarray_id": 1,
            "assigned_timing_beams_id": [1, 2],
            "deployed_timing_beams_id": ["low-pst/beam/01", "low-pst/beam/02"],
        }
        json_input = json.dumps(config_input)
        with pytest.raises(
            ValueError,
            match=(
                r"Incoherent configuration: the PST beam IDs specified in "
                r"the CBF section \(\[1\]\) "
                r"do not match those specified in the PST section \(\[11\]\)"
            ),
        ):
            CspLowJsonValidator("Configure", attrs_dict=attrs_dict).validate(
                json_input
            )

    def test_validate_configure_config_json_multiple_pst_beam_id(self):
        """Test the config json script is not loaded if multiple values
        of pst_beam_id are not coherent"""
        config_input = load_json_file(
            filename="Configure_CBF_PSS_PST_multibeams.json"
        )
        attrs_dict = {
            "subarray_id": 1,
            "assigned_timing_beams_id": [1, 2, 11],
            "deployed_timing_beams_id": [
                "low-pst/beam/01",
                "low-pst/beam/02",
                "low-pst/beam/11",
            ],
        }
        config_input["pst"]["beams"][1]["beam_id"] = 11
        json_input = json.dumps(config_input)
        with pytest.raises(ValueError, match=r"^Incoherent configuration"):
            CspLowJsonValidator("Configure", attrs_dict=attrs_dict).validate(
                json_input
            )

    def test_validate_scan_config_json_no_lowcbf(self):
        """Test the config json script is not loaded if the
        requested section ('lowcbf') is missing inside the json script."""
        scan_input = load_json_file(filename="Scan_CBF_wrong.json")
        json_input = json.dumps(scan_input)
        with pytest.raises(ValueError, match=r"\blowcbf\b"):
            CspLowJsonValidator("Scan").validate(json_input)

    def test_validate_config_with_wrong_sdp_addr_format(self):
        """
        Test the config json script raises an error if SDP addresses
        for visibilities are not a numerical dotted one.
        """
        config_input = load_json_file(filename="Configure_CBF.json")
        # modify the host address to a formal one
        config_input["lowcbf"]["vis"]["stn_beams"][0]["host"][0][1] = "a.b.c.d"
        json_input = json.dumps(config_input)

        with pytest.raises(
            ValueError,
            match="host address 'a.b.c.d' for station beam 1 is not"
            " in numerical dotted format",
        ):
            CspLowJsonValidator("Configure").validate(json_input)
