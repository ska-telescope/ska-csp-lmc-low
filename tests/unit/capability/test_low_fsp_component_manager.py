import logging
import time

import mock
import pytest
from low_mocked_connector import LowMockedConnector
from low_properties import test_properties_low_fsp_capability

# from ska_control_model import HealthState
from ska_csp_lmc_common.manager.manager_configuration import (
    ComponentManagerConfiguration,
)
from ska_csp_lmc_common.testing.poller import probe_poller
from ska_csp_lmc_common.testing.test_classes import TestBase

from ska_csp_lmc_low.capability.low_fsp_capability_component_manager import (
    LowFspCapabilityComponentManager,
)

# from tango import DevState


module_logger = logging.getLogger(__name__)


def callback(attr_name, attr_value):
    module_logger.debug(f"attr_name: {attr_name} value; {attr_value}")


@pytest.fixture(scope="function")
@mock.patch("ska_csp_lmc_common.component.Connector", new=LowMockedConnector)
def low_fsp_capability_component_manager(
    capability_op_state_model=None, capability_health_state_model=None
):
    with mock.patch(
        "ska_csp_lmc_common.manager.manager_configuration."
        "ComponentManagerConfiguration.get_device_properties"
    ) as mock_prop:
        mock_prop.return_value = test_properties_low_fsp_capability
        cm_conf = ComponentManagerConfiguration(
            "low-fsp-capability", module_logger
        )
        cm_conf.add_attributes()
        low_fsp_capability_component_manager = (
            LowFspCapabilityComponentManager(
                cm_conf,
                callback,
                module_logger,
            )
        )
        low_fsp_capability_component_manager.init()
        low_fsp_capability_component_manager.start_communicating()

        # this sleep is to allow the connection to Allocator
        # there is no tango attribute indicator that could be used in tests
        time.sleep(3)
        return low_fsp_capability_component_manager


@mock.patch("ska_csp_lmc_common.component.Connector", new=LowMockedConnector)
class TestLowFspCapabilityComponentManager(TestBase):
    def test_fsp_capability_cm_not_connected(
        self, low_fsp_capability_component_manager
    ):
        """Test that the capability cm is not connected
        if no processor are registered in the Allocator
        even if start communicating has been called (in the fixture)"""
        # this sleep to ensure that the test doesn't pass just because
        # attribute was checked before time.
        # 3 secs should be enough, see below test for connection
        time.sleep(3)
        assert low_fsp_capability_component_manager.is_communicating is False

    def test_fsp_capability_cm_connected(
        cls, low_fsp_capability_component_manager
    ):
        """
        Test the connection with the FSP devices.
        The connection is made to two processors in two separate moments.
        The connection is triggered by events on the allocator.
        """

        cls.raise_event_on_sub_system(
            "fsps",
            '{"fsp_01": ["XFL1RCFEG244"]}',
            "low-cbf/allocator/0",
            None,
            None,
        )

        time.sleep(3)
        # if there is only event on one of the subsystems,
        # it shouldn't start the connection
        # time.sleep is to be certain that we don't do the connection too soon
        assert low_fsp_capability_component_manager.is_communicating is False
        assert low_fsp_capability_component_manager.devices_deployed == 0
        assert low_fsp_capability_component_manager.devices_id == []
        assert low_fsp_capability_component_manager.devices_fqdn == []
        assert low_fsp_capability_component_manager.devices_serialnumber == []

        cls.raise_event_on_sub_system(
            "procdevfqdn",
            '{"XFL1RCFEG244": "low-cbf/processor/0.0.0"}',
            "low-cbf/allocator/0",
            None,
            None,
        )

        probe_poller(
            low_fsp_capability_component_manager,
            "is_communicating",
            True,
            time=3,
        )

        assert low_fsp_capability_component_manager.devices_deployed == 1
        assert low_fsp_capability_component_manager.devices_id == [1]
        assert low_fsp_capability_component_manager.devices_fqdn == [
            "low-cbf/processor/0.0.0"
        ]
        assert low_fsp_capability_component_manager.devices_serialnumber == [
            "XFL1RCFEG244"
        ]

        cls.raise_event_on_sub_system(
            "fsps",
            '{"fsp_01": ["XFL1RCFEG244"], "fsp_02": ["XFL14SLO1LIF"]}',
            "low-cbf/allocator/0",
            None,
            None,
        )
        cls.raise_event_on_sub_system(
            "procdevfqdn",
            '{"XFL1RCFEG244": "low-cbf/processor/0.0.0",'
            '"XFL14SLO1LIF":"low-cbf/processor/0.0.1"}',
            "low-cbf/allocator/0",
            None,
            None,
        )

        probe_poller(
            low_fsp_capability_component_manager,
            "devices_deployed",
            2,
            time=3,
        )

        assert low_fsp_capability_component_manager.devices_serialnumber == [
            "XFL1RCFEG244",
            "XFL14SLO1LIF",
        ]
        assert low_fsp_capability_component_manager.is_communicating is True
        assert low_fsp_capability_component_manager.devices_id == [1, 2]
        assert low_fsp_capability_component_manager.devices_fqdn == [
            "low-cbf/processor/0.0.0",
            "low-cbf/processor/0.0.1",
        ]

    def test_fsp_capability_cm_events_on_processors(
        cls, low_fsp_capability_component_manager
    ):
        """Test that attributes are updated if an event arrives
        on processor specific attributes"""

        # connection to two processors

        cls.raise_event_on_sub_system(
            "fsps",
            '{"fsp_01": ["XFL1RCFEG244"], "fsp_02": ["XFL14SLO1LIF"]}',
            "low-cbf/allocator/0",
            None,
            None,
        )

        cls.raise_event_on_sub_system(
            "procdevfqdn",
            '{"XFL1RCFEG244": "low-cbf/processor/0.0.1",'
            '"XFL14SLO1LIF":"low-cbf/processor/0.0.0"}',
            "low-cbf/allocator/0",
            None,
            None,
        )

        probe_poller(
            low_fsp_capability_component_manager,
            "is_communicating",
            True,
            time=3,
        )

        # event on stats_mode attribute on processor 0.0.0
        cls.raise_event_on_sub_system(
            "stats_mode",
            '{"ready": "true", "firmware":"vis0.0.1"}',
            "low-cbf/processor/0.0.0",
            None,
            None,
        )

        probe_poller(
            low_fsp_capability_component_manager,
            "devices_ready",
            ["false", "true"],
            time=3,
        )

        assert low_fsp_capability_component_manager.devices_firmware == [
            "",
            "vis0.0.1",
        ]

        # event on stats_mode attribute on processor 0.0.1
        # note: vis_pkts_out will be ignored
        cls.raise_event_on_sub_system(
            "stats_io",
            '{"total_pkts_in": 123456, "total_pkts_out": 34567,'
            '"vis_pkts_out": 123}',
            "low-cbf/processor/0.0.1",
            None,
            None,
        )

        probe_poller(
            low_fsp_capability_component_manager,
            "devices_pkts_in",
            [123456, 0],
            time=3,
        )

        assert low_fsp_capability_component_manager.devices_pkts_out == [
            34567,
            0,
        ]

        # event on subarrayids on processor 0.0.0
        cls.raise_event_on_sub_system(
            "subarrayids",
            [1],
            "low-cbf/processor/0.0.0",
            None,
            None,
        )

        probe_poller(
            low_fsp_capability_component_manager,
            "devices_subarray_ids",
            ["", "1"],
            time=3,
        )
