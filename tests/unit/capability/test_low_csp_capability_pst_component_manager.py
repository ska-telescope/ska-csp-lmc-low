import logging

import mock
import pytest
from low_mocked_connector import LowMockedConnector
from low_properties import test_properties_pst_capability
from low_test_classes import TestLowPstCapability
from ska_csp_lmc_common.manager.manager_configuration import (
    ComponentManagerConfiguration,
)
from ska_csp_lmc_common.testing.poller import probe_poller

from ska_csp_lmc_low.capability import LowPstCapabilityComponentManager

module_logger = logging.getLogger(__name__)


def callback(attr_name, attr_value):
    module_logger.debug(f"attr_name: {attr_name} value; {attr_value}")


@pytest.fixture(scope="function")
def capability_component_manager_disconnected(
    capability_op_state_model=None, capability_health_state_model=None
):
    with mock.patch(
        "ska_csp_lmc_common.manager.manager_configuration."
        "ComponentManagerConfiguration.get_device_properties"
    ) as mock_prop:
        mock_prop.return_value = test_properties_pst_capability
        cm_conf = ComponentManagerConfiguration("", module_logger)
        cm_conf.add_attributes()
        capability_component_manager = LowPstCapabilityComponentManager(
            cm_conf,
            callback,
            module_logger,
        )
        return capability_component_manager


@pytest.fixture(scope="function")
@mock.patch("ska_csp_lmc_common.component.Connector", new=LowMockedConnector)
def capability_component_manager(
    capability_op_state_model=None, capability_health_state_model=None
):
    with mock.patch(
        "ska_csp_lmc_common.manager.manager_configuration."
        "ComponentManagerConfiguration.get_device_properties"
    ) as mock_prop:
        mock_prop.return_value = test_properties_pst_capability
        cm_conf = ComponentManagerConfiguration("", module_logger)
        cm_conf.add_attributes()
        capability_component_manager = LowPstCapabilityComponentManager(
            cm_conf,
            callback,
            module_logger,
        )
        capability_component_manager.init()
        capability_component_manager.start_communicating()
        probe_poller(
            capability_component_manager, "is_communicating", True, time=3
        )
        pst_fqdn_list = capability_component_manager.devices_fqdn
        devices_dict = dict()
        for fqdn in pst_fqdn_list:
            pst_id = f"pst-0{pst_fqdn_list.index(fqdn)+1}"

            devices_dict.update({pst_id: fqdn})
        TestLowPstCapabilityComponentManager.devices_list = devices_dict
        return capability_component_manager


class TestLowPstCapabilityComponentManager(TestLowPstCapability):
    def test_pst_capability_cm_not_connected(
        self, capability_component_manager_disconnected
    ):
        probe_poller(
            capability_component_manager_disconnected,
            "is_communicating",
            False,
            time=3,
        )
        assert not capability_component_manager_disconnected.devices_state

    def test_pst_capability_cm_connected(self, capability_component_manager):
        """
        Test the connection with the VCC devices and the initial values for the
        main attributes.
        """

        probe_poller(
            capability_component_manager,
            "devices_state",
            [
                "OFF",
                "OFF",
            ],
            time=10,
        )
        probe_poller(
            capability_component_manager,
            "devices_admin_mode",
            [
                "ONLINE",
                "ONLINE",
            ],
            time=4,
        )
        probe_poller(
            capability_component_manager,
            "devices_health_state",
            ["OK", "OK"],
            time=4,
        )
        assert capability_component_manager.devices_deployed == len(
            capability_component_manager.devices_fqdn
        )
