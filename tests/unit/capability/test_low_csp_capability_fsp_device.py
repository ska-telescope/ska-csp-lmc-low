import json
import logging
import time

import mock
import numpy

# from low_attribute_factory import LowAttributeDataFactory
from low_mocked_connector import LowMockedConnector
from ska_control_model import AdminMode, HealthState
from ska_csp_lmc_common.testing.poller import probe_poller
from ska_csp_lmc_common.testing.test_classes import TestBase
from tango import DevState, EventType

device_to_load = {
    "path": "tests/unit/csplmc_dsconfig.json",
    "package": "ska_csp_lmc_low",
    "device": "capabilityfsp",
}

module_logger = logging.getLogger(__name__)


def test_fsp_capability_failure_connecting_to_master_device(device_under_test):
    """
    Verify that the device's state and healthState transition to
    FAULT and FAILED, respectively, if the connection to the CBF
    Allocator fails. In this scenario, the proxy to the CBF
    Allocator is not mocked, causing the connection to fail due to
    a timeout.
    """

    probe_poller(device_under_test, "healthState", HealthState.FAILED, time=7)
    probe_poller(device_under_test, "state", DevState.FAULT, time=3)
    assert not device_under_test.isCommunicating


@mock.patch("ska_csp_lmc_common.component.Connector", new=LowMockedConnector)
class TestLowFspCapabilityDevice(TestBase):

    # NOTE: a placeholder to think about wrong connectivity in FSPDevice
    # when HealthState is FAILED, when DEGRADED?
    # for how to test reference to test_low_pst_device.py

    def test_fsp_capability_failure_connecting_to_master_device_2(
        cls, device_under_test
    ):
        """
        Test that the device state and healthState are FAULT, FAILED
        if the connection with the CBF Allocator fails.
        """
        with mock.patch(
            "ska_csp_lmc_common.component.Component.read_attr"
        ) as mock_read_attr:
            mock_read_attr.side_effect = ValueError("Error in reading")
            probe_poller(
                device_under_test, "healthState", HealthState.FAILED, time=5
            )
            probe_poller(device_under_test, "state", DevState.FAULT, time=3)
            assert not device_under_test.isCommunicating

    def test_fsp_capability_initialization(cls, device_under_test):

        # this sleep is to allow the connection to Allocator
        # there is no tango attribute indicator that could be used in tests
        time.sleep(3)
        assert device_under_test.devicesDeployed == 0

        cls.raise_event_on_sub_system(
            "fsps",
            '{"fsp_01": ["XFL1RCFEG244"]}',
            "low-cbf/allocator/0",
            None,
            None,
        )

        cls.raise_event_on_sub_system(
            "procdevfqdn",
            '{"XFL1RCFEG244": "low-cbf/processor/0.0.0"}',
            "low-cbf/allocator/0",
            None,
            None,
        )

        probe_poller(device_under_test, "state", DevState.ON, time=5)
        assert device_under_test.healthState == HealthState.OK
        assert device_under_test.adminMode == AdminMode.ONLINE
        assert device_under_test.devicesDeployed == 1

        cls.raise_event_on_sub_system(
            "fsps",
            '{"fsp_01": ["XFL1RCFEG244"], "fsp_02": ["XFL14SLO1LIF"]}',
            "low-cbf/allocator/0",
            None,
            None,
        )
        cls.raise_event_on_sub_system(
            "procdevfqdn",
            '{"XFL1RCFEG244": "low-cbf/processor/0.0.0",'
            '"XFL14SLO1LIF":"low-cbf/processor/0.0.1"}',
            "low-cbf/allocator/0",
            None,
            None,
        )

        probe_poller(device_under_test, "devicesDeployed", 2, time=5)

        fsp_data_default = {
            "dev_id": 0,
            "fqdn": "",
            "state": "ON",
            "health_state": "OK",
            "admin_mode": "ONLINE",
            "serialnumber": "",
            "ready": "false",
            "firmware": "",
            "subarray_ids": "",
            "pkts_in": 0,
            "pkts_out": 0,
        }

        devicesJson = {}
        fsp_data_list_default = []

        device_deployed = device_under_test.devicesDeployed
        devicesJson["devices_deployed"] = device_deployed
        for counter in range(device_deployed):
            x = fsp_data_default.copy()
            x["dev_id"] = counter + 1
            x["fqdn"] = f"low-cbf/processor/0.0.{counter}"
            fsp_data_list_default.append(x)
        fsp_data_list_default[0]["serialnumber"] = "XFL1RCFEG244"
        fsp_data_list_default[1]["serialnumber"] = "XFL14SLO1LIF"

        devicesJson["fsp"] = fsp_data_list_default

        probe_poller(device_under_test, "devicesState", ("ON", "ON"), time=5)
        # note: just waiting for states to be ON make the test flaky because
        # adminmode and healthstate are update in a subsequent moment
        # (to be checked)
        probe_poller(
            device_under_test, "devicesAdminMode", ("ONLINE", "ONLINE"), time=5
        )
        assert json.loads(device_under_test.devicesJson) == devicesJson

        # fsp devices attributes
        assert device_under_test.devicesHealthState == (
            "OK",
            "OK",
        )

        assert device_under_test.devicesFqdn == (
            "low-cbf/processor/0.0.0",
            "low-cbf/processor/0.0.1",
        )

        assert device_under_test.devicesSerialnumber == (
            "XFL1RCFEG244",
            "XFL14SLO1LIF",
        )

        assert device_under_test.devicesAdminMode == (
            "ONLINE",
            "ONLINE",
        )

        assert device_under_test.devicesSerialNumber == (
            "XFL1RCFEG244",
            "XFL14SLO1LIF",
        )

        for counter in range(device_under_test.devicesDeployed):
            assert device_under_test.devicesReady[counter] == "false"
            assert device_under_test.devicesFirmware[counter] == ""
            assert device_under_test.devicesPktsIn[counter] == 0
            assert device_under_test.devicesPktsOut[counter] == 0
            assert device_under_test.devicesSubarrayIds[counter] == ""

    def test_fsp_capability_attributes_configured_for_events(
        cls,  #: TestLowPstCapabilityDevice,
        device_under_test,  #: DeviceTestContext,
        change_event_callbacks,  #: MockTangoEventCallbackGroup,
    ):
        """
        Test that the attributes of the PST Capability devices are
        properly configured to push events from the device.
        """

        time.sleep(3)
        cls.raise_event_on_sub_system(
            "fsps",
            '{"fsp_01": ["XFL1RCFEG244"], "fsp_02": ["XFL14SLO1LIF"]}',
            "low-cbf/allocator/0",
            None,
            None,
        )
        cls.raise_event_on_sub_system(
            "procdevfqdn",
            '{"XFL1RCFEG244": "low-cbf/processor/0.0.0",'
            '"XFL14SLO1LIF":"low-cbf/processor/0.0.1"}',
            "low-cbf/allocator/0",
            None,
            None,
        )

        probe_poller(device_under_test, "state", DevState.ON, time=10)
        assert device_under_test.devicesDeployed == 2
        evt_id = []

        attrs_list = [
            "isCommunicating",
            "devicesState",
            "devicesHealthState",
            "devicesAdminMode",
            "devicesJson",
            "devicesSerialNumber",
            "devicesReady",
            "devicesFirmware",
            "devicesPktsIn",
            "devicesPktsOut",
            "devicesSubarrayIds",
        ]
        for attr_name in attrs_list:
            evt_id.append(
                device_under_test.subscribe_event(
                    attr_name,
                    EventType.CHANGE_EVENT,
                    change_event_callbacks[attr_name],
                )
            )
            # read the current value
            device_attr = getattr(
                device_under_test,
                attr_name,
            )
            if isinstance(device_attr, numpy.ndarray):
                device_attr = device_attr.tolist()
            change_event_callbacks.assert_change_event(
                attr_name,
                attribute_value=device_attr,
                lookahead=len(attrs_list),
            )
        for _id in evt_id:
            device_under_test.unsubscribe_event(_id)
