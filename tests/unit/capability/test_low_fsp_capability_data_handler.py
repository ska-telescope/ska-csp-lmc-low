import logging

from mock import mock
from tango import DevState

from ska_csp_lmc_low.capability.low_fsp_capability_data_handler import (
    LowFspDataHandler,
)

module_logger = logging.getLogger(__name__)

capability_name = "fsp_cap"
list_of_devices = ["low-cbf/processor/0.0.0", "low-cbf/processor/0.0.1"]


def test_init_fsp_data_handler():
    """Test that the json_dit property of the data handler is
    correctly populated at initializiation"""

    dummy_callback = mock.MagicMock()
    cdh = LowFspDataHandler(capability_name, dummy_callback, module_logger)
    assert cdh.json_dict["devices_deployed"] == 0

    cdh.init(list_of_devices)

    assert cdh._json_dict["devices_deployed"] == 2
    assert len(cdh._json_dict["fsp_cap"]) == 2
    for i in range(2):
        # Check at least the state to see that dict is well inherited from base
        # data handler
        assert cdh.json_dict["fsp_cap"][i]["state"] == "UNKNOWN"
        assert cdh.json_dict["fsp_cap"][i]["serialnumber"] == ""
        assert cdh.json_dict["fsp_cap"][i]["ready"] == "false"
        assert cdh.json_dict["fsp_cap"][i]["subarray_ids"] == ""
        assert cdh.json_dict["fsp_cap"][i]["pkts_in"] == 0
        assert cdh.json_dict["fsp_cap"][i]["pkts_out"] == 0


def test_fsp_data_handler_update():
    """Test that the json_dit property of the data handler is
    correctly populated at initializiation"""

    dummy_callback = mock.Mock()
    cdh = LowFspDataHandler(capability_name, dummy_callback, module_logger)
    cdh.init(list_of_devices)

    # cdh.update("low-cbf/processor/0.0.0", "state", DevState.OFF)
    # assert False

    # update ready and firmware on device 01
    cdh.update(
        "low-cbf/processor/0.0.0",
        "stats_mode",
        '{"ready": "true", "firmware": "visibility0.1.1"}',
    )
    assert cdh.json_dict["fsp_cap"][0]["ready"] == "true"
    assert cdh.json_dict["fsp_cap"][0]["firmware"] == "visibility0.1.1"
    dummy_callback.assert_any_call("devicesJson", cdh.json_dict)
    dummy_callback.assert_any_call("devicesready", ["true", "false"])
    dummy_callback.assert_any_call("devicesfirmware", ["visibility0.1.1", ""])

    # update pktsin and pktsout  on device 01
    cdh.update(
        "low-cbf/processor/0.0.0",
        "stats_io",
        '{"total_pkts_in": 1000, "total_pkts_out": 900}',
    )
    assert cdh.json_dict["fsp_cap"][0]["pkts_in"] == 1000
    assert cdh.json_dict["fsp_cap"][0]["pkts_out"] == 900
    dummy_callback.assert_any_call("devicesJson", cdh.json_dict)
    dummy_callback.assert_any_call("devicespktsin", [1000, 0])
    dummy_callback.assert_any_call("devicespktsout", [900, 0])

    # update state on device 02
    cdh.update(
        "low-cbf/processor/0.0.1",
        "state",
        DevState.ALARM,
    )
    assert cdh.json_dict["fsp_cap"][1]["state"] == "ALARM"
    dummy_callback.assert_any_call("devicesJson", cdh.json_dict)
    dummy_callback.assert_any_call("devicesstate", ["UNKNOWN", "ALARM"])

    # update subarrayids on device01
    cdh.update(
        "low-cbf/processor/0.0.0",
        "subarrayids",
        [1, 2],
    )

    assert cdh.json_dict["fsp_cap"][0]["subarray_ids"] == "1, 2"
