import json
import logging

import mock
import numpy
from low_attribute_factory import LowAttributeDataFactory
from low_mocked_connector import LowMockedConnector
from low_test_classes import TestLowPstCapability
from ska_control_model import AdminMode, HealthState
from ska_csp_lmc_common.testing.poller import probe_poller
from ska_csp_lmc_common.utils.cspcommons import read_release_version
from tango import DevState, EventType

from ska_csp_lmc_low import release

device_to_load = {
    "path": "tests/unit/csplmc_dsconfig.json",
    "package": "ska_csp_lmc_low",
    "device": "capabilitypst",
}

module_logger = logging.getLogger(__name__)


def tango_fail(attr_name):
    raise ValueError("Tango failure!")


def connection_failure():
    raise ValueError("Connection failure")


def test_pst_capability_exception_in_reading_pst_addresses(
    device_under_test,
):
    """
    Verify the state and health state of the capability are in failed state
    when the read of pstBeamsAddresses from CSP Controller fails
    """
    with mock.patch(
        "ska_csp_lmc_common.component.Connector.get_attribute"
    ) as mock_get_attribute, mock.patch(
        "ska_csp_lmc_common.component.Connector._get_deviceproxy"
    ) as mock_get_deviceproxy:
        mock_get_deviceproxy.return_value = mock.MagicMock()()

        mock_get_attribute.side_effect = tango_fail
        probe_poller(
            device_under_test, "healthState", HealthState.FAILED, time=5
        )
        probe_poller(device_under_test, "State", DevState.FAULT, time=3)


@mock.patch("ska_csp_lmc_common.component.Connector", new=LowMockedConnector)
class TestLowPstCapabilityDevice(TestLowPstCapability):
    def test_pst_capability_wrong_pst_addresses(
        self,
        device_under_test,
    ):
        """
        Verify the health state of the capability is FAILED if the list
        of PST beams addresses is an empty list.
        """
        LowAttributeDataFactory.attributes_ctrl_default[
            "pstbeamsaddresses"
        ] = []
        probe_poller(
            device_under_test, "healthState", HealthState.FAILED, time=5
        )
        probe_poller(device_under_test, "State", DevState.FAULT, time=3)

    def test_pst_capability_connection_with_all_pst_beams_failed(
        self,
        device_under_test,
    ):
        """
        Verify the state and health state of the capability are in failed state
        when the connection with all the PST beams is not established.
        """
        with mock.patch(
            "ska_csp_lmc_common.observing_component.ObservingComponent.connect"
        ) as mock_connect:
            mock_connect.side_effect = connection_failure

            probe_poller(
                device_under_test, "healthState", HealthState.FAILED, time=5
            )
            probe_poller(device_under_test, "State", DevState.FAULT, time=3)
            assert not device_under_test.isCommunicating

    def test_pst_capability_connection_with_one_pst_beam_failed(
        self,
        device_under_test,
    ):
        """
        Test that the state and health status of the capability are set to ON
        and DEGRADED when the connection with at least one PST beam fails.

        The succeeded() and failure_detected() methods of the ComponentCommand
        are mocked to simulate a connection error.
        When side_effect is set to an iterable every call to the mock returns
        the next value from the iterable.
        """

        def side_effect_function(*args, **kwargs):
            call_count = side_effect_function.call_count
            side_effect_function.call_count += 1
            if call_count == 0:
                return DevState.OFF
            elif call_count == 1:
                raise ValueError("Error in reading state value")
            return DevState.OFF

        # Initialize call count
        side_effect_function.call_count = 0
        device_under_test.logginglevel = 5
        with mock.patch(
            "ska_csp_lmc_common.observing_component"
            ".ObservingComponent.read_attr"
        ) as mock_succeeded:
            mock_succeeded.side_effect = side_effect_function
            probe_poller(
                device_under_test, "healthState", HealthState.DEGRADED, time=5
            )
            probe_poller(device_under_test, "State", DevState.ON, time=3)
            assert device_under_test.isCommunicating

    def test_pst_capability_initialization(self, device_under_test):

        pstbeam_data_default = {
            "dev_id": 0,
            "fqdn": "",
            "state": "OFF",
            "health_state": "OK",
            "admin_mode": "ONLINE",
            # "subarray_membership": [],
            "data_received": 0,
            "data_receive_rate": 0.0,
            "data_dropped": 0,
            "data_drop_rate": 0.0,
            "processing_mode": "IDLE",
            "obs_state": "IDLE",
        }
        devicesJson = {}
        pstbeam_data_list_default = []

        probe_poller(device_under_test, "state", DevState.ON, time=10)

        devicesJson["devices_deployed"] = device_under_test.devicesDeployed
        for counter in range(device_under_test.devicesDeployed):
            x = pstbeam_data_default.copy()
            id = counter + 1
            x["dev_id"] = id
            id_str = str(id).zfill(2)
            x["fqdn"] = f"low-pst/beam/{id_str}"
            pstbeam_data_list_default.append(x)

        devicesJson["pst"] = pstbeam_data_list_default

        probe_poller(device_under_test, "healthState", HealthState.OK, time=5)
        assert device_under_test.adminMode == AdminMode.ONLINE

        # pst devices attributes
        assert device_under_test.devicesDeployed == 2
        assert json.loads(device_under_test.devicesJson) == devicesJson
        # {
        #   "devices_deployed": 2,
        #   "pst": [
        #     {
        #       "dev_id": 1,
        #       "fqdn": "low-pst/beam/01",
        #       *"simulation_mode": True,
        #       "state": "OFF",
        #       "health_state": "OK",
        #       "admin_mode": "ONLINE",
        #       * "subarray_membership": [3],
        #       "data_received": 0,
        #       "data_receive_rate": 0.0,
        #       "data_dropped": 0,
        #       "data_drop_rate": 0.0,
        #       "processing_mode" : "IDLE",
        #       "obs_state" : "IDLE"
        #     },
        #     {
        #       "dev_id": 2,
        #       "fqdn": "low-pst/beam/01",
        #       * "simulation_mode": True,
        #       "state": "ON",
        #       "health_state": "OK",
        #       "admin_mode": "ONLINE",
        #       * "subarray_membership": [5],
        #       "data_received": 0,
        #       "data_receive_rate": 0.0,
        #       "data_dropped": 0,
        #       "data_drop_rate": 0.0,
        #       "processing_mode" : "IDLE",
        #       "obs_state" : "IDLE"
        #     },
        #   ]
        # }
        assert device_under_test.devicesPstProcessingMode == (
            "IDLE",
            "IDLE",
        )
        assert device_under_test.devicesObsState == (
            "IDLE",
            "IDLE",
        )
        assert device_under_test.devicesFqdn == (
            "low-pst/beam/01",
            "low-pst/beam/02",
        )
        assert (device_under_test.devicesState) == (
            "OFF",
            "OFF",
        )
        assert device_under_test.devicesHealthState == (
            "OK",
            "OK",
        )
        assert device_under_test.devicesAdminMode == (
            "ONLINE",
            "ONLINE",
        )
        for counter in range(device_under_test.devicesDeployed):
            assert device_under_test.devicesDataReceived[counter] == 0
            assert device_under_test.devicesDataReceiveRate[counter] == 0.0
            assert device_under_test.devicesDataDropped[counter] == 0
            assert device_under_test.devicesDataDropRate[counter] == 0.0

        # assert device_under_test.isCommunicating

        device_type = "pst"
        fqdn_member = []
        for fqdn in device_under_test.devicesFqdn:
            fqdn_member.append(fqdn[-2:])

        pst_json_dict = json.loads(device_under_test.devicesJson)
        assert (
            pst_json_dict["devices_deployed"]
            == device_under_test.devicesDeployed
        )
        for i in range(device_under_test.devicesDeployed):
            assert (
                pst_json_dict[device_type][i]["fqdn"]
                == device_under_test.devicesFqdn[i]
            )
            assert (
                pst_json_dict[device_type][i]["health_state"]
                == device_under_test.devicesHealthState[i]
            )
            assert (
                pst_json_dict[device_type][i]["admin_mode"]
                == device_under_test.devicesAdminMode[i]
            )
            #                    f"{device_under_test.devicesId}")
            # assert pst_json_dict[device_type][i]["subarray_membership"] == ""
            # assert (
            #     pst_json_dict[device_type][i]["dev_id"]
            #     == device_under_test.devicesId[i]
            # )

            assert (
                pst_json_dict[device_type][i]["data_received"]
                == device_under_test.devicesDataReceived[i]
            )
            assert (
                pst_json_dict[device_type][i]["data_receive_rate"]
                == device_under_test.devicesDataReceiveRate[i]
            )
            assert (
                pst_json_dict[device_type][i]["data_dropped"]
                == device_under_test.devicesDataDropped[i]
            )
            assert (
                pst_json_dict[device_type][i]["data_drop_rate"]
                == device_under_test.devicesDataDropRate[i]
            )
            assert (
                pst_json_dict[device_type][i]["processing_mode"]
                == device_under_test.devicesPstProcessingMode[i]
            )
            assert (
                pst_json_dict[device_type][i]["obs_state"]
                == device_under_test.devicesObsState[i]
            )

    def test_pst_capability_version_is_initialized(self, device_under_test):

        # reach PSTs states on
        probe_poller(device_under_test, "state", DevState.ON, time=10)
        version_id, build_state = read_release_version(release)

        assert device_under_test.versionId == version_id
        assert device_under_test.buildState == build_state

        assert device_under_test.devicesSwVersion == (
            "0.16.0",
            "0.16.0",
        )
        assert device_under_test.devicesHwVersion == ("NA", "NA")
        assert device_under_test.devicesFwVersion == ("NA", "NA")

    def test_attributes_configured_for_events(
        self,  #: TestLowPstCapabilityDevice,
        device_under_test,  #: DeviceTestContext,
        change_event_callbacks,  #: MockTangoEventCallbackGroup,
    ):
        """
        Test that the attributes of the PST Capability devices are
        properly configured to push events from the device.
        """
        probe_poller(device_under_test, "state", DevState.ON, time=10)
        assert device_under_test.devicesDeployed == 2
        evt_id = []
        attrs_list = [
            "isCommunicating",
            "devicesJson",
            "devicesState",
            "devicesHealthState",
            "devicesAdminMode",
            "devicesPstProcessingMode",
            "devicesDataReceived",
            "devicesDataReceiveRate",
            "devicesDataDropped",
            "devicesDataDropRate",
        ]
        for attr_name in attrs_list:
            evt_id.append(
                device_under_test.subscribe_event(
                    attr_name,
                    EventType.CHANGE_EVENT,
                    change_event_callbacks[attr_name],
                )
            )
            # read the current value
            device_attr = getattr(
                device_under_test,
                attr_name,
            )
            if isinstance(device_attr, numpy.ndarray):
                device_attr = device_attr.tolist()
            change_event_callbacks.assert_change_event(
                attr_name,
                attribute_value=device_attr,
                lookahead=len(attrs_list),
            )
        for _id in evt_id:
            device_under_test.unsubscribe_event(_id)

    # this test has been splited from the previous one to avoid
    # subscription check timeout
    def test_attributes_configured_for_monitoring_events(
        self,  #: TestLowPstCapabilityDevice,
        device_under_test,  #: DeviceTestContext,
        change_event_callbacks,  #: MockTangoEventCallbackGroup,
    ):
        """
        Test that the monitoring attributes of the PST Capability devices are
        properly configured to push events from the device.
        """
        probe_poller(device_under_test, "state", DevState.ON, time=10)
        assert device_under_test.devicesDeployed == 2
        evt_id = []
        attrs_list = [
            "devicesDataDropRateAvg",
            "devicesDataDropRateStddev",
            "devicesDataReceiveRateAvg",
            "devicesDataReceiveRateStddev",
            "devicesDataReceiveRateMonitoring",
            "devicesDataDropRateMonitoring",
        ]
        for attr_name in attrs_list:
            evt_id.append(
                device_under_test.subscribe_event(
                    attr_name,
                    EventType.CHANGE_EVENT,
                    change_event_callbacks[attr_name],
                )
            )
            # read the current value
            device_attr = getattr(
                device_under_test,
                attr_name,
            )
            if isinstance(device_attr, numpy.ndarray):
                device_attr = device_attr.tolist()
            change_event_callbacks.assert_change_event(
                attr_name,
                device_attr,
            )
        for _id in evt_id:
            device_under_test.unsubscribe_event(_id)
