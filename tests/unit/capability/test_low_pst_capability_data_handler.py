import json
import logging

from mock import mock
from ska_control_model import ObsState
from ska_csp_lmc_common.utils.monitorlib import meanstd_data

from ska_csp_lmc_low.capability.low_pst_capability_data_handler import (
    PstDataHandler,
)

module_logger = logging.getLogger(__name__)

capability_name = "pst_cap"
list_of_devices = ["low-pst/beam/01", "low-pst/beam/02", "low-pst/beam/03"]

# json_string_monitoring_data == {
#     "datareceiverate": {
#         "low-pst/beam/01": {
#             "value": 0.0,
#             "avg_value": 0.0,
#             "stddev_value": 0.0,
#             "avg_buffer": [],
#             "stddev_buffer": [],
#         },
#         "low-pst/beam/02": {
#             "value": 0.0,
#             "avg_value": 0.0,
#             "stddev_value": 0.0,
#             "avg_buffer": [],
#             "stddev_buffer": [],
#         },
#         "low-pst/beam/03": {
#             "value": 0.0,
#             "avg_value": 0.0,
#             "stddev_value": 0.0,
#             "avg_buffer": [],
#             "stddev_buffer": [],
#         },
#     },
#     "datadroprate": {
#         "low-pst/beam/01": {
#             "value": 0.0,
#             "avg_value": 0.0,
#             "stddev_value": 0.0,
#             "avg_buffer": [],
#             "stddev_buffer": [],
#         },
#         "low-pst/beam/02": {
#             "value": 0.0,
#             "avg_value": 0.0,
#             "stddev_value": 0.0,
#             "avg_buffer": [],
#             "stddev_buffer": [],
#         },
#         "low-pst/beam/03": {
#             "value": 0.0,
#             "avg_value": 0.0,
#             "stddev_value": 0.0,
#             "avg_buffer": [],
#             "stddev_buffer": [],
#         },
#     },
# }


def test_init_pst_data_handler():
    """Test that the json_dict property of the data handler is
    correctly populated at initializiation"""

    dummy_callback = mock.MagicMock()
    cdh = PstDataHandler(capability_name, dummy_callback, module_logger)
    cdh.init(list_of_devices)
    deployed_devices = cdh._json_dict["devices_deployed"]
    assert cdh._json_dict["devices_deployed"] == 3
    assert len(cdh._json_dict["pst_cap"]) == 3
    assert cdh.data_receive_rate_avg == [0.0] * deployed_devices
    assert cdh.data_receive_rate_stddev == [0.0] * deployed_devices
    assert cdh.data_drop_rate_avg == [0.0] * deployed_devices
    assert cdh.data_drop_rate_stddev == [0.0] * deployed_devices

    for i in range(3):
        assert cdh.json_dict["pst_cap"][i]["data_received"] == 0
        assert cdh.json_dict["pst_cap"][i]["data_receive_rate"] == 0.0
        assert cdh.json_dict["pst_cap"][i]["data_dropped"] == 0
        assert cdh.json_dict["pst_cap"][i]["data_drop_rate"] == 0.0
        assert cdh.json_dict["pst_cap"][i]["processing_mode"] == "IDLE"
        assert cdh.json_dict["pst_cap"][i]["obs_state"] == "IDLE"

    json_init = {
        "value": 0.0,
        "avg_value": 0.0,
        "stddev_value": 0.0,
        "avg_buffer": [],
        "stddev_buffer": [],
    }

    monitored_attr = ["datareceiverate", "datadroprate"]
    for attr in monitored_attr:
        assert cdh.avg_monitored_data[attr] == [0.0] * deployed_devices
        assert cdh.stddev_monitored_data[attr] == [0.0] * deployed_devices
        for fqdn in list_of_devices:
            assert cdh.monitoring_data[attr][fqdn].get_avg() == 0.0
            assert cdh.monitoring_data[attr][fqdn].get_stddev() == 0.0
            assert cdh.monitoring_data[attr][fqdn].get_json_data() == json_init

    json_init_data = {}
    tmp_beam_data = {}
    for beam in list_of_devices:
        tmp_beam_data[beam] = json_init

    for attr_name in monitored_attr:
        json_init_data[attr_name] = tmp_beam_data

    assert cdh.json_string_monitoring_data == json_init_data


def test_pst_data_handler_update():
    """Test that the json_dict property of the data handler is
    correctly populated at initializiation"""

    dummy_callback = mock.MagicMock()
    cdh = PstDataHandler(capability_name, dummy_callback, module_logger)
    cdh.init(list_of_devices)

    # update data_received on device 01
    cdh.update("low-pst/beam/01", "datareceived", 12)
    assert cdh.json_dict["pst_cap"][0]["data_received"] == 12
    dummy_callback.assert_any_call("devicesJson", cdh.json_dict)

    # update data_received on device 03
    cdh.update("low-pst/beam/03", "datareceived", 34)
    assert cdh.json_dict["pst_cap"][2]["data_received"] == 34
    dummy_callback.assert_any_call("devicesdatareceived", [12, 0, 34])

    # update data_drop_rate on device 02
    cdh.update("low-pst/beam/02", "datadroprate", 0.7)
    assert cdh.json_dict["pst_cap"][1]["data_drop_rate"] == 0.7
    json_str = json.dumps(cdh.json_string_monitoring_data["datadroprate"])
    dummy_callback.assert_any_call("devicesdatadroprate", [0.0, 0.7, 0.0])
    dummy_callback.assert_any_call("devicesdatadroprateavg", [0.0, 0.7, 0.0])
    dummy_callback.assert_any_call(
        "devicesdatadropratestddev", [0.0, 0.0, 0.0]
    )
    dummy_callback.assert_called_with(
        "devicesdatadropratemonitoring", json_str
    )
    #
    # update data_drop_rate on device 02
    cdh.update("low-pst/beam/02", "datadroprate", 0.6)
    assert cdh.json_dict["pst_cap"][1]["data_drop_rate"] == 0.6
    json_str = json.dumps(cdh.json_string_monitoring_data["datadroprate"])
    dummy_callback.assert_any_call("devicesdatadroprate", [0.0, 0.6, 0.0])
    dummy_callback.assert_any_call("devicesdatadroprateavg", [0.0, 0.65, 0.0])
    # the event for event devicesdatadropratestddev update is not pushed
    # since the attribute does not change value:
    # "devicesdatadropratestddev"= [0.0, 0.0, 0.0]
    dummy_callback.assert_called_with(
        "devicesdatadropratemonitoring", json_str
    )

    # # update processing_mode on device 01
    cdh.update(
        "low-pst/beam/01",
        "pstprocessingmode",
        "VOLTAGE_RECORDER",
    )
    assert cdh.json_dict["pst_cap"][0]["processing_mode"] == "VOLTAGE_RECORDER"
    dummy_callback.assert_any_call("devicesJson", cdh.json_dict)
    dummy_callback.assert_called_with(
        "devicespstprocessingmode",
        ["VOLTAGE_RECORDER", "IDLE", "IDLE"],
    )

    # update obs_state on device 03
    cdh.update("low-pst/beam/03", "obsstate", ObsState.READY)
    assert cdh.json_dict["pst_cap"][2]["obs_state"] == "READY"
    dummy_callback.assert_any_call("devicesJson", cdh.json_dict)
    dummy_callback.assert_called_with(
        "devicesobsstate", ["IDLE", "IDLE", "READY"]
    )


def test_mobile_mean():
    """
    Test that the devicesdatadroprateavg and devicesdatadropratestddev
    attributes contain the expected value
    """
    dummy_callback = mock.MagicMock()
    cdh = PstDataHandler(capability_name, dummy_callback, module_logger)
    cdh.init(list_of_devices)
    import random

    data_drop_rate = []
    for i in range(20):
        fval = random.uniform(0.0, 1.0)
        data_drop_rate.append(fval)
        cdh.update("low-pst/beam/02", "datadroprate", fval)
    avg_buffer = [0.0] * 11
    stddev_buffer = [0.0] * 11
    for i in range(0, 11):
        # split data_drop_rate[i : 10 + i] in 2 steps to avoid E201 lint error
        mean_data = data_drop_rate[i:]
        avg_buffer[i], stddev_buffer[i] = meanstd_data(mean_data[:10])
    assert (
        cdh.monitoring_data["datadroprate"]["low-pst/beam/02"].get_avg_buffer()
        == avg_buffer[1:]
    )
