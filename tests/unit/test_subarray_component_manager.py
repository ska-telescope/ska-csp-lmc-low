import logging

import pytest
from low_properties import test_properties_low_subarray
from mock import Mock, patch

# SKA  imports
from ska_control_model import ObsState
from ska_csp_lmc_common.testing.mocked_connector import MockedConnector
from ska_csp_lmc_common.testing.poller import probe_poller
from ska_csp_lmc_common.testing.test_classes import (
    TestBaseSubarray,
    load_json_file,
)
from ska_tango_base.utils import generate_command_id
from ska_tango_testing.mock import MockCallableGroup
from tango import DevState

logger = logging.getLogger(__name__)


@pytest.fixture()
def callbacks() -> MockCallableGroup:
    """
    Return a dictionary of callbacks with asynchrony support.

    :return: a collections.defaultdict that returns callbacks by name.
    """
    return MockCallableGroup(
        "off_task",
        timeout=5.0,
    )


def test_subarray_cm_init_failed(subarray_component_manager):
    """Test the communicating flag after communication failure."""
    import time

    subarray_component_manager.start_communicating()
    time.sleep(2)
    # wait to establish communication
    assert not subarray_component_manager.is_communicating
    assert not len(subarray_component_manager.online_components)


@patch("ska_csp_lmc_common.component.Connector", new=MockedConnector)
class TestLowSubarrayComponentManager(TestBaseSubarray):
    devices_list = {
        "cbf-subarray": test_properties_low_subarray["CbfSubarray"],
        "pss-subarray": test_properties_low_subarray["PssSubarray"],
        "pst-beam-01": test_properties_low_subarray["PstBeams"][0],
        "pst-beam-02": test_properties_low_subarray["PstBeams"][1],
    }

    def test_subarray_cm_init_callbacks_registered(
        self, subarray_component_manager, subarray_cm_connected
    ):
        """Verify that callbacks are created in initilization's succeed
        method."""
        assert len(subarray_component_manager.online_components) == 2

    def test_subarray_cm_stop_communicating(
        self, subarray_component_manager, subarray_cm_connected
    ):
        subarray_component_manager.stop_communicating()
        probe_poller(
            subarray_component_manager,
            "is_communicating",
            False,
            time=2,
        )

    def test_subarray_cm_components_created(
        self, subarray_component_manager, subarray_cm_connected
    ):
        assert subarray_component_manager.sub_id == 1
        assert len(subarray_component_manager.components) == 4
        assert (
            subarray_component_manager.components[
                test_properties_low_subarray["CbfSubarray"]
            ].name
            == "cbf-subarray-01"  # noqa E503
        )
        assert (
            subarray_component_manager.components[
                test_properties_low_subarray["PssSubarray"]
            ].name
            == "pss-subarray-01"  # noqa E503
        )
        assert (
            subarray_component_manager.components[
                test_properties_low_subarray["PstBeams"][0]
            ].name
            == "pst-beam-01"  # noqa E503
        )
        assert (
            subarray_component_manager.components[
                test_properties_low_subarray["PstBeams"][1]
            ].name
            == "pst-beam-02"  # noqa E503
        )

    def test_scan_invalid_sub_id(
        self, subarray_component_manager, subarray_cm_connected
    ):
        """Test execution of the ScanCommand do() method when the input script
        specifies a wrong subarray ID.

        Expected bahavior: Csp Subarray obsstate: FAULT
        """
        assert subarray_component_manager.is_communicating
        # drive the subarray state model to the desired state
        TestLowSubarrayComponentManager.set_subsystems_and_go_to_state(
            subarray_component_manager, DevState.ON
        )
        TestLowSubarrayComponentManager.set_subsystems_and_go_to_obsstate(
            subarray_component_manager, ObsState.READY
        )
        assert (
            subarray_component_manager.obs_state_model.obs_state
            == ObsState.READY
        )
        scan_dict = load_json_file("Scan_CBF.json")
        scan_dict["common"]["subarray_id"] = 3
        task_callback = Mock()
        task_callback.args = (generate_command_id("Scan"),)
        subarray_component_manager.scan(task_callback, **scan_dict)

    def test_scan_valid_json_format(
        self, subarray_component_manager, subarray_cm_connected
    ):
        """Test execution of the ScanCommand do() method when the input
        argument specifies a right json script.

        Expected bahavior: Csp Subarray obsstate: FAULT
        """
        assert subarray_component_manager.is_communicating
        # drive the subarray state model to the desired state
        TestLowSubarrayComponentManager.set_subsystems_and_go_to_state(
            subarray_component_manager, DevState.ON
        )
        TestLowSubarrayComponentManager.set_subsystems_and_go_to_obsstate(
            subarray_component_manager, ObsState.READY
        )
        scan_dict = load_json_file("Scan_CBF.json")
        task_callback = Mock()
        task_callback.args = (generate_command_id("Scan"),)
        subarray_component_manager.scan(task_callback, **scan_dict)

    def test_split_resources(
        self, subarray_component_manager, subarray_cm_connected
    ):
        config_dict = load_json_file("Configure_CBF_PSS_PST.json")
        # Note: Dec 2022
        # transaction_id entry is added in the subarray device (not the
        # component manager)
        # current version of LOW CBF (0.5.7)does not handle the transation_id
        # so in the low json parser this entry is removed (temporary solution
        # before agreement on Low telescope model
        config_dict["transaction_id"] = "dummy_transaction"
        config_dict = subarray_component_manager._json_configuration_parser(
            config_dict
        ).configure()
        assert config_dict
        # Oct 2022 the config_dict returned does not contain the lowcbf section
        # to agree with CBF (helm chart 0.5.5)
        assert "lowcbf" in config_dict["low-cbf/subarray/01"]
