import copy

import mock
import tango
from low_attribute_factory import LowAttributeDataFactory
from low_mocked_connector import LowMockedConnector

from ska_csp_lmc_low.subarray.low_subarray_component import (
    LowCbfSubarrayComponent,
)


def tango_fail(attr_name):
    tango.Except.throw_exception(
        "API_CantConnectToDevice",
        "This is error message for devfailed",
        " ",
        tango.ErrSeverity.ERR,
    )


class TestLowCbfSubarrayComponent:
    @classmethod
    def set_up(cls):
        component = LowCbfSubarrayComponent("low-cbf/subarray/01")
        component.connect()
        callback = mock.MagicMock()
        return component, callback

    @mock.patch(
        "ska_csp_lmc_common.component.Connector", new=LowMockedConnector
    )
    def test_assigned_resources(self):
        """Test the reading of the CBF sub-system subarray assigned
        resources."""
        component, callback = TestLowCbfSubarrayComponent.set_up()
        assert (
            component.assigned_pst_beams
            == LowAttributeDataFactory.attributes_subarray_default["pstbeams"]
        )
        assert (
            component.assigned_pss_beams
            == LowAttributeDataFactory.attributes_subarray_default["pssbeams"]
        )
        assert (
            component.assigned_station_beams
            == LowAttributeDataFactory.attributes_subarray_default[
                "stationbeams"
            ]  # noqa E503
        )
        assert (
            component.assigned_stations
            == LowAttributeDataFactory.attributes_subarray_default["stations"]
        )

    def test_assigned_resources_with_exception_in_read(self):
        """Test that a failure in reading the assigned resources returns empty
        strings."""
        # Note: here we mock the get_attribute of the common Connector package
        # because the Connector for low is already been mocked before the test
        # class
        with mock.patch(
            "ska_csp_lmc_common.component.Connector.get_attribute"
        ) as mock_get_attribute, mock.patch(
            "ska_csp_lmc_common.component.Connector._get_deviceproxy"
        ) as mock_get_deviceproxy:
            mock_get_deviceproxy.return_value = mock.MagicMock()

            mock_get_attribute.side_effect = tango_fail
            component = LowCbfSubarrayComponent("low-cbf/subarray/01")
            component.connect()
            assigned_stations = component.assigned_stations
            assigned_station_beams = component.assigned_station_beams
            assigned_pss_beams = component.assigned_pss_beams
            assigned_pst_beams = component.assigned_pst_beams
            assert not assigned_stations
            assert not assigned_station_beams
            assert not assigned_pss_beams
            assert not assigned_pst_beams

    def test_update_pst_json_configuration(self):
        """
        Test the update of the timing beam section of cbf configuration
        """

        def test_execution():
            component, _ = TestLowCbfSubarrayComponent.set_up()
            new_dict = component.update_pst_json_configuration(
                original_dict, updated_info
            )
            correct_dict = copy.deepcopy(original_dict)
            correct_dict["lowcbf"]["timing_beams"]["beams"][0][
                "destinations"
            ] = destination_section["destinations"]
            assert new_dict == correct_dict

        updated_info = {
            "low-pst/beam/01": {
                "num_channel_blocks": 1,
                "channel_blocks": [
                    {
                        "destination_host": "127.0.0.1",
                        "destination_port": 32080,
                        "start_pst_channel": 0,
                        "num_pst_channels": 432,
                    }
                ],
            }
        }
        destination_section = {
            "destinations": [
                {
                    "data_host": "127.0.0.1",
                    "data_port": 32080,
                    "start_channel": 0,
                    "num_channels": 432,
                }
            ],
        }

        original_dict = {
            "interface": "https://schema.skao.int/ska-low-csp-configure/3.0",
            "subarray": {"subarray_name": "science period 23"},
            "common": {
                "config_id": "sbi-mvp01-20200325-00001-science_A",
                "subarray_id": 1,
                "frequency_band": "low",
                "eb_id": "eb-x449-20231105-34696",
            },
            "lowcbf": {
                "stations": {
                    "stns": [[1, 1], [2, 1], [3, 1], [4, 1], [5, 1], [6, 1]],
                    "stn_beams": [
                        {
                            "beam_id": 1,
                            "freq_ids": [400],
                            "delay_poly": "tango://delays.skao.int/...",
                        }
                    ],
                },
                "vis": {
                    "fsp": {"firmware": "vis", "fsp_ids": [1]},
                    "stn_beams": [
                        {
                            "stn_beam_id": 1,
                            "host": [[0, "192.168.0.1"]],
                            "port": [[0, 9000, 1]],
                            "mac": [[0, "02-03-04-0a-0b-0c"]],
                            "integration_ms": 849,
                        }
                    ],
                },
                "timing_beams": {
                    "fsp": {"firmware": "pst", "fsp_ids": [2]},
                    "beams": [
                        {
                            "pst_beam_id": 1,
                            "stn_beam_id": 1,
                            "stn_weights": [0.9, 1.0, 1.0, 1.0, 0.9, 1.0],
                            "delay_poly": "tango://delays.skao.int/...",
                            "jones": "tango://jones.skao.int/low/stn-beam/1",
                        }
                    ],
                },
            },
        }

        test_execution()
