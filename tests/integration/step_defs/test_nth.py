"""Your fixtures to use the test harness.

(Probably defined in a ``conftest.py`` file)
"""

import json
import logging

import pytest
from pytest_bdd import given, parsers, scenarios, then, when
from test_real import (  # noqa: F401
    all_beams,
    all_capabilities,
    all_controllers,
    all_subarrays,
    fresh_new_subsystems_with_pst_beam,
)

from ska_csp_lmc_low.testing.nth.csp_facade import CSPFacade
from ska_csp_lmc_low.testing.nth.wrappers import (
    CBFWrapper,
    CSPWrapper,
    LMCWrapper,
    PSSWrapper,
    PSTWrapper,
)
from ska_csp_lmc_low.testing.test_bdd_utils import LowTestDevice  # noqa: F401

module_logger = logging.getLogger()

scenarios("../features/new_ith.feature")

num_subarrays = 4
num_beams = 1

# -----------------------------------------------------------
# Set up the test harness

# NOTE: now we are not using the BUILDER and the FACTORY that can
# be used in a future development when we will want to switch between
# emulators and real devices


@pytest.fixture
def csp_wrapper():
    csp_wrap = CSPWrapper()
    csp_wrap.set_up(
        lmc=LMCWrapper(num_subarrays, logger=module_logger),
        cbf=CBFWrapper(num_subarrays, logger=module_logger),
        pss=PSSWrapper(logger=module_logger),
        pst=PSTWrapper(num_beams, logger=module_logger),
    )
    module_logger.info("Setting up CSP Wrapper")
    yield csp_wrap


# -----------------------------------------------------------
# Facade to access the devices


@pytest.fixture
def csp(csp_wrapper: CSPWrapper):
    """Create a facade to TMC central node and all its operations."""
    csplmc_facade = CSPFacade(
        csp_wrapper, num_subarrays, num_beams, logger=module_logger
    )
    yield csplmc_facade
    csplmc_facade.teardown()


# -----------------------------------------------------------
# Utilities


def select_device(csp, device_name):
    """
    Select a device from the list of all devices in the system.

    The function first creates a dictionary with all the devices in the system
    and then check if the device name is in the dictionary. If it is, it
    returns the corresponding device proxy. If it is not, it prints a warning
    and raises an assertion error.

    :param csp: CSPFacade
    :param device_name: The name of the device to select.
    :return: The device proxy corresponding to the device name."""

    proxies = {}

    proxies.update(
        {
            f"CspSubarray0{i+1}": csp.lmc.subarrays[i]
            for i in range(num_subarrays)
        }
    )
    proxies.update(
        {
            f"CbfSubarray0{i+1}": csp.cbf.subarrays[i]
            for i in range(num_subarrays)
        }
    )
    proxies.update(
        {f"PstBeam0{i+1}": csp.pst.beams[i] for i in range(num_beams)}
    )
    proxies["CspController"] = csp.lmc.controller
    proxies["CbfController"] = csp.cbf.controller
    if device_name in proxies:
        return proxies[device_name]

    module_logger.error(
        f"The device {device_name} is not in the list of all devices"
    )
    assert False


# ------------------------------------------------------------
# check on attributes


@given(parsers.parse("{device_name} {attribute} is(nth) {value}"))
@then(parsers.parse("{device_name} {attribute} is(nth) {value}"))
@when(parsers.parse("{device_name} {attribute} is(nth) {value}"))
def attribute_of_device(csp, device_name, attribute, value):
    # add logs temporarily to resolve the issue with init
    module_logger.info(
        f"Checking {attribute} on {device_name} in {value} (nth)"
    )

    proxy = select_device(csp, device_name)
    value = csp.translate_attributes_in_enum(attribute, value)
    csp.check_attribute_on_device(attribute, value, proxy, new_tracer=True)


# /********* COMMANDS **********/


@when(
    parsers.parse(
        "{command_name} command (nth)is issued WITH SUCCESS on {device_name}"
    )
)
def command_success(csp, command_name, device_name):
    proxy = select_device(csp, device_name)
    csp.perform_action_success(command_name, proxy)


@given(
    parsers.parse(
        'input file for {command_name} set: "{filename}" ({comment})'
    )
)
def define_command_inputs(csp, command_name, filename, comment):
    module_logger.info(
        f"Setting command input for {command_name}: "
        "file {filename} ({comment})"
    )
    with open(f"tests/test_data/{filename}", "r") as file:
        command_input = json.dumps(json.load(file))

    csp.command_inputs[command_name] = command_input


# ********* initial conditions **********


@given("CSP is in initial status (nth)")
def initial_status(csp):

    module_logger.info("Checking CSP initial status as expected")

    if csp.is_in_initial_status():
        module_logger.info("CSP already in its initial status")
        assert True
    else:
        module_logger.info(
            "CSP was not in its initial status, " "resetting all devices"
        )
        csp.hard_reset()


@given(parsers.parse("CspSubarray0{id} is in initial status"))
def ensure_subarray_in_initial_status(csp, id):
    """
    Ensures the subarray is in its initial status. If it is not,
    then it will reset the LMC to its initial status.
    """
    if not csp.lmc.is_subarray_in_initial_status(int(id)):
        csp.hard_reset()


@given("CspController has turned ON PstBeam01")
def ensure_pst_beam_on(csp):

    csp.turn_on_pst_beam(beam_id=1)


@given(
    parsers.parse(
        "CspSubarray{sub_id} has assigned PstBeam01 and ObsState IDLE"
    )
)
def ensure_subarray_assigned_with_beam(csp, sub_id):

    csp.assign_pst_beam_to_subarray(sub_id=sub_id, beam_id=1)


@given(parsers.parse("CspSubarray{sub_id} has been configured with PstBeam01"))
def ensure_subarray_configured_with_beam(csp, sub_id):
    csp.configure_with_pst_beam(sub_id=sub_id, beam_id=1)


@given(parsers.parse("CspSubarray{sub_id} is scanning with PstBeam01"))
def ensure_subarray_is_scanning(csp, sub_id):
    csp.start_scanning(sub_id=sub_id, beam_id=1)


@given(parsers.parse("CspSubarray{sub_id} has ended a scan with PstBeam01"))
def ensure_subarray_has_ended_scan(csp, sub_id):
    csp.end_scan(sub_id=sub_id, beam_id=1)


@given(
    parsers.parse(
        "CspSubarray{sub_id} has been deconfigured "
        "{when_deconfigured} with PstBeam01"
    )
)
def ensure_subarray_has_been_deconfigured(csp, sub_id, when_deconfigured):
    csp.deconfigure_with_pst_beam(
        sub_id=sub_id, beam_id=1, when_deconfigured=when_deconfigured
    )
