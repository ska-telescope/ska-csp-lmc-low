import datetime
import json
import logging
import os
import time

import pytest
import tango
from pytest_bdd import given, parsers, scenarios, then, when
from ska_control_model import ObsState
from ska_csp_lmc_common.testing.poller import probe_poller
from ska_csp_lmc_common.testing.test_bdd_utils import command_invalid_input

from ska_csp_lmc_low.testing.test_bdd_utils import (
    LowTestDevice,
    fresh_all_new_subsystems,
    is_pst_acquiring_data,
)

# from ska_control_model import AdminMode, SimulationMode


module_logger = logging.getLogger(__name__)

#  -------------- SCENARIOS --------------

scenarios("../features/controller_basic_commands.feature")
scenarios("../features/controller_attributes.feature")
scenarios("../features/subarray_assign_resources.feature")
scenarios("../features/subarray_initialization.feature")
scenarios("../features/subarray_attributes.feature")
scenarios("../features/subarray_on_off.feature")
scenarios("../features/all_commands.feature")
scenarios("../features/subarray_configure_and_scan.feature")
scenarios("../features/subarray_scan.feature")
scenarios("../features/pst_beams.feature")
scenarios("../features/psi_low.feature")
scenarios("../features/archive_subscription.feature")
scenarios("../features/reject_wrong_inputs_with_pst.feature")
scenarios("../features/reject_wrong_inputs.feature")
# scenarios("../features/capability_attributes.feature")
scenarios("../features/reconfigure.feature")
scenarios("../features/pst_capability.feature")
scenarios("../features/subarray_abort.feature")


#  -------------- MARKERS --------------
def pytest_configure(config):
    config.addinivalue_line("markers", "thisone", "low")


# -------------- FIXTURES --------------

temp_dev_info = None

repository = "low"
test_context = "real"

list_of_controller_names = [
    "LowCbfController",
    "LowCspController",
]

list_of_subarray_names = [
    "LowCbfSubarray01",
    "LowCbfSubarray02",
    "LowCbfSubarray03",
    "LowCbfSubarray04",
    "LowCspSubarray01",
    "LowCspSubarray02",
    "LowCspSubarray03",
    "LowCspSubarray04",
]


list_of_capabilities_names = ["LowCspCapabilityPst", "LowCspCapabilityFsp"]

list_of_beam_names = [
    "LowPstBeam01",
    # "LowPstBeam02",
]

are_beams_available = False


def are_beams_deployed():
    """Check in the database if the beam devices have been deployed
    :return are_beams_available: bool flag.
    True if beam have been deployed
    """
    global are_beams_available
    low_beam_name = "low-pst/beam/"

    if not are_beams_available:
        db = tango.Database()
        is_beam_deployed = False
        for beam_device in list_of_beam_names:
            if "beam" in beam_device:
                beam_name = low_beam_name + beam_device[-2:]
                is_beam_deployed = bool(
                    db.get_device_exported(beam_name).value_string
                )
                if is_beam_deployed:
                    break
        are_beams_available = is_beam_deployed
    return are_beams_available


@pytest.fixture(scope="session")
def all_controllers():
    controllers = dict()
    for name in list_of_controller_names:
        idx = name
        if name[0:3] in ["Sim", "Mid", "Low"]:
            idx = name[3:]
        module_logger.info(f"Creating proxy for {idx}")
        controllers[idx] = LowTestDevice(name, repository, test_context)
    return controllers


@pytest.fixture(scope="session")
def all_subarrays():
    subarrays = dict()
    for name in list_of_subarray_names:
        idx = name
        if name[0:3] in ["Sim", "Mid", "Low"]:
            idx = name[3:]
        module_logger.info(f"Creating proxy for {idx}")
        subarrays[idx] = LowTestDevice(name, repository, test_context)
    return subarrays


@pytest.fixture(scope="session")
def all_capabilities():
    capabilities = {}
    for name in list_of_capabilities_names:
        idx = name
        if name[0:3] in ["Sim", "Mid", "Low"]:
            idx = name[3:]
        module_logger.info(f"Creating proxy for {idx}")
        capabilities[idx] = LowTestDevice(name, repository, test_context)
    return capabilities


@pytest.fixture(scope="session")
def all_beams():
    beams = dict()
    global are_beams_available
    for name in list_of_beam_names:
        idx = name
        if name[0:3] in ["Sim", "Mid", "Low"]:
            idx = name[3:]
        module_logger.info(f"Creating proxy for {idx} name: {name}")
        try:
            beams[idx] = LowTestDevice(name, repository, test_context)
        except Exception as ext:
            module_logger.info(f"Beam DeviceProxy not found: {ext}")
    if not beams:
        are_beams_available = False
    else:
        are_beams_available = True
    return beams


def skip_test(reason):
    pytest.skip(f"skip test since {reason}", allow_module_level=True)


@given("All subsystems are fresh initialized")
def fresh_new_subsystems(
    all_capabilities, all_controllers, all_subarrays, all_beams
):
    """Initialize all subsystems of the deployment."""

    for device in all_capabilities.values():
        # TODO: reinizialize capabilities
        module_logger.info(f"{device} is not reinitialized")

    fresh_all_new_subsystems(all_controllers, all_subarrays, all_beams)


@given(
    parsers.parse("Subarray{number} is fresh initialized without PST beams")
)
def fresh_new_subarray_without_beams(number, all_subarrays):
    """
    skip test if beam have been deployed
    """
    if are_beams_deployed():
        skip_test("pst have been deployed")
    else:
        fresh_new_subarray(number, all_subarrays)


@given(parsers.parse("Subarray{number} is fresh initialized with PST beams"))
def fresh_new_subarray_with_beam(number, all_subarrays):
    """
    skip test if no beam have been deployed
    """
    if not are_beams_deployed():
        skip_test("no pst have been deployed")
    else:
        fresh_new_subarray(number, all_subarrays)


@given(parsers.parse("Subarray{number} is fresh initialized"))
def fresh_new_subarray(number, all_subarrays):
    """
    Re-initialize a CSP Sub-array.

    The CSP Subarray with the number specified is re-initialized.
    Its adminmode is set to DISABLE, all subordinate CSP
    sub-systems sub-arrays are re-initialized and then the CSP Subarray
    is set to online.
    """
    devices_to_reinit = []
    csp_subarray_name = f"CspSubarray{number}"
    csp_subarray_device = None

    module_logger.debug(f"Fresh subarray {number}")
    for device in all_subarrays.values():
        device_name = device.return_device_name()
        if number not in device_name:
            continue
        devices_to_reinit.append(device)
        module_logger.debug(f"devices to reinitialize: {device_name}")
        if device_name == csp_subarray_name:
            module_logger.info(f"{time.asctime()} - Disabling {device_name}")
            device.disable_device(True)
            device.get_attribute("iscommunicating", "False")
            time.sleep(1)
            device.get_attribute("State", "DISABLE")
            csp_subarray_device = device
    # re-init all the subordinate devices and the CSP subarray
    for device in devices_to_reinit:
        device.initialize_device()
    # ping all the devices to see if are on-line
    for device in devices_to_reinit:
        device.ping_device()
    time.sleep(1)
    module_logger.info(f"{time.asctime()} Re-enabling {csp_subarray_name}")
    # Re-enable the CSP Subarray and check for the status of all the
    # subarrays devices
    if csp_subarray_device:
        csp_subarray_device.disable_device(False)
        csp_subarray_device.get_attribute("isCommunicating", "True")
    for device in devices_to_reinit:
        device_name = device.return_device_name()
        device.check_subarray_init_state()


@given("All subsystems are fresh initialized without PST beams")
def fresh_new_subsystems_without_pst_beam(
    all_capabilities, all_controllers, all_subarrays, all_beams
):
    if are_beams_deployed():
        skip_test("no pst have been deployed")
    else:
        fresh_new_subsystems(
            all_capabilities, all_controllers, all_subarrays, all_beams
        )


@given("All subsystems are fresh initialized with PST beams")
def fresh_new_subsystems_with_pst_beam(
    all_capabilities, all_controllers, all_subarrays, all_beams
):
    if not are_beams_deployed():
        skip_test("pst have been deployed")
    else:
        fresh_new_subsystems(
            all_capabilities, all_controllers, all_subarrays, all_beams
        )


@given("CbfProcessor is subscribed by the CbfAllocator")
def subscribe_to_allocator():

    processor1 = tango.DeviceProxy("low-cbf/processor/0.0.0")

    processor1.serialnumber = "XFL14SLO1LIF"
    processor1.subscribetoallocator("low-cbf/allocator/0")
    processor1.register()

    processor2 = tango.DeviceProxy("low-cbf/processor/0.0.1")
    processor2.serialnumber = "XFL1HOOQ1Y44"
    processor2.subscribetoallocator("low-cbf/allocator/0")
    processor2.register()


key_to_remove = {
    "AssignResources": {"with PST": "lowcbf", "without PST": "lowcbf"},
    "Configure": {
        "with PST": ["['pst']", "beams"],
        "without PST": ['["lowcbf"]["vis"]', "fsp"],
    },
}


@then(parsers.parse("{device_name} {attribute} acquiring data on {beam_name}"))
def check_pst_is_acquiring_data(
    device_name, attribute, beam_name, all_capabilities, all_beams
):
    """Check that both the beam attribute (i.e. dataReceived) and the
    capability element of the list (i.e devicesDataReceived).
    right now this step is done for the following attributes:
    [devicesDataDropRate, devicesDataReceived,
    devicesDataReceiveRate, devicesDataDropped]
    """
    return_value = is_pst_acquiring_data(
        device_name, attribute, beam_name, all_capabilities, all_beams
    )

    return return_value


@when(
    parsers.parse(
        "{command_name} is issued on CspSubarray01"
        " {is_pst_present} (invalid input)"
    )
)
def low_command_invalid_input(command_name, is_pst_present):
    """
    Test that command is rejected after telmodel validation
    if input is invalid
    It uses a common function, while the key to remove
    to make invalid input are specialized.
    """
    command_invalid_input(
        command_name, is_pst_present, key_to_remove, telescope="low"
    )


@given(parsers.parse("an IDLE CspSubarray01 with a PST beam assigned"))
def idle_subarray_pst_beam():
    csp_sub = tango.DeviceProxy("low-csp/subarray/01")
    pst_beam = tango.DeviceProxy("low-pst/beam/01")
    pst_beam.on()
    module_logger.info("Checking state of PST-Beam: ON")
    probe_poller(pst_beam, "state", tango.DevState.ON, time=10)
    with open("tests/test_data/AssignResources_CBF_PSS_PST.json") as file:
        csp_sub.assignresources(file.read())
    module_logger.info("Checking obsstate of CSP-Subarray: IDLE")
    probe_poller(csp_sub, "obsstate", ObsState.IDLE, time=10)
    module_logger.info("Checking obsstate of PST-Beam: IDLE")
    probe_poller(pst_beam, "obsstate", ObsState.IDLE, time=10)


# ----------------------
# PSI Low specific steps

SUBARRAY_ID = 1
SUBSTATION_ID = 1
BEAM_ID = 1
FSP_ID = 1  # random.randint(1, 4)  # arbitrary FSP

pst_svr_ip = "192.168.0.101"
aj_pst_port = "28/0"
init_epoch = 13180
pst_beam_id = 15
SDP_IP = "192.168.2.2"

station_ids = [18, 34, 21, 42, 54, 72]
n_stations = len(station_ids)  # pylint: disable=invalid-name
STATIONS = [[station, SUBSTATION_ID] for station in station_ids]

n_channels = 1  # pylint: disable=invalid-name
CHANNELS = list(range(64, 64 + n_channels))
sps_spead_ver = 2

spead = {"in": 0, "out": 0}


@given("All subsystems are fresh deployed on PSI Low")
def fresh_deployments_on_psi(all_controllers, all_subarrays, all_beams):
    all_subsystems = {**all_controllers, **all_subarrays, **all_beams}
    for device in all_subsystems.values():
        device.ping_device()
        device.get_attribute("State", "DISABLE")
        device.get_attribute("AdminMode", "OFFLINE")


@when(parsers.parse("CspController adminmode set to {adminmode}"))
def set_adminmode_on_csp(adminmode, all_controllers):
    for device in all_controllers.values():
        if device.return_device_name() == "CspController":
            if adminmode == "ONLINE":
                device.disable_device(False)
            elif adminmode == "OFFLINE":
                device.disable_device(True)
            else:
                raise ValueError(f"impossible to set adminmode to {adminmode}")


def identify_fsp(fsp_id):
    cbf_p1 = tango.DeviceProxy("low-cbf/processor/0.0.0")
    cbf_p2 = tango.DeviceProxy("low-cbf/processor/0.0.1")
    cbf_a = tango.DeviceProxy("low-cbf/allocator/0")
    fsps_dict = json.loads(cbf_a.fsps)

    if fsps_dict[fsp_id][0] == cbf_p1.serialnumber:
        processor = cbf_p1
    elif fsps_dict[fsp_id][0] == cbf_p2.serialnumber:
        processor = cbf_p2
    else:
        assert False, "impossible to identify FSPs"

    return processor


def check_processor_status(fsp_id, status, personality=None):
    """
    This function is utility to check the status of a cbf processor
    i.e. ready= true/false
    """
    global spead

    processor = identify_fsp(fsp_id)

    start_time = time.time()
    timeout = 60
    elapsed_time = 0
    res = False
    while elapsed_time < timeout:
        elapsed_time = time.time() - start_time
        try:
            stats_mode_dict = json.loads(processor.stats_mode)
        except tango.DevFailed:
            module_logger.warning("Device is busy, probably configuring")
            continue

        if stats_mode_dict["ready"] == status:
            res = True
            break

        time.sleep(1)

    if elapsed_time > timeout:
        module_logger.error(
            f"FSP 'stats_mode' is not {status}."
            f"It is {stats_mode_dict['ready']}"
        )

    if res and personality:
        if personality == "visibilities":
            assert "vis" in stats_mode_dict["firmware"]
            if "spead_pkts_in" in processor.stats_io:
                spead["in"] = json.loads(processor.stats_io)["spead_pkts_in"]
            if "vis_pkts_out" in processor.stats_io:
                spead["out"] = json.loads(processor.stats_io)["vis_pkts_out"]
            module_logger.info(f"{spead=}")

        elif personality == "pst-beam":
            assert "pst" in stats_mode_dict["firmware"]
        else:
            raise ValueError(f"{personality} is not a real personality")

    return res


@then(parsers.parse("{fsp} is configured for {personality}"))
def check_processor_is_configured(fsp, personality):
    """
    This step checks if the processors are configured with real hardware.
    Since the obsstate transition happens at the beginning of command
    and not at the completion,
    a specific attribute of the processors need to be checked (Stats mode)
    """
    module_logger.info(f"Checking that {fsp} is configured")
    res = check_processor_status(f"fsp_{fsp[-2:]}", True, personality)
    if not res:
        module_logger.error("Configure failed, timeout expired")
    assert res


@then(parsers.parse("{fsp} is de-configured"))
def check_processor_is_deconfigured(fsp):
    """
    This step checks if the processors are configured with real hardware.
    Since the obsstate transition happens at the beginning of command
    and not at the completion,
    a specific attribute of the processors need to be checked (Stats mode)
    """

    module_logger.info(f"Checking {fsp} to be de-configured")
    res = check_processor_status(f"fsp_{fsp[-2:]}", False)
    if not res:
        module_logger.error("De-configure failed, timeout expired")
    assert res


@given("CNIC board is initialized")
def initialize_cnic():
    module_logger.info("Initializing CNIC")
    cnic = tango.DeviceProxy("low-cbf/cnic/1")
    cnic.set_timeout_millis(90_000)
    CNIC_FW_VERSION = "0.1.11"
    CNIC_FW_SOURCE = "nexus"  # "nexus" or "gitlab"

    CNIC_FORCE_DOWNLOAD = False  # Force CNICs to re-download firmware.

    if not CNIC_FORCE_DOWNLOAD and cnic.activePersonality == "cnic":
        module_logger.info(
            f"Already running CNIC firmware on {cnic.name()}, "
            "skipping download!"
        )
        cnic.StopSourceDelays()
        cnic.CallMethod(json.dumps({"method": "stop_receive"}))
        cnic.CallMethod(json.dumps({"method": "reset"}))
    else:
        cnic.SelectPersonality(
            json.dumps(
                {
                    "version": CNIC_FW_VERSION,
                    "source": CNIC_FW_SOURCE,
                    "memory": "4095Ms:4095Ms:4095Mi:4095Mi",
                }
            )
        )


@given(parsers.parse("P4 routing configured for CNIC and {fsp}"))
def p4_routing(fsp):
    module_logger.info(f"Configuring P4 routing for CNIC and {fsp}")

    fsp_id = f"fsp_{fsp[-2:]}"
    processor = identify_fsp(fsp_id)
    cnic = tango.DeviceProxy("low-cbf/cnic/1")

    serial_port = {
        "XFL1TJCHM3ON": "7",
        "XFL1VCYSXCL0": "9",
        "XFL10NIYKVEU": "11",
        "XFL1XCRTUC22": "13",
        "XFL1E35JVJTQ": "15",
        "XFL1RCFEG244": "17",
        "XFL14SLO1LIF": "19",
        "XFL1DKXBEVG2": "21",
        "XFL1HOOQ1Y44": "23",
        "XFL1LHN4TXO2": "25",
    }

    correlator_port = f"{serial_port[processor.serialnumber]}/0"
    cnic_port = f"{serial_port[cnic.serialnumber]}/0"
    module_logger.info(f"CNIC on port {cnic_port}")
    module_logger.info(f"Correlator on port {correlator_port}")

    db_host = "tango-databaseds.ska-low-cbf-conn:10000"
    connector = tango.DeviceProxy(f"{db_host}/low-cbf/connector/0")

    module_logger.info(
        "Adding routes from input simulator (CNIC) to correlator"
    )
    spead_cfg = {
        "spead": [
            {
                "src": {
                    "frequency": channel,
                    "beam": BEAM_ID,
                    "sub_array": SUBARRAY_ID,
                },
                "dst": {"port": correlator_port},
            }
            for channel in CHANNELS
        ]
    }
    connector.UpdateSpeadUnicastEntry(json.dumps(spead_cfg))

    module_logger.info("Adding route from correlator to output CNIC")
    command_sdp_ip = {
        "sdp_ip": [{"src": {"ip": SDP_IP}, "dst": {"port": cnic_port}}]
    }

    connector.UpdateSdpIpEntry(json.dumps(command_sdp_ip))


def check_increase_packets(way, processor):
    # way can be "in" or "out"
    module_logger.info(f"Checking that spead_{way} are increasing")

    global spead

    initial_spead = spead[way]

    if way == "out":
        key = "vis_pkts_out"
    elif way == "in":
        key = "spead_pkts_in"
    else:
        raise ValueError("Unexisting way!")

    start_time = time.time()
    timeout = 60
    elapsed_time = 0
    while elapsed_time < timeout:
        elapsed_time = time.time() - start_time
        try:
            current_spead = json.loads(processor.stats_io)[key]
            if current_spead > initial_spead:
                spead[way] = current_spead
                module_logger.info(f"{spead=}")
                return True
        except KeyError:
            module_logger.warning(
                f"{key} still not present in processor.stats_io"
            )
        time.sleep(1)

    if elapsed_time > timeout:
        module_logger.error(
            f"FSP 'stats_io' doesn't show an increase in '{key}'"
        )

    return False


@then(parsers.parse("{fsp} starts a scan"))
@then(parsers.parse("{fsp} creates visibilities"))
def fsp_start_scan(fsp):
    fsp_id = f"fsp_{fsp[-2:]}"
    processor = identify_fsp(fsp_id)
    res = check_increase_packets("out", processor)
    if not res:
        module_logger.error(f"Failing in starting the scan on {fsp}")
    assert res


@then(parsers.parse("{fsp} sees data incoming from CNIC"))
def fsp_income_data(fsp):
    module_logger.info(f"Checking data incoming for {fsp}")
    fsp_id = f"fsp_{fsp[-2:]}"
    processor = identify_fsp(fsp_id)
    res = check_increase_packets("in", processor)
    if not res:
        module_logger.error(f"Data are incoming from CNIC to {fsp}")
    assert res


@given("CNIC is configured for receiving data")
def configure_cnic():
    print("Preparing to receive with CNIC")
    cnic = tango.DeviceProxy("low-cbf/cnic/1")
    cor_fw = "vis:0.0.6-dev.636d2e14:gitlab"  # 2-nov
    cnic.hbm_pktcontroller__duplex = True
    cnic.enable_vd = False

    file_prefix = datetime.datetime.now().strftime("%Y%m%d_%H%M%S_")
    PCAP_FILENAME = (
        file_prefix
        + f"beam-{cor_fw}.cnic-{cnic.fw_version.replace('.', '_')}.sdp.pcap"
    )
    PCAP_FILE_RX = os.path.join("/", "test-data", "pcap", PCAP_FILENAME)

    N_VISIBILITIES = 5
    OUTPUT_PACKET_COUNT = 144 * len(CHANNELS) * (N_VISIBILITIES + 1)
    OUTPUT_PACKET_SIZE = 200

    RX_PARAMS = {
        "method": "receive_pcap",
        "arguments": {
            "out_filename": PCAP_FILE_RX,
            "packet_size": OUTPUT_PACKET_SIZE,
            "n_packets": OUTPUT_PACKET_COUNT,
        },
    }

    print(f"Configuring {cnic.name()} for Rx\n")
    cnic.CallMethod(json.dumps(RX_PARAMS))

    print("Configuring VD")
    vd_config = {
        "sps_packet_version": sps_spead_ver,
        "stream_configs": [  # config is a list of dicts - one per SPEAD stream
            {
                "scan": 0,
                "subarray": SUBARRAY_ID,
                "station": station,
                "substation": 1,
                "frequency": channel,
                "beam": BEAM_ID,
                "sources": {
                    "x": [
                        # {"tone": True, "fine_frequency": 16, "scale": scale},
                        {"tone": False, "seed": 1000, "scale": 4000},
                    ],
                    "y": [
                        # {"tone": True, "fine_frequency": 16, "scale": scale},
                        {"tone": False, "seed": 1000, "scale": 4000},
                    ],
                },
            }
            for station, substation in STATIONS
            for channel in CHANNELS
        ],
    }
    time.sleep(5)
    cnic.ConfigureVirtualDigitiser(json.dumps(vd_config))

    cnic.vd__enable_vd_hbm_path = False
    cnic.vd__time_between_channel_bursts = 2_211_840
    cnic.vd__time_between_packets = 505
    cnic.vd__enable_vd_hbm_path = True
    time.sleep(0.2)
    cnic.enable_vd = True


@given(parsers.parse("Correlator is reset in {fsp}"))
def reset_correlator(fsp):
    module_logger.info(f"Resetting Correlator for {fsp}")
    fsp_id = f"fsp_{fsp[-2:]}"
    processor = identify_fsp(fsp_id)
    processor.DebugRegWrite(
        json.dumps({"name": "corr_ct1.full_reset", "offset": 0, "value": 1})
    )
    processor.DebugRegWrite(
        json.dumps({"name": "corr_ct1.full_reset", "offset": 0, "value": 0})
    )
