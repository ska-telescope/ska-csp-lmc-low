Feature: Csp-Lmc Low Controller attributes
    As the Csp-Lmc controller for Low telescope
    I want to read basic attributes

    @pipeline @XTP-35917
    Scenario: Csp controller reports SimulationMode
        Given All subsystems are fresh initialized
        And CspController isCommunicating is True
        And CspController SimulationMode is FALSE

        When CspController set SimulationMode to TRUE 
        Then CspController SimulationMode is TRUE
        When CspController set SimulationMode to FALSE 
        Then CspController SimulationMode is FALSE

    @pipeline @XTP-35916
    Scenario: Csp controller reports HealthState
        Given All subsystems are fresh initialized
        And CspController isCommunicating is True
        # Current implementation sets HealtState to UNKNOWN if no connector 
        # or processor could be configured
        And CspController set adminmode to ONLINE
        Then CspController HealthState is UNKNOWN