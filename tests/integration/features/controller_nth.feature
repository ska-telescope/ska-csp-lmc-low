Feature: CSP.LMC Controller operations
    
    # CSP initial status (no hardware involved)
    # (after initialization and Adminmode setted to ONLINE on CSP.LMC Controller)
    #
    #                       | DevState | HealthState | AdminMode | ObsState | AssignedTimingBeamIDs (?)
    # CSP.LMC Controller        ON         UNKNOWN      ONLINE       --             --
    # CSP.LMC Subarray          ON         UNKNOWN      ONLINE      EMPTY           []
    # CBF Controller            ON         UNKNOWN      ONLINE       --             --
    # CBF Subarray              ON         UNKNOWN      ONLINE      EMPTY           --
    # PST Beam                  OFF           OK        ONLINE      IDLE            --
       
    Scenario: CSP.LMC turns ON Pst Beam 01
        
        Given CSP is in initial status (nth)        
        When On Command (nth)is issued WITH SUCCESS on CspController
        Then PstBeam01 state is(nth) ON
    
    Scenario: CSP.LMC turns OFF Pst Beam 01
        
        Given CSP has turned ON Pstbeam01        
        When Off Command (nth)is issued WITH SUCCESS on CspController
        Then PstBeam01 state is(nth) OFF
    