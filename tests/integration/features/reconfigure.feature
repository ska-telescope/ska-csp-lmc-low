# ------------- Reconfigure subarray -------------------
Feature: Csp-Lmc Low Controller reconfigure
    As the Csp-Lmc controller for Low telescope
    I want to reconfigure subarray

    @pipeline
    Background:
        Given All subsystems are fresh initialized with PST beams

    @pipeline @XTP-58528
    Scenario: Csp Subarray obsMode when configuring resources and scanning

        When On Command is issued on CspController with argument []
        Then CspController longRunningCommandAttributes is (0, 'COMPLETED')
        And CspSubarray01 state is ON
        And CbfSubarray01 state is ON
        And CspSubarray01 ObsState is EMPTY
        And CbfSubarray01 ObsState is EMPTY
        And PstBeam01 state is ON
        And CspController CspCbfState is ON
        And CspController state is ON
        And CspSubarray01 cbfSubarrayState is ON
        And CspSubarray01 cbfSubarrayObsState is EMPTY
        And CspSubarray01 obsMode is (ObsMode.IDLE,) 

        When AssignResources Command is issued on CspSubarray01 (argument from file: AssignResources_CBF_PST.json)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED')
        And CspSubarray01 commandResult is ('assignresources', '0')
        And CspSubarray01 cbfSubarrayObsState is IDLE
        And CspSubarray01 obsState is IDLE
        And CspSubarray01 assignedTimingBeamIDs is [1]
        And PstBeam01 obsState is IDLE
        And CspSubarray01 obsMode is (ObsMode.IDLE,) 

        When Configure Command is issued on CspSubarray01 (argument from file: Configure_CBF_PST.json)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED') 
        And CspSubarray01 commandResult is ('configure', '0')
        And PstBeam01 obsState is READY
        And CspSubarray01 cbfSubarrayObsState is READY
        And CspSubarray01 obsState is READY
        And CspSubarray01 configurationID is "sbi-mvp01-20200325-00001-science_A"
        And CspSubarray01 obsMode is (ObsMode.IMAGING, ObsMode.PULSAR_TIMING,) 

        When Scan Command is issued on CspSubarray01 (argument from file: Scan_CBF.json)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED')
        And CspSubarray01 cbfSubarrayObsState is SCANNING
        And CspSubarray01 obsState is SCANNING
        And PstBeam01 obsState is SCANNING
        And CspSubarray01 obsMode is (ObsMode.IMAGING, ObsMode.PULSAR_TIMING,)  

        When EndScan Command is issued on CspSubarray01 (no argument)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED') 
        And CspSubarray01 cbfSubarrayObsState is READY
        And CspSubarray01 obsState is READY
        And PstBeam01 obsState is READY
        And CspSubarray01 obsMode is (ObsMode.IMAGING, ObsMode.PULSAR_TIMING,)  

        When GoToIdle Command is issued on CspSubarray01 (no argument)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED') 
        And CspSubarray01 cbfSubarrayObsState is IDLE
        And CspSubarray01 obsState is IDLE
        And PstBeam01 obsState is IDLE
        And CspSubarray01 obsMode is (ObsMode.IDLE,) 

        When Configure Command is issued on CspSubarray01 (argument from file: Configure_CBF_PST_VR.json)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED') 
        And CspSubarray01 commandResult is ('configure', '0')
        And PstBeam01 obsState is READY
        And CspSubarray01 cbfSubarrayObsState is READY
        And CspSubarray01 obsState is READY
        And CspSubarray01 configurationID is "sbi-mvp01-20200325-00001-science_A"
        And CspSubarray01 obsMode is (ObsMode.PULSAR_TIMING,)    

        When Scan Command is issued on CspSubarray01 (argument from file: Scan_CBF.json)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED')
        And CspSubarray01 cbfSubarrayObsState is SCANNING
        And CspSubarray01 obsState is SCANNING
        And PstBeam01 obsState is SCANNING
        And CspSubarray01 obsMode is (ObsMode.PULSAR_TIMING,)  

        When EndScan Command is issued on CspSubarray01 (no argument)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED') 
        And CspSubarray01 cbfSubarrayObsState is READY
        And CspSubarray01 obsState is READY
        And PstBeam01 obsState is READY
        And CspSubarray01 obsMode is (ObsMode.PULSAR_TIMING,)               

        When GoToIdle Command is issued on CspSubarray01 (no argument)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED') 
        And CspSubarray01 cbfSubarrayObsState is IDLE
        And CspSubarray01 obsState is IDLE
        And PstBeam01 obsState is IDLE
        And CspSubarray01 obsMode is (ObsMode.IDLE,)         

        When ReleaseAllResources Command is issued on CspSubarray01 (no argument)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED') 
        And CspSubarray01 obsState is EMPTY
        And CspSubarray01 assignedTimingBeamIDs is []
        And CspSubarray01 obsMode is (ObsMode.IDLE,) 