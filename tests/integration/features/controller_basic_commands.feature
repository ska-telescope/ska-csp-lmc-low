Feature: Csp-Lmc Low Controller commands
    As the Csp-Lmc controller for Low telescope
    I want to send the basic commands to sub-elements

    # @pipeline
    # Scenario: Csp is powered On with success
    #     Given All subsystems are fresh initialized
    #     When On Command is issued on CspController with argument []
    #     Then CspController longRunningCommandAttributes is (0, 'COMPLETED')  
    #     And CbfController state is ON 
    #     And CbfSubarray01 state is ON
    #     And CspController state is ON         
    #     And CspSubarray01 state is ON 
    #     And CspController commandResult is ('on', '0')

    @pipeline
    Scenario: Csp executes Off with success remaining in ON state
        Given All subsystems are fresh initialized
        When On Command is issued on CspController with argument []
        Then CspController longRunningCommandAttributes is (0, 'COMPLETED')
        And CbfController state is ON 
        And CbfSubarray01 state is ON
        And CspController state is ON         
        And CspSubarray01 state is ON 
        And CspController commandResult is ('on', '0')
        
        When Off Command is issued on CspController with argument []
        Then CspController longRunningCommandAttributes is (0, 'COMPLETED')
        And CbfController state is ON 
        And CbfSubarray01 state is ON
        And CspController state is ON
        And CspSubarray01 state is ON 
        And CspController commandResult is ('off', '0')

    @pipeline
    Scenario: Csp executes Standby and Off commands in sequence
        Given All subsystems are fresh initialized
        When On Command is issued on CspController with argument []
        Then CspController longRunningCommandAttributes is (0, 'COMPLETED')
        And CbfController state is ON
        And CbfSubarray01 state is ON
        And CspController state is ON

        # nothing happen with standby command since it is rejected
        When Standby Command is issued on CspController with argument []
        Then CbfController state is ON
        And CbfSubarray01 state is ON
        And CspController state is ON

        When Off Command is issued on CspController with argument []
        Then CspController longRunningCommandAttributes is (0, 'COMPLETED')
        Then CspController commandResult is ('off', '0')
        And CbfController state is ON
        And CbfSubarray01 state is ON
        And CspController state is ON                

