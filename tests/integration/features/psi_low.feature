# ------------- Test happy path on PSI Low -------------------

    @psi_low @XTP-28920
    Scenario: Configure and De-configure Subarray01 with PST beam01 on CBF Hardware        
        Given All subsystems are fresh deployed on PSI Low

        When CspController adminmode set to ONLINE
        Then CspController state is ON
        And CbfController state is ON
        And CspSubarray01 state is ON
        And CspSubarray01 ObsState is EMPTY
        And CbfSubarray01 state is ON
        And CbfSubarray01 ObsState is EMPTY
        And PstBeam01 state is OFF
        
        When On Command is issued on CspController with argument []
        Then PstBeam01 state is ON
        And PstBeam01 obsState is IDLE
        
        When AssignResources Command is issued on CspSubarray01 (argument from file: AssignResources_CBF_PST.json)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED')
        And CspSubarray01 commandResult is ('assignresources', '0')
        And CspSubarray01 cbfSubarrayObsState is IDLE
        And CspSubarray01 obsState is IDLE
        And CspSubarray01 assignedTimingBeamIDs is [1]
        
        When Configure Command is issued on CspSubarray01 (argument from file: Configure_CBF_PST.json)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED') 
        And CspSubarray01 commandResult is ('configure', '0')
        And PstBeam01 obsState is READY
        And CspSubarray01 cbfSubarrayObsState is READY
        And CspSubarray01 obsState is READY
        And Fsp01 is configured for visibilities
        And Fsp02 is configured for pst-beam

        When GoToIdle Command is issued on CspSubarray01 (no argument)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED') 
        And CspSubarray01 cbfSubarrayObsState is IDLE
        And CspSubarray01 obsState is IDLE
        And PstBeam01 obsState is IDLE
        And Fsp01 is de-configured
        And Fsp02 is de-configured

        When ReleaseAllResources Command is issued on CspSubarray01 (no argument)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED') 
        And CspSubarray01 obsState is EMPTY
        And CspSubarray01 assignedTimingBeamIDs is []

        When CspController adminmode set to OFFLINE
        Then CspController state is DISABLE
        And CspSubarray01 state is DISABLE
        And CbfSubarray01 state is DISABLE
        And PstBeam01 state is DISABLE


    @psi_low @XTP-47071
    Scenario: Run a scan for only visibilities on CBF Hardware        
        Given All subsystems are fresh deployed on PSI Low
        And CNIC board is initialized
        And P4 routing configured for CNIC and Fsp01
        And CNIC is configured for receiving data
        And Correlator is reset in Fsp01

        When CspController adminmode set to ONLINE
        Then CspController state is ON
        And CbfController state is ON
        And CspSubarray01 state is ON
        And CspSubarray01 ObsState is EMPTY
        And CbfSubarray01 state is ON
        And CbfSubarray01 ObsState is EMPTY
        
        When AssignResources Command is issued on CspSubarray01 (argument from file: AssignResources_CBF.json)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED')
        And CspSubarray01 commandResult is ('assignresources', '0')
        And CspSubarray01 cbfSubarrayObsState is IDLE
        And CspSubarray01 obsState is IDLE
        
        When Configure Command is issued on CspSubarray01 (argument from file: Configure_CBF.json)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED') 
        And CspSubarray01 commandResult is ('configure', '0')
        And CspSubarray01 cbfSubarrayObsState is READY
        And CspSubarray01 obsState is READY
        And Fsp01 is configured for visibilities

        When Scan Command is issued on CspSubarray01 (argument from file: Scan_CBF.json)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED') 
        And CspSubarray01 cbfSubarrayObsState is SCANNING
        And CspSubarray01 obsState is SCANNING
        And Fsp01 starts a scan
        And Fsp01 sees data incoming from CNIC
        And Fsp01 creates visibilities

        When EndScan Command is issued on CspSubarray01 (no argument)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED') 
        And CspSubarray01 cbfSubarrayObsState is READY
        And CspSubarray01 obsState is READY
        #And Fsp01 stops the scan

        When GoToIdle Command is issued on CspSubarray01 (no argument)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED') 
        And CspSubarray01 cbfSubarrayObsState is IDLE
        And CspSubarray01 obsState is IDLE
        And Fsp01 is de-configured
        And Fsp02 is de-configured

        When ReleaseAllResources Command is issued on CspSubarray01 (no argument)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED') 
        And CspSubarray01 obsState is EMPTY

        When CspController adminmode set to OFFLINE
        Then CspController state is DISABLE
        And CspSubarray01 state is DISABLE
        And CbfSubarray01 state is DISABLE
