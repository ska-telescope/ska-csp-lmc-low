# ------------- Test all commands -------------------
Feature: Csp-Lmc Low Controller commands
    As the Csp-Lmc controller for Low telescope
    I want to send the basic commands to sub-elements

    @pipeline
    Background:
        Given All subsystems are fresh initialized with PST beams


    @pipeline @XTP-28918
    Scenario: Test all commands on Subarray01 with PST beam

        When On Command is issued on CspController with argument []
        Then CspController longRunningCommandAttributes is (0, 'COMPLETED')
        And CspSubarray01 state is ON
        And CbfSubarray01 state is ON
        And CspSubarray01 ObsState is EMPTY
        And CbfSubarray01 ObsState is EMPTY
        And PstBeam01 state is ON
        And CspController CspCbfState is ON
        And CspController state is ON
        And CspSubarray01 cbfSubarrayState is ON
        And CspSubarray01 cbfSubarrayObsState is EMPTY
        And CspSubarray01 obsMode is (ObsMode.IDLE,) 

        When AssignResources Command is issued on CspSubarray01 (argument from file: AssignResources_CBF_PSS_PST.json)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED')
        And CspSubarray01 commandResult is ('assignresources', '0')
        And CspSubarray01 cbfSubarrayObsState is IDLE
        And CspSubarray01 obsState is IDLE
        And CspSubarray01 assignedTimingBeamIDs is [1]
        And PstBeam01 obsState is IDLE
        And CspSubarray01 obsMode is (ObsMode.IDLE,)

        When Configure Command is issued on CspSubarray01 (argument from file: Configure_CBF_PST_VR.json)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED') 
        And PstBeam01 obsState is READY
        And CspSubarray01 cbfSubarrayObsState is READY
        And CspSubarray01 obsState is READY
        And CspSubarray01 configurationID is "sbi-mvp01-20200325-00001-science_A"
        And CspSubarray01 obsMode is (ObsMode.PULSAR_TIMING,) 

        When Scan Command is issued on CspSubarray01 (argument from file: Scan_CBF.json)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED')
        And CspSubarray01 cbfSubarrayObsState is SCANNING
        And CspSubarray01 obsState is SCANNING
        And PstBeam01 obsState is SCANNING
        And CspSubarray01 obsMode is ( ObsMode.PULSAR_TIMING,)

        When EndScan Command is issued on CspSubarray01 (no argument)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED') 
        And CspSubarray01 cbfSubarrayObsState is READY
        And CspSubarray01 obsState is READY
        And PstBeam01 obsState is READY
        And CspSubarray01 obsMode is (ObsMode.PULSAR_TIMING,)

        When GoToIdle Command is issued on CspSubarray01 (no argument)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED') 
        And CspSubarray01 cbfSubarrayObsState is IDLE
        And CspSubarray01 obsState is IDLE
        And PstBeam01 obsState is IDLE
        And CspSubarray01 obsMode is (ObsMode.IDLE,)

        When ReleaseAllResources Command is issued on CspSubarray01 (no argument)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED') 
        And CspSubarray01 obsState is EMPTY
        And CspSubarray01 assignedTimingBeamIDs is []
        And CspSubarray01 obsMode is (ObsMode.IDLE,)

    Scenario: Configure multiple PST beams on Subarray01

        When On Command is issued on CspController with argument []
        Then CspController longRunningCommandAttributes is (0, 'COMPLETED')
        And CspSubarray01 state is ON
        And CbfSubarray01 state is ON
        And CspSubarray01 ObsState is EMPTY
        And CbfSubarray01 ObsState is EMPTY
        And PstBeam01 state is ON
        And CspController CspCbfState is ON
        And CspController state is ON
        And CspSubarray01 cbfSubarrayState is ON
        And CspSubarray01 cbfSubarrayObsState is EMPTY
        And CspSubarray01 obsMode is (ObsMode.IDLE,)

        When AssignResources Command is issued on CspSubarray01 (argument from file: AssignResources_CBF_PST_multibeams.json)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED')
        And CspSubarray01 commandResult is ('assignresources', '0')
        And CspSubarray01 cbfSubarrayObsState is IDLE
        And CspSubarray01 obsState is IDLE
        And CspSubarray01 assignedTimingBeamIDs is [1, 2]
        And PstBeam01 obsState is IDLE
        And CspSubarray01 obsMode is (ObsMode.IDLE,)

        When Configure Command is issued on CspSubarray01 (argument from file: Configure_CBF_PST_multibeams.json)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED') 
        And PstBeam01 obsState is READY
        And CspSubarray01 cbfSubarrayObsState is READY
        And CspSubarray01 obsState is READY
        And CspSubarray01 configurationID is "sbi-mvp01-20200325-00001-science_A"
        And CspSubarray01 obsMode is (ObsMode.PULSAR_TIMING,)

        When Scan Command is issued on CspSubarray01 (argument from file: Scan_CBF.json)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED')
        And CspSubarray01 cbfSubarrayObsState is SCANNING
        And CspSubarray01 obsState is SCANNING
        And PstBeam01 obsState is SCANNING
        And CspSubarray01 obsMode is ( ObsMode.PULSAR_TIMING,)

        When EndScan Command is issued on CspSubarray01 (no argument)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED')
        And CspSubarray01 cbfSubarrayObsState is READY
        And CspSubarray01 obsState is READY
        And PstBeam01 obsState is READY
        And CspSubarray01 obsMode is (ObsMode.PULSAR_TIMING,)

        When GoToIdle Command is issued on CspSubarray01 (no argument)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED')
        And CspSubarray01 cbfSubarrayObsState is IDLE
        And CspSubarray01 obsState is IDLE
        And PstBeam01 obsState is IDLE
        And CspSubarray01 obsMode is (ObsMode.IDLE,)

        When ReleaseAllResources Command is issued on CspSubarray01 (no argument)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED')
        And CspSubarray01 obsState is EMPTY
        And CspSubarray01 assignedTimingBeamIDs is []
        And CspSubarray01 obsMode is (ObsMode.IDLE,)


    @pipeline
    Scenario: Test abort Scan on Subarray01 with PST beam

        When On Command is issued on CspController with argument []
        Then CspController longRunningCommandAttributes is (0, 'COMPLETED')
        And CspSubarray01 state is ON
        And CbfSubarray01 state is ON
        And CspSubarray01 ObsState is EMPTY
        And CbfSubarray01 ObsState is EMPTY
        And PstBeam01 state is ON
        And CspController CspCbfState is ON
        And CspController state is ON
        And CspSubarray01 cbfSubarrayState is ON
        And CspSubarray01 cbfSubarrayObsState is EMPTY
        And CspSubarray01 obsMode is (ObsMode.IDLE,) 

        When AssignResources Command is issued on CspSubarray01 (argument from file: AssignResources_CBF_PSS_PST.json)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED')
        And CspSubarray01 commandResult is ('assignresources', '0')
        And CspSubarray01 cbfSubarrayObsState is IDLE
        And CspSubarray01 obsState is IDLE
        And CspSubarray01 assignedTimingBeamIDs is [1]
        And PstBeam01 obsState is IDLE
        And CspSubarray01 obsMode is (ObsMode.IDLE,) 

        When Configure Command is issued on CspSubarray01 (argument from file: Configure_CBF_PST_VR.json)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED') 
        And PstBeam01 obsState is READY
        And CspSubarray01 cbfSubarrayObsState is READY
        And CspSubarray01 obsState is READY
        And CspSubarray01 obsMode is (ObsMode.PULSAR_TIMING,) 

        When Scan Command is issued on CspSubarray01 (argument from file: Scan_CBF.json)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED')
        And CspSubarray01 cbfSubarrayObsState is SCANNING
        And CspSubarray01 obsState is SCANNING
        And PstBeam01 obsState is SCANNING
        And CspSubarray01 obsMode is ( ObsMode.PULSAR_TIMING,)  

        When Abort Command is issued on CspSubarray01 (no argument)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED') 
        And CspSubarray01 obsState is ABORTED
        And CspSubarray01 obsMode is (ObsMode.IDLE,)

        When Restart Command is issued on CbfSubarray01 (no argument)
        And CbfSubarray01 obsState is EMPTY

    
    Scenario: Csp Subarray release resources with PST beam
        
        #this test cannot pass because releaseresources is not forwarded properly to low-cbfSubarrayObsState

        When On Command is issued on CspController with argument []
        Then CspController longRunningCommandAttributes is (0, 'COMPLETED')
        And CspSubarray01 state is ON
        And CbfSubarray01 state is ON
        And CspSubarray01 ObsState is EMPTY
        And CbfSubarray01 ObsState is EMPTY
        And PstBeam01 state is ON
        And CspController CspCbfState is ON
        And CspController state is ON
        And CspSubarray01 cbfSubarrayState is ON
        And CspSubarray01 cbfSubarrayObsState is EMPTY

        When AssignResources command is issued on CspSubarray01 (argument from file: AssignResources_CBF_PSS_PST.json)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED')
        And CspSubarray01 commandResult is ('assignresources', '0')
        And CspSubarray01 assignedTimingBeamIDS is [1]
        And PstBeam01 obsState is IDLE

        When ReleaseResources command is issued on CspSubarray01 (argument from file: ReleaseResources_PST.json)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED') 
        And CspSubarray01 commandResult is ('releaseresources', '0')
        And CbfSubarray01 ObsState is EMPTY
        And CspSubarray01 ObsState is EMPTY
        And CspSubarray01 assignedTimingBeamIDS is []

    # this test has to be updated, it fails because scan input now is blocked by telmodel    
    Scenario: Fail scan on Subarray01 with PST beam

        When On Command is issued on CspController with argument []
        Then CspController longRunningCommandAttributes is (0, 'COMPLETED')
        And CspSubarray01 state is ON
        And CbfSubarray01 state is ON
        And CspSubarray01 ObsState is EMPTY
        And CbfSubarray01 ObsState is EMPTY
        And PstBeam01 state is ON
        And CspController CspCbfState is ON
        And CspController state is ON
        And CspSubarray01 cbfSubarrayState is ON
        And CspSubarray01 cbfSubarrayObsState is EMPTY

        When AssignResources Command is issued on CspSubarray01 (argument from file: AssignResources_CBF_PSS_PST.json)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED') 
        And CspSubarray01 commandResult is ('assignresources', '0')
        And CspSubarray01 cbfSubarrayObsState is IDLE
        And CspSubarray01 obsState is IDLE
        And CspSubarray01 assignedTimingBeamIDs is [1]
        And PstBeam01 obsState is IDLE

        When Configure Command is issued on CspSubarray01 (argument from file: Configure_CBF_PSS_PST.json)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED') 
        And CspSubarray01 commandResult is ('configure', '0')
        And PstBeam01 obsState is READY
        And CspSubarray01 cbfSubarrayObsState is READY
        And CspSubarray01 obsState is READY

        When Scan Command is issued on CspSubarray01 (argument from file: Scan_CBF_wrong.json)
        Then CspSubarray01 longRunningCommandAttributes is (3, 'COMPLETED') 
        And CspSubarray01 cbfSubarrayObsState is READY
        And CspSubarray01 obsState is READY
        And PstBeam01 obsState is READY
        And CspSubarray01 commandResult is ('scan', '3')

        When GoToIdle Command is issued on CspSubarray01 (no argument)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED') 
        And CspSubarray01 cbfSubarrayObsState is IDLE
        And CspSubarray01 obsState is IDLE
        And PstBeam01 obsState is IDLE

    @pipeline
    Scenario: Csp is inizialized twice
        And CspController state is ON
        And CspController HealthState is UNKNOWN 
        When CspController adminmode is ONLINE
        
        When CspSubarray01 set adminmode to OFFLINE
        Then CspSubarray01 adminmode is OFFLINE

        When CspController set adminmode to ENGINEERING
        And CspController HealthState is UNKNOWN 
        Then CspController state is ON

    @pipeline
    Scenario: Test configure 3.2 on Subarray01 with PST beam

        When On Command is issued on CspController with argument []
        Then CspController longRunningCommandAttributes is (0, 'COMPLETED')
        And CspSubarray01 state is ON
        And CbfSubarray01 state is ON
        And CspSubarray01 ObsState is EMPTY
        And CbfSubarray01 ObsState is EMPTY
        And PstBeam01 state is ON
        And CspController CspCbfState is ON
        And CspController state is ON
        And CspSubarray01 cbfSubarrayState is ON
        And CspSubarray01 cbfSubarrayObsState is EMPTY

        When AssignResources Command is issued on CspSubarray01 (argument from file: AssignResources_CBF_PST.json)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED')
        And CspSubarray01 commandResult is ('assignresources', '0')
        And CspSubarray01 cbfSubarrayObsState is IDLE
        And CspSubarray01 obsState is IDLE
        And CspSubarray01 assignedTimingBeamIDs is [1]
        And PstBeam01 obsState is IDLE

        When Configure Command is issued on CspSubarray01 (argument from file: Configure_skb_429.json)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED') 
        And CspSubarray01 commandResult is ('configure', '0')
        And PstBeam01 obsState is READY
        And CspSubarray01 cbfSubarrayObsState is READY
        And CspSubarray01 obsState is READY

        When GoToIdle Command is issued on CspSubarray01 (no argument)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED') 
        And CspSubarray01 cbfSubarrayObsState is IDLE
        And CspSubarray01 obsState is IDLE
        And PstBeam01 obsState is IDLE

        When ReleaseAllResources Command is issued on CspSubarray01 (no argument)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED') 
        And CspSubarray01 obsState is EMPTY
        And CspSubarray01 assignedTimingBeamIDs is []

@oipeline
Scenario: Test obsMode value after abort restart and configuring

        When On Command is issued on CspController with argument []
        Then CspController longRunningCommandAttributes is (0, 'COMPLETED')
        And CspSubarray01 state is ON
        And CbfSubarray01 state is ON
        And CspSubarray01 ObsState is EMPTY
        And CbfSubarray01 ObsState is EMPTY
        And PstBeam01 state is ON
        And CspController CspCbfState is ON
        And CspController state is ON
        And CspSubarray01 cbfSubarrayState is ON
        And CspSubarray01 cbfSubarrayObsState is EMPTY
        And CspSubarray01 obsMode is (ObsMode.IDLE,) 

        When AssignResources Command is issued on CspSubarray01 (argument from file: AssignResources_CBF_PSS_PST.json)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED')
        And CspSubarray01 commandResult is ('assignresources', '0')
        And CspSubarray01 cbfSubarrayObsState is IDLE
        And CspSubarray01 obsState is IDLE
        And CspSubarray01 assignedTimingBeamIDs is [1]
        And PstBeam01 obsState is IDLE
        And CspSubarray01 obsMode is (ObsMode.IDLE,) 

        When Configure Command is issued on CspSubarray01 (argument from file: Configure_CBF_PST_VR.json)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED') 
        And PstBeam01 obsState is READY
        And CspSubarray01 cbfSubarrayObsState is READY
        And CspSubarray01 obsState is READY
        And CspSubarray01 obsMode is (ObsMode.PULSAR_TIMING,) 

        When Scan Command is issued on CspSubarray01 (argument from file: Scan_CBF.json)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED')
        And CspSubarray01 cbfSubarrayObsState is SCANNING
        And CspSubarray01 obsState is SCANNING
        And PstBeam01 obsState is SCANNING
        And CspSubarray01 obsMode is ( ObsMode.PULSAR_TIMING,)  

        When Abort Command is issued on CspSubarray01 (no argument)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED')
        And CspSubarray01 obsMode is (ObsMode.IDLE,)

        When Restart Command is issued on CbfSubarray01 (no argument)
        And CbfSubarray01 obsState is EMPTY
        And CspSubarary01 obsState is EMPTY

        When AssignResources Command is issued on CspSubarray01 (argument from file: AssignResources_CBF.json)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED')
        And CspSubarray01 commandResult is ('assignresources', '0')
        And CspSubarray01 cbfSubarrayObsState is IDLE
        And CspSubarray01 obsState is IDLE

        When Configure Command is issued on CspSubarray01 (argument from file: Configure_CBF.json)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED') 
        And CspSubarray01 cbfSubarrayObsState is READY
        And CspSubarray01 obsState is READY
        And CspSubarray01 obsMode is (ObsMode.IMAGING,)
        
        When GoToIdle Command is issued on CspSubarray01 (no argument)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED') 
        And CspSubarray01 cbfSubarrayObsState is IDLE
        And CspSubarray01 obsState is IDLE
        And PstBeam01 obsState is IDLE

        When ReleaseAllResources Command is issued on CspSubarray01 (no argument)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED') 
        And CspSubarray01 obsState is EMPTY
        And CspSubarray01 assignedTimingBeamIDs is []
