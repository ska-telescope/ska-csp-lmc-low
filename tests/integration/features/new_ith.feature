Feature: CSP.LMC Happy Path commands on CSP subarrays with one PstBeam assigned 

    # CSP initial status (no hardware involved)
    # Testwere will make sure that at the beginning of each test these states are reached
    #
    #                       | DevState | HealthState | AdminMode | ObsState | AssignedTimingBeamIDs
    # CSP.LMC Subarray          ON         UNKNOWN      ONLINE      EMPTY           []
    # CBF Subarray              ON         UNKNOWN      ONLINE      EMPTY           --
    # PST Beam                  OFF           OK        ONLINE      IDLE            --
    
    @nth 
    Background: 
        # Define input files
        Given input file for AssignResources set: "AssignResources_CBF_PST.json" (connection with PstBeam01)
        And input file for Configure set: "Configure_CBF_PST_VR.json" (configure for VR CBF and PstBeam01)
        And input file for Scan set: "Scan_CBF.json" (set a scan id)
        
        # Turn ON PstBeam01 from CspController
        And CspController has turned ON PstBeam01 
    
    @nth @XTP-77027
    Scenario Outline: CSP.LMC Subarray successfully assign PSTBeam01 and transition to IDLE
        Given CspSubarray<sub_id> is in initial status
        When AssignResources command (nth)is issued WITH SUCCESS on CspSubarray<sub_id>
        Then CspSubarray<sub_id> ObsState is(nth) IDLE 
        And CbfSubarray<sub_id> ObsState is(nth) IDLE
        And CspSubarray<sub_id> assignedTimingBeamIDS is(nth) [1]

    Examples:
        | sub_id | 
        | 01  | 
        | 02  | 
        | 03  | 
        | 04  |
    
    @nth @XTP-77030
    Scenario Outline: CSP.LMC Subarray is successfully configured (with PST Beam 01) and transition to READY
        Given CspSubarray<sub_id> has assigned PstBeam01 and ObsState IDLE
        When Configure command (nth)is issued WITH SUCCESS on CspSubarray<sub_id> 
        Then CspSubarray<sub_id> ObsState is(nth) READY
        And CbfSubarray<sub_id> ObsState is(nth) READY
        And PstBeam01 ObsState is(nth) READY

    Examples:
        | sub_id | 
        | 01  | 
        | 02  | 
        | 03  | 
        | 04  |

    @nth @XTP-77031
    Scenario Outline: CSP.LMC Subarray successfully issues a Scan with PSTBeam01
        Given CspSubarray<sub_id> has been configured with PstBeam01
        When Scan command (nth)is issued WITH SUCCESS on CspSubarray<sub_id>
        Then CspSubarray<sub_id> ObsState is(nth) SCANNING
        And CbfSubarray<sub_id> ObsState is(nth) SCANNING
        And PstBeam01 ObsState is(nth) SCANNING

    Examples:
        | sub_id | 
        | 01  | 
        | 02  | 
        | 03  | 
        | 04  |

    @nth @XTP-77033
    Scenario Outline: CSP.LMC Subarray successfully ends a scan with PSTBeam01
        Given CspSubarray<sub_id> is scanning with PstBeam01
        When EndScan command (nth)is issued WITH SUCCESS on CspSubarray<sub_id>
        Then CspSubarray<sub_id> ObsState is(nth) READY
        And CbfSubarray<sub_id> ObsState is(nth) READY
        And PstBeam01 ObsState is(nth) READY

    Examples:
        | sub_id | 
        | 01  | 
        | 02  | 
        | 03  | 
        | 04  | 

    @nth @XTP-77032
    Scenario Outline: CSP.LMC Subarray is successfully deconfigured after a scan is ended (with PST Beam 01)
        Given CspSubarray<sub_id> has ended a scan with PstBeam01
        When GoToIdle command (nth)is issued WITH SUCCESS on CspSubarray<sub_id>
        Then CspSubarray<sub_id> ObsState is(nth) IDLE
        And CbfSubarray<sub_id> ObsState is(nth) IDLE
        And PstBeam01 ObsState is(nth) IDLE

    Examples:
        | sub_id | 
        | 01  | 
        | 02  | 
        | 03  | 
        | 04  |      

    @nth @XTP-77034
    Scenario Outline: CSP.LMC Subarray is successfully deconfigured before issuing a scan (with PST Beam 01)
        Given CspSubarray<sub_id> has been configured with PstBeam01 
        When GoToIdle command (nth)is issued WITH SUCCESS on CspSubarray<sub_id>
        Then CspSubarray<sub_id> ObsState is(nth) IDLE
        And CbfSubarray<sub_id> ObsState is(nth) IDLE
        And PstBeam01 ObsState is(nth) IDLE

    Examples:
        | sub_id | 
        | 01  | 
        | 02  | 
        | 03  | 
        | 04  |   

    @nth @XTP-77035
    Scenario Outline: CSP.LMC Subarray successfully release all resources (with PSTBeam01) before being configured 
        Given CspSubarray<sub_id> has assigned PstBeam01 and ObsState IDLE
        When ReleaseAllResources command (nth)is issued WITH SUCCESS on CspSubarray<sub_id>
        Then CspSubarray<sub_id> ObsState is(nth) EMPTY
        And CbfSubarray<sub_id> ObsState is(nth) EMPTY
        And CspSubarray<sub_id> assignedTimingBeamIdS is(nth) []

    Examples:
        | sub_id | 
        | 01  | 
        | 02  | 
        | 03  | 
        | 04  |  

    @nth @XTP-77036
    Scenario Outline: CSP.LMC Subarray successfully release all resources (with PSTBeam01) after being de-configured
        Given CspSubarray<sub_id> has been deconfigured <when_deconfigured> with PstBeam01
        When ReleaseAllResources command (nth)is issued WITH SUCCESS on CspSubarray<sub_id>
        Then CspSubarray<sub_id> ObsState is(nth) EMPTY
        And CbfSubarray<sub_id> ObsState is(nth) EMPTY
        And CspSubarray<sub_id> assignedTimingBeamIdS is(nth) []

    Examples:
        | sub_id | when_deconfigured   |
        | 01     | after a scan        |
        | 01     | before a scan       |
        | 02     | after a scan        |
        | 02     | before a scan       |
        | 03     | after a scan        |
        | 03     | before a scan       |
        | 04     | after a scan        |
        | 04     | before a scan       |