Feature: Csp-Lmc Low Subarray initialization
    As the Csp-Lmc Subarray for Mid telescope
    1) I want to connect to Cbf Subarray
    and report its states and modes
    2) I want to subscribe the changes in states and mode of Cbf Subarray
    and change my state accordingly

    Background:
        Given Subarray01 is fresh initialized
    
    Scenario: Csp Subarray is initialized with success
        Then CspSubarray01 state is ON
        And CspSubarray01 ObsState is EMPTY
        And CspSubarray01 AdminMode is ENGINEERING
        #And CspSubarray01 HealthState is OK
        And CbfSubarray01 state is ON
        And CbfSubarray01 ObsState is EMPTY
        And CbfSubarray01 AdminMode is ENGINEERING
        #And CbfSubarray01 HealthState is OK
        And CspSubarray01 cbfSubarrayState is ON
        And CspSubarray01 cbfSubarrayObsState is EMPTY
        And CspSubarray01 cbfSubarrayAdminMode is ENGINEERING
        #And CspSubarray01 cbfSubarrayHealthState is OK


    Scenario: Cbf Subarray is turned On
        And CspSubarray01 state is OFF
        And CspSubarray01 ObsState is EMPTY
        And CbfSubarray01 state is OFF
        And CbfSubarray01 ObsState is EMPTY

        When On Command is issued on CbfSubarray01 (no argument)
        Then CbfSubarray01 state is ON
        And CspSubarray01 state is ON
        And CspSubarray01 cbfSubarrayState is ON
    
    
    Scenario: Cbf Subarray changes ObsState to IDLE
        And On Command is issued on CspSubarray01 (no argument)
        And CspSubarray01 state is ON
        And CspSubarray01 ObsState is EMPTY
        And CbfSubarray01 state is ON
        And CbfSubarray01 ObsState is EMPTY

        When AssignResources Command is issued on CbfSubarray01 (argument from file: AssignResources_CBF.json)
        Then CbfSubarray01 ObsState is IDLE
        And CspSubarray01 ObsState is IDLE
