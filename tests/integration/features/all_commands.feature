# ------------- Test all commands -------------------

    Background:
        Given All subsystems are fresh initialized without PST beams
    
    @XTP-30215
    Scenario: Test all commands on Subarray01 without PST beams

        When On Command is issued on CspController with argument []
        Then CspController longRunningCommandAttributes is (0, 'COMPLETED')
        And CspController CspCbfState is ON
        And CspController state is ON
        And CspSubarray01 cbfSubarrayState is ON
        And CspSubarray01 state is ON
        And CspSubarray01 cbfSubarrayObsState is EMPTY
        And CspSubarray01 obsState is EMPTY

        When AssignResources Command is issued on CspSubarray01 (argument from file: AssignResources_CBF.json)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED')
        And CspSubarray01 commandResult is ('assignresources', '0')
        And CspSubarray01 cbfSubarrayObsState is IDLE
        And CspSubarray01 obsState is IDLE

        When Configure Command is issued on CspSubarray01 (argument from file: Configure_CBF.json)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED') 
        And CspSubarray01 commandResult is ('configure', '0')
        And CspSubarray01 cbfSubarrayObsState is READY
        And CspSubarray01 obsState is READY
        And CspSubarray01 configurationID is "sbi-mvp01-20200325-00001-science_A"

        When Scan Command is issued on CspSubarray01 (argument from file: Scan_CBF.json)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED')
        And CspSubarray01 cbfSubarrayObsState is SCANNING
        And CspSubarray01 obsState is SCANNING
        And CspSubarray01 obsMode is ('ObsMode.IMAGING')

        When EndScan Command is issued on CspSubarray01 (no argument)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED') 
        And CspSubarray01 cbfSubarrayObsState is READY
        And CspSubarray01 obsState is READY

        When GoToIdle Command is issued on CspSubarray01 (no argument)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED') 
        And CspSubarray01 cbfSubarrayObsState is IDLE
        And CspSubarray01 obsState is IDLE
        And CspSubarray01 obsMode is ('ObsMode.IDLE')

        When ReleaseAllResources Command is issued on CspSubarray01 (no argument)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED') 
        And CspSubarray01 obsState is EMPTY

    @XTP-35915
    Scenario: Csp is inizialized twice

        And CspController state is ON
        And CspController HealthState is UNKNOWN 
        When CspController adminmode is ONLINE
        
        When CspSubarray01 set adminmode to OFFLINE
        Then CspSubarray01 adminmode is OFFLINE

        When CspController set adminmode to ENGINEERING
        And CspController HealthState is UNKNOWN 
        Then CspController state is ON
