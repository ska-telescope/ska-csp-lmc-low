Feature: Csp-Lmc Low Subarray Assign and Release resources
      As the Csp-Lmc Subarray for Mid telescope
      I want to Assign and Release resources on Cbf Subarray
      and change my state accordingly

    Background:
        Given All subsystems are fresh initialized with PST beams
        And CspSubarray01 state is ON
        And CspSubarray01 ObsState is EMPTY
        And CspSubarray01 assignedTimingBeamIDs is []
        
        When On Command is issued on CspController with argument ["low-pst/beam/01"]
        And CspController longRunningCommandAttributes is (0, 'COMPLETED')
        And PstBeam01 state is ON
        And PstBeam01 ObsState is IDLE

    @pipeline 
    Scenario: Csp Subarray assign and release resources with PST Beam 01 
        
        When AssignResources command is issued on CspSubarray01 (argument from file: AssignResources_CBF_PST.json)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED')
        And CspSubarray01 commandResult is ('assignresources', '0')
        And CspSubarray01 ObsState is IDLE
        And CbfSubarray01 ObsState is IDLE
        And CspSubarray01 assignedTimingBeamIDs is [1]

        When ReleaseAllResources command is issued on CspSubarray01 (no argument)
        Then CspSubarray01 commandResult is ('releaseallresources', '0')
        And CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED') 
        And CspSubarray01 ObsState is EMPTY
        And CbfSubarray01 ObsState is EMPTY
        And CspSubarray01 assignedTimingBeamIDs is []

    @pipeline @XTP-75530
    Scenario: AdminMode OFFLINE is rejected on Controller if Subarray not Empty (SKB-734)
        When AssignResources command is issued on CspSubarray01 (argument from file: AssignResources_CBF_PST.json)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED')
        And CspSubarray01 ObsState is IDLE
        And CbfSubarray01 ObsState is IDLE
        And CspSubarray01 assignedTimingBeamIDs is [1]

        When CspController set adminMode to OFFLINE
        Then CspController AdminMode is ONLINE
        And CspSubarray01 AdminMode is ONLINE
        And CspSubarray01 ObsState is IDLE
        And CspSubarray01 assignedTimingBeamIDs is [1]
        And PstBeam01 AdminMode is ONLINE

    @pipeline @XTP-75531
    Scenario: Restart command is accepted on CSP Subarray with Pst in OFF state (SKB-739)
        When AssignResources command is issued on CspSubarray01 (argument from file: AssignResources_CBF_PST.json)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED')
        And CspSubarray01 ObsState is IDLE
        And CspSubarray01 assignedTimingBeamIDs is [1]
        
        When Abort command is issued on CspSubarray01 (no argument)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED')
        And CspSubarray01 ObsState is ABORTED
        When Off command is issued on CspController with argument ["low-pst/beam/01"]
        Then CspController longRunningCommandAttributes is (0, 'COMPLETED')
        And PstBeam01 state is OFF

        When Restart command is issued on CspSubarray01 (no argument)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED')
        And CspSubarray01 ObsState is EMPTY
        And PstBeam01 ObsState is IDLE
        And CspSubarray01 assignedTimingBeamIDs is []
    
    @pipeline @XTP-75532
    Scenario: Configure a different subarray after releasing resources (SKB-736)
        When AssignResources command is issued on CspSubarray03 (argument from file: AssignResources_CBF_PST_sub3.json)
        Then CspSubarray03 longRunningCommandAttributes is (0, 'COMPLETED')
        And CspSubarray03 ObsState is IDLE
        And CspSubarray03 assignedTimingBeamIDs is [1]
        
        When Configure command is issued on CspSubarray03 (argument from file: Configure_CBF_PST_VR_sub3.json)
        Then CspSubarray03 longRunningCommandAttributes is (0, 'COMPLETED')
        And CspSubarray03 ObsState is READY
        When GoToIdle command is issued on CspSubarray03 (no argument)
        Then CspSubarray03 longRunningCommandAttributes is (0, 'COMPLETED')
        And CspSubarray03 ObsState is IDLE

        When ReleaseAllResources command is issued on CspSubarray03 (no argument)
        Then CspSubarray03 longRunningCommandAttributes is (0, 'COMPLETED')
        And CspSubarray03 ObsState is EMPTY
        And PstBeam01 ObsState is IDLE
        And CspSubarray03 assignedTimingBeamIDs is []

        When AssignResources command is issued on CspSubarray01 (argument from file: AssignResources_CBF_PST.json)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED')
        And CspSubarray01 ObsState is IDLE
        And CspSubarray01 assignedTimingBeamIDs is [1]
        
        When Configure command is issued on CspSubarray01 (argument from file: Configure_CBF_PST_VR.json)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED')
        And CspSubarray01 ObsState is READY
        When GoToIdle command is issued on CspSubarray01 (no argument)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED')
        And CspSubarray01 ObsState is IDLE

        When ReleaseAllResources command is issued on CspSubarray01 (no argument)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED')
        And CspSubarray01 ObsState is EMPTY
        And PstBeam01 ObsState is IDLE
        And CspSubarray01 assignedTimingBeamIDs is []