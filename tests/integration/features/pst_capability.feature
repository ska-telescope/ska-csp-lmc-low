Feature: Csp-Lmc Low Pst Capability reports information about Pst beams

    @pipeline
    Scenario: Low Pst Capability is properly initialized
        Given All subsystems are fresh initialized
        And CspCapabilityPst isCommunicating is True
        Then CspCapabilityPst state is ON
        And CspCapabilityPst healthState is OK
        And CspCapabilityPst AdminMode is ONLINE
        # And CspCapabilityPst devicesId is [1]
        And CspCapabilityPst devicesDeployed is 1
        And CspCapabilityPst devicesFqdn is ("low-pst/beam/01",)
        And CspCapabilityPst devicesHealthState is "OK",
        And CspCapabilityPst devicesAdminMode is "ONLINE",
        And CspCapabilityPst devicesObsState is "IDLE",
        And CspCapabilityPst devicesPstProcessingMode is "IDLE",
        And CspCapabilityPst devicesDataReceived is [0]
        And CspCapabilityPst devicesDataReceiveRate is [0.0]
        And CspCapabilityPst devicesDataDropped is [0]
        And CspCapabilityPst devicesDataDropRate is [0.0]


