Feature: Csp-Lmc Low Subarray Assign and Release resources
      As the Csp-Lmc Subarray for Low telescope
      I want to Assign and Release resources on Cbf Subarray
      and change my state accordingly

    Background:
    
        Given CSP is in initial status (nth)
        And CspSubarray01 state is(nth) ON
        And CspSubarray01 ObsState is(nth) EMPTY
        And CspSubarray01 assignedTimingBeamIDs is []
        
        When On Command (nth)is issued on CspController with argument ["low-pst/beam/01"]
        #And CspController longRunningCommandAttributes is (0, 'COMPLETED')
        And PstBeam01 state is(nth) ON
        And PstBeam01 ObsState is(nth) IDLE
    
    Scenario: Csp Subarray assign and release resources with PST Beam 01 (nth)
        
        When AssignResources command (nth)is issued on CspSubarray01 (argument from file: AssignResources_CBF_PST.json)
        #Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED')
        And CspSubarray01 commandResult is(nth) ('assignresources', '0')
        And CspSubarray01 ObsState is(nth) IDLE 
        And CbfSubarray01 ObsState is(nth) IDLE
        And CspSubarray01 assignedTimingBeamIDs is(nth) [1]

        When ReleaseAllResources command (nth)is issued on CspSubarray01 (no argument)
        Then CspSubarray01 commandResult is(nth) ('releaseallresources', '0')
        #And CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED') 
        And CspSubarray01 ObsState is(nth) EMPTY
        And CbfSubarray01 ObsState is(nth) EMPTY
        And CspSubarray01 assignedTimingBeamIDs is []
