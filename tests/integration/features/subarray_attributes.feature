Feature: Csp-Lmc Low Subarray attributes
    As the Csp-Lmc Subarray for Low telescope
    I want to read basic attributes

    @pipeline @XTP-35913
    Scenario: Csp subarray reports HealthState
        Given All subsystems are fresh initialized
        And CspSubarray01 isCommunicating is True
        # Current implementation sets HealthState to UNKNOWN if no connector 
        # or processor could be configured
        And CspController set adminmode to ONLINE
        And CspSubarray01 healthstate is UNKNOWN

    @pipeline @obsmode @XTP-57388
    Scenario: Csp subarray reports ObsMode
        Given All subsystems are fresh initialized
        And CspSubarray01 isCommunicating is True
        And CspController set adminmode to ONLINE
        And CspSubarray01 obsMode is (ObsMode.IDLE,)
