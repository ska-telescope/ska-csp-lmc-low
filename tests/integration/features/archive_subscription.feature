Feature: Csp-Lmc Low test subscription to archive
           
# ------------- Test archive subscription -------------------
    @pipeline @XTP-30220 @xfail
    Scenario: Test state subscription on Controller and Subarray01
        Given All subsystems are fresh initialized
        And CspController state is subscribed for archiving
        And CspSubarray01 state is subscribed for archiving
        When CspController set adminmode to OFFLINE
        Then CspSubarray01 adminmode is OFFLINE
        And CspSubarray01 isCommunicating is False
        And CspController adminmode is OFFLINE
        And CspController isCommunicating is False
        And CspController state is DISABLE
        And CspSubarray01 state is DISABLE
        # to avoid CBF problem the adminmode has to
        # be set again to ONLINE
        When CspController set adminmode to ONLINE
        Then CspController isCommunicating is True
        And CspSubarray01 adminmode is ONLINE
        And CspSubarray01 isCommunicating is True
        And CspController pushed 4 archive events on state
        And CspSubarray01 pushed 5 archive events on state
        

    @pipeline @xfail @XTP-30222 
    Scenario: Test healthstate subscription on Controller and Subarray01
        Given All subsystems are fresh initialized
        And CspController healthstate is subscribed for archiving
        And CspSubarray01 healthstate is subscribed for archiving
        
        When CspController set adminmode to OFFLINE
        Then CspSubarray01 adminmode is OFFLINE
        And CspSubarray01 isCommunicating is False
        And CspController adminmode is OFFLINE
        And CspController isCommunicating is False
        And CspController healthState is UNKNOWN 
        And CspSubarray01 healthState is UNKNOWN
        # to avoid CBF problem the adminmode has to
        # be set again to ONLINEncel_future
        When CspController set adminmode to ONLINE
        Then CspController isCommunicating is True
        And CspSubarray01 adminmode is ONLINE
        And CspSubarray01 isCommunicating is True
        And CspController pushed 4 archive events on healthstate
        And CspSubarray01 pushed 4 archive events on healthstate
        

    @pipeline @XTP-30221 
    Scenario: Test obsState subscription on Subarray01
        Given All subsystems are fresh initialized
        And CspSubarray01 obsstate is subscribed for archiving
        
        When On Command is issued on CspController with argument []
        Then CspController longRunningCommandAttributes is (0, 'COMPLETED')
        And CspController state is ON
        And CspSubarray01 state is ON
        And CspSubarray01 obsState is EMPTY
        When AssignResources Command is issued on CspSubarray01 (argument from file: AssignResources_CBF.json)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED')
        And CspSubarray01 obsState is IDLE
        When ReleaseAllResources Command is issued on CspSubarray01 (no argument)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED')
        And CspSubarray01 obsState is EMPTY
        And CspSubarray01 pushed 5 archive events on obsstate
