
Feature: Csp-Lmc Low Subsystems abort
    Verify the behaviour of the abort command at all levels
    of the telescope state machine

    Background:
        Given All subsystems are fresh initialized with PST beams

# ------------- Abort after Assign resources-------------------
    @XTP-39922
    Scenario: Send Abort command to Subarray01 without PST beams from IDLE obstate

        When On Command is issued on CspController with argument []
        Then CspController longRunningCommandAttributes is (0, 'COMPLETED')
        And CspController CspCbfState is ON
        And CspController state is ON
        And CspSubarray01 cbfSubarrayState is ON
        And CspSubarray01 state is ON
        And CspSubarray01 cbfSubarrayObsState is EMPTY
        And CspSubarray01 obsState is EMPTY

        When AssignResources Command is issued on CspSubarray01 (argument from file: AssignResources_CBF.json)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED')
        And CspSubarray01 commandResult is ('assignresources', '0')
        And CspSubarray01 cbfSubarrayObsState is IDLE
        And CspSubarray01 obsState is IDLE

        When Abort Command is issued on CspSubarray01 (no argument)
        # get the end of configure
        #And CspSubarray01 longRunningCommandAttributes is (3, 'ABORTED')
        # get the end of Abort
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED')
        And CspSubarray01 obsState is ABORTED
        And CspSubarray01 state is ON
        And CbfSubarray01 obsState is ABORTED
        And CbfSubarray01 state is ON

# ------------- Abort after COnfigure -------------------
    Scenario: Send Abort command on Subarray01 without PST beams from READY obstate 

        When On Command is issued on CspController with argument []
        Then CspController longRunningCommandAttributes is (0, 'COMPLETED')
        And CspController CspCbfState is ON
        And CspController state is ON
        And CspSubarray01 cbfSubarrayState is ON
        And CspSubarray01 state is ON
        And CspSubarray01 cbfSubarrayObsState is EMPTY
        And CspSubarray01 obsState is EMPTY

        When AssignResources Command is issued on CspSubarray01 (argument from file: AssignResources_CBF.json)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED')
        And CspSubarray01 commandResult is ('assignresources', '0')
        And CspSubarray01 cbfSubarrayObsState is IDLE
        And CspSubarray01 obsState is IDLE

        When Configure Command is issued on CspSubarray01 (argument from file: Configure_CBF.json)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED') 
        And CspSubarray01 commandResult is ('configure', '0')
        And CspSubarray01 cbfSubarrayObsState is READY
        And CspSubarray01 obsState is READY

        When Abort Command is issued on CspSubarray01 (no argument)
        # get the end of configure
        #And CspSubarray01 longRunningCommandAttributes is (3, 'ABORTED')
        # get the end of Abort
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED')
        And CspSubarray01 obsState is ABORTED
        And CspSubarray01 state is ON
        And CbfSubarray01 obsState is ABORTED
        And CbfSubarray01 state is ON

# ------------- Abort after scan start -------------------
    Scenario: Set Scan on Subarray01 without PST beams and then abort

        When On Command is issued on CspController with argument []
        Then CspController longRunningCommandAttributes is (0, 'COMPLETED')
        And CspController CspCbfState is ON
        And CspController state is ON
        And CspSubarray01 cbfSubarrayState is ON
        And CspSubarray01 state is ON
        And CspSubarray01 cbfSubarrayObsState is EMPTY
        And CspSubarray01 obsState is EMPTY

        When AssignResources Command is issued on CspSubarray01 (argument from file: AssignResources_CBF.json)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED')
        And CspSubarray01 commandResult is ('assignresources', '0')
        And CspSubarray01 cbfSubarrayObsState is IDLE
        And CspSubarray01 obsState is IDLE

        When Configure Command is issued on CspSubarray01 (argument from file: Configure_CBF.json)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED') 
        And CspSubarray01 commandResult is ('configure', '0')
        And CspSubarray01 cbfSubarrayObsState is READY
        And CspSubarray01 obsState is READY

        When Scan Command is issued on CspSubarray01 (argument from file: Scan_CBF.json)
        #And CspSubarray01 commandResult is ('scan', '2')
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED')
        And CspSubarray01 ObsState is SCANNING
        And CbfSubarray01 ObsState is SCANNING

        When Abort Command is issued on CspSubarray01 (no argument)
        # get the end of configure
        #And CspSubarray01 longRunningCommandAttributes is (3, 'ABORTED')
        # get the end of Abort
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED')
        And CspSubarray01 obsState is ABORTED
        And CspSubarray01 state is ON
        And CbfSubarray01 obsState is ABORTED
        And CbfSubarray01 state is ON

# ------------- Excecute Abort after Scan start and then perform another scan -------------------
    Scenario: Abort a Scan and then perform another scan on Subarray01 without PST

        When On Command is issued on CspController with argument []
        Then CspController longRunningCommandAttributes is (0, 'COMPLETED')
        And CspController CspCbfState is ON
        And CspController state is ON
        And CspSubarray01 cbfSubarrayState is ON
        And CspSubarray01 state is ON
        And CspSubarray01 cbfSubarrayObsState is EMPTY
        And CspSubarray01 obsState is EMPTY

        When AssignResources Command is issued on CspSubarray01 (argument from file: AssignResources_CBF.json)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED')
        And CspSubarray01 commandResult is ('assignresources', '0')
        And CspSubarray01 cbfSubarrayObsState is IDLE
        And CspSubarray01 obsState is IDLE

        When Configure Command is issued on CspSubarray01 (argument from file: Configure_CBF.json)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED') 
        And CspSubarray01 commandResult is ('configure', '0')
        And CspSubarray01 cbfSubarrayObsState is READY
        And CspSubarray01 obsState is READY

        When Scan Command is issued on CspSubarray01 (argument from file: Scan_CBF.json)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED')
        And CspSubarray01 ObsState is SCANNING
        And CbfSubarray01 ObsState is SCANNING

        When Abort Command is issued on CspSubarray01 (no argument)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED')
        And CspSubarray01 obsState is ABORTED
        And CspSubarray01 state is ON
        And CbfSubarray01 obsState is ABORTED
        And CbfSubarray01 state is ON

        When Restart Command is issued on CspSubarray01 (no argument)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED')
        And CspSubarray01 cbfSubarrayObsState is EMPTY
        And CspSubarray01 obsState is EMPTY

        When AssignResources Command is issued on CspSubarray01 (argument from file: AssignResources_CBF.json)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED')
        And CspSubarray01 commandResult is ('assignresources', '0')
        And CspSubarray01 cbfSubarrayObsState is IDLE
        And CspSubarray01 obsState is IDLE

        When Configure Command is issued on CspSubarray01 (argument from file: Configure_CBF.json)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED') 
        And CspSubarray01 commandResult is ('configure', '0')
        And CspSubarray01 cbfSubarrayObsState is READY
        And CspSubarray01 obsState is READY

        When Scan Command is issued on CspSubarray01 (argument from file: Scan_CBF.json)
        And CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED')
        And CspSubarray01 ObsState is SCANNING
        And CbfSubarray01 ObsState is SCANNING

        When EndScan Command is issued on CspSubarray01 (no argument)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED') 
        And CspSubarray01 cbfSubarrayObsState is READY
        And CspSubarray01 obsState is READY

        When GoToIdle Command is issued on CspSubarray01 (no argument)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED') 
        And CspSubarray01 cbfSubarrayObsState is IDLE
        And CspSubarray01 obsState is IDLE

    @pipeline
    Scenario: Abort a Scan and Restart with PST Beam assigned

        When On Command is issued on CspController with argument []
        Then CspController longRunningCommandAttributes is (0, 'COMPLETED')
        And CspController CspCbfState is ON
        And CspController state is ON
        And CspSubarray01 cbfSubarrayState is ON
        And CspSubarray01 state is ON
        And CspSubarray01 cbfSubarrayObsState is EMPTY
        And CspSubarray01 obsState is EMPTY

        When AssignResources Command is issued on CspSubarray01 (argument from file: AssignResources_CBF_PST.json)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED')
        And CspSubarray01 commandResult is ('assignresources', '0')
        And CspSubarray01 cbfSubarrayObsState is IDLE
        And CspSubarray01 obsState is IDLE
        And CspSubarray01 assignedTimingBeamIDs is [1]

        When Configure Command is issued on CspSubarray01 (argument from file: Configure_CBF_PST_VR.json)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED') 
        And CspSubarray01 commandResult is ('configure', '0')
        And CspSubarray01 cbfSubarrayObsState is READY
        And CspSubarray01 obsState is READY

        When Scan Command is issued on CspSubarray01 (argument from file: Scan_CBF.json)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED')
        And CspSubarray01 ObsState is SCANNING
        And CbfSubarray01 ObsState is SCANNING

        When Abort Command is issued on CspSubarray01 (no argument)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED')
        And CspSubarray01 obsState is ABORTED
        And CspSubarray01 state is ON
        And CbfSubarray01 obsState is ABORTED
        And CbfSubarray01 state is ON

        When Restart Command is issued on CspSubarray01 (no argument)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED')
        And CspSubarray01 cbfSubarrayObsState is EMPTY
        And CspSubarray01 obsState is EMPTY
        And CspSubarray01 assignedTimingBeamIDs is []