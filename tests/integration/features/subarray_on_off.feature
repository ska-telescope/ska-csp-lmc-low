Feature: Csp-Lmc Low Subarray On and Off
    As the Csp-Lmc Subarray for Low telescope
    I want to turn On and Off Cbf Subarray
    and change my state accordingly

    Background:
        Given Subarray01 is fresh initialized


    Scenario: Csp Subarray is powered ON
        When On Command is issued on CspSubarray01 (no argument)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED') 
        And CspSubarray01 commandResult is ('on', '0')
        And CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED') 
        And CspSubarray01 state is ON
        And CbfSubarray01 state is ON
        And CspSubarray01 cbfSubarrayState is ON


    Scenario: Csp Subarray is powered OFF if ON/EMPTY
        When On Command is issued on CspSubarray01 (no argument)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED') 
        And CspSubarray01 state is ON
        And CbfSubarray01 state is ON
        And CspSubarray01 ObsState is EMPTY
        And CbfSubarray01 ObsState is EMPTY

        When Off Command is issued on CspSubarray01 (no argument)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED') 
        And CspSubarray01 commandResult is ('off', '0')
        And CspSubarray01 state is OFF
        And CbfSubarray01 state is OFF
        And CspSubarray01 cbfSubarrayState is OFF


    Scenario: Csp Subarray is powered OFF if only CBF is IDLE
        When On Command is issued on CspSubarray01 (no argument)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED') 
        And CspSubarray01 state is ON
        And CbfSubarray01 state is ON

        When AssignResources Command is issued on CbfSubarray01 (argument from file: AssignResources_CBF.json)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED') 
        And CbfSubarray01 ObsState is IDLE
        And CspSubarray01 ObsState is IDLE

        When Off Command is issued on CspSubarray01 (no argument)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED') 
        And CspSubarray01 commandResult is ('off', '0')
        And CspSubarray01 state is OFF
        And CbfSubarray01 state is OFF
        And CspSubarray01 cbfSubarrayState is OFF
        And CbfSubarray01 ObsState is EMPTY
        And CbfSubarray01 ObsState is EMPTY


    Scenario: Csp Subarray OFF if IDLE
        And Csp Subarray setup as IDLE with random resources

        When Off Command is issued on CspSubarray01 (no argument)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED') 
        And CspSubarray01 commandResult is ('off', '0')
        And CspSubarray01 state is OFF
        And CbfSubarray01 state is OFF
        And CspSubarray01 cbfSubarrayState is OFF
        And CbfSubarray01 ObsState is EMPTY
        And CbfSubarray01 ObsState is EMPTY


    Scenario: Csp Subarray OFF if READY
        And Csp Subarray setup as READY with random resources

        When Off Command is issued on CspSubarray01 (no argument)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED') 
        And CspSubarray01 commandResult is ('off', '0')
        And CspSubarray01 state is OFF
        And CbfSubarray01 state is OFF
        And CspSubarray01 cbfSubarrayState is OFF
        And CbfSubarray01 ObsState is EMPTY
        And CbfSubarray01 ObsState is EMPTY


    Scenario: Csp Subarray OFF if SCANNING
        And Csp Subarray setup as SCANNING with random resources

        When Off Command is issued on CspSubarray01 (no argument)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED') 
        And CspSubarray01 commandResult is ('off', '0')
        And CspSubarray01 state is OFF
        And CbfSubarray01 state is OFF
        And CspSubarray01 cbfSubarrayState is OFF
        And CbfSubarray01 ObsState is EMPTY
        And CbfSubarray01 ObsState is EMPTY


    Scenario: Csp Subarray OFF if ABORTED
        And Csp Subarray setup as ABORTED with random resources

        When Off Command is issued on CspSubarray01 (no argument)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED') 
        And CspSubarray01 commandResult is ('off', '0')
        And CspSubarray01 state is OFF
        And CbfSubarray01 state is OFF
        And CspSubarray01 cbfSubarrayState is OFF
        And CbfSubarray01 ObsState is EMPTY
        And CbfSubarray01 ObsState is EMPTY
