Feature: Csp-Lmc Low Capability attributes
    As the Csp-Lmc Capability for Low telescope
    I want to read basic attributes

    # @pipeline
    # Scenario: Csp capability reports HealthState
    #     Given All subsystems are fresh initialized
    #     And CspCapability isCommunicating is True
    #     # Current implementation sets HealtState to UNKNOWN if no connector 
    #     # or processor could be configured
    #     And CspCapability set adminmode to ONLINE
    #     Then CspCapability HealthState is UNKNOWN

    @pipeline
    Scenario: Csp capability reports HealthState
        Given All subsystems are fresh initialized
        And CspCapabilityPst isCommunicating is True
        # Current implementation sets HealtState to UNKNOWN if no connector 
        # or processor could be configured
        And CspCapabilityPst set adminmode to ONLINE
        Then CspCapabilityPst HealthState is UNKNOWN
