import logging
import time

import numpy as np
import pytest
import tango
from ska_control_model import ObsState

module_logger = logging.getLogger(__name__)


def push_event(evt):
    if evt.attr_value.name == "obsstate":
        module_logger.info(
            f"Received event at time  {time.asctime()} "
            f"{ObsState(evt.attr_value.value).name}"
        )
    if evt.attr_value.name == "longrunningcommandstatus":
        module_logger.info(
            f"Received event at time  {time.asctime()} {evt.attr_value.value}"
        )


def wait_for(proxy, attribute, value):
    time_start = time.time()
    if attribute.lower() == "obsstate":
        module_logger.info(
            f"Start waiting for value = {ObsState(value).name}"
            f" at {time.asctime()}{ time.time()}"
        )
    else:
        module_logger.info(
            f"Start waiting for value = {value} at"
            f" {time.asctime()}{ time.time()}"
        )
    while time.time() - time_start < 5:
        attr_val = proxy.read_attribute(attribute).value
        if attr_val == value:
            module_logger.info(
                f"Condition verified at {time.asctime()} {time.time()}"
            )
            return
        time.sleep(0.1)
    module_logger.info(f"Timeout reading attribute {attribute}!!")


def wait_for_lrc(proxy, attribute, value, command_id):
    time_start = time.time()
    module_logger.info(
        f"Start checking {attribute} at {time.asctime()}{ time.time()}"
    )
    while time.time() - time_start < 5:
        attr_val = proxy.read_attribute(attribute).value
        if command_id in attr_val:
            idx = attr_val.index(command_id)
            if attr_val[idx + 1] == "COMPLETED":
                module_logger.info(
                    f"Stop checking value at {time.asctime()} {time.time()}"
                )
                break
        time.sleep(0.1)


@pytest.mark.bc
def test_bc():
    """
    Run the sequence of commands assign-abort-restart with different delays
    between the assign and the abort.
    """
    csp_ctrl = tango.DeviceProxy("low-csp/control/0")
    csp_sub1 = tango.DeviceProxy("low-csp/subarray/01")
    csp_sub1.subscribe_event(
        "obsstate", tango.EventType.CHANGE_EVENT, push_event
    )
    # csp_sub1.subscribe_event('longrunningcommandstatus',
    #             tango.EventType.CHANGE_EVENT, push_event)
    csp_sub1.logginglevel = 5
    if csp_ctrl.adminmode:
        csp_ctrl.adminmode = 0
        module_logger.info("Wait for setup communication")
        wait_for(csp_ctrl, "isCommunicating", True)
        wait_for(csp_sub1, "isCommunicating", True)
        wait_for(csp_sub1, "state", tango.DevState.ON)
        wait_for(csp_ctrl, "state", tango.DevState.ON)

    assign_json = """
    {
        "interface": "https://schema.skao.int/ska-low-csp-assignresources/3.0",
        "common": {
            "subarray_id": 1
        },
        "lowcbf":{}
    }
    """
    sleep_times = np.arange(0.300, 0.550, 0.001).tolist()
    for t in sleep_times:
        module_logger.info(f"Sleeping for {t:.4f} sec")
        csp_sub1.assignresources(assign_json)
        time.sleep(t)
        csp_sub1.abort()
        wait_for(csp_sub1, "obsstate", 7)
        # time.sleep(0.5)
        module_logger.info("Invoking restart")
        _, command_id = csp_sub1.restart()
        # wait_for_lrc(
        #    csp_sub1, "longrunningcommandstatus", "COMPLETED", command_id[0]
        # )
        wait_for(csp_sub1, "obsstate", 0)
