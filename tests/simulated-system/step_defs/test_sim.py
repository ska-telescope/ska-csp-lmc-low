import json
import logging
import time

import pytest
from pytest_bdd import given, parsers, scenarios, then, when
from ska_control_model import AdminMode
from ska_csp_lmc_common.testing.poller import probe_poller
from tango import Database, DeviceProxy, DevState

from ska_csp_lmc_low.testing.test_bdd_utils import (
    LowTestDevice,
    are_pst_monitoring_data_acquired,
    fresh_all_new_subsystems,
    is_pst_acquiring_data,
)

module_logger = logging.getLogger(__name__)


#  -------------- SCENARIOS --------------

scenarios("../features/happy_paths_low.feature")
scenarios("../features/controller_healthstate.feature")
scenarios("../features/subarray_healthstate.feature")
scenarios("../features/pst_capability.feature")
scenarios("../features/fsp_capability.feature")
scenarios("../features/pst_beam_unhappy_path.feature")

#  -------------- MARKERS --------------


def pytest_configure(config):
    config.addinivalue_line("markers", "thisone")
    config.addinivalue_line("markers", "pipeline")
    config.addinivalue_line("markers", "all")
    config.addinivalue_line("markers", "stress")


# -------------- FIXTURES --------------

temp_dev_info = None

repository = "low"
test_context = "sim"


def get_list_of_devices_from_db():
    """Retrieve a list of all deployed CSP devices from Tango Database."""
    try:
        database = Database()
        device_list = database.get_device_exported("*")
        filtered_list = [
            element
            for element in device_list
            if "dserver" not in element and "sys" not in element
        ]
    # pylint: disable-next=broad-except
    except Exception as e:
        module_logger.error(f"Error {e}")
        filtered_list = []
    return filtered_list


def get_lists_of_devices(list_of_device_fqdns):
    """Create a separate list of devices for each device type."""
    controller_names = []
    subarray_names = []
    beam_names = []
    capability_names = []
    for element in list_of_device_fqdns:
        name = "none"
        element = element.lower()
        split_fqdn = element.split("/")
        device_type = split_fqdn[-3] if len(split_fqdn) > 1 else ""
        device_id = split_fqdn[-1]
        if "control" in element:
            if "csp" in element:
                name = "LowCspController"
                controller_names.append(name)
            else:
                name = (
                    f"Low{device_type.split('-')[-1].capitalize()}"
                    f"CtrlSimulator"
                )
                controller_names.append(name)
        elif "subarray" in element:
            if "csp" in element.lower():
                name = f"LowCspSubarray{device_id}"
                subarray_names.append(name)
            else:
                name = (
                    f"Low{device_type.split('-')[-1].capitalize()}"
                    f"SubarraySimulator{device_id}"
                )
                subarray_names.append(name)
        elif "beam" in element:
            name = f"PstBeamSimulatorDevice{device_id}"
            beam_names.append(name)
        elif "capability-fsp" in element:
            name = "LowCspCapabilityFsp"
            capability_names.append(name)
        elif "capability-pst" in element:
            name = "LowCspCapabilityPst"
            capability_names.append(name)
        module_logger.info(f"Adding element {name} to the list of devices")
    return controller_names, subarray_names, beam_names, capability_names


list_of_device_fqdns = get_list_of_devices_from_db()
(
    list_of_controller_names,
    list_of_subarray_names,
    list_of_beam_names,
    list_of_capability_names,
) = get_lists_of_devices(list_of_device_fqdns)


@pytest.fixture(scope="session")
def all_controllers():
    controllers = dict()
    for name in list_of_controller_names:
        module_logger.info(f"Creating proxy for {name}")
        idx = (
            name.replace("Simulator", "")
            .replace("Low", "")
            .replace("Ctrl", "Controller")
        )
        controllers[idx] = LowTestDevice(name, repository, test_context)
    return controllers


@pytest.fixture(scope="session")
def all_subarrays():
    subarrays = dict()
    for name in list_of_subarray_names:
        module_logger.info(f"Creating proxy for {name}")
        idx = name.replace("Simulator", "").replace("Low", "")
        subarrays[idx] = LowTestDevice(name, repository, test_context)
    return subarrays


@pytest.fixture(scope="session")
def all_beams():
    beams = dict()
    for name in list_of_beam_names:
        module_logger.info(f"Creating proxy for {name}")
        idx = name.replace("SimulatorDevice", "")
        beams[idx] = LowTestDevice(name, repository, test_context)
    return beams


@pytest.fixture(scope="session")
def all_capabilities():
    capabilities = dict()
    for name in list_of_capability_names:
        module_logger.info(f"Creating proxy for {name}")
        idx = name.replace("SimulatorDevice", "").replace("Low", "")
        capabilities[idx] = LowTestDevice(name, repository, test_context)
    return capabilities


# @pytest.fixture(scope="session")
# def all_capabilities():
#     capabilities = dict()
#     return capabilities


@given("All subsystems are fresh initialized")
def fresh_new_subsystems(
    all_controllers, all_subarrays, all_beams, all_capabilities
):
    """Initialize all subsystems of the deployment."""

    for device in all_capabilities.values():
        # TODO: reinizialize capabilities
        module_logger.info(f"{device} is not reinitialized")

    fresh_all_new_subsystems(all_controllers, all_subarrays, all_beams)


@then(parsers.parse("{device_name} {attribute} acquires data on {beam_name}"))
def check_pst_is_acquiring_data(
    device_name, attribute, beam_name, all_capabilities, all_beams
):
    """Check that both the beam attribute (i.e. dataReceived) and the
    capability element of the list (i.e devicesDataReceived).
    right now this step is done for the following attributes:
    [devicesDataDropRate, devicesDataReceived,
    devicesDataReceiveRate, devicesDataDropped]
    """
    return_value = is_pst_acquiring_data(
        device_name, attribute, beam_name, all_capabilities, all_beams
    )

    return return_value


@when(parsers.parse("{device_name} raise a command fault"))
def raise_command_fault_flag(
    device_name, all_controllers, all_subarrays, all_beams
):
    all_subsystems = {**all_controllers, **all_subarrays, **all_beams}
    device = all_subsystems[device_name]
    device.set_command_faulty(True)
    module_logger.info(f"{device_name} raise command fault flag")


@then(parsers.parse("{device_name} reset a command fault"))
def reset_command_fault_flag(
    device_name, all_controllers, all_subarrays, all_beams
):
    all_subsystems = {**all_controllers, **all_subarrays, **all_beams}
    device = all_subsystems[device_name]
    device.set_command_faulty(False)
    module_logger.info(f"{device_name} reset command fault flag")


@then(parsers.parse("{device_name} {attribute} calculated for {beam_name}"))
def check_pst_monitoring_data_are_acquired(
    device_name, attribute, beam_name, all_capabilities, all_beams
):
    """Check that both the beam attribute (i.e. dataReceived) and the
    capability element of the list (i.e devicesDataReceived).
    right now this step is done for the following attributes:
    [devicesDataDropRate, devicesDataReceived,
    devicesDataReceiveRate, devicesDataDropped]
    """
    return_value = are_pst_monitoring_data_acquired(
        device_name, attribute, beam_name, all_capabilities, all_beams
    )

    return return_value


@when(
    (parsers.parse("{processor} is registered as FSP{id} in the CbfAllocator"))
)
def register_processor_in_allocator(processor, id):
    """Reigister the indicated processor as FSP in the allocator simulator"""
    module_logger.info(f"Registering {processor} in CBF Allocator")
    cbf_a = DeviceProxy("sim-low-cbf/allocator/0")
    if id == "01":
        serial_nr = "XFL1RCFEG244"
    elif id == "02":
        serial_nr = "XFL1E35JVJTQ"

    cbf_a.ForceRegisterFsps(
        f'{{"serial": ["{serial_nr}"],"fqdn":  "{processor}"}}'
    )
    time.sleep(2)

    # for some reason the attribute has single quotes and can't be
    # loaded as json
    procdevfqdn_dict = json.loads(cbf_a.procdevfqdn.replace("'", '"'))
    fsps_dict = json.loads(cbf_a.fsps.replace("'", '"'))

    assert procdevfqdn_dict[serial_nr] == processor
    assert fsps_dict[f"fsp_{id}"] == [serial_nr]
    # probe_poller(
    #     cbf_a, "procdevfqdn", f"{{'{serial_nr}': '{processor}'}}", time=2
    # )
    # assert cbf_a.fsps == f"{{'fsp_{id}': ['{serial_nr}']}}"


@then(
    parsers.parse(
        "information on FSP{id} is reported by CspCapabilityFsp devicesJson"
    )
)
def check_information_in_json(id):
    """Check that all information on the requested FSP
    are present in the json"""
    module_logger.info(
        f"Check that information on FSP{id} is reported in Json"
    )
    fsp_c = DeviceProxy("low-csp/capability-fsp/0")
    deviceJson_dict = json.loads(fsp_c.devicesJson)
    for item in deviceJson_dict["fsp"]:
        if item["dev_id"] == int(id):
            assert item["fqdn"]
            assert item["state"]
            assert item["health_state"]
            assert item["admin_mode"]
            assert item["serialnumber"]
            assert item["ready"]
            # firmware and subarray_ids could be empty strings
            assert "subarray_ids" in item.keys()
            assert "firmware" in item.keys()
            assert item["pkts_in"]
            assert item["pkts_out"]


@when(parsers.parse("{processor} is set to ONLINE"))
def set_online_processors(processor):
    """Set the adminmode of the emulated cbf processor to online"""
    module_logger.info(f"Setting Adminmode=ONLINE for {processor}")
    cbf_p = DeviceProxy(processor)
    cbf_p.adminmode = 0

    probe_poller(cbf_p, "state", DevState.ON, time=3)
    probe_poller(cbf_p, "adminmode", AdminMode.ONLINE, time=3)


@when(
    parsers.parse(
        '{processor} is configured with "{firmware}"'
        " firmware from subarray {sub_id}"
    )
)
def configure_processor(processor, firmware, sub_id):
    """Emulate the cbf processor configuration to be successful with a specific
    firmware downloaded"""
    cbf_p = DeviceProxy(processor)
    module_logger.info(f"Configuring {processor}")
    # processor emulator's adminmode is not set to ONLINE by default (TBD)
    # if we don't do state will be DISABLE and the command rejeceted

    cbf_p.configure(
        f'{{"ready": "true", "firmware": "{firmware}", '
        f'"subarray_ids": {sub_id}}}'
    )
    probe_poller(cbf_p, "subarrayids", [int(sub_id)], time=3)
    assert json.loads(cbf_p.stats_mode)["ready"] == "true"
    assert json.loads(cbf_p.stats_mode)["firmware"] == firmware


@when(parsers.parse("data are flowing through {processor}"))
def data_through_processor(processor):
    """Emulate the data flow throught processor by increasing stats_io"""
    module_logger.info(f"Starting Scan on {processor}")
    cbf_p = DeviceProxy(processor)

    cbf_p.scan("1")
    time.sleep(20)

    assert check_dict_entries_increase_in_tango_json_attr(
        cbf_p, "stats_io", ["total_pkts_in", "total_pkts_out"]
    )

    # assert stats_io_dict["total_pkts_in"] is not 0
    # assert stats_io_dict["total_pkts_out"] is not 0


@then(
    parsers.parse(
        "CspCapabilityFsp reports an increase in pkts_in"
        " and pkts_out for {processor}"
    )
)
def data_increase_reported_in_capability(processor):
    """Check that data rate is increased in capability Json attribute"""
    module_logger.info(f"Checking data flow on {processor}")
    fsp_c = DeviceProxy("low-csp/capability-fsp/0")
    deviceJson_dict = json.loads(fsp_c.devicesJson)
    for i in range(len(deviceJson_dict["fsp"])):
        if deviceJson_dict["fsp"][i]["fqdn"] == processor:
            dict_exp_add = f'["fsp"][{i}]'
            check_dict_entries_increase_in_tango_json_attr(
                fsp_c, "devicesJson", ["pkts_in", "pkts_out"], dict_exp_add
            )


def check_dict_entries_increase_in_tango_json_attr(
    proxy, attr, entries, dict_exp_add=None, timeout=30
):
    """Utility to check if numerical entries of a tango json attribute,
    on a specific proxy, increases over time"""
    start_time = time.time()
    dict_exp = (
        """json.loads(proxy.read_attribute(attr).value.replace("'", '"'))"""
    )
    if dict_exp_add:
        dict_exp = f"{dict_exp}{dict_exp_add}"
    old_attr_dict = eval(dict_exp)
    while (time.time() - start_time) < timeout:
        new_attr_dict = eval(dict_exp)
        for entry in entries:
            if new_attr_dict[entry] > old_attr_dict[entry]:
                check = True
            else:
                check = False
        if check:
            return True
        time.sleep(0.5)
    return False
