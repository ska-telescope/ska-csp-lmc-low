Feature: Csp-Lmc Low Controller reports and aggregates healthState
      As the Csp-Lmc Controller for Low telescope
      I want to collect healthState of connected devices and resources
      aggregate it into healthState of a Csp-Lmc Low Controller
      and report it to TMC

    @pipeline @healthstate
    Scenario: Low Csp Controller aggregates healthstate
        Given All subsystems are fresh initialized
        And CspController isCommunicating is True
        And On Command is issued on CspController with argument []
        And CspController longRunningCommandAttributes is (0, 'COMPLETED') 

        When CbfController HealthState are sent straight to <cbf_health>
        And CbfController HealthState is <cbf_health> 
        And CbfSubarray01 HealthState are sent straight to <cbf_sub01_health>
        And CbfSubarray01 HealthState is <cbf_sub01_health> 
        And CbfSubarray02 HealthState are sent straight to <cbf_sub02_health>
        And CbfSubarray02 HealthState is <cbf_sub02_health>
        And CbfSubarray03 HealthState are sent straight to <cbf_sub03_health>
        And CbfSubarray03 HealthState is <cbf_sub03_health>
        #And PssController HealthState are sent straight to <pss_health>
        #And PssController HealthState is <pss_health> 
        #And PssSubarray01 HealthState are sent straight to <pss_sub01_health>
        #And PssSubarray01 HealthState is <pss_sub01_health>
        #And PssSubarray02 HealthState are sent straight to <pss_sub02_health>
        #And PssSubarray02 HealthState is <pss_sub02_health>
        #And PssSubarray03 HealthState are sent straight to <pss_sub03_health>    
        #And PssSubarray03 HealthState is <pss_sub03_health>      
        And PstBeam01 HealthState are sent straight to <pst_beam01_health> 
        And PstBeam01 HealthState is <pst_beam01_health>
        And PstBeam02 HealthState are sent straight to <pst_beam02_health> 
        And PstBeam02 HealthState is <pst_beam02_health>
        And PstBeam03 HealthState are sent straight to <pst_beam03_health>   
        And PstBeam03 HealthState is <pst_beam03_health>         
        Then CspController HealthState is <csp_health>
        Examples:
        | cbf_health  | cbf_sub01_health | cbf_sub02_health | cbf_sub03_health | pst_beam01_health | pst_beam02_health | pst_beam03_health | csp_health |
        | OK          | OK               | OK               |OK                | OK                | OK                | OK                | OK         | 
        | DEGRADED    | OK               | OK               |OK                | OK                | OK                | OK                | DEGRADED   | 
        | FAILED      | OK               | OK               |OK                | OK                | OK                | OK                | FAILED     |
        | UNKNOWN     | OK               | OK               |OK                | OK                | OK                | OK                | UNKNOWN    |
        | OK          | FAILED           | OK               |OK                | OK                | OK                | OK                | DEGRADED   | 
        | OK          | DEGRADED         | OK               |OK                | OK                | OK                | OK                | DEGRADED   |   
        #| OK          | UNKNOWN          | OK               |OK                | OK                | OK                | OK                | DEGRADED   |
        | OK          | OK               | OK               |OK                | FAILED            | OK                | OK                | DEGRADED   |
        | OK          | OK               | OK               |OK                | DEGRADED          | OK                | OK                | DEGRADED   |
        | OK          | OK               | OK               |OK                | UNKNOWN           | OK                | OK                | DEGRADED   |                                           
                    