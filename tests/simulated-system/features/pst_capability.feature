Feature: Csp-Lmc Low Pst Capability reports information about Pst beams

    @pipeline 
    Scenario: Test all commands on Subarray01 with PST beam and PST capability
        Given All subsystems are fresh initialized
        And CspController isCommunicating is True
        And CspController adminmode is ONLINE
        
        # And CbfProcessor is subscribed by the CbfAllocator
        When On Command is issued on CspController with argument []
        Then CspController longRunningCommandAttributes is (0, 'COMPLETED')
        And CspSubarray01 state is ON
        And CbfSubarray01 state is ON
        And CspSubarray01 ObsState is EMPTY
        And CbfSubarray01 ObsState is EMPTY
        And PstBeam01 state is ON
        And PstBeam02 state is ON
        And PstBeam03 state is ON
        And CspCapabilityPst devicesState is "ON", "ON", "ON"
        And CspController CspCbfState is ON
        And CspController state is ON
        And CspSubarray01 cbfSubarrayState is ON
        And CspSubarray01 cbfSubarrayObsState is EMPTY
        And CspSubarray01 obsMode is (ObsMode.IDLE,)
        And CspCapabilityPst state is ON
        And CspCapabilityPst healthState is OK
        And CspCapabilityPst AdminMode is ONLINE
        And CspCapabilityPst devicesDeployed is 3
        And CspCapabilityPst devicesFqdn is ("sim-low-pst/beam/01", "sim-low-pst/beam/02", "sim-low-pst/beam/03")
        And CspCapabilityPst devicesHealthState is "OK", "OK", "OK",
        And CspCapabilityPst devicesAdminMode is "ONLINE", "ONLINE", "ONLINE",
        # And CspCapabilityPst devicesId is [1]
        And PstBeam01 dataReceived is 0
        And CspCapabilityPst devicesDataReceived is [0, 0, 0]
        And PstBeam01 dataReceiveRate is 0
        And CspCapabilityPst devicesDataReceiveRate is [0.0, 0.0, 0.0]
        And PstBeam01 dataDropped is 0
        And CspCapabilityPst devicesDataDropped is [0, 0, 0]
        And PstBeam01 dataDropRate is 0
        And CspCapabilityPst devicesDataDropRate is [0.0, 0.0, 0.0]
        And PstBeam01 pstProcessingMode is "IDLE"
        And PstBeam02 pstProcessingMode is "IDLE"
        And PstBeam03 pstProcessingMode is "IDLE"
        And PstBeam01 obsState is IDLE
        And PstBeam02 obsState is IDLE
        And CspCapabilityPst devicesObsState is "IDLE", "IDLE", "IDLE"
        And CspCapabilityPst devicesPstProcessingMode is "IDLE","IDLE","IDLE"

        When AssignResources Command is issued on CspSubarray01 (argument from file: AssignResources_CBF_PST.json)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED')
        And CspSubarray01 commandResult is ('assignresources', '0')
        And CspSubarray01 obsState is IDLE
        And PstBeam01 obsState is IDLE
        And CspSubarray01 obsMode is (ObsMode.IDLE,)
        And CspCapabilityPst devicesObsState is "IDLE", "IDLE", "IDLE"
        And CspCapabilityPst devicesPstProcessingMode is "IDLE", "IDLE", "IDLE"

        When Configure Command is issued on CspSubarray01 (argument from file: Configure_CBF_PST_VR.json)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED') 
        And CspSubarray01 commandResult is ('configure', '0')
        And PstBeam01 obsState is READY
        And CspSubarray01 cbfSubarrayObsState is READY
        And CspSubarray01 obsState is READY
        And CspCapabilityPst devicesObsState is "READY", "IDLE", "IDLE"
        And CspCapabilityPst devicesPstProcessingMode is "VOLTAGE_RECORDER", "IDLE", "IDLE"
        And CspCapabilityPst devicesDataReceived is [0, 0, 0]
        And CspCapabilityPst devicesDataReceiveRate is [0.0, 0.0, 0.0]
        And CspCapabilityPst devicesDataDropped is [0, 0, 0]
        And CspCapabilityPst devicesDataDropRate is [0.0, 0.0, 0.0]
        # Moved to end of line for timing issues
        And CspSubarray01 obsMode is (ObsMode.PULSAR_TIMING,)

        When Scan Command is issued on CspSubarray01 (argument from file: Scan_CBF.json)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED')
        And CspSubarray01 cbfSubarrayObsState is SCANNING
        And CspSubarray01 obsState is SCANNING
        And PstBeam01 obsState is SCANNING
        And CspSubarray01 obsMode is ( ObsMode.PULSAR_TIMING,)
        And PstBeam01 pstProcessingMode is "VOLTAGE_RECORDER"
        And CspCapabilityPst devicesObsState is "SCANNING", "IDLE", "IDLE"
        And CspCapabilityPst devicesPstProcessingMode is "VOLTAGE_RECORDER", "IDLE", "IDLE"
        
        And CspCapabilityPst devicesDataReceived acquires data on PstBeam01
        And CspCapabilityPst devicesDataReceiveRate acquires data on PstBeam01
        And CspCapabilityPst devicesDataDropped acquires data on PstBeam01
        And CspCapabilityPst devicesDataDropRate acquires data on PstBeam01

        And CspCapabilityPst devicesDataReceiveRateStddev calculated for PstBeam01
        And CspCapabilityPst devicesDataReceiveRateAvg calculated for PstBeam01
        And CspCapabilityPst devicesDataDropRateStddev calculated for PstBeam01
        And CspCapabilityPst devicesDataDropRateAvg calculated for PstBeam01

        When EndScan Command is issued on CspSubarray01 (no argument)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED')
        And CspSubarray01 cbfSubarrayObsState is READY
        And CspSubarray01 obsState is READY
        And PstBeam01 obsState is READY
        And CspSubarray01 obsMode is (ObsMode.PULSAR_TIMING,)
        And PstBeam01 pstProcessingMode is "VOLTAGE_RECORDER"
        And CspCapabilityPst devicesPstProcessingMode is "VOLTAGE_RECORDER", "IDLE", "IDLE"
        And CspCapabilityPst devicesObsState is "READY", "IDLE", "IDLE"
        And CspCapabilityPst devicesDataReceived is [0, 0, 0]
        And CspCapabilityPst devicesDataReceiveRate is [0.0, 0.0, 0.0]
        And CspCapabilityPst devicesDataDropped is [0, 0, 0]
        And CspCapabilityPst devicesDataDropRate is [0.0, 0.0, 0.0]

        When GoToIdle Command is issued on CspSubarray01 (no argument)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED') 
        And CspSubarray01 cbfSubarrayObsState is IDLE
        And CspSubarray01 obsState is IDLE
        And PstBeam01 obsState is IDLE
        And CspSubarray01 obsMode is (ObsMode.IDLE,)
        And PstBeam01 pstProcessingMode is "IDLE"
        And CspCapabilityPst devicesObsState is "IDLE", "IDLE", "IDLE"
        And CspCapabilityPst devicesPstProcessingMode is "IDLE", "IDLE", "IDLE"

        When ReleaseAllResources Command is issued on CspSubarray01 (no argument)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED') 
        And CspSubarray01 obsState is EMPTY

