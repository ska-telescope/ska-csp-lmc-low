Feature: Csp-Lmc Low Fsp Capability reports information about Fsp processors

    @pipeline 
    Scenario: Test that FSP capability is able to report that processors are configured and data are flowing

        Given All subsystems are fresh initialized
        And CbfAllocator isCommunicating is True
        And CspCapabilityFsp AdminMode is ONLINE
        And CspCapabilityFsp devicesDeployed is 0

        When sim-low-cbf/processor/0.0.0 is registered as FSP01 in the CbfAllocator
        Then information on FSP01 is reported by CspCapabilityFsp devicesJson
        And CspCapabilityFsp devicesDeployed is 1
        And CspCapabilityFsp devicesFqdn is ("sim-low-cbf/processor/0.0.0",)
        And CspCapabilityFsp devicesId is [1]
        And CspCapabilityFsp devicesState is ("DISABLE",)
        And CspCapabilityFsp devicesAdminmode is ("OFFLINE",)
        
        When sim-low-cbf/processor/0.0.1 is registered as FSP02 in the CbfAllocator
        Then information on FSP02 is reported by CspCapabilityFsp devicesJson
        And CspCapabilityFsp devicesDeployed is 2
        And CspCapabilityFsp devicesFqdn is ("sim-low-cbf/processor/0.0.0", "sim-low-cbf/processor/0.0.1")
        And CspCapabilityFsp devicesId is [1, 2]
        And CspCapabilityFsp devicesState is ("DISABLE", "DISABLE")
        And CspCapabilityFsp devicesAdminmode is ("OFFLINE", "OFFLINE")

        When sim-low-cbf/processor/0.0.0 is set to ONLINE
        Then CspCapabilityFsp devicesState is ("ON", "DISABLE")
        And CspCapabilityFsp devicesAdminmode is ("ONLINE", "OFFLINE")

        When sim-low-cbf/processor/0.0.1 is set to ONLINE
        Then CspCapabilityFsp devicesState is ("ON", "ON")
        And CspCapabilityFsp devicesAdminmode is ("ONLINE", "ONLINE")

        When sim-low-cbf/processor/0.0.0 is configured with "vis0.1.0" firmware from subarray 1
        Then CspCapabilityFsp devicesReady is ("true", "false")
        And CspCapabilityFsp devicesFirmware is ("vis0.1.0", "")
        And CspCapabilityFsp devicesSubarrayIds is ("1", "")

        When sim-low-cbf/processor/0.0.1 is configured with "pst0.1.0" firmware from subarray 2
        Then CspCapabilityFsp devicesReady is ("true", "true")
        And CspCapabilityFsp devicesFirmware is ("vis0.1.0", "pst0.1.0")
        And CspCapabilityFsp devicesSubarrayIds is ("1", "2")

        When data are flowing through sim-low-cbf/processor/0.0.0
        And data are flowing through sim-low-cbf/processor/0.0.1
        Then CspCapabilityFsp reports an increase in pkts_in and pkts_out for sim-low-cbf/processor/0.0.0
        And CspCapabilityFsp reports an increase in pkts_in and pkts_out for sim-low-cbf/processor/0.0.1






