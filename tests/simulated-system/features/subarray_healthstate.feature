
Feature: Csp-Lmc Low Subarray reports and aggregates healthState
      As the Csp-Lmc Subarray for Low telescope
      I want to collect healthState of connected devices and resources
      aggregate it into healthState of a Csp-Lmc Low subarray
      and report it to TMC

    # add a test that checks healthstate on csp before admin mode is online!

    @healthstate
    Scenario: Low Csp Subarray aggregates healthstate
        Given All subsystems are fresh initialized
        And CspController adminmode is ONLINE

        When CbfSubarray01 HealthState are sent straight to <cbf_health> 
        And CbfSubarray01 HealthState is <cbf_health> 
        #And PssSubarray01 HealthState are sent straight to <pss_health>  
        #And PssSubarray01 HealthState is <pss_health>
        And PstBeam01 HealthState are sent straight to <pst_health> 
        And PstBeam01 HealthState is <pst_health>
        
        And On Command is issued on CspSubarray01 (no argument)
        And CspSubarray01 state is ON

        Then CspSubarray01 HealthState is <csp_health>
        Examples:
        | cbf_health  | pst_health | csp_health    |
        | OK          | OK         | OK            |
        | FAILED      | OK         | FAILED        |
        | DEGRADED    | OK         | DEGRADED      |
        | UNKNOWN     | OK         | UNKNOWN       | 
        | OK          | FAILED     | OK            |
        | OK          | DEGRADED   | OK            |
        | OK          | UNKNOWN    | OK            |        
        | FAILED      | DEGRADED   | FAILED        |
        | FAILED      | UNKNOWN    | FAILED        |
        | DEGRADED    | UNKNOWN    | DEGRADED      | 
        | UNKNOWN     | DEGRADED   | DEGRADED      |         
        | UNKNOWN     | FAILED     | UNKNOWN       | 
        | DEGRADED    | FAILED     | DEGRADED      |       
        | DEGRADED    | DEGRADED   | DEGRADED      |     
        | UNKNOWN     | UNKNOWN    | DEGRADED      | 
        | FAILED      | FAILED     | FAILED        |           
 

    @healthstate
    Scenario: Low Csp Subarray aggregates healthstate with assigned resources
        Given All subsystems are fresh initialized
        And CspController adminmode is ONLINE
        And On Command is issued on CspSubarray01 (no argument)
        And CspSubarray01 state is ON
        And AssignResources Command is issued on CspSubarray01 (argument from file: AssignResources_CBF_PSS_PST.json)
        And CspSubarray01 obsState is IDLE           

        When CbfSubarray01 HealthState are sent straight to <cbf_health> 
        And CbfSubarray01 HealthState is <cbf_health> 
        And PstBeam01 HealthState are sent straight to <pst_health> 
        And PstBeam01 HealthState is <pst_health>

        Then CspSubarray01 HealthState is <csp_health>
        Examples:
        | cbf_health  | pst_health | csp_health    |
        | OK          | OK         | OK            |
        | FAILED      | OK         | FAILED        |
        | DEGRADED    | OK         | DEGRADED      |
        | UNKNOWN     | OK         | UNKNOWN       |
        | OK          | FAILED     | DEGRADED      |
        | OK          | DEGRADED   | DEGRADED      |
        | OK          | UNKNOWN    | DEGRADED      |        
        | FAILED      | DEGRADED   | FAILED        |
        | FAILED      | UNKNOWN    | FAILED        |
        | DEGRADED    | UNKNOWN    | DEGRADED      | 
        | UNKNOWN     | DEGRADED   | DEGRADED      |
        | UNKNOWN     | FAILED     | UNKNOWN       | 
        | DEGRADED    | FAILED     | DEGRADED      |       
        | DEGRADED    | DEGRADED   | DEGRADED      |     
        | UNKNOWN     | UNKNOWN    | UNKNOWN       |
        | FAILED      | FAILED     | FAILED        | 
