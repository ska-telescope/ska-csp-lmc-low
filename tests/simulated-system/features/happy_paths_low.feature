@low @pipeline
Scenario: CSP-LOW is turning ON only CBF
    Given All subsystems are fresh initialized
    When On Command is issued on CspController with argument ['sim-low-cbf/control/0']
    Then CbfController state is ON
    And CbfSubarray01 state is ON

    #And PssController state is STANDBY
    #And PssSubarray01 state is ON

    And PstBeam01 state is OFF
    And PstBeam02 state is OFF

    And CspController state is ON
    And CspSubarray01 state is ON
    And CspController commandResult is ('on', '0')
    And CspController longRunningCommandAttributes is (0, 'COMPLETED') 

Scenario: CSP-LOW is turning STANDBY only PSS
    Given All subsystems are fresh initialized
    And On Command is issued on CspController with argument []
    And CspController commandResult is ('on', '0')
    And CspController longRunningCommandAttributes is (0, 'COMPLETED') 

    When Standby Command is issued on CspController with argument ['sim-low-pss/control/0']

    Then CbfController state is ON
    And CbfSubarray01 state is OFF

    And PssController state is STANDBY
    And PssSubarray01 state is OFF

    And PstBeam01 state is OFF
    And PstBeam02 state is OFF

    And CspController state is ON
    And CspSubarray01 state is OFF
    And CspController commandResult is ('standby', '0')
    And CspController longRunningCommandAttributes is (0, 'COMPLETED') 

Scenario: CSP-LOW is turning OFF only PST
    Given All subsystems are fresh initialized
    And On Command is issued on CspController with argument []
    And CspController commandResult is ('on', '0')
    And CspController longRunningCommandAttributes is (0, 'COMPLETED') 

    When Off Command is issued on CspController with argument ['sim-low-pst/beam/01']

    Then CbfController state is ON
    And CbfSubarray01 state is ON

    And PssController state is ON
    And PssSubarray01 state is ON

    And PstBeam01 state is OFF
    And PstBeam02 state is ON

    And CspController state is STANDBY
    And CspSubarray01 state is OFF
    And CspController commandResult is ('off', '0')
    And CspController longRunningCommandAttributes is (0, 'COMPLETED') 


Scenario: CSP-LOW Subarray ObsState READY to SCANNING
    Given Subarray01 is fresh initialized
    And On Command is issued on CspSubarray01 (no argument)
    And CspSubarray01 commandResult is ('on', '0')
    And CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED') 
    And CspSubarray01 cbfSubarrayState is ON
    And CspSubarray01 pssSubarrayState is ON

    And CbfSubarray01 ObsState are sent straight to READY
    And PssSubarray01 ObsState are sent straight to READY
    And CspSubarray01 cbfSubarrayObsState is READY
    And CspSubarray01 pssSubarrayObsState is READY

    When Scan Command is issued on CspSubarray01 with argument {"common": {"subarray_id": 1}, "lowcbf": {"scan_id": 11}}
    Then CspSubarray01 ObsState is SCANNING
    And CbfSubarray01 ObsState is SCANNING
    And PssSubarray01 ObsState is SCANNING
    And CspSubarray01 commandResult is ('scan', '1')
    And CspSubarray01 longRunningCommandAttributes is (1, 'COMPLETED') 
    And CbfSubarray01 ObsState is READY
    And PssSubarray01 ObsState is READY
    And CspSubarray01 ObsState is READY

Scenario: CSP-LOW Subarray ObsState SCANNING to ABORTED
    Given Subarray01 is fresh initialized
    And On Command is issued on CspSubarray01 (no argument)
    And CspSubarray01 commandResult is ('on', '0')
    And CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED') 
    And CspSubarray01 state is ON

    And CbfSubarray01 ObsState are sent straight to READY
    And PssSubarray01 ObsState are sent straight to READY
    And CspSubarray01 cbfSubarrayObsState is READY
    #And CspSubarray01 pssSubarrayObsState is READY

    When Scan Command is issued on CspSubarray01 with argument {"common": {"subarray_id": 1}, "lowcbf": {"scan_id": 11}}
    Then CspSubarray01 ObsState is SCANNING
    And CbfSubarray01 ObsState is SCANNING
    #And PssSubarray01 ObsState is SCANNING
    And CspSubarray01 commandResult is ('scan', '1')
    And CspSubarray01 longRunningCommandAttributes is (1, 'COMPLETED') 

    When Abort Command is issued on CspSubarray01 (no argument)
    Then CspSubarray01 ObsState is ABORTED
    And CbfSubarray01 ObsState is ABORTED
    #And PssSubarray01 ObsState is ABORTED
    #And CspSubarray01 commandResult is ('abort', '0')
    And CspSubarray01 longRunningCommandAttributes is (4, 'COMPLETED') 

# ------------- Test all commands -------------------
    # @pipeline @stress @debug
    # Scenario: Test all commands on Subarray01
    #     Given All subsystems are fresh initialized
    #     And CspController state is ON

    #     When On Command is issued on CspController with argument []
    #     Then CspController commandResult is ('on', '0')

    #     And CspSubarray01 state is ON
    #     And CbfSubarray01 state is ON
    #     And CbfSubarray01 obsState is EMPTY
    #     And CspSubarray01 obsState is EMPTY
    #     And CspSubarray01 cbfSubarrayObsState is EMPTY
    #     And CspSubarray01 pssSubarrayObsState is EMPTY
    #     And CspSubarray01 obsState is EMPTY
        
    #     When AssignResources Command is issued on CspSubarray01 (argument from file: AssignResources_CBF_PSS_PST.json)
    #     Then CspSubarray01 commandResult is ('assignresources', '1')
    #     #Then CspSubarray01 longRunningCommandAttributes is (1, 'IN_PROGRESS') 
    #     And CspSubarray01 obsState is RESOURCING
    #     And CbfSubarray01 obsState is RESOURCING
    #     #And CspSubarray01 cbfSubarrayObsState is RESOURCING
    #     #And CspSubarray01 pssSubarrayObsState is RESOURCING
    #     # And CspSubarray01 commandResult is ('assignresources', '0')
    #     And CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED') 
    #     And CspSubarray01 obsState is IDLE
    #     And CbfSubarray01 obsState is IDLE 
    #     #And CspSubarray01 cbfSubarrayObsState is IDLE
    #     #And CspSubarray01 pssSubarrayObsState is IDLE

    #     When Configure Command is issued on CspSubarray01 (argument from file: Configure_CBF_PSS_PST.json)
    #     Then CspSubarray01 commandResult is ('configure', '1')
    #     # Then CspSubarray01 longRunningCommandAttributes is (1, 'IN_PROGRESS') 
    #     And CspSubarray01 obsState is CONFIGURING
    #     And CbfSubarray01 obsState is CONFIGURING 
    #     #And CspSubarray01 cbfSubarrayObsState is CONFIGURING
    #     #And CspSubarray01 pssSubarrayObsState is CONFIGURING
    #     # Then CspSubarray01 commandResult is ('configure', '0')
    #     And CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED') 
    #     And CspSubarray01 obsState is READY
    #     And CbfSubarray01 obsState is READY
    #     #And CspSubarray01 cbfSubarrayObsState is READY
    #     #And CspSubarray01 pssSubarrayObsState is READY

    #     When Scan Command is issued on CspSubarray01 (argument from file: Scan_CBF.json)
    #     Then CspSubarray01 commandResult is ('scan', '0')
    #     And CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED') 
    #     And CspSubarray01 obsState is SCANNING
    #     And CbfSubarray01 obsState is SCANNING

    #     When Endscan Command is issued on CspSubarray01 (no argument)
    #     #Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED') 
    #     And CspSubarray01 obsState is READY
    #     And CbfSubarray01 obsState is READY
    #     And CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED') 
    #     #And CspSubarray01 cbfSubarrayObsState is READY
    #     #And CspSubarray01 pssSubarrayObsState is READY

    #     When GoToIdle Command is issued on CspSubarray01 (no argument)
    #     #Then CspSubarray01 commandResult is ('gotoidle', '0')
    #     Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED') 
    #     And CspSubarray01 obsState is IDLE
    #     And CbfSubarray01 obsState is IDLE
    #     #And CspSubarray01 cbfSubarrayObsState is IDLE
    #     #And CspSubarray01 pssSubarrayObsState is IDLE

    #     When ReleaseAllResources Command is issued on CspSubarray01 (no argument)
    #     Then CspSubarray01 commandResult is ('releaseallresources', '1')
    #     And CspSubarray01 obsState is EMPTY
    #     And CbfSubarray01 obsState is EMPTY 
    #     And CspSubarray01 cbfSubarrayObsState is EMPTY
    #     And CspSubarray01 pssSubarrayObsState is EMPTY
    #     Then CspSubarray01 commandResult is ('releaseallresources', '0')
    #     And CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED') 


    #     When Off Command is issued on CspSubarray01 (no argument)
    #     Then CspSubarray01 commandResult is ('off', '1')
    #     And CbfSubarray01 state is OFF
    #     And PssSubarray01 state is OFF
    #     And CspSubarray01 state is OFF
    #     And CspSubarray01 pssSubarrayState is OFF
    #     And CspSubarray01 cbfSubarrayState is OFF
    #     And CspSubarray01 commandResult is ('off', '0')
    #     And CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED')
    #     And CspSubarray01 cbfSubarrayObsState is EMPTY
    #     And CspSubarray01 pssSubarrayObsState is EMPTY
    #     And CspSubarray01 obsState is EMPTY


    @pipeline @stress @debug @thisone
    Scenario: Test all commands on Subarray01
        Given All subsystems are fresh initialized
        And CspController isCommunicating is True
        And On Command is issued on CspSubarray01 (no argument)
        And On command is issued on PstBeam01 (no argument)
        
        When CspSubarray01 commandResult is ('on', '0')
        And CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED')         
        And CspSubarray01 state is ON
        And CbfSubarray01 state is ON
        And CbfSubarray01 obsState is EMPTY
        And CspSubarray01 obsState is EMPTY
        #And CspSubarray01 pssSubarrayState is ON
        And CspSubarray01 cbfSubarrayState is ON
        And CspSubarray01 cbfSubarrayObsState is EMPTY
        #And CspSubarray01 pssSubarrayObsState is EMPTY

        When AssignResources Command is issued on CspSubarray01 (argument from file: AssignResources_CBF_PSS_PST.json)
        And CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED')
        And CspSubarray01 commandResult is ('assignresources', '0')
        And CspSubarray01 obsState is IDLE
        And CbfSubarray01 obsState is IDLE 
        And CspSubarray01 cbfSubarrayObsState is IDLE
        #And CspSubarray01 pssSubarrayObsState is IDLE

        When Configure Command is issued on CspSubarray01 (argument from file: Configure_CBF_PSS_PST.json)
        And CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED') 
        And CspSubarray01 obsState is READY
        And CbfSubarray01 obsState is READY
        And CspSubarray01 cbfSubarrayObsState is READY
        #And CspSubarray01 pssSubarrayObsState is READY

        When Scan Command is issued on CspSubarray01 (argument from file: Scan_CBF.json)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED') 
        And CspSubarray01 obsState is SCANNING
        And CbfSubarray01 obsState is SCANNING         
        And CspSubarray01 cbfSubarrayObsState is SCANNING
        #And CspSubarray01 pssSubarrayObsState is SCANNING

        When Endscan Command is issued on CspSubarray01 (no argument)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED')
        And CspSubarray01 obsState is READY
        And CbfSubarray01 obsState is READY
        And CspSubarray01 cbfSubarrayObsState is READY
        #And CspSubarray01 pssSubarrayObsState is READY

        When GoToIdle Command is issued on CspSubarray01 (no argument)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED') 
        And CspSubarray01 obsState is IDLE
        And CbfSubarray01 obsState is IDLE
        And CspSubarray01 cbfSubarrayObsState is IDLE
        #And CspSubarray01 pssSubarrayObsState is IDLE

        When ReleaseAllResources Command is issued on CspSubarray01 (no argument)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED')
        And CspSubarray01 obsState is EMPTY
        And CbfSubarray01 obsState is EMPTY 
        And CspSubarray01 cbfSubarrayObsState is EMPTY
        
        #When Off Command is issued on CspSubarray01 (no argument)
        #Then CspSubarray01 commandResult is ('off', '0')
        #And CspSubarray01 cbfSubarrayState is OFF
        #And CbfSubarray01 state is ON
        #And PssSubarray01 state is OFF
        #And CspSubarray01 state is ON
        #And CspSubarray01 pssSubarrayState is OFF
        #And CspSubarray01 cbfSubarrayState is ON
        #And CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED')
        #And CspSubarray01 cbfSubarrayObsState is EMPTY
        #And CspSubarray01 pssSubarrayObsState is EMPTY
        #And CspSubarray01 obsState is EMPTY
