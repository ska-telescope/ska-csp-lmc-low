Feature: PST beam unhappy path


    @pipeline
    Scenario: Test configure command on Subarray01 with failure on PSTBeam01
        Given All subsystems are fresh initialized
        And CspController isCommunicating is True
        And On Command is issued on CspSubarray01 (no argument)
        And On command is issued on PstBeam01 (no argument)
        
        When CspSubarray01 commandResult is ('on', '0')
        And CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED')         
        And CspSubarray01 state is ON
        And CbfSubarray01 state is ON
        And CbfSubarray01 obsState is EMPTY
        And CspSubarray01 obsState is EMPTY
        #And CspSubarray01 pssSubarrayState is ON
        And CspSubarray01 cbfSubarrayState is ON
        And CspSubarray01 cbfSubarrayObsState is EMPTY
        And PstBeam01 state is ON
        And PstBeam01 obsState is IDLE

        When AssignResources Command is issued on CspSubarray01 (argument from file: AssignResources_CBF_PSS_PST.json)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED')
        And CspSubarray01 commandResult is ('assignresources', '0')
        And CspSubarray01 obsState is IDLE
        And CbfSubarray01 obsState is IDLE 
        And CspSubarray01 cbfSubarrayObsState is IDLE
        And PstBeam01 obsState is IDLE

        When PstBeam01 raise a command fault
        And Configure Command is issued on CspSubarray01 (argument from file: Configure_CBF_PSS_PST.json)
        Then CspSubarray01 longRunningCommandAttributes is (3, 'REJECTED')
        And PstBeam01 obsState is IDLE
        And CspSubarray01 obsState is IDLE
        And CbfSubarray01 obsState is IDLE
        And CspSubarray01 cbfSubarrayObsState is IDLE
        And PstBeam01 reset a command fault

        When ReleaseAllResources Command is issued on CspSubarray01 (no argument)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED')
        And CspSubarray01 obsState is EMPTY
        And CbfSubarray01 obsState is EMPTY 
        And CspSubarray01 cbfSubarrayObsState is EMPTY

