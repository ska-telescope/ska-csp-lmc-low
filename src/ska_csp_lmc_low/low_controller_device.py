# -*- coding: utf-8 -*-
#
# This file is part of the LowCspController project
#
# INAF-SKA Telescope
#
# Distributed under the terms of the GPL license.
# See LICENSE.txt for more info.

"""LowCspController class.

The base class for LOW CspController. Fuctionality to monitor CSP.LMC
Capabilities are implemented in separate TANGO Devices.
"""

# Python standard library
from __future__ import annotations

from typing import List

# import CSP.LMC Common package
from ska_csp_lmc_common import CspController
from ska_csp_lmc_common.manager.manager_configuration import (
    ComponentManagerConfiguration,
)
from tango.server import attribute, device_property, run

from ska_csp_lmc_low.manager import LowCspControllerComponentManager

# pylint: disable=invalid-name
# pylint: disable=broad-except
# pylint: disable=logging-fstring-interpolation


class LowCspController(CspController):
    """The base class for Low CspController. Fuctionality to monitor CSP.LMC
    Capabilities are implemented in separate TANGO Devices.

    **Properties:**
    NOTE: not implemented
    """

    CbfAllocator = device_property(
        dtype="DevString",
    )

    def set_component_manager(
        self: LowCspController, cm_configuration: ComponentManagerConfiguration
    ) -> LowCspControllerComponentManager:
        """Set the CM for the Low CSP Controller device.

        :param cm_configuration: A class with all the device properties
            accessible as attributes

        :returns: The Low.Csp ControllerComponentManager
        """
        return LowCspControllerComponentManager(
            self.op_state_model,
            self.health_model,
            cm_configuration,
            self.update_property,
            self.logger,
        )

    # ----------
    # Attributes
    # ----------

    @attribute(
        dtype=("DevString",),
        max_dim_x=20,
        doc=(
            "A list of available number of instances of each capability "
            "type, e.g. `CORRELATOR:512`, `PSS-BEAMS:4`."
        ),
    )
    def availableCapabilities(self: LowCspController) -> List[str]:
        """Return the list of avaliable capabilities.

        :return: List of available capabilities.

        NOTE: not implemented
        """

    @attribute(
        dtype=("DevUShort",),
        max_dim_x=512,
        label="stationList",
        doc="The list of all stations",
    )
    def stationList(self: LowCspController) -> List[int]:
        """Report the list of stations as list of integer.

        :return: List of stations.
        """
        try:
            return self.component_manager.components[
                self.CspCbf
            ].list_of_stations
        except KeyError:
            return [0]

    @attribute(
        dtype="DevString",
        label="resourceTableP4",
        doc="The P4 resource table as JSON string.",
    )
    def resourceTableP4(self):
        """Return the P4 resource table attribute."""
        p4_table = ""
        try:
            p4_table = self.component_manager.components[
                self.CbfAllocator
            ].resource_table_p4
        except Exception as e:
            self.logger.error(f"Error reading resourceTableP4 attribute: {e}")
        return p4_table

    @attribute(
        dtype="DevString",
        label="resourceTableFSP",
        doc="The FSP resource table as JSON string.",
    )
    def resourceTableFSP(self):
        """Return the FSP resource table attribute."""
        fsp_table = ""
        try:
            fsp_table = self.component_manager.components[
                self.CbfAllocator
            ].resource_table_fsp
        except Exception as e:
            self.logger.error(f"Error reading resourceTableFSP attribute: {e}")
        return fsp_table


# ----------
# Run server
# ----------


def main(args=None, **kwargs):
    # pylint: disable=missing-function-docstring
    return run((LowCspController,), args=args, **kwargs)


if __name__ == "__main__":
    main()
