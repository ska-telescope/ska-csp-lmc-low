# -*- coding: utf-8 -*-
#
# This file is part of the CentralNode project
#
#
#
# Distributed under the terms of the BSD-3-Clause license.
# See LICENSE.txt for more info.

"""Release information for Python Package."""

# pylint: disable=invalid-name
# pylint: disable=redefined-builtin

name = """ska-csp-lmc-low"""
version = "1.0.0-rc.2"
version_info = version.split(".")
description = """SKA LOW CSP.LMC"""
author = "INAF-OAA"
author_email = "elisabetta.giani@inaf.it"
license = """BSD-3-Clause"""
url = """https://gitlab.com/ska-telescope/ska-csp-lmc-low.git"""
copyright = """INAF, SKA Telescope"""
