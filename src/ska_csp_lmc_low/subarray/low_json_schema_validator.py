"""
JSON Validator for LOW domain
"""

from __future__ import annotations

import logging
from ipaddress import ip_address
from typing import Any

import jsonschema
from ska_csp_lmc_common.subarray.json_schema_validator import CspJsonValidator

# pylint: disable=invalid-name
# pylint: disable=no-member
# pylint: disable=missing-function-docstring
# pylint: disable=unused-argument

module_logger = logging.getLogger(__name__)


class CspLowJsonValidator(
    CspJsonValidator
):  # pylint: disable=too-few-public-methods
    """
    An argument validator for LOW JSON commands.

    It performs coherence checks in the LOW domain.

    This class is a derivation from JsonValidator of ska_tango_base
    """

    # pylint: disable=super-init-not-called
    def __init__(
        self: CspLowJsonValidator,
        command_name: str,
        input_schema=None,
        attrs_dict: dict = None,
        logger: logging.Logger | None = None,
    ) -> None:
        """
        Initialise a new instance.

        :param command_name: name of the command to be validated.
        :param input_schema: an optional schema against which the JSON
            argument should be validated. If not provided, a warning is
            issued.
        :param attrs_dict: an optional dictionary, providing the key
            device attributes that need to be checked in the JSON file
        :param logger: a logger for this validator to use
        """
        super().__init__(command_name, input_schema, attrs_dict, logger)

    def validate(
        self: CspLowJsonValidator,
        *args: Any,
        **kwargs: Any,
    ) -> tuple[tuple[Any, ...], dict[str, Any]]:
        """
        Validate the command arguments.

        :param args: positional args to the command
        :param kwargs: keyword args to the command

        :returns: validated args and kwargs
        """

        # Perform common coherence check at first
        _, decoded_dict = super().validate(*args, **kwargs)

        if self._command_name.lower() == "assignresources":
            self._validate_low_assignresources(decoded_dict, self._attrs_dict)
        elif self._command_name.lower() == "configure":
            self._validate_low_configure(decoded_dict, self._attrs_dict)
        elif self._command_name.lower() == "scan":
            self._validate_low_scan(decoded_dict, self._attrs_dict)
        elif self._command_name.lower() == "releaseresources":
            self._validate_low_releaseresources(decoded_dict, self._attrs_dict)

        if "interface" not in decoded_dict:
            warning_msg = (
                f"JSON argument to command {self._command_name} "
                "will only be validated as a dictionary."
            )
            jsonschema.validate(decoded_dict, self._schema)
            if self.logger:
                self.logger.warning(warning_msg)

        return (), decoded_dict

    def _validate_low_assignresources(
        self: CspLowJsonValidator, decoded_dict: dict, attrs_dict: dict
    ):
        """
        Coherence checks for the assingresources command

        :param decoded_dict: json input file decoded as a dictionary
        :param attrs_dict: device attributes to be checked in the json file
        """

    def _validate_low_configure(
        self: CspLowJsonValidator, decoded_dict: dict, attrs_dict: dict
    ):
        """
        Coherence checks for the configure command

        :param decoded_dict: json input file decoded as a dictionary
        :param attrs_dict: device attributes to be checked in the json file
        """
        # Check if timing_beams ans pst field are present (not mandatory)
        tb_condition = "timing_beams" in decoded_dict["lowcbf"]
        pst_condition = "pst" in decoded_dict
        if tb_condition or pst_condition:
            # Raise an error if they are not both present
            if not (tb_condition and pst_condition):
                message = (
                    "'timing_beams' key included but 'pst' key is missing"
                    if tb_condition
                    else "'pst' key included but 'timing_beams' key is missing"
                )
                self._log_and_raise_dict_error(KeyError, message)
            else:
                # Check if pst_beam_id is coherent
                self._validate_pst_beam_id_for_configure(
                    decoded_dict, attrs_dict
                )
        self._validate_sdp_address(decoded_dict)
        self.logger.info(
            "Semantic validation for configuration input performed"
            " with success"
        )

    def _validate_low_scan(
        self: CspLowJsonValidator, decoded_dict: dict, attrs_dict: dict
    ):
        """
        Coherence checks for the scan command

        :param decoded_dict: json input file decoded as a dictionary
        :param attrs_dict: device attributes to be checked in the json file
        """

    def _validate_low_releaseresources(
        self: CspLowJsonValidator, decoded_dict: dict, attrs_dict: dict
    ):
        """
        Coherence checks for the releaseresources command

        :param decoded_dict: json input file decoded as a dictionary
        :param attrs_dict: device attributes to be checked in the json file
        """

    def _validate_sdp_address(
        self: CspLowJsonValidator, decoded_dict: dict, attrs_dict: dict = None
    ):
        """ Validate the format of the SDP address for visibility"""
        if "vis" in decoded_dict["lowcbf"]:
            for stn_beam in decoded_dict["lowcbf"]["vis"]["stn_beams"]:
                for host_entry in stn_beam["host"]:
                    host_addr = host_entry[1]
                    try:
                        ip_address(host_addr)
                    except ValueError:
                        self._log_and_raise_dict_error(
                            ValueError,
                            f"host address '{host_addr}' for station beam "
                            f"{stn_beam['stn_beam_id']} "
                            "is not in numerical dotted format",
                        )

    def _validate_pst_beam_id_for_configure(
        self: CspLowJsonValidator, decoded_dict: dict, attrs_dict: dict = None
    ):
        """Helper method to validate pst_beam_id from the given dictionaries.

        :param decoded_dict: json input file decoded as a dictionary
        :param attrs_dict: device attributes to be checked in the json file
        """

        pst_beam_id_list = [
            int(beam["pst_beam_id"])
            for beam in decoded_dict["lowcbf"]["timing_beams"]["beams"]
        ]
        # Extract the list of beam_id values from ["pst"]["beams"]
        beam_id_list = [
            int(beam["beam_id"]) for beam in decoded_dict["pst"]["beams"]
        ]

        # Check the equality of the lists in CBF and PST sections
        if sorted(pst_beam_id_list) != sorted(beam_id_list):
            self._log_and_raise_dict_error(
                ValueError,
                "Incoherent configuration: the PST beam IDs "
                f"specified in the CBF section ({sorted(pst_beam_id_list)}) "
                "do not match those specified in the PST "
                f"section ({sorted(beam_id_list)})",
            )
        # check if the pst beams to configure are in the list of
        # assigned beams
        if attrs_dict:
            if "assigned_timing_beams_id" in list(attrs_dict.keys()):
                # check if the required PST beams are a subset of
                # the assigned ones
                assigned_pst_beams = attrs_dict["assigned_timing_beams_id"]
                if not set(beam_id_list).issubset(set(assigned_pst_beams)):
                    self._log_and_raise_dict_error(
                        ValueError,
                        f"Incoherent configuration: the PST beams' IDs to "
                        f" configure ({sorted(beam_id_list)}) do not match "
                        "the assigned beams IDs "
                        f"({sorted(assigned_pst_beams)})",
                    )
        else:
            self.logger.error(
                "Coeherence check on PST beams can't be executed "
                "Internal error: the dictionary with attributes"
                " to check is empty"
            )
            return
