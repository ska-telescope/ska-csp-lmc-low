"""Handling the parsing of the configuration strings for AssignResources,
ReleaseResources and Configure in Low Telescope."""
from __future__ import annotations

import logging
from typing import Dict, Optional

from ska_csp_lmc_common.subarray.json_configuration_parser import (
    JsonConfigurationParser,
)
from ska_telmodel.csp.low_version import get_csp_subsystem_version
from ska_telmodel.lowcbf.version import (
    LOWCBF_ASSIGNRESOURCES_PREFIX,
    LOWCBF_CONFIGURESCAN_PREFIX,
    LOWCBF_RELEASERESOURCES_PREFIX,
    LOWCBF_SCAN_PREFIX,
)
from ska_telmodel.pst.version import PST_CONFIGURE_PREFIX

# pylint: disable=use-dict-literal
# pylint: disable=protected-access
# pylint: disable=too-many-nested-blocks
# pylint: disable=logging-fstring-interpolation


class LowJsonConfigurationParser(JsonConfigurationParser):
    """Class handling the parsing of the configuration strings for
    AssignResources, ReleaseResources and Configure in Low Telescope."""

    def __init__(
        self: LowJsonConfigurationParser,
        component_manager,
        json_config: dict,
        logger: Optional[logging.Logger] = None,
    ):
        """Instantiate the LowJsonConfigurationParser object.

        :param: the device component_manager
        :json_config: the dictionary with the configuration to
            apply.
        :logger: the logger for this object
        """
        super().__init__(component_manager, json_config, logger)
        self.key_to_subsystem_map = {
            "cbf": "lowcbf",
            "pss": "pss",
            "pst": "pst",
            "vlbi": "lowvlbi",
        }
        self.cbf_iface_num = self.get_subsystem_iface_num("cbf")
        self.pst_iface_num = self.get_subsystem_iface_num("pst")
        self.pss_iface_num = self.get_subsystem_iface_num("pss")

    def get_subsystem_iface_num(self, subsystem: str) -> str:
        """
        Fetch the interface version number of the subsystem utilizing
        the CompatibilityMap defined within the Telescope Model.

        :param subsystem: the CSP Subsystem string: 'cbf','pss' or 'pst'.

        :return: the version number of the subsystem command interface.
        """
        if "interface" in self.config:
            # recover the real subsystem name. For example: 'lowcbf'
            # instead of 'cbf'
            subsystem = self.key_to_subsystem_map[subsystem]
            return get_csp_subsystem_version(
                self.config["interface"], subsystem
            )

        raise ValueError("Interface field not specified in the JSON script")

    def configure(self: LowJsonConfigurationParser) -> Dict:
        """Retrieve the json script with the configuration for each CSP
        subordinate sub-system. The configuration specific for a sub-system is
        stored in a python dictionary, addressable via the FQDN of the sub-
        system.

        Override the parent class method.

        :return: on success a dict mapping keys (sub-system FQDN) to
            the corresponding sub-system configuration fetched.
            On failure the returned dict is empty.
        """

        def update_entries_in_pst_section(pst_cfg_dict):
            """
            Update the PST dictionary to add o remove keys.

            :param pst_cfg_dict: the input dictionary

            :return: the updated dictionary
            """
            pst_uri = PST_CONFIGURE_PREFIX + self.pst_iface_num
            pst_cfg_dict["interface"] = pst_uri
            pst_cfg_dict["common"]["frequency_band"] = "low"
            # removed unused keys
            if "subarray" in pst_cfg_dict:
                pst_cfg_dict.pop("subarray")
            if "transaction_id" in pst_cfg_dict:
                pst_cfg_dict.pop("transaction_id")

            return pst_cfg_dict

        def update_entries_in_cbf_section(cbf_cfg_dict):
            """
            Update the CBF dictionary to add o remove keys.

            :param cbf_cfg_dict: the input dictionary

            :return: the updated dictionary
            """
            cbf_uri = LOWCBF_CONFIGURESCAN_PREFIX + self.cbf_iface_num
            cbf_cfg_dict["interface"] = cbf_uri
            # Low CSP has to remove some keywords from the JSON file
            # because Low CBF does not use the Telescope Model, yet.
            cbf_cfg_dict.pop("interface")
            cbf_cfg_dict.pop("common")
            if "subarray" in cbf_cfg_dict:
                cbf_cfg_dict.pop("subarray")
            if "transaction_id" in cbf_cfg_dict:
                cbf_cfg_dict.pop("transaction_id")

            # the base classes need an "id" in the json.
            # We are sending a dummy one.
            cbf_cfg_dict["id"] = 123
            if "timing_beams" in cbf_cfg_dict:
                tmp = cbf_cfg_dict["lowcbf"]["timing_beams"]["beams"][0]
                tmp.pop("stn_weights")
            return cbf_cfg_dict

        self.logger.debug("Low json parser for configure resources")
        resources_dict = {}
        try:
            cbf_cfg_dict = self.generate_subsystem_config_dict("cbf")
            if cbf_cfg_dict:
                cbf_cfg_dict = update_entries_in_cbf_section(cbf_cfg_dict)
                resources_dict[self.cm.CbfSubarrayFqdn] = cbf_cfg_dict

            if pss_json_script := self.generate_subsystem_config_dict("pss"):
                resources_dict[self.cm.PssSubarrayFqdn] = pss_json_script
            if pst_json_script := self.generate_subsystem_config_dict("pst"):
                self.logger.debug(f"pst_json_script: {pst_json_script}")
                for single_beam_conf in pst_json_script["pst"]["beams"]:
                    beam_id = single_beam_conf["beam_id"]
                    for fqdn in self.cm.PstBeamsFqdn:
                        if int(fqdn[-2:]) == int(beam_id):
                            resources_dict[
                                fqdn
                            ] = self.generate_subsystem_config_dict("pst")

                            resources_dict[fqdn]["pst"] = {
                                "scan": single_beam_conf["scan"]
                            }
                            resources_dict[fqdn]["pst"]["scan"][
                                "timing_beam_id"
                            ] = str(beam_id)
                            resources_dict[
                                fqdn
                            ] = update_entries_in_pst_section(
                                resources_dict[fqdn]
                            )

        except KeyError as err:
            self.logger.error(
                "Parsing of the configuration JSON script"
                f"got the following key error: {err}"
            )
        # pylint: disable-next=broad-except
        except Exception as err:
            self.logger.error(
                "Parsing of the configuration JSON script"
                f"got the following exception: {err}"
            )
        return resources_dict

    def assignresources(self: LowJsonConfigurationParser) -> Dict:
        """Retrieve the json script for assignment of resources to be forwarded
        to each CSP sub-system device.

        Override the parent class method.

        :return: a dictionary with the extracted resources for each sub-system
                on success, otherwise an empty one if the json is not valid.
                The dictionary is addressable via the sub-system fqdn.
        """
        self.logger.debug("Low json parser for assign resources")
        resources_to_send = {}
        try:
            common_dict = self.get_section("common")
            sub_id = int(common_dict["subarray_id"])
            # check if the json script is for the current CSP Subarray
            if sub_id != int(self.cm.sub_id):
                self.logger.error(
                    f"The subarray id {sub_id} specified in "
                    "the JSON script does not match the CSP "
                    f" Subarray id {self.cm.sub_id}"
                )
            else:
                if "lowcbf" in self.config:
                    cbf_uri = (
                        LOWCBF_ASSIGNRESOURCES_PREFIX + self.cbf_iface_num
                    )
                    cbf_cfg_dict = self.generate_subsystem_config_dict("cbf")
                    if cbf_cfg_dict:
                        cbf_cfg_dict.pop("common")
                    else:
                        cbf_cfg_dict["lowcbf"] = {}
                    cbf_cfg_dict["interface"] = cbf_uri
                    resources_to_send[self.cm.CbfSubarrayFqdn] = cbf_cfg_dict

                if pss_json_script := self.generate_subsystem_config_dict(
                    "pss"
                ):
                    resources_to_send[
                        self.cm.PssSubarrayFqdn
                    ] = pss_json_script
                    self.logger.debug(
                        "PSS RESOURCES: "
                        f"{resources_to_send[self.cm.PssSubarrayFqdn]}"
                    )
                pst_json_script = self.generate_subsystem_config_dict("pst")
                self.logger.debug(
                    "Json script to assign resources to "
                    f"PST: {pst_json_script}"
                )
                if pst_json_script:
                    section = self.key_to_subsystem_map["pst"]
                    beams_ids = pst_json_script[section]["beams_id"]
                    for fqdn in self.cm.PstBeamsFqdn:
                        if int(fqdn[-2:]) in beams_ids:
                            resources_to_send[fqdn] = {"subarray_id": sub_id}
        # pylint: disable-next=broad-except
        except Exception as err:
            self.logger.error(
                "Assignresources json parsing" f" got error: : {err}"
            )
        return resources_to_send

    def releaseresources(self: LowJsonConfigurationParser):
        """Retrieve the json script for release of resources to be forwarded
        to each CSP sub-system device.

        Override the parent class method.

        :return: a dictionary with the extracted resources for each sub-system
                on success, otherwise an empty one if the json is not valid.
                The dictionary is addressable via the sub-system fqdn.
        """
        self.logger.debug("Low json parser for release all resources")
        resources_to_remove = {}
        try:
            common_dict = self.get_section("common")
            sub_id = int(common_dict["subarray_id"])
            # check if the json script is for the current CSP Subarray
            if sub_id != int(self.cm.sub_id):
                self.logger.error(
                    f"The subarray id {sub_id} specified in "
                    "the JSON script does not match the CSP "
                    f" Subarray id {self.cm.sub_id}"
                )
            else:
                if cbf_cfg_dict := self.generate_subsystem_config_dict("cbf"):
                    cbf_uri = (
                        LOWCBF_RELEASERESOURCES_PREFIX + self.cbf_iface_num
                    )
                    resources_to_remove[self.cm.CbfSubarrayFqdn] = cbf_cfg_dict
                    cbf_cfg_dict["interface"] = cbf_uri
                    resources_to_remove[self.cm.CbfSubarrayFqdn].pop("common")
                if pss_json_script := self.generate_subsystem_config_dict(
                    "pss"
                ):
                    resources_to_remove[
                        self.cm.PssSubarrayFqdn
                    ] = pss_json_script
                    self.logger.debug(
                        "PSS RESOURCES: "
                        f"{resources_to_remove[self.cm.PssSubarrayFqdn]}"
                    )
                if pst_json_script := self.generate_subsystem_config_dict(
                    "pst"
                ):
                    beams_ids = pst_json_script["pst"]["beams_id"]
                    for fqdn in self.cm.PstBeamsFqdn:
                        if int(fqdn[-2:]) in beams_ids:
                            resources_to_remove[fqdn] = {"subarray_id": sub_id}
        # pylint: disable-next=broad-except
        except Exception as err:
            self.logger.error(
                f"Release resourcing json parsing got error: {err}"
            )
        return resources_to_remove

    def scan_id(self: LowJsonConfigurationParser):
        """
        Return the scan ID.
        """
        return (
            self.get_section("lowcbf")["scan_id"]
            if "lowcbf" in self.config
            else self.get_section("scan_id")
        )

    def scan(self: LowJsonConfigurationParser):
        """Retrieve the json script for scan command to be forwarded
        to each CSP sub-system device.

        :return: a dictionary with the extracted resources for each sub-system
                on success, otherwise an empty one if the json is not valid.
                The dictionary is addressable via the sub-system fqdn.
        """
        self.logger.debug("Low json parser for scan command")
        scan_id = self.scan_id()
        resources_to_send = {
            self.cm.CbfSubarrayFqdn: {
                "interface": LOWCBF_SCAN_PREFIX + self.cbf_iface_num,
                "id": scan_id,
            }
        }
        if self.cm.PstBeamsFqdn:
            for fqdn in self.cm.PstBeamsFqdn:
                resources_to_send[fqdn] = {"scan_id": scan_id}
        if self.cm.PssSubarrayFqdn:
            resources_to_send[self.cm.PssSubarrayFqdn] = {"scan_id": scan_id}
        return resources_to_send
