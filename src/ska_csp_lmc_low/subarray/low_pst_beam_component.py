"""Specialization of the CbfSubarray and PssSubarray component class for the
Low.CSP."""
from __future__ import annotations  # allow forward references in type hints

from ska_csp_lmc_common.subarray import PstBeamComponent

# pylint: disable=invalid-name
# pylint: disable=unused-argument
# pylint: disable=arguments-renamed


class LowPstBeamComponent(PstBeamComponent):
    """Specialization of the PstBeam component class for the Low
    Telescope."""

    def __init__(self: LowPstBeamComponent, fqdn, logger=None) -> None:
        super().__init__(fqdn, logger=logger)
        self.logger.info("LOW PST beam")

    def read_timeout(self: LowPstBeamComponent, command_name: str) -> int:
        """Read the timeout configured for a command.

        :param command_name: the command name

        :return: the timeout configured (in secs) or 0 on failure
        """
        value = super().read_timeout(command_name)
        if "configure" in command_name:
            default_value = 10
            value = max(value, default_value)
        return value
