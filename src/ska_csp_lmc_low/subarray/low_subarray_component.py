"""Specialization of the CbfSubarray and PssSubarray component class for the
Mid.CSP."""
from __future__ import annotations  # allow forward references in type hints

from typing import Dict, List

from ska_csp_lmc_common.subarray import (
    CbfSubarrayComponent,
    PssSubarrayComponent,
)

# pylint: disable=invalid-name
# pylint: disable=unused-argument
# pylint: disable=arguments-renamed


class LowPssSubarrayComponent(PssSubarrayComponent):
    """Specialization of the PssSubarray component class for the Low
    Telescope."""

    def __init__(self: LowPssSubarrayComponent, fqdn, logger=None) -> None:
        super().__init__(fqdn, logger=logger)
        self.logger.info("LOW PSS Subarray")

    def validated_resources(
        self: LowPssSubarrayComponent, resources: dict, action: str
    ) -> List[int,]:  # noqa E231
        """Validate the input configuration for the resources to assign.

        :param resources: the PSS resources to assign/remove to/from the
                          Low Subarray
        :param action: the action to perform: assign or release
        """
        # pylint: disable-next=fixme
        # TODO: need to fix this method!
        self.logger.debug("PSS VALIDATED RESOURCES")
        return [1, 2, 3]

    # def assignresources(
    #     self: LowPssSubarrayComponent,
    #     resources_list: Dict,
    #     callback: Optional[Callable] = None,
    # ) -> None:
    #     """Invoke the assign resources command on the Low PSS subarray.

    #     :param resources_list: The assign resource dictionary with
    #                            configuration
    #     :param callback: Method invoked when the commands end on the target
    #                      device
    #     :return: None
    #     :raise: ValueError exception if the list of receptors specified into
    #             the configuration dictionary is not valid
    #     """
    #     self.logger.info(f"Assign resources to PSS: {resources_list}")
    #     resources_list_str = json.dumps(resources_list)
    #     self.run(
    #         "assignresources", argument=resources_list_str, callback=callback
    #     )

    # def releaseallresources(
    #     self: LowPssSubarrayComponent, callback: Optional[Callable] = None
    # ):
    #     """Invoke the releaseall resources command on the Low PSS subarray.

    #     :param callback: Method invoked when the commands end on the target
    #         device
    #     :return: None
    #     :raise: ValueError exception if the list of receptors specified into
    #         the configuration dictionary is not valid
    #     """
    #     # invoke the RemoveAllReceptors command on the Low Cbf subarray
    #     self.run("releaseallresources", callback=callback)


class LowCbfSubarrayComponent(CbfSubarrayComponent):
    """Specialization of the Cbf Subarray component class for the LOW
    Telescope."""

    def __init__(self: LowCbfSubarrayComponent, fqdn, logger=None) -> None:
        super().__init__(fqdn, logger=logger)
        self.csp_ctrl_proxy = None

        self.stations = None
        self.substations = None
        self.station_beams = None
        self.pss_beams = None
        self.pst_beams = None
        self.logger.debug("LOW CBF Subarray")
        self.attrs_for_change_events = [
            "longrunningcommandstatus",
            "longrunningcommandresult",
        ]

    @property
    def assigned_stations(
        self: LowCbfSubarrayComponent,
    ) -> List[int,]:  # noqa E231
        """
        :return: stations assigned to Cbf subarray on success, otherwise an
                 empty list
        """
        assigned_stations = []
        try:
            assigned_stations = self.read_attr("stations")
        # pylint: disable-next=broad-except
        except Exception as e:
            self.logger.error(e)
        return assigned_stations

    @property
    def assigned_station_beams(
        self: LowCbfSubarrayComponent,
    ) -> List[int,]:  # noqa E231
        """
        :return: stations beams currently assigned to Cbf subarray on success,
                 otherwise an empty list
        """
        assigned_station_beams = []
        try:
            assigned_station_beams = self.read_attr("stationBeams")
        # pylint: disable-next=broad-except
        except Exception as e:
            self.logger.error(e)
        return assigned_station_beams

    @property
    def assigned_pss_beams(
        self: LowCbfSubarrayComponent,
    ) -> List[int,]:  # noqa E231
        """
        :return: pss Beams assigned to Cbf subarray on success, otherwise an
                 empty list
        """
        assigned_pss_beams = []
        try:
            assigned_pss_beams = self.read_attr("pssBeams")
        # pylint: disable-next=broad-except
        except Exception as e:
            self.logger.error(e)
        return assigned_pss_beams

    @property
    def assigned_pst_beams(
        self: LowCbfSubarrayComponent,
    ) -> List[int,]:  # noqa E231
        """
        :return: pst Beams currently assigned to Cbf subarray on success,
            otherwise an empty list
        """
        assigned_pst_beams = []
        try:
            assigned_pst_beams = self.read_attr("pstBeams")
        # pylint: disable-next=broad-except
        except Exception as e:
            self.logger.error(e)
        return assigned_pst_beams

    def update_pst_json_configuration(
        self: CbfSubarrayComponent, original_dict: Dict, updated_info: Dict
    ):
        """Update the configuration script adding the PST Beam(s)
        addresses.

        :param original_dict: the original input configuration.
        :param updated_info: the updated configuration.
        """
        if "timing_beams" not in original_dict["lowcbf"].keys():
            return original_dict
        # clean the update_info from common structure
        updated_info_dict = list(updated_info.values())[0]["channel_blocks"][0]
        # retrieve beam id from the first key of update_info
        id_beam = int(list(updated_info.keys())[0].split("/")[-1])

        # destinations is the section to be inserted in the original_dict
        destinations = {
            "data_host": updated_info_dict["destination_host"],
            "data_port": updated_info_dict["destination_port"],
            "start_channel": updated_info_dict["start_pst_channel"],
            "num_channels": updated_info_dict["num_pst_channels"],
        }
        original_dict["lowcbf"]["timing_beams"]["beams"][id_beam - 1][
            "destinations"
        ] = [destinations]

        return original_dict
