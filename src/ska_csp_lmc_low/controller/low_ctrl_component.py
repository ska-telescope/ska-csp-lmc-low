"""Specialize the Subsystem Controller Component classes for the Low.CSP
LMC."""

from __future__ import annotations

import logging
from typing import Optional

from ska_csp_lmc_common.component import Component
from ska_csp_lmc_common.controller.cbf_controller import CbfControllerComponent
from ska_csp_lmc_common.controller.pss_controller import PssControllerComponent
from ska_csp_lmc_common.subarray.pst_beam import PstBeamComponent


class LowCbfControllerComponent(CbfControllerComponent):
    """Specialize the CBF Controller Component class for the Low.CSP LMC.

    This class works as a cache and adaptor towards the real Low device.
    """

    def __init__(
        self: LowCbfControllerComponent,
        fqdn: str,
        logger: Optional[logging.Logger] = None,
    ):
        super().__init__(fqdn, logger=logger)
        self.attrs_for_change_events = [
            "longrunningcommandstatus",
            "longrunningcommandresult",
        ]

    @property
    def list_of_stations(self):
        """Return the list of receptors IDs."""
        return [1, 2, 3, 4]


class LowPssControllerComponent(PssControllerComponent):
    """Specialization of the PssController component class for the Low.CSP."""

    def __init__(
        self: LowCbfControllerComponent,
        fqdn: str,
        logger: Optional[logging.Logger] = None,
    ):
        super().__init__(fqdn, logger=logger)

    @property
    def list_of_beams(self):
        """Return the list of PSS beams IDs."""
        return [1, 2, 3, 4]


class LowPstBeamComponent(PstBeamComponent):
    """Specialization of the PstBeam component class for the Low.CSP."""

    def __init__(
        self: LowPstBeamComponent,
        fqdn: str,
        logger: Optional[logging.Logger] = None,
    ):
        super().__init__(fqdn, logger=logger)

    @property
    def list_of_beams(self):
        """Return the list of PST beams IDs."""
        return [1, 2, 3, 4]


class LowCbfAllocatorComponent(Component):
    """Specialization of the AllocatorController component class."""

    def __init__(
        self: Component,
        fqdn: str,
        logger: Optional[logging.Logger] = None,
    ):
        super().__init__(fqdn, "cbf-allocator", logger=logger)
        self.attrs_for_change_events = {
            "resourceTableP4": False,
            "resourceTableFsp": False,
        }

    @property
    def resource_table_fsp(self):
        """Return the list of PST beams IDs."""
        fsp_table = ""
        try:
            fsp_table = self.read_attr("resourceTableFSP")
        # pylint: disable-next=broad-except
        except Exception as e:
            self.logger.error(e)
        return fsp_table

    @property
    def resource_table_p4(self):
        """Return the list of PST beams IDs."""
        p4_table = ""
        try:
            p4_table = self.read_attr("resourceTableP4")
        # pylint: disable-next=broad-except
        except Exception as e:
            self.logger.error(e)
        return p4_table
