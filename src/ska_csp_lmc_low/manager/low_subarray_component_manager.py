"""Specialization Class for Low Csp Component Manager."""

from __future__ import annotations

# import functools
import json
import logging
from typing import Callable, Dict, Tuple

from ska_control_model import ObsMode, ObsState, ResultCode, TaskStatus
from ska_csp_lmc_common.manager import CSPSubarrayComponentManager
from ska_csp_lmc_common.manager.manager_configuration import (
    ComponentManagerConfiguration,
)
from ska_csp_lmc_common.subarray import (
    SubarrayHealthModel,
    SubarrayObsStateModel,
    SubarrayOpStateModel,
)

from ska_csp_lmc_low import LowObservingComponentFactory, release
from ska_csp_lmc_low.commands.low_command_map import LowCommandMap
from ska_csp_lmc_low.subarray.low_json_config_parser import (
    LowJsonConfigurationParser,
)

# pylint: disable=too-many-arguments
# pylint: disable=abstract-method
# pylint: disable=too-many-locals
# pylint: disable=logging-fstring-interpolation
# pylint: disable=broad-exception-caught


class LowCspSubarrayComponentManager(CSPSubarrayComponentManager):
    """Specialization Class for Low Csp Subarray Component Manager."""

    def __init__(
        self: LowCspSubarrayComponentManager,
        health_model: SubarrayHealthModel,
        op_state_model: SubarrayOpStateModel,
        obs_state_model: SubarrayObsStateModel,
        properties: ComponentManagerConfiguration,
        update_device_property_cbk: Callable = None,
        logger: logging.Logger = None,
    ) -> None:
        super().__init__(
            health_model,
            op_state_model,
            obs_state_model,
            properties,
            update_device_property_cbk=update_device_property_cbk,
            logger=logger,
        )

        self.assigned_stations = []
        self.assigned_station_beams = []
        self.assigned_pss_beams = []
        self._init_version_info(release)
        self._command_map = LowCommandMap().get_subarray_command_map()

    def _observing_factory(
        self: LowCspSubarrayComponentManager, logger: logging.Logger
    ):
        """Specialize the factory to create the Low CSP sub-system
        components."""
        return LowObservingComponentFactory(logger)

    def _json_configuration_parser(
        self: CSPSubarrayComponentManager, argin: dict
    ) -> LowJsonConfigurationParser:
        """Configure the class that handles the parsing of the input JSON files
        sent as argument of AssignResources, Configure and Scan.
        This class is specialized in Mid.CSP and Low.CSP.

        param: argin The input dictionary

        """
        return LowJsonConfigurationParser(self, argin, self.logger)

    def update_obs_mode(
        self: LowCspSubarrayComponentManager, resources_to_send: Dict
    ):
        """Specialize method to set the obsMode based on Low specific
        resources.

        param: resources_to_send Resources to be parsed for obsMode
        """

        self.logger.info("Updating LOW obs mode")
        if (
            self.obs_state_model.obs_state
            not in [
                ObsState.EMPTY,
                ObsState.IDLE,
            ]
            and resources_to_send
        ):
            if self.CbfSubarrayFqdn in resources_to_send:
                if (
                    "vis" in resources_to_send[self.CbfSubarrayFqdn]["lowcbf"]
                    and ObsMode.IMAGING not in self._obs_modes
                ):
                    self._obs_modes.append(ObsMode.IMAGING)
                if (
                    "timing_beams"
                    in resources_to_send[self.CbfSubarrayFqdn]["lowcbf"]
                    and ObsMode.PULSAR_TIMING not in self._obs_modes
                ):
                    self._obs_modes.append(ObsMode.PULSAR_TIMING)
                if (
                    "vlbi" in resources_to_send[self.CbfSubarrayFqdn]["lowcbf"]
                    and ObsMode.VLBI not in self._obs_modes
                ):
                    self._obs_modes.append(ObsMode.VLBI)
            self.logger.info(f"Obsmodes: {self._obs_modes}")
        super().update_obs_mode(resources_to_send)

    def update_json(
        self: CSPSubarrayComponentManager,
    ) -> Tuple[TaskStatus, ResultCode]:
        """
        Update the CBF configuration script adding the PST Beam(s)
        addresses.

        :param original_dict: the original input configuration.
        :param updated_info: the updated configuration.

        :return: a tuple with the task status and the result code
            of the execution
        """
        self.logger.debug(
            "Update the CBF configuration to include PST beam addresses"
        )
        status = TaskStatus.COMPLETED
        result_code = ResultCode.OK
        original_cbf_dict = self.resources[self.CbfSubarrayFqdn]["lowcbf"]
        try:
            if "timing_beams" in original_cbf_dict.keys():
                self.logger.error(
                    f"assigned_station_beams: {self._assigned_pst_beam}"
                )
                for pst_beam_fqdn in self._assigned_pst_beam:
                    if pst_beam_fqdn in self.online_components:
                        pst_component = self.online_components[pst_beam_fqdn]
                        channel_block_config = json.loads(
                            pst_component.channel_block_configuration
                        )
                        # Ensure "channel_blocks" exists and has data
                        self.logger.error(
                            f"channel block config: {channel_block_config}"
                        )
                        if "channel_blocks" in channel_block_config:
                            updated_info_dict = channel_block_config[
                                "channel_blocks"
                            ][0]
                            # Get beam ID from the PST beam FQDN
                            id_beam = int(pst_beam_fqdn.split("/")[-1])
                            self.logger.error(f"id beam: {id_beam}")
                            # destinations is the section to be inserted in
                            # the original_dict
                            destinations = {
                                "data_host": updated_info_dict[
                                    "destination_host"
                                ],
                                "data_port": updated_info_dict[
                                    "destination_port"
                                ],
                                "start_channel": updated_info_dict[
                                    "start_pst_channel"
                                ],
                                "num_channels": updated_info_dict[
                                    "num_pst_channels"
                                ],
                            }
                            original_cbf_dict["timing_beams"]["beams"][
                                id_beam - 1
                            ]["destinations"] = [destinations]
                            self.logger.info(
                                f"Updated CBF configuration is "
                                f"{self.resources[self.CbfSubarrayFqdn]}"
                            )
        except Exception as exc:
            self.logger.error(
                f"Updating of the CBF configuration failed with error: {exc}"
            )
            status = TaskStatus.FAILED
            result_code = ResultCode.FAILED
        return status, result_code

    def _scan_preparation(self, json_input: dict) -> bool:
        """Preliminary check for the scan command and
        store scan id.

        :param json_input: scan input json (dict)
        """
        input_ok = True
        self._store_scan_id(json_input)
        # pylint: disable-next=attribute-defined-outside-init
        self.resources = {}
        if resources_to_send := self._json_configuration_parser(
            json_input
        ).scan():
            for fqdn, _ in self.online_components.items():
                self.resources[fqdn] = resources_to_send[fqdn]
        else:
            error_msg = "Error in parsing the scan input json file"
            self.logger.error(error_msg)
            input_ok = False

        return input_ok

    def _store_scan_id(self, json_input: dict) -> None:
        """Extract and store the Scan Id from the scan input json.

        :param json_input: scan input json (dict)
        """
        self._scan_id = self._json_configuration_parser(json_input).scan_id()
        if self._update_device_property_cbk:
            self._update_device_property_cbk("scanID", int(self._scan_id))
