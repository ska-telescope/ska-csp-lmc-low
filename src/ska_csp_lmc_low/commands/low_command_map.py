""" Define the command map for Low commands"""

import logging

from ska_control_model import ObsState
from ska_csp_lmc_common.commands.command_map import BaseCommandMap
from tango import DevState

# How to create/populate a command map:
# - the most external key refers to the name of the Job (i.e. the external
#   task)
# - a Task is characterized by having mandatory "type" and "tasks" keywords;
#   in this case the name of the task is the "parent" key;
# - "cbf" and "pss" keywords create LeafTasks on cbf or pss connected
#   subsystem (i.e. subarray or controller), the command_name is the Tango
#   Command Name of the subsystem;
# - "internal" keyword creates a LeafTask of type "internal" with component
#   manager as target and no argin; command_name is the method_name
# - "pst" will create a parallel task with LeafTasks on all applicable beams
#   (i.e. online and/or specified in input)
# - "csp_subs" will create a parallel task with LeafTasks on all online
#   subarrays with no argin (only for controller)
# - if the resources dictionary of component manager is empty, all LeafTasks
#   will be automatically created without argin;
# - if the resources dictionary of component manager is not empty, but the
#   the command is supposed to be sent the a subsystem without argin,
#   "no_argin": "true" has to be specified in the subsystem dictionary.

# NOTE: this command map is still a draft and here for testing purposes

module_logger = logging.getLogger(__name__)

# pylint: disable=broad-exception-caught
# pylint: disable=broad-except


class LowCommandMap(BaseCommandMap):
    """Command map used for Low devices"""

    def __init__(self):
        super().__init__()
        self.integrate_controller_command_map()
        self.integrate_subarray_command_map()

    def integrate_controller_command_map(self):
        """Merge the controller common command map with the one specific for
        Low devices"""
        controller_command_map = {
            "off": {
                "type": "parallel",
                "allowed_states": {
                    "attr_name": "state",
                    "attr_value": [
                        DevState.ON,
                        DevState.STANDBY,
                        DevState.UNKNOWN,
                    ],
                },
                "tasks": {
                    "pst": {"command_name": "off"},
                    "pss": {"command_name": "off"},
                },
            },
        }
        try:
            self.controller_command_map.update(controller_command_map)
        except Exception as exc:
            module_logger.error(
                "Error while merging the controller command map: %s", exc
            )

    def integrate_subarray_command_map(self):
        """Merge the subarray common command map with the one specific for
        Low devices"""
        subarray_command_map = {
            "off": {
                "type": "parallel",
                "allowed_states": {
                    "attr_name": "state",
                    "attr_value": [
                        DevState.ON,
                        DevState.STANDBY,
                        DevState.UNKNOWN,
                    ],
                },
                "tasks": {
                    "pss": {"command_name": "off"},
                },
            },
            "gotoidle": {
                "type": "parallel",
                "allowed_states": {
                    "attr_name": "obs_state",
                    "attr_value": [
                        ObsState.READY,
                    ],
                },
                "tasks": {
                    "pst": {"command_name": "gotoidle"},
                    "pss": {"command_name": "gotoidle"},
                    "cbf": {"command_name": "end"},
                },
            },
        }
        try:
            self.subarray_command_map.update(subarray_command_map)
        except Exception as exc:
            module_logger.error(
                "Error while merging the subarray command map: %s", exc
            )
