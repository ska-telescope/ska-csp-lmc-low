# -*- coding: utf-8 -*-
#
# This file is part of the LowCspSubarray project
#
# INAF, SKA Telescope
#
# Distributed under the terms of the GPL license.
# See LICENSE.txt for more info.

"""LowCspSubarray.

The base class for LOW CspSubarray. Fuctionality to monitor assigned
CSP.LMC Capabilities, as well as inherent Capabilities, are implemented
in separate TANGO Devices.
"""

# Python standard library
from __future__ import annotations

from typing import List

from ska_csp_lmc_common import CspSubarray
from ska_csp_lmc_common.manager.manager_configuration import (
    ComponentManagerConfiguration,
)

# Additional import
from tango.server import attribute, run

from ska_csp_lmc_low.manager import LowCspSubarrayComponentManager
from ska_csp_lmc_low.subarray.low_json_schema_validator import (
    CspLowJsonValidator,
)

# pylint: disable=invalid-name
# pylint: disable=logging-fstring-interpolation
# pylint: disable=broad-except

__all__ = ["LowCspSubarray", "main"]


class LowCspSubarray(CspSubarray):
    """The base class for LOW CspSubarray.

    Functionality to monitor assigned CSP.LMC Capabilities, as well as inherent
    Capabilities, are implemented in separate TANGO Devices.
    """

    def _get_schema_validator(
        self: LowCspSubarray,
    ) -> CspLowJsonValidator:
        """Retrieve the class that handles the validation of the input
        JSON files sent as argument of AssignResources, Configure and Scan.
        """
        return CspLowJsonValidator

    # ----------------
    # Class Properties
    # ----------------

    # -----------------
    # Device Properties
    # -----------------

    # ---------------
    # Public methods
    # ---------------

    def set_component_manager(
        self: LowCspSubarray, cm_configuration: ComponentManagerConfiguration
    ) -> LowCspSubarrayComponentManager:
        """Set the CM for the Low CSP Subarray device.

        :param cm_configuration: A class with all the device properties
            accessible as attributes

        :returns: The Low.Csp SubarrayComponentManager
        """
        return LowCspSubarrayComponentManager(
            self.health_model,
            self.op_state_model,
            self.obs_state_model,
            cm_configuration,
            self.update_property,
            self.logger,
        )

    # ----------
    # Attributes
    # ----------

    @attribute(
        dtype="DevString",
        label="commandResultName",
        doc=(
            "The first entry of commandResult attribute, i.e. the name of"
            " latest command executed. For Taranta visualization"
        ),
    )
    def commandResultName(self: LowCspSubarray) -> str:
        """Return the name of the last executed CSP task.

        :return: The name of the CSP task.
        """
        try:
            # return self.component_manager.command_result[0]
            return self._csp_command_result[0]
        except Exception:
            return ""

    @attribute(
        dtype="DevString",
        label="commandResultCode",
        doc=(
            "The second entry of commandResult attribute, i.e. the resultCode"
            " of latest command executed. For Taranta visualization"
        ),
    )
    def commandResultCode(self: LowCspSubarray) -> str:
        """Return the *ResultCode* of the last executed CSP task.

        :return: The result code (see :py:class:`ResultCode`) of the last
            executed task.
        """
        try:
            # return self.component_manager.command_result[1]
            return self._csp_command_result[1]
        except Exception:
            return ""

    @attribute(
        dtype="DevString",
        label="Stations",
        doc="Report station & substation membership in subarray",
    )
    def stations(self) -> List[int]:
        """Return the stations attribute.

        :return: List of stations.
        """
        try:
            return self.component_manager.online_components[
                self.CbfSubarray
            ].assigned_stations
        # pylint: disable-next=broad-except
        except Exception as e:
            self.logger.error(f"read stations failed with err {e}")
            return []

    @attribute(
        dtype="DevString",
        label="stationBeams",
        doc="Report Station Beams membership in subarray",
    )
    def stationBeams(self) -> List[int]:
        """Return the stationBeams attribute.

        :return: List of station beams.
        """
        try:
            return self.component_manager.online_components[
                self.CbfSubarray
            ].assigned_station_beams
        # pylint: disable-next=broad-except
        except Exception as e:
            self.logger.error(f"read stationBeams failed with err {e}")
            return []

    @attribute(
        dtype="DevString",
        label="pssBeams",
        doc=(
            "Each Pulsar Search Beam is associated with one Station Beam, "
            "and has additional configuration parameters including a delay "
            "polynomial source (supplied via Configure)"
        ),
    )
    def pssBeams(self) -> List[int]:
        """Return the pssBeams attribute.

        :return: List of pss beams.
        """
        try:
            return self.component_manager.online_components[
                self.CbfSubarray
            ].assigned_pss_beams
        # pylint: disable-next=broad-except
        except Exception as e:
            self.logger.error(f"read pssBeams failed with err {e}")
            return []

    @attribute(
        dtype="DevString",
        label="pstBeams",
        doc=(
            "Each Pulsar Timing Beam is associated with one Station Beam, "
            "and has additional configuration parameters including a delay "
            "polynomial source (supplied via Configure)"
        ),
    )
    def pstBeams(self) -> List[int]:
        """Return the pstBeams attribute.

        :return: List of pst beams.
        """
        try:
            return self.component_manager.online_components[
                self.CbfSubarray
            ].assigned_pst_beams
        # pylint: disable-next=broad-except
        except Exception as e:
            self.logger.error(f"read pstBeams failed with err {e}")
            return []


# ----------
# Run server
# ----------


def main(args=None, **kwargs):
    # pylint: disable=missing-function-docstring
    return run((LowCspSubarray,), args=args, **kwargs)


if __name__ == "__main__":
    main()
