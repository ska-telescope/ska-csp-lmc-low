"""LowComponentFactory.

The Observing Device and Controller Component Factories to specialize Low CSP
sub-systems components classes
"""

from __future__ import annotations

import logging

from ska_csp_lmc_common.controller.controller_factory import (
    ControllerComponentFactory,
)
from ska_csp_lmc_common.subarray.csp_subarray import CspSubarrayComponent
from ska_csp_lmc_common.subarray.subarray_factory import (
    ObservingComponentFactory,
)

from ska_csp_lmc_low import (
    LowCbfControllerComponent,
    LowPssControllerComponent,
)
from ska_csp_lmc_low.controller.low_ctrl_component import (
    LowCbfAllocatorComponent,
)
from ska_csp_lmc_low.subarray.low_pst_beam_component import LowPstBeamComponent
from ska_csp_lmc_low.subarray.low_subarray_component import (
    LowCbfSubarrayComponent,
    LowPssSubarrayComponent,
)

# pylint: disable=too-few-public-methods
# pylint: disable=super-init-not-called

module_logger = logging.getLogger(__name__)


class LowObservingComponentFactory(ObservingComponentFactory):
    """Specialize Low CSP sub-systems components classes for observing
    devices."""

    def __init__(
        self: LowObservingComponentFactory, logger: logging.Logger = None
    ) -> None:
        self._creators = {
            "cbf": LowCbfSubarrayComponent,
            "pss": LowPssSubarrayComponent,
            "pst": LowPstBeamComponent,
        }
        self.logger = logger or module_logger


class LowControllerComponentFactory(ControllerComponentFactory):
    """Specialize Low CSP sub-systems components classes for controller
    devices."""

    def __init__(
        self: LowControllerComponentFactory, logger: logging.Logger = None
    ) -> None:
        self._creators = {
            "cbf": LowCbfControllerComponent,
            "pss": LowPssControllerComponent,
            "pst": LowPstBeamComponent,
            "subarray": CspSubarrayComponent,
            "allocator": LowCbfAllocatorComponent,
        }
        self.logger = logger or module_logger
