# -*- coding: utf-8 -*-
#
# This file is part of the Csp Tests project
#
# INAF - SKA Telescope
#
# Distributed under the terms of the GPL license.
# See LICENSE.txt for more info.

"""CSP.LMC Low tests class."""

import logging
import time

from ska_control_model import AdminMode, SimulationMode
from ska_csp_lmc_common.testing.poller import Poller, Probe
from ska_csp_lmc_common.testing.test_bdd_utils import TestDevice

module_logger = logging.getLogger(__name__)

# pylint: disable=logging-fstring-interpolation
# pylint: disable=broad-exception-raised
# pylint: disable=eval-used
# pylint: disable=broad-exception-caught


class LowTestDevice(TestDevice):
    """A class responsible for creation of device proxies and step definitions
    for bdd tests."""

    def set_attribute(self, attribute, value):
        """Override set_attribute from common."""
        module_logger.info("override set_attribute")
        if "adminmode" in attribute.lower():
            value = AdminMode[value.upper()]
            module_logger.info(
                f"Setting admin mode to {AdminMode(value).name}"
            )
        if "simulationmode" in attribute.lower():
            value = SimulationMode[value.upper()]
            module_logger.info(
                f"set SimulationMode to {SimulationMode(value).name}"
            )
        try:
            self.proxy.write_attribute(attribute, value)
        except Exception as err:
            module_logger.error(
                f"Error in setting {attribute} to value " f"{value}: {err}"
            )

    def get_attribute(self, attribute, value, timeout=15):
        prober = None
        attr_list = [
            "deviceshealthstate",
            "devicesadminmode",
            "devicesstate",
            "devicesobsstate",
        ]
        if isinstance(value, str) and attribute.lower() in attr_list:
            command_id = None
            prober = Probe(
                self.proxy,
                attribute,
                eval(value),
                f"{attribute} is not {value}."
                f"It is {self.proxy.read_attribute(attribute).value}",
                command_id=command_id,
            )
            Poller(timeout, 0.1).check(prober)
        else:
            if "longrunning" in attribute.lower():
                timeout = 20
            super().get_attribute(attribute, value, timeout=timeout)

    # redefine the state for real Low devices
    def check_subarray_init_state(self):
        """Check subarrays initial state"""
        # check the init status of the subarrays against the expected
        # values. This method is called for the CSP subarrays as well as
        # for the subarrays of the CSP sub-systems
        module_logger.info(f"Check on {self.name} {self.proxy.name()}")
        if "pss" in self.name.lower():
            self.get_attribute("State", "OFF")
            # self.get_attribute("HealthState", "OK")
        else:
            module_logger.info("CBF and CSP cases")
            self.get_attribute("State", "ON")
            # self.get_attribute("HealthState", "OK")
        # self.get_attribute("ObsState", "EMPTY")
        self.get_attribute("AdminMode", "ONLINE")

    def check_controller_init_state(self):
        """Check controllers initial state"""
        module_logger.info(f"Check on {self.name} {self.proxy.name()}")
        module_logger.info(f"Check Controller {self.name} init state")
        if "pss" in self.name.lower():
            self.get_attribute("State", "OFF")
            # self.get_attribute("HealthState", "OK")
        else:
            self.get_attribute("State", "ON")
            # self.get_attribute("HealthState", "UNKNOWN")
        self.get_attribute("AdminMode", "ONLINE")

    def check_beam_init_state(self):
        """Check controllers initial state"""
        module_logger.info(f"Check on {self.name} {self.proxy.name()}")
        module_logger.info(f"Check Controller {self.name} init state")
        if "pst" in self.name.lower():
            self.get_attribute("State", "OFF")
            # self.get_attribute("HealthState", "OK")
        else:
            self.get_attribute("State", "ON")
            # self.get_attribute("HealthState", "UNKNOWN")
        self.get_attribute("AdminMode", "ONLINE")

    def set_command_faulty(self, value):
        """Set/Reset the fualty flag"""
        self.proxy.faultyInCommand = value


def fresh_all_new_subsystems(all_controllers, all_subarrays, all_beams):
    """Initialize all subsystems of the deployment."""

    all_subsystems = {
        **all_controllers,
        **all_subarrays,
        **all_beams,
    }

    for device in all_subsystems.values():
        device_name = device.return_device_name()
        device.initialize_device()
    for device in all_subsystems.values():
        device_name = device.return_device_name()
        device.ping_device()

    time.sleep(1)
    # Order of iteration is important. CspController has to be last.
    for device in all_subarrays.values():
        device_name = device.return_device_name()
        if "CspSubarray" in device_name:
            module_logger.info(f"Re-enabling {device_name}")
            device.disable_device(False)
            time.sleep(2)
            device.get_attribute("isCommunicating", "True")
    for device in all_beams.values():
        device_name = device.return_device_name()
        module_logger.info(f"Re-enabling {device_name}")
        device.disable_device(False)
        time.sleep(2)
    for device in all_controllers.values():
        device_name = device.return_device_name()
        if device_name == "CspController":
            module_logger.info(f"Re-enabling {device_name}")
            device.disable_device(False)
            time.sleep(2)
            device.get_attribute("isCommunicating", "True", timeout=20)

    time.sleep(1)

    # check initial state of all subarrays
    # check initial state of all subarrays
    for device in all_subarrays.values():
        device.check_subarray_init_state()
    # check initial state of all subarrays
    for device in all_beams.values():
        device.check_beam_init_state()
    # check initial state of all controllers
    for device in all_controllers.values():
        device.check_controller_init_state()


def is_pst_acquiring_data(
    device_name, attribute, beam_name, all_capabilities, all_beams
) -> bool:
    """Check that both the beam attribute (i.e. dataReceived) and the
    capability element of the list (i.e devicesDataReceived).
    right now this step is done for the following attributes:
    [devicesDataDropRate, devicesDataReceived,
    devicesDataReceiveRate, devicesDataDropped]
    :param device_name: name of the capability device
    :param attribute: name of the attribute to be read
    :param beam_name: name of the beam device from which
    the attribute is acquired
    :param all_capabilities: feature that contains the list of
    deployed capabilities
    :param all_beams: feature that contains the list of deployed beam

    :retrun: Flag. True if the data acquired from the beam and the
    the capability is different from zero (the acquisition is working)
    the time
    """
    all_subsystems = {
        **all_beams,
        **all_capabilities,
    }
    beam_attr_value = 0
    cap_attr_value = 0
    ret = False

    if beam_name in all_beams:
        device = all_subsystems[beam_name]
        beam_attribute = attribute.replace("devicesD", "d")
        beam_attr_value = device.read_attribute(beam_attribute)

    if device_name in all_capabilities:
        device = all_subsystems[device_name]
        cap_attr_list = device.read_attribute(attribute)
        beam_number = int(beam_name[-2:])
        cap_attr_value = cap_attr_list[beam_number - 1]

    if beam_attr_value > 0 and cap_attr_value > 0:
        ret = True

    return ret


def are_pst_monitoring_data_acquired(
    device_name, attribute, beam_name, all_capabilities, all_beams
) -> bool:
    """Check that the attribute acquired by the capability is
    changing during the acquisitoin time.
    :param device_name: name of the capability device
    :param attribute: name of the attribute to be read
    :param beam_name: name of the beam device from which
    the attribute is acquired
    :param all_capabilities: feature that contains the list of
    deployed capabilities
    :param all_beams: feature that contains the list of deployed beam

    :retrun: Flag. True if the data acquired are changing during
    the time
    """
    all_subsystems = {
        **all_beams,
        **all_capabilities,
    }
    data_are_changing = True
    cap_attr_value = 0
    check_value_change = []
    if device_name in all_capabilities:
        for _ in range(5):
            device = all_subsystems[device_name]
            cap_attr_list = device.read_attribute(attribute)
            beam_number = int(beam_name[-2:])
            cap_attr_value = cap_attr_list[beam_number - 1]
            check_value_change.append(cap_attr_value)
            time.sleep(1.5)

    for i in range(1, len(check_value_change)):
        # module_logger.debug(
        #     f"-compare monitor attributes:"
        #     f"{attribute}, on device {beam_name}: "
        #     f"{check_value_change[i-1]} vs {check_value_change[i]}"
        # )
        if check_value_change[i - 1] == check_value_change[i]:

            # values are not updated (simulators use random generator)
            data_are_changing = False
            break
    return data_are_changing
