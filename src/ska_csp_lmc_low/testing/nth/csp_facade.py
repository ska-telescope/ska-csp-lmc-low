"""The 'facade' of CSP.LMC integration tests"""

import json
from typing import Any

from assertpy import assert_that
from ska_control_model import HealthState, ObsState, ResultCode
from ska_integration_test_harness.core.assertions import (
    AssertDevicesAreInState,
    AssertDevicesStateChanges,
)
from ska_integration_test_harness.extensions.actions import TangoLRCAction
from ska_tango_testing.integration import TangoEventTracer

# pylint: disable=unused-import
from tango import DeviceProxy, DevState  # noqa: F401

from ska_csp_lmc_low.testing.nth.wrappers import (
    CBFWrapper,
    CSPWrapper,
    LMCWrapper,
    PSTWrapper,
)


class CSPFacade:
    """A facade to Central Signal Processor and its actions.

    A facade to CSP sub-system, providing a simplified interface to the
    system and its actions. The facade is the entry point of CSP.LMC
    integration tests.
    """

    def __init__(
        self, csp: CSPWrapper, num_subarrays, num_beams, logger
    ) -> None:
        self._csp = csp
        self.num_subarrays = num_subarrays
        self.num_beams = num_beams

        self.event_tracer = TangoEventTracer(
            event_enum_mapping={
                "ObsState": ObsState,
                "HealthState": HealthState,
            }
        )
        self.command_inputs = {}
        self.logger = logger
        self.subcribe_relevant_events()

    # -----------------------------------------------------------
    # CSPLMC DEVICES

    @property
    def lmc(self) -> LMCWrapper:
        """The CSP.LMC wrapper"""
        return self._csp.lmc

    @property
    def cbf(self) -> CBFWrapper:
        """The CBF wrapper"""
        return self._csp.cbf

    @property
    def pst(self) -> PSTWrapper:
        """The PST wrapper"""
        return self._csp.pst

    # -----------------------------------------------------------
    # CSPLMC state actions

    def turn_on_pst_beam(self, beam_id: int):
        """
        Turn on all or some of the PST beams.

        :param input_list: A list of PST device names to turn on.
        :param timeout: timeout in seconds to wait for the action to complete
        """

        if (
            not self.lmc.is_controller_in_initial_status()
            or not self.pst.is_beam_in_initial_status(beam_id=beam_id)
        ):
            self.logger.info(
                "System is not in initial state, resetting all devices"
            )
            self.hard_reset()

        self.logger.info("PstBeam is not ON, turning it ON")
        action = self.generic_lrc_action(
            "on",
            self.lmc.controller,
            command_input=[self.pst.beams[beam_id - 1].name()],
        )
        action.add_preconditions(
            AssertDevicesAreInState(
                devices=[self.lmc.controller],
                attribute_name="State",
                attribute_value=DevState.ON,
            )
        )
        action.add_postconditions(
            AssertDevicesStateChanges(
                devices=[self.pst.beams[beam_id - 1]],
                attribute_name="state",
                attribute_value=DevState.ON,
            )
        )
        action.execute(10)

    def command_on_subarray(
        self,
        command_name: str,
        sub_id: int,
        postconditions: dict,
        preconditions: dict,
    ):
        """
        Executes a command on a given subarray and check for given
        postconditions.

        :param command_name: The name of the command to execute.
        :param sub_id: The id of the subarray to execute the command on.
        :param postconditions: A dictionary of postconditions to check after
            the command has been executed.
        :raises AssertionError: If any of the postconditions are not met after
            the command has been executed.
        """
        subarray = self.lmc.subarrays[int(sub_id) - 1]
        command_input = self.evaluate_command_input(command_name, subarray)
        action = self.generic_lrc_action(
            command_name, subarray, command_input=command_input
        )
        for attr_name, attr_value in postconditions.items():
            action.add_postconditions(
                AssertDevicesStateChanges(
                    devices=[subarray],
                    attribute_name=attr_name,
                    attribute_value=attr_value,
                )
            )
        for attr_name, attr_value in preconditions.items():
            action.add_preconditions(
                AssertDevicesAreInState(
                    devices=[subarray],
                    attribute_name=attr_name,
                    attribute_value=attr_value,
                )
            )

        action.execute(10)

    def assign_pst_beam_to_subarray(self, beam_id: int, sub_id: int):
        """
        Assigns a PST beam to a subarray.

        :param beam_id: The id of the PST beam to assign.
        :param sub_id: The id of the subarray to assign the PST beam to.

        The function first checks if the subarray is in its initial status.
        If not, it resets all devices and then turns on the requested
        PST beam.

        Then it checks if the PST beam is already assigned to another subarray.
        If it is, it resets all devices and then turns on the requested
        PST beam.

        Then it assigns the PST beam to the requested subarray, if it is not
        already assigned. It checks that the subarray is assigned with the
        PST beam by checking its state and assignedTimingBeamIDs attribute.

        :raises AssertionError: If the subarray is not assigned with the
            PST beam after the assignment.
        """

        if not self.lmc.is_subarray_in_initial_status(int(sub_id)):
            self.logger.info(
                "CspSubarray not in its initial status, resetting all devices"
            )
            self.hard_reset()
            self.turn_on_pst_beam(beam_id)

        for subarray in self.lmc.subarrays:
            # check that other subarrays doesn't have pst beam assigned
            if str(subarray.assignedTimingBeamIDs) != "[]":
                self.logger.info(
                    "PstBeam already belongs to a different "
                    "subarray, resetting all devices"
                )
                self.hard_reset()
                self.turn_on_pst_beam(beam_id)

        self.command_on_subarray(
            "AssignResources",
            sub_id,
            postconditions={
                "ObsState": ObsState.IDLE,
                "assignedTimingBeamIDs": [1],
            },
            preconditions={
                "ObsState": ObsState.EMPTY,
                "assignedTimingBeamIDs": [],
            },
        )

    def configure_with_pst_beam(self, beam_id: int, sub_id: int):
        """
        Configures a subarray with a PST beam.

        :param beam_id: The id of the PST beam to assign.
        :param sub_id: The id of the subarray to assign the PST beam to.

        The function first assigns the PST beam to the requested subarray.
        Then it configures the subarray. The function checks that the subarray
        is in the READY state after the configuration.
        """
        self.assign_pst_beam_to_subarray(beam_id, sub_id)
        self.command_on_subarray(
            "Configure",
            sub_id,
            postconditions={"ObsState": ObsState.READY},
            preconditions={"ObsState": ObsState.IDLE},
        )

    def start_scanning(self, sub_id: int, beam_id: int):
        """
        Initiates a scanning process on a subarray with a specified PST beam.

        This function first configures the subarray with the given PST beam,
        ensuring that the subarray transitions to the READY state. It then
        issues a scan command to the subarray, transitioning its observation
        state to SCANNING.

        :param sub_id: The ID of the subarray to scan.
        :param beam_id: The ID of the PST beam to use for scanning.
        """

        self.configure_with_pst_beam(beam_id, sub_id)
        self.command_on_subarray(
            "Scan",
            sub_id,
            postconditions={"ObsState": ObsState.SCANNING},
            preconditions={"ObsState": ObsState.READY},
        )

    def end_scan(self, sub_id: int, beam_id: int):
        """
        Ends a scanning process on a subarray with a specified PST beam.

        This function first starts a scanning process on the specified
        subarray with the given PST beam, and then ends the scanning
        process.

        :param sub_id: The ID of the subarray to end the scan.
        :param beam_id: The ID of the PST beam to use for scanning.
        """
        self.start_scanning(sub_id, beam_id)
        self.command_on_subarray(
            "EndScan",
            sub_id,
            postconditions={"ObsState": ObsState.READY},
            preconditions={"ObsState": ObsState.SCANNING},
        )

    def deconfigure_with_pst_beam(
        self, sub_id: int, beam_id: int, when_deconfigured: str
    ):
        """
        Deconfigures a subarray with a PST beam.

        This function first configures or ends a scan on the specified
        subarray with the given PST beam, depending on the value of
        when_deconfigured. It then issues a GoToIdle command to the subarray,
        transitioning its observation state to IDLE.

        :param sub_id: The ID of the subarray to deconfigure.
        :param beam_id: The ID of the PST beam to use for deconfiguring.
        :param when_deconfigured: The point at which the subarray is
            deconfigured. Must be either "before a scan" or "after a scan".
        """
        if when_deconfigured == "before a scan":
            self.configure_with_pst_beam(beam_id, sub_id)
        elif when_deconfigured == "after a scan":
            self.end_scan(sub_id, beam_id)
        else:
            self.logger.error(
                "Invalid Gherkin: when_deconfigured must be before a scan or"
                f" after a scan, got {when_deconfigured} "
            )

        self.command_on_subarray(
            "GoToIdle",
            sub_id,
            postconditions={"ObsState": ObsState.IDLE},
            preconditions={"ObsState": ObsState.READY},
        )

    # -----------------------------------------------------------
    # Generic CSPLMC action

    def generic_lrc_action(
        self, command_name: str, proxy: DeviceProxy, command_input: str = None
    ):

        """
        Create a TangoLRCAction that sends a command to a device and
        includes expected result codes and LRC error handling.

        :param command_name: The command to send to the device
        :param proxy: The device proxy that the command is sent to
        :param command_input: The input to the command if required
        :return: The TangoLRCAction
        """

        action = TangoLRCAction(
            target_device=proxy,
            command_name=command_name,
            command_param=command_input,
        )
        action.add_lrc_completion_to_postconditions(
            expected_result_codes=[ResultCode.OK]
        )
        action.add_lrc_errors_to_early_stop()

        return action

    def evaluate_command_input(self, command_name: str, proxy: DeviceProxy):
        """
        Evaluate the command input for a given command and proxy.

        If the command_name is in self.command_inputs, this method
        evaluates the command input and returns the evaluated input.

        If the command_name is Configure or Assign, and the proxy is a
        subarray, this method sets the subarray_id field of the command
        input to the subarray number of the proxy.

        :param command_name: The name of the command to execute.
        :param proxy: The DeviceProxy representing the target Tango device.
        :return: The evaluated command input.
        """
        command_input = None
        if command_name in self.command_inputs:

            command_input = self.command_inputs[command_name]

            if "subarray" in proxy.name() and (
                "Configure" in command_name
                or "Assign" in command_name
                or "Scan" in command_name
            ):
                command_input = json.loads(command_input)
                command_input["common"]["subarray_id"] = int(proxy.name()[-2:])
                command_input = json.dumps(command_input)

        return command_input

    def perform_action_success(
        self,
        command_name: str,
        proxy: DeviceProxy,
        command_input: Any = None,
        timeout: int = 10,
    ):
        """
        Execute a command successfully on a Tango device.

        This method wraps the execution of a command as a
        LongRunningCommand (LRC) and ensures its completion by
        executing it with a specified postconditions timeout.

        :param command_name: The name of the command to execute.
        :param proxy: The DeviceProxy representing the target Tango device.
        :param command_input: Optional input parameter for the command.
        :param timeout: The time in seconds to wait for postconditions
            to be met.
        """

        if not command_input:
            command_input = self.evaluate_command_input(command_name, proxy)

        action = self.generic_lrc_action(command_name, proxy, command_input)
        action.execute(postconditions_timeout=timeout)

    def is_in_initial_status(self, include_pst: bool = True) -> bool:
        """Check that all CSP devices are in their initial status

        :return: A boolean representing that all subsystem are in their
            initial status"""

        conditions = [
            self.lmc.is_controller_in_initial_status(),
            self.cbf.is_controller_in_initial_status(),
        ]

        conditions += [
            self.lmc.is_subarray_in_initial_status(id)
            for id in range(1, self.num_subarrays + 1)
        ]
        conditions += [
            self.cbf.is_subarray_in_initial_status(id)
            for id in range(1, self.num_subarrays + 1)
        ]
        conditions += [
            self.pst.is_beam_in_initial_status(id)
            for id in range(1, self.num_beams + 1)
        ]

        if include_pst:
            conditions.append(self.pst.is_beam_in_initial_status())
        return all(conditions)

    def hard_reset(self):
        """Reset all devices and then check their initial status"""

        self.cbf.reinit_all_devices()
        self.pst.reinit_all_devices()
        self.lmc.reinit_all_devices()

        self.logger.info("Setting adminmode to ONLINE on CSP Controller")
        self.lmc.controller.adminmode = 0

        self.check_attribute_on_device(
            "state",
            DevState.ON,
            self.lmc.controller,
            previous_value=DevState.DISABLE,
        )
        for subarray in self.lmc.subarrays:
            self.check_attribute_on_device(
                "state", DevState.ON, subarray, previous_value=DevState.DISABLE
            )
            self.check_attribute_on_device(
                "assignedtimingbeamids",
                [],
                subarray,
            )

        for beam in self.pst.beams:
            self.check_attribute_on_device(
                "state", DevState.OFF, beam, previous_value=DevState.UNKNOWN
            )

    def translate_attributes_in_enum(self, attribute, value):
        """
        Translates a string attribute value to its corresponding
        enumeration type.

        This function takes an attribute name and its string value,
        typically parsed from Gherkin, and translates it into the
        corresponding value. The translation is based on a predefined map
        of attribute names to their respective enumeration classes.

        :param attribute: The name of the attribute to be translated.
        :param value: The string value of the attribute to be translated.
        :return: The translated enumeration value.
        """

        enumMap = {
            "state": "DevState",
            "ObsState": "ObsState",
            "HealthState": "HealthState",
        }

        # we need to translate the string that comes from gherkin parsing
        # in the correspondant python/tango attribute
        if attribute in enumMap:
            # pylint: disable=eval-used
            correct_value = eval(f"{enumMap[attribute]}.{value}")
        else:
            correct_value = eval(value)  # pylint: disable=eval-used

        return correct_value

    # pylint:disable=too-many-arguments
    def check_attribute_on_device(
        self,
        attribute: str,
        value: Any,
        proxy: DeviceProxy,
        timeout: str = 10,
        previous_value: Any = None,
        new_tracer=False,
    ):
        """Check the value of an attribute on a specific proxy

        :param: attribute: the name of Tango attribute to be checked
        :param: value: the value of the attribute to be checked
        :param: proxy: the Tango device on which the attribute
            has to be checked"""

        if new_tracer:
            event_tracer = TangoEventTracer(
                event_enum_mapping={
                    "ObsState": ObsState,
                    "HealthState": HealthState,
                }
            )
            event_tracer.subscribe_event(proxy, attribute)
        else:
            event_tracer = self.event_tracer
            self.subscribe_if_not_subscribed(proxy, attribute)

        kwargs = {}
        if previous_value:
            kwargs["previous_value"] = previous_value

        if value == []:
            # this is a special case when to check an empty list
            kwargs["custom_matcher"] = (
                lambda e: len(list(e.attribute_value)) == 0
            )
        else:
            kwargs["attribute_value"] = value

        assert_that(event_tracer).described_as(
            f"Attribute {attribute} of {proxy.name()} is not {value} "
            f"it is {proxy.read_attribute(attribute).value}"
        ).within_timeout(timeout).has_change_event_occurred(
            proxy, attribute.lower(), **kwargs
        )

    def subscribe_if_not_subscribed(self, proxy, attribute):
        """
        If the event for the given proxy and attribute is not yet subscribed,
        subscribe it.

        :param proxy: the device proxy to subscribe to
        :param attribute: the attribute to subscribe to
        """
        for event in self.event_tracer.events:
            if (
                event.device_name == proxy.name()
                and event.attribute_name == attribute
            ):
                return

        self.logger.info(
            f"Suscribing for changes in {attribute} on {proxy.name()}"
        )
        self.event_tracer.subscribe_event(proxy, attribute)

    def subcribe_relevant_events(self):
        """
        Subscribe to the relevant events for the device proxies provided.

        It will subscribe to the following events:
        - State
        - HealthState
        - ObsState for non control devices

        :param self: the instance of the class
        """
        attribute_list = ["state", "HealthState"]
        proxy_list = [
            self.lmc.controller,
            self.cbf.controller,
        ]
        proxy_list += self.lmc.subarrays
        proxy_list += self.cbf.subarrays
        proxy_list += self.pst.beams

        for proxy in proxy_list:
            if "ObsState" not in attribute_list:
                if "control" not in proxy.name():
                    attribute_list.append("ObsState")
            else:
                if "control" in proxy.name():
                    attribute_list.remove("ObsState")
            for attribute in attribute_list:
                self.logger.info(
                    f"Suscribing for changes in {attribute} on {proxy.name()}"
                )
                self.event_tracer.subscribe_event(proxy, attribute)

    def teardown(self):
        """
        Tears down the CSP in an orderly fashion.

        This method will put the Low CSP in an empty state, and the Low PST
        beams in an off state. It will abort any active scans and restart the
        subarrays to ensure they are in the empty state. It will then turn off
        the Low PST beams.
        """
        self.logger.info("Tearing down CSP")
        try:
            for subarray in self.lmc.subarrays:
                if subarray.obsstate not in [ObsState.EMPTY, ObsState.ABORTED]:
                    action = self.generic_lrc_action("Abort", subarray)
                    action.add_postconditions(
                        AssertDevicesStateChanges(
                            devices=[subarray],
                            attribute_name="ObsState",
                            attribute_value=ObsState.ABORTED,
                        )
                    )
                    action.execute(5)

                    action = self.generic_lrc_action("Restart", subarray)
                    action.add_preconditions(
                        AssertDevicesAreInState(
                            devices=[subarray],
                            attribute_name="ObsState",
                            attribute_value=ObsState.ABORTED,
                        ),
                    )
                    action.add_postconditions(
                        AssertDevicesStateChanges(
                            devices=[subarray],
                            attribute_name="ObsState",
                            attribute_value=ObsState.EMPTY,
                        )
                    )
                    action.execute(5)

            action = self.generic_lrc_action("Off", self.lmc.controller, [])
            action.add_postconditions(
                AssertDevicesStateChanges(
                    devices=[self.pst.beams[0]],
                    attribute_name="State",
                    attribute_value=DevState.OFF,
                )
            )
            action.execute(5)
        except AssertionError as error:
            self.logger.error(f"Teardown FAILED: {error}")
