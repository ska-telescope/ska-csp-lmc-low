"""The Base Class of all CSP subsystems wrapper"""

import time

from ska_control_model import AdminMode, HealthState, LoggingLevel, ObsState
from tango import DeviceProxy, DevState


class BaseCSPWrapper:
    """The Base Class of all CSP subsystems wrapper"""

    def __init__(self, logger=None):
        self.logger = logger
        self.controller = None
        self.subarrays = None

    def is_controller_in_initial_status(self) -> bool:
        """Check that Controller is in it initial state.
        If the Wrapper has no Controller defined, raise and exception

        Controller initial state:
            State.ON
            AdminMode.ONLINE
            HealthState.UNKNOWN

        :return: a boolean value representing if the controller is
            in its initial status"""

        device = self.controller

        if not device:
            raise NotImplementedError("No controller defined")

        conditions = self._base_initial_status_conditions(device)

        self._log_failed_initial_status_conditions(conditions, device)

        return all(conditions.values())

    def is_subarray_in_initial_status(self, sub_id: int) -> bool:
        """Check that Subarray is in it initial state.
        If the Wrapper has no Subarray defined, raise and exception

        Subarray initial state:
            State.ON
            AdminMode.ONLINE
            HealthState.UNKNOWN
            ObsState.EMPTY

        :return: a boolean value representing if the subarray is
            in its initial status"""

        device = self.subarrays[sub_id - 1]

        if not device:
            raise NotImplementedError("No subarray defined")

        conditions = self._base_initial_status_conditions(device)

        conditions["obsstate"] = device.obsstate == ObsState.EMPTY

        self._log_failed_initial_status_conditions(conditions, device)

        return all(conditions.values())

    def _base_initial_status_conditions(self, device: DeviceProxy) -> dict:
        """Return the base initial status conditions,
        valid for most devices

        :param: device: the device on which the conditions are checked
        :return: a dictionary with the boolean values of
            the base conditions checks"""

        conditions = {
            "state": device.state() == DevState.ON,
            "adminmode": device.adminmode == AdminMode.ONLINE,
            "healthstate": device.healthstate == HealthState.UNKNOWN,
        }
        return conditions

    def _log_failed_initial_status_conditions(
        self, conditions: dict, device: DeviceProxy
    ) -> None:
        """Print a log of the failed condition on the initial status check
        :param: conditions: the dictionary with the boolean values of
            the conditions checked
        :param: device: the Tango deviceProxy on which the conditions
            are checked"""

        failed_checks = [
            name for name, result in conditions.items() if not result
        ]
        dev_name = device.name()
        if self.logger and failed_checks:
            self.logger.warning(
                f"Check on {dev_name} initial status"
                f"failed for: {', '.join(failed_checks)}"
            )
            for check in failed_checks:
                self.logger.warning(
                    f"{device.read_attribute(check).name} is"
                    f" {device.read_attribute(check).value}"
                )
        else:
            self.logger.info(f"All initial Checks are met for {dev_name}")

    def check_device(self, device: DeviceProxy, timeout: int = 10) -> None:
        """Check that the device can be reached by Tango communications
        :param: device: the Tango deviceproxy to be checked"""
        total = 0
        step = 1
        dev_name = device.name()
        while True:
            try:
                device.ping()
                device.state()
                if "sim" not in dev_name and "pst" not in dev_name:
                    device.logginglevel = LoggingLevel.DEBUG
                self.logger.info(
                    f"{dev_name} available at {time.asctime()} -> {total}"
                )
                break
            # pylint: disable-next=broad-except
            except Exception as e:
                if total > timeout:
                    self.logger.error(f"ping_device: got exception {e}")
                    assert False, f"Initialziation of device {dev_name} failed"
                time.sleep(step)
                total += step
                continue

    def initialize_device(self, device: DeviceProxy) -> None:
        """Try to send init to the device 5 times
        :param: device: the device to be initialized"""
        dev_name = device.name()
        self.logger.info(f"Reinitialize {dev_name} at :{time.asctime()}")
        retry_count = 0
        ret = False
        while retry_count < 5:
            try:
                device.init()
                self.logger.info(
                    f"Device {dev_name} restarted at :{time.asctime()}"
                    f" retry: {retry_count}"
                )
                ret = True
                break
            # pylint: disable=broad-exception-caught
            except Exception as e:
                self.logger.warning(
                    f" initialize_device: retry: {retry_count}"
                )
                retry_count += 1
                if retry_count == 5:
                    self.logger.error(f"initialize_device: got exception {e} ")

                time.sleep(1.5)
        return ret

    def _check_devices_are_alive(self, devices: list) -> None:
        """Check that all the devices in the list are alive

        :param: devices: the list of devices to be checked"""
        for device in devices:
            self.check_device(device)
