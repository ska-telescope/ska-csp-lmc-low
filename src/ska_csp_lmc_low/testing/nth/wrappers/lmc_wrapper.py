"""A test wrapper for the CSP."""
from tango import DeviceProxy

from .base_wrapper import BaseCSPWrapper

# pylint: disable=too-few-public-methods


class LMCWrapper(BaseCSPWrapper):
    """A test wrapper for the CSP.LMC."""

    def __init__(self, num_subarrays: int, logger=None):
        """Initialise the CSPLMC wrapper."""
        super().__init__(logger)
        self.controller = DeviceProxy("low-csp/control/0")
        self.subarrays = [
            DeviceProxy(f"low-csp/subarray/0{i+1}")
            for i in range(num_subarrays)
        ]
        self._check_devices_are_alive([self.controller] + self.subarrays)

    def reinit_all_devices(self):
        """Reinizialize all the devices of CSP.LMC"""

        for subarray in self.subarrays:
            self.initialize_device(subarray)
            self.check_device(subarray)

        self.initialize_device(self.controller)
        self.check_device(self.controller)
