"""A test wrapper for the CSP."""
from ska_control_model import HealthState, ObsState
from tango import DeviceProxy, DevState

from .base_wrapper import BaseCSPWrapper


# pylint: disable=too-few-public-methods
class PSTWrapper(BaseCSPWrapper):
    """A test wrapper for the PST."""

    def __init__(self, num_beams: int, logger=None):
        """Initialise the PST wrapper."""
        self.beams = [
            DeviceProxy(f"low-pst/beam/0{i+1}") for i in range(num_beams)
        ]
        super().__init__(logger)
        self._check_devices_are_alive(self.beams)

    def is_beam_in_initial_status(self, beam_id: int) -> bool:
        """Check that PST Beam is in it initial state.
        Controller initial state:
            State.OFF
            AdminMode.OK
            HealthState.UNKNOWN
            ObsState.IDLE

        :return: a boolean value representing if the beam is
            in its initial status"""

        device = self.beams[beam_id - 1]
        conditions = self._base_initial_status_conditions(device)

        conditions["state"] = device.state() == DevState.OFF
        conditions["healthstate"] = device.healthstate == HealthState.OK
        conditions["obsstate"] = device.obsstate == ObsState.IDLE

        self._log_failed_initial_status_conditions(conditions, device)

        return all(conditions.values())

    def reinit_all_devices(self):
        """Reinizialize all the devices of PST"""
        for beam in self.beams:
            self.initialize_device(beam)
            self.check_device(beam, timeout=15)
