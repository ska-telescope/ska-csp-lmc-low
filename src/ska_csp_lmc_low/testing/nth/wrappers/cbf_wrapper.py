"""A test wrapper for the CBF."""
from tango import DeviceProxy

from .base_wrapper import BaseCSPWrapper


# pylint: disable=too-few-public-methods
class CBFWrapper(BaseCSPWrapper):
    """A test wrapper for the CBF."""

    def __init__(self, num_subarrays: int, logger=None):
        """Initialise the CBF wrapper."""
        super().__init__(logger)
        self.controller = DeviceProxy("low-cbf/control/0")
        self.subarrays = [
            DeviceProxy(f"low-cbf/subarray/0{i+1}")
            for i in range(num_subarrays)
        ]

        self._check_devices_are_alive([self.controller] + self.subarrays)

        for subarray in self.subarrays:
            if subarray.get_attribute_poll_period("ObsState") != 0:
                # stop polling obsstate, it interferes with event checks
                subarray.stop_poll_attribute("ObsState")
                self.logger.info(
                    f"Polling stopped for ObsState in {subarray.name()}"
                )

    def reinit_all_devices(self):
        """Reinizialize all the devices of CBF"""

        for subarray in self.subarrays:
            self.initialize_device(subarray)
            self.check_device(subarray)
            # subarray.adminmode = 1

        self.initialize_device(self.controller)
        self.check_device(self.controller)
        # self.controller.adminmode = 1
