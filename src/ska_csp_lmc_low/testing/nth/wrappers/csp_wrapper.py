"""A wrapper class that contains all the telescope sub-systems."""


from numpy import random as rdm

from .cbf_wrapper import CBFWrapper
from .lmc_wrapper import LMCWrapper
from .pss_wrapper import PSSWrapper
from .pst_wrapper import PSTWrapper


class CSPWrapper:
    """A wrapper class that contains all the Central Signal Processor sub-systems.
    It is a lower level copy of the Telescope Wrapper of the SKA Integration Testig Harness
    https://gitlab.com/ska-telescope/ska-integration-test-harness/-/blob/main/src/ska_integration_test_harness/structure/telescope_wrapper.py?ref_type=heads

    This infrastructural class is used as an unique entry point to access
    all the CSP sub-systems and its devices. Given an instance of this
    class, using its properties, it is possible to access the CBF, PSS and PST sub-systems.

    This class is a *Singleton*, so this mean that there is only one instance
    of it in the entire code. This is done to avoid the creation of multiple
    "telescope" instances, potentially inconsistently initialised with
    different sub-subsystems (which may be configured differently).

    To initialise the telescope test structure, create an instance of this
    class and call the `set_up` method with the instances of the TMC, SDP, CSP,
    and Dishes sub-systems. After the initialisation, in any point of the
    code you can create an instance of this class and have it already
    initialised with the sub-systems.

    .. code-block:: python

        # Initialise the telescope test structure
        telescope = TelescopeWrapper()
        telescope.set_up(tmc, sdp, csp, dishes)

        # ...

        # In any point of the code, you can access the sub-systems
        # (and their devices) using the properties of the telescope instance.
        telescope = TelescopeWrapper()
        do_something(telescope.tmc.central_node)

    When you are done with testing, you can tear down the entire telescope
    test structure and reset it to the initial state calling the `tear_down`
    method.

    .. code-block:: python

        # Tear down the telescope test structure
        telescope = TelescopeWrapper()
        telescope.tear_down()

    The *Singleton* is a pretty much standard design pattern. To learn more
    about it, you can refer to the following resources.

    - General idea explanation: https://refactoring.guru/design-patterns/singleton
    - Python implementation: https://www.geeksforgeeks.org/singleton-pattern-in-python-a-complete-guide/
    """  # pylint: disable=line-too-long # noqa: E501

    # -----------------------------------------------------------------
    # SINGLETON PATTERN IMPLEMENTATION

    _instance = None

    _lmc: LMCWrapper | None = None
    _cbf: CBFWrapper | None = None
    _pss: PSSWrapper | None = None
    _pst: PSTWrapper | None = None
    sing_check = rdm.rand()

    def __new__(cls):
        if cls._instance is None:
            cls._instance = super(CSPWrapper, cls).__new__(cls)
        return cls._instance

    # -----------------------------------------------------------------
    # Subsystem access points

    @property
    def lmc(self) -> LMCWrapper:
        """A wrapper for the CSP.LMC sub-system and its devices.

        :return: The LMC Wrapper instance.

        :raises ValueError: If one or more sub-systems are missing.
        """
        self.fail_if_not_set_up()
        return self._lmc

    @property
    def cbf(self) -> CBFWrapper:
        """A wrapper for the CBF sub-system and its devices.

        :return: The CBF Wrapper instance.

        :raises ValueError: If one or more sub-systems are missing.
        """
        self.fail_if_not_set_up()
        return self._cbf

    @property
    def pss(self) -> PSSWrapper:
        """A wrapper for the PSS sub-system and its devices.

        :return: The CBF Wrapper instance.

        :raises ValueError: If one or more sub-systems are missing.
        """
        self.fail_if_not_set_up()
        return self._pss

    @property
    def pst(self) -> PSTWrapper:
        """A wrapper for the PST sub-system and its devices.

        :return: The PST Wrapper instance.

        :raises ValueError: If one or more sub-systems are missing.
        """
        self.fail_if_not_set_up()
        return self._pst

    # -----------------------------------------------------------------
    # Initialisation and tear down methods

    def set_up(
        self,
        lmc: LMCWrapper,
        cbf: CBFWrapper,
        pss: PSSWrapper,
        pst: PSTWrapper,
    ) -> None:
        """Initialise the telescope test structure with the given devices."""
        self._lmc = lmc
        self._cbf = cbf
        self._pss = pss
        self._pst = pst

    def fail_if_not_set_up(self) -> None:
        """Fail if the telescope test structure is not set up.

        :raises ValueError: If one or more sub-systems are missing.
        """
        if (
            self._lmc is None
            or self._cbf is None
            or self._pss is None
            or self._pst is None
        ):
            raise ValueError(
                "Telescope test structure is not set up "
                "(one or more sub-systems are missing). Sub-systems: "
                f"CSP.LMC={self._lmc}, CBF={self._cbf}, PSS={self._pss}, "
                f"PST={self._pst}, \n"
                "Please set up the telescope test structure first calling the "
                "`set_up` method."
            )
