"""Class to manage the update and aggregation of the PST beams data."""

from __future__ import annotations  # allow forward references in type hints

import json
import logging
from typing import Any, Callable, Optional

from ska_control_model import ObsState
from ska_control_model.pst_processing_mode import PstProcessingMode
from ska_csp_lmc_common.capability.capability_data_handler import (
    CapabilityDataHandler,
)
from ska_csp_lmc_common.capability.capability_monitor import MonitoringData

module_logger = logging.getLogger(__name__)

# data_receive_rate_monitoring_class = {
#     attr_name1:{fqdn1:MonitoringData,
#                 fqdn2:MonitoringData,
#                 fqdnn:MonitoringData,
#                 },
#     attr_name2:{fqdn1:MonitoringData,
#                 fqdn2:MonitoringData,
#                 fqdnn:MonitoringData,},
#     attr_namen:{fqdn1:MonitoringData,
#                 fqdn2:MonitoringData,
#                 fqdnn:MonitoringData,},
#     }

# json_string_monitoring_data = {
#     attr_name1:{fqdn1:MonitoringData.get_json_data(),
#                 fqdnn:MonitoringData.get_json_data(),
#                 },
#     attr_namen:{fqdn1:MonitoringData.get_json_data(),
#                 fqdnn:MonitoringData.get_json_data(),
#                 },
#                 }

# pylint: disable=too-many-instance-attributes


class PstDataHandler(CapabilityDataHandler):
    """
    Class handling the update and access to the dictionary with the
    overall devices status.
    """

    def __init__(
        self: PstDataHandler,
        capability_name,
        task_callback: Optional[Callable] = None,
        logger: Optional[logging.Logger] = None,
    ):

        self.data_receive_rate_monitoring_class = {}
        self.data_drop_rate_monitoring_class = {}
        self.data_drop_rate_monitoring_json = {}
        self.data_receive_rate_monitoring_json = {}
        self.data_receive_rate_avg = []
        self.data_receive_rate_stddev = []
        self.data_drop_rate_avg = []
        self.data_drop_rate_stddev = []
        self.devices_fqdn_order = []

        self.json_string_monitoring_data = {}
        self.monitoring_data = {}
        self.avg_monitored_data = {}
        self.stddev_monitored_data = {}

        super().__init__(
            capability_name=capability_name,
            task_callback=task_callback,
            logger=logger,
        )

    #
    # Class public methods
    #

    def init(self: CapabilityDataHandler, list_of_devices):
        """Extend the inizialization of the dataHandler to add the monitoring
        data structures"""
        super().init(list_of_devices=list_of_devices)

        device_number = len(list_of_devices)
        self.devices_fqdn_order = list_of_devices
        self.data_receive_rate_avg = [0.0] * device_number
        self.data_receive_rate_stddev = [0.0] * device_number
        self.data_drop_rate_avg = [0.0] * device_number
        self.data_drop_rate_stddev = [0.0] * device_number

        for fqdn in list_of_devices:
            # create the monitoring data structure for the devices information
            device_index = fqdn[-2:]
            self.data_receive_rate_monitoring_class[fqdn] = MonitoringData(
                attr_name="datareceiverate",
                device_index=device_index,
                buffer_size=10,
            )
            self.data_drop_rate_monitoring_class[fqdn] = MonitoringData(
                attr_name="datadroprate",
                device_index=device_index,
                buffer_size=10,
            )
            self.data_receive_rate_monitoring_json[
                fqdn
            ] = self.data_receive_rate_monitoring_class[fqdn].get_json_data()
            self.data_drop_rate_monitoring_json[
                fqdn
            ] = self.data_drop_rate_monitoring_class[fqdn].get_json_data()

        self._monitoring_data()

    def add_device_data(self: PstDataHandler):
        """
        Update the internal device_data json dictionary with specific
        information of PST Capability. function executed during __init__()
        """
        pst_data = {
            "data_received": 0,
            "data_receive_rate": 0.0,
            "data_dropped": 0,
            "data_drop_rate": 0.0,
            "processing_mode": PstProcessingMode.IDLE.name,
            "obs_state": ObsState.IDLE.name,
        }

        self.device_data |= pst_data

        attrname_to_key_map = {
            "datareceived": "data_received",
            "datareceiverate": "data_receive_rate",
            "datadropped": "data_dropped",
            "datadroprate": "data_drop_rate",
            "pstprocessingmode": "processing_mode",
            "obsstate": "obs_state",
        }
        self.attrname_to_key_map |= attrname_to_key_map

        # Note: PST handles the pstProcessingMode attribute as a string not as
        # an enum value!
        attrname_to_enum_class_map = {
            # "pstprocessingmode": PstProcessingMode,
            "obsstate": ObsState,
        }
        self.attrname_to_enum_class_map |= attrname_to_enum_class_map

    def _monitoring_data(self):
        """Initialize the internal device_data with specific information
        related to the devices monitoring parameters
        """

        self.json_string_monitoring_data = {
            "datareceiverate": self.data_receive_rate_monitoring_json,
            "datadroprate": self.data_drop_rate_monitoring_json,
        }

        self.monitoring_data = {
            "datareceiverate": self.data_receive_rate_monitoring_class,
            "datadroprate": self.data_drop_rate_monitoring_class,
        }
        # create the monitoring data structure for the avg information
        self.avg_monitored_data = {
            "datareceiverate": self.data_receive_rate_avg,
            "datadroprate": self.data_drop_rate_avg,
        }
        self.stddev_monitored_data = {
            "datareceiverate": self.data_receive_rate_stddev,
            "datadroprate": self.data_drop_rate_stddev,
        }

    def update(
        self: PstDataHandler,
        device_fqdn: str,
        attr_name: str,
        attr_value: Any,
    ):
        """Extend Method to update the monitoring parameters"""

        if attr_name in self.monitoring_data:
            self.monitoring_data[attr_name][device_fqdn].update(attr_value)

            avg = self.monitoring_data[attr_name][device_fqdn].get_avg()
            std = self.monitoring_data[attr_name][device_fqdn].get_stddev()
            json_data = self.monitoring_data[attr_name][
                device_fqdn
            ].get_json_data()
            device_index = self.devices_fqdn_order.index(device_fqdn)

            self.avg_monitored_data[attr_name][device_index] = avg
            self.stddev_monitored_data[attr_name][device_index] = std
            self.json_string_monitoring_data[attr_name][
                device_fqdn
            ] = json_data
        super().update(
            device_fqdn=device_fqdn, attr_name=attr_name, attr_value=attr_value
        )

    def update_attribute(self, attr_name):
        """Extend Method to update the monitoring attribute in the device"""
        super().update_attribute(attr_name=attr_name)

        if attr_name in self.monitoring_data:
            self._update_device_property(
                f"devices{attr_name}avg", self.avg_monitored_data[attr_name]
            )
            self._update_device_property(
                f"devices{attr_name}stddev",
                self.stddev_monitored_data[attr_name],
            )
            self._update_device_property(
                f"devices{attr_name}monitoring",
                json.dumps(self.json_string_monitoring_data[attr_name]),
            )
