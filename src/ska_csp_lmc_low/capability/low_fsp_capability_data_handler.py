"""FSP Capability Info Manager Class."""

from __future__ import annotations  # allow forward references in type hints

import json
import logging
from typing import Callable, Optional

from ska_csp_lmc_common.capability.capability_data_handler import (
    CapabilityDataHandler,
)

# pylint: disable=too-many-instance-attributes
# pylint: disable=broad-except
# pylint: disable=too-many-nested-blocks


class LowFspDataHandler(CapabilityDataHandler):
    """
    Class handling the update and access to the dictionary with the
    overall FSPs status.
    """

    #
    # Class public methods
    #

    def __init__(
        self: CapabilityDataHandler,
        capability_name,
        task_callback: Optional[Callable] = None,
        logger: Optional[logging.Logger] = None,
    ) -> None:

        super().__init__(capability_name, task_callback, logger)
        self.list_of_connected_devices = []

    def init(self: CapabilityDataHandler, list_of_devices: list) -> None:
        """Override  the initialization of the DataHandler.
        This is needed to allow connection to processors
        at different times.
        """
        super().init(list_of_devices)
        self.list_of_connected_devices += list_of_devices

        # we need to override the number of device deployed
        # in fsp capability devices are connected in different time,
        # In parent class devices_deployed is just the len(list_of_devices)
        # so it will be overwritten to 1 for every single device connected
        self._json_dict["devices_deployed"] = len(
            self.list_of_connected_devices
        )

    def add_device_data(self: LowFspDataHandler):
        """
        Update the internal device_data json dictionary with specific
        information of FSP Capability
        """
        fsp_data = {
            "serialnumber": "",
            "ready": "false",
            "firmware": "",
            "subarray_ids": "",
            "pkts_in": 0,
            "pkts_out": 0,
        }

        self.device_data |= fsp_data

        attrname_to_key_map = {
            "dev_id": "dev_id",
            "ready": "ready",
            "firmware": "firmware",
            "subarrayids": "subarray_ids",
            "pktsin": "pkts_in",
            "pktsout": "pkts_out",
            "serialnumber": "serialnumber",
        }

        self.attrname_to_key_map |= attrname_to_key_map

    def _update_device_dict_and_attribute(self, attr_name, attr_value, idx):
        """
        Update the device_dict after parsing the attribute
        received into the appropriated ones

        :param attr_name: the name of the attribute
        :param attr_value: the value the attribute
        :param idx: the device index
        """
        if attr_name == "stats_mode":
            attr_value = json.loads(attr_value)
            for sub_attr in ["ready", "firmware"]:
                self._update_device_dict_entry(
                    sub_attr, attr_value[sub_attr], idx
                )
                self.update_attribute(sub_attr)

        # note: at the moment we consider total_pkts for simplicity,
        # it is also possible to subscribe to spead_pkts_in
        # and <"valid">_pkts_out
        elif attr_name == "stats_io":
            attr_value = json.loads(attr_value)
            if attr_value:
                for sub_attr_name, sub_attr in [
                    ("pktsin", "total_pkts_in"),
                    ("pktsout", "total_pkts_out"),
                ]:
                    self._update_device_dict_entry(
                        sub_attr_name, attr_value[sub_attr], idx
                    )
                    self.update_attribute(sub_attr_name)
        else:
            if attr_name == "subarrayids":
                # this line translate into a string the list of
                # subarrayids associated.
                # This because we can report a list of lists in Tango
                # e.g. [1, 2] -> "1, 2"
                attr_value = (
                    str(attr_value)
                    .rsplit("[", maxsplit=1)[-1]
                    .rsplit("]", maxsplit=1)[0]
                )

            self._update_device_dict_entry(attr_name, attr_value, idx)
            self.update_attribute(attr_name)
