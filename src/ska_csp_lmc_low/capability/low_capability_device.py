# -*- coding: utf-8 -*-
#
# This file is part of the LowCspCapability project
#
# INAF - SKA Telescope
#
# Distributed under the terms of the GPL license.
# See LICENSE.txt for more info.

"""Specialize the Capability classes for the Low.CSP.LMC."""
from __future__ import annotations

import logging
from typing import Any

from ska_csp_lmc_common.capability.capability_device import CspCapability
from tango.server import run

module_logger = logging.getLogger(__name__)

# pylint: disable=logging-format-interpolation
# pylint: disable=logging-fstring-interpolation
# pylint: disable=attribute-defined-outside-init
# pylint: disable=protected-access
# pylint: disable=broad-except


class LowCspCapability(CspCapability):
    """
    LOW CSP Capability device
    Aggregates information and presents to higher level devices
    """

    def __init__(
        self: LowCspCapability,
        *args: Any,
        **kwargs: Any,
    ) -> None:
        """
        Initialise a new instance.

        :param args: positional arguments.
        :param kwargs: keyword arguments.
        """
        # This __init__ method is created for type-hinting purposes only.
        # Tango devices are not supposed to have __init__ methods,
        # And they have a strange __new__ method,
        # that calls __init__ when you least expect it.
        # So don't put anything executable in here
        # (other than the super() call).

        super().__init__(*args, **kwargs)

        # CT-1338: always connected since there is still
        # no real monitored capability device
        self.component_manager._connected: bool = True


def main(args=None, **kwargs):
    """
    Launch an SKABaseDevice device.

    :param args: positional args to tango.server.run
    :param kwargs: named args to tango.server.run
    """
    return run((LowCspCapability,), args=args, **kwargs)


if __name__ == "__main__":
    main()
