# -*- coding: utf-8 -*-
#
# This file is part of the LowCspCapabilityPst project
#
# INAF - SKA Telescope
#
# Distributed under the terms of the GPL license.
# See LICENSE.txt for more info.

"""Specialize the Capability classes for the Low.CSP.LMC."""
from __future__ import annotations

import logging
from typing import Any, List, Tuple

from ska_control_model import AdminMode, ResultCode
from ska_csp_lmc_common.capability.capability_device import CspCapability
from ska_csp_lmc_common.manager.manager_configuration import (
    ComponentManagerConfiguration,
)
from ska_tango_base import SKABaseDevice
from tango.server import attribute, device_property, run

from ska_csp_lmc_low.capability.low_pst_capability_component_manager import (
    LowPstCapabilityComponentManager,
)

module_logger = logging.getLogger(__name__)

# pylint: disable=logging-format-interpolation
# pylint: disable=logging-fstring-interpolation
# pylint: disable=attribute-defined-outside-init
# pylint: disable=protected-access
# pylint: disable=broad-except
# pylint: disable=too-many-public-methods


class LowCspCapabilityPst(CspCapability):
    """
    LOW CSP Capability device
    Aggregates information and presents to higher level devices
    """

    def _init_state_model(self: LowCspCapabilityPst) -> None:
        # attribute list that push change_event
        # push_event_attributes_list = ["pstBeamsDeployed", "pstBeamsJson"]
        push_event_attributes_list = [
            "devicesState",
            "devicesHealthState",
            "devicesAdminMode",
            "devicesJson",
            "devicesPstProcessingMode",
            "devicesDataReceived",
            "devicesDataReceiveRate",
            "devicesDataDropped",
            "devicesDataDropRate",
            "devicesObsState",
            "devicesDataDropRateAvg",
            "devicesDataDropRateStddev",
            "devicesDataReceiveRateAvg",
            "devicesDataReceiveRateStddev",
            "devicesDataReceiveRateMonitoring",
            "devicesDataDropRateMonitoring",
        ]
        self._list_of_attributes = (
            self._list_of_attributes + push_event_attributes_list
        )

        super()._init_state_model()

    def set_component_manager(
        self: LowCspCapabilityPst,
        cm_configuration: ComponentManagerConfiguration,
    ) -> None:
        """Configure the ComponentManager for the CSP Capability device.

        :param cm_configuration: A class with all the device properties
            accessible as attributes
        """
        return LowPstCapabilityComponentManager(
            cm_configuration,
            update_device_property_cbk=self.update_device_attribute,
            logger=self.logger,
        )

    class InitCommand(SKABaseDevice.InitCommand):
        """Class to handle the CSP.LMC Controller Init command."""

        def do(
            self: LowCspCapabilityPst.InitCommand,
            *args: Any,
            **kwargs: Any,
        ) -> Tuple[ResultCode, str]:
            """Invoke the *init* command on the Controller ComponentManager.

            :return: a tuple with the command result code and an informative
                message.
            """
            # NOTE: in InitCommand parent class, the healthState of the device
            # is set to OK. To make the HealthStateModel work fine, the initial
            # value of the healthState must be set to OK, too.
            super().do()
            # device._health_state = HealthState.UNKNOWN
            # This is a temporary line to make the component_manager able to
            # manage old initialization
            self._device.component_manager.init()
            self._device._admin_mode = AdminMode.ONLINE
            return ResultCode.OK, "Low PstCapability init command finished"

    # -----------------
    # Device Properties
    # -----------------

    CspController = device_property(dtype="DevString")

    # ----------
    # Attributes
    # ----------

    @attribute(
        dtype=("DevString",),
        max_dim_x=16,
        label="devicesSwVersion",
        doc="List of Psts software version.",
    )
    def devicesSwVersion(self: LowCspCapabilityPst) -> List[str]:
        """Read list of Psts software version."""
        return self.component_manager.devices_sw_version

    @attribute(
        dtype=("DevString",),
        max_dim_x=16,
        label="devicesHwVersion",
        doc="List of Psts hardware version.",
    )
    def devicesHwVersion(self: LowCspCapabilityPst) -> List[str]:
        """Read list of Psts hardware version."""
        return self.component_manager.devices_hw_version

    @attribute(
        dtype=("DevString",),
        max_dim_x=16,
        label="devicesFwVersion",
        doc="List of Psts firmware version.",
    )
    def devicesFwVersion(self: LowCspCapabilityPst) -> List[str]:
        """Read sist of Psts firmware version."""
        return self.component_manager.devices_fw_version

    @attribute(
        dtype=("DevString",),
        max_dim_x=16,
        label="devicesState",
        doc="List of operational state for all PSTs.",
    )
    def devicesState(self: LowCspCapabilityPst) -> List[str]:
        """Read list of state for PSTs"""
        return self.component_manager.devices_state

    @attribute(
        dtype=("DevString",),
        max_dim_x=16,
        label="devicesHealthState",
        doc="List of HealthState for all PSTs",
    )
    def devicesHealthState(self: LowCspCapabilityPst) -> List[str]:
        """Read list of health state for PSTs"""
        return self.component_manager.devices_health_state

    @attribute(
        dtype=("DevString",),
        max_dim_x=16,
        label="devicesAdminMode",
        doc="List of administration mode for all PSTs.",
    )
    def devicesAdminMode(self: LowCspCapabilityPst) -> List[str]:
        """Read list of admin mode for PSTs"""
        return self.component_manager.devices_admin_mode

    # @attribute(
    #     dtype=("str",),
    #     max_dim_x=16,
    #     label="PSTSimulationMode",
    #     doc="List of simulation mode for all PSTs.",
    # )
    # def devicesSimulationMode(self: LowCspCapabilityPst) -> List[str]:
    #     """Read list of simulation mode for PSTs"""
    #     return self.component_manager.devices_simulation_mode

    @attribute(
        dtype="uint16",
        label="devicesDeployed",
        doc="Number of PST beams deployed",
    )
    def devicesDeployed(self: LowCspCapabilityPst) -> int:
        """Read number of deployed PSTbeams"""
        return self.component_manager.devices_deployed

    @attribute(
        dtype=("uint16",),
        label="devicesId",
        max_dim_x=16,
        doc="List of the PST id deployed",
    )
    def devicesId(self: LowCspCapabilityPst) -> list[int]:
        """Read ids list of the deployed PST beams"""
        return self.component_manager.devices_id

    @attribute(
        dtype=("DevString",),
        label="devicesFqdn",
        max_dim_x=16,
        doc="List of the PSTs fqdn",
    )
    def devicesFqdn(self: LowCspCapabilityPst) -> list[int]:
        """Read list of the deployed PST fqdn"""
        return self.component_manager.devices_fqdn

    @attribute(
        dtype="DevString",
        label="devicesJson",
        doc="Json containing the aggregated data for all PSTs",
    )
    def devicesJson(self: LowCspCapabilityPst) -> str:
        """Read json which contain raw PSTbeams information."""
        # See example below:
        # {
        #   "devices_deployed": 2,
        #   "pst": [
        #     {
        #       "dev_id": 0,
        #       "fqdn": "xxx.yyy.zzz",
        #       *"simulation_mode": True,
        #       "state": "ON",
        #       "health_state": "OK",
        #       "admin_mode": "ONLINE",
        #       * "subarray_membership": [3],
        #       "data_received": 0,
        #       "data_receive_rate": 0.0,
        #       "data_dropped": 0,
        #       "data_drop_rate": 0.0,
        #       "processing_mode" : "IDLE",
        #       "obs_state" : "IDLE"
        #     },
        #     {
        #       "dev_id": 1,
        #       "fqdn": "xxx.yyy.zzz",
        #       * "simulation_mode": True,
        #       "state": "ON",
        #       "health_state": "OK",
        #       "admin_mode": "ONLINE",
        #       * "subarray_membership": [5],
        #       "data_received": 0,
        #       "data_receive_rate": 0.0,
        #       "data_dropped": 0,
        #       "data_drop_rate": 0.0,
        #       "processing_mode" : "IDLE",
        #       "obs_state" : "IDLE"
        #     },
        #   ]
        # }
        return self.component_manager.devices_json

    @attribute(
        dtype=("DevString",),
        max_dim_x=16,
        label="devicesPstProcessingMode",
        doc="List of the observation mode of the PST beams deployed",
    )
    def devicesPstProcessingMode(self: LowCspCapabilityPst) -> list[str]:
        """Read the list of the observation mode of the deployed PSTbeams"""
        return self.component_manager.devices_processing_mode

    @attribute(
        dtype=("DevString",),
        max_dim_x=16,
        label="devicesObsState",
        doc="List of the obsstate of the PST beams",
    )
    def devicesObsState(self: LowCspCapabilityPst) -> list[str]:
        """Read the list of the obsstate of the PST beams."""
        return self.component_manager.devices_obs_state

    @attribute(
        dtype=("int",),
        max_dim_x=16,
        label="devicesDataReceived",
        doc="List of the data received by all PST beams deployed",
    )
    def devicesDataReceived(self: LowCspCapabilityPst) -> list[int]:
        """Read the list of received data of the deployed PSTbeams"""
        return self.component_manager.devices_data_received

    @attribute(
        dtype=("float",),
        max_dim_x=16,
        label="devicesDataReceiveRate",
        doc="List of the data receive rate by all PST beams deployed",
    )
    def devicesDataReceiveRate(self: LowCspCapabilityPst) -> list[float]:
        """Read the list of received data rate of the deployed PSTbeams"""
        return self.component_manager.devices_data_receive_rate

    @attribute(
        dtype=("int",),
        max_dim_x=16,
        label="devicesDataDropped",
        doc="List of the data dropped by all PST beams deployed",
    )
    def devicesDataDropped(self: LowCspCapabilityPst) -> list[int]:
        """Read the list of the data dropped of the deployed PSTbeams"""
        return self.component_manager.devices_data_dropped

    @attribute(
        dtype=("float",),
        max_dim_x=16,
        label="devicesDataDropRate",
        doc="List of the data drop rate by all PST beams deployed",
    )
    def devicesDataDropRate(self: LowCspCapabilityPst) -> list[float]:
        """Read the list of drop rate data of the beams deployed"""
        return self.component_manager.devices_data_drop_rate

    @attribute(
        dtype=("float",),
        max_dim_x=16,
        label="devicesDataDropRateAvg",
        doc="List of drop rate average data of the beams deployed",
    )
    def devicesDataDropRateAvg(self: LowCspCapabilityPst) -> list[float]:
        """Read the list of drop rate average data of the beams deployed"""
        return self.component_manager.devices_data_drop_rate_avg

    @attribute(
        dtype=("float",),
        max_dim_x=16,
        label="devicesDataDropRateStddev",
        doc=(
            "List of the data drop rate standard deviation of the"
            "beams deployed"
        ),
    )
    def devicesDataDropRateStddev(self: LowCspCapabilityPst) -> list[float]:
        """Read the list of drop rate data standard deviation of the
        beams deployed"""
        return self.component_manager.devices_data_drop_rate_stddev

    @attribute(
        dtype=("float",),
        max_dim_x=16,
        label="devicesDataReceiveRateAvg",
        doc="List of received average average data rate of the beams deployed",
    )
    def devicesDataReceiveRateAvg(self: LowCspCapabilityPst) -> list[float]:
        """Read the list of drop rate average data of the beams deployed"""
        return self.component_manager.devices_data_receive_rate_avg

    @attribute(
        dtype=("float",),
        max_dim_x=16,
        label="devicesDataReceiveRateStddev",
        doc=(
            "List of the data receive rate standard deviation of the beams"
            "deployed"
        ),
    )
    def devicesDataReceiveRateStddev(self: LowCspCapabilityPst) -> list[float]:
        """Read the list of receive rate data standard deviation of the
        beams deployed"""
        return self.component_manager.devices_data_receive_rate_stddev

    @attribute(
        dtype=str,
        label="devicesDataReceiveRateMonitoring",
        doc="Data receive rate monitoring attributes in Json string format",
    )
    def devicesDataReceiveRateMonitoring(self: LowCspCapabilityPst) -> str:
        """Read the Data receive rate monitoring attributes in Json
        string format"""
        return self.component_manager.devices_data_receive_rate_monitoring

    @attribute(
        dtype=str,
        label="devicesDataDropRateMonitoring",
        doc="Data drop rate monitoring attributes in Json string format",
    )
    def devicesDataDropRateMonitoring(self: LowCspCapabilityPst) -> str:
        """Data drop rate monitoring attributes in Json string format"""
        return self.component_manager.devices_data_receive_rate_monitoring


def main(args=None, **kwargs):
    """.
    Launch an SKABaseDevice device.

    :param args: positional args to tango.server.run
    :param kwargs: named args to tango.server.run
    """
    return run((LowCspCapabilityPst,), args=args, **kwargs)


if __name__ == "__main__":
    main()
