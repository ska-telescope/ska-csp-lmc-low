# -*- coding: utf-8 -*-
#
# Distributed under the terms of the GPL license.
# See LICENSE.txt for more info.

"""Specialize the FSP Capability class for the Low.CSP.LMC."""
from __future__ import annotations

import logging
from typing import Any, List, Tuple

from ska_control_model import AdminMode, ResultCode
from ska_csp_lmc_common.capability.capability_device import CspCapability
from ska_csp_lmc_common.manager.manager_configuration import (
    ComponentManagerConfiguration,
)
from ska_tango_base import SKABaseDevice
from tango import DevState
from tango.server import attribute, device_property, run

from ska_csp_lmc_low.capability.low_fsp_capability_component_manager import (
    LowFspCapabilityComponentManager,
)

module_logger = logging.getLogger(__name__)
# pylint: disable=protected-access


class LowCspCapabilityFsp(CspCapability):
    """
    LOW CSP Capability device
    Aggregates information and presents to higher level devices
    """

    def _init_state_model(self: LowCspCapabilityFsp) -> None:
        # attribute list that push change_event
        push_event_attributes_list = [
            "devicesState",
            "devicesHealthState",
            "devicesAdminMode",
            "devicesJson",
            "devicesSerialNumber",
            "devicesReady",
            "devicesFirmware",
            "devicesPktsIn",
            "devicesPktsOut",
            "devicesSubarrayIds",
        ]
        self._list_of_attributes = (
            self._list_of_attributes + push_event_attributes_list
        )

        super()._init_state_model()

    def set_component_manager(
        self, cm_configuration: ComponentManagerConfiguration
    ):
        """Configure the ComponentManager for the CSP Capability device.

        :param cm_configuration: A class with all the device properties
            accessible as attributes

        :return: The CSP Capability ComponentManager
        """
        return LowFspCapabilityComponentManager(
            cm_configuration,
            update_device_property_cbk=self.update_device_attribute,
            logger=self.logger,
        )

    class InitCommand(SKABaseDevice.InitCommand):
        """Class to handle the CSP.LMC Controller Init command."""

        def do(
            self: LowCspCapabilityFsp.InitCommand,
            *args: Any,
            **kwargs: Any,
        ) -> Tuple[ResultCode, str]:
            """Invoke the *init* command on the Controller ComponentManager.

            :return: a tuple with the command result code and an informative
                message.
            """
            # NOTE: in InitCommand parent class, the healthState of the device
            # is set to OK. To make the HealthStateModel work fine, the initial
            # value of the healthState must be set to OK, too.
            super().do()
            # device._health_state = HealthState.UNKNOWN
            # This is a temporary line to make the component_manager able to
            # manage old initialization
            self._device._admin_mode = AdminMode.ONLINE
            self._device.component_manager.init()
            return ResultCode.OK, "Low FspCapability init command finished"

    # -----------------
    # Device Properties
    # -----------------

    CbfAllocator = device_property(dtype="DevString")

    # ----------
    # Attributes
    # ----------

    @attribute(
        dtype=("DevString",),
        max_dim_x=16,
        label="devicesSwVersion",
        doc="List of Fsps software version.",
    )
    def devicesSwVersion(self: LowCspCapabilityFsp) -> List[str]:
        """Read list of Fsps software version."""
        return self.component_manager.devices_sw_version

    @attribute(
        dtype=("DevString",),
        max_dim_x=16,
        label="devicesHwVersion",
        doc="List of Fsps hardware version.",
    )
    def devicesHwVersion(self: LowCspCapabilityFsp) -> List[str]:
        """Read list of Fsps hardware version."""
        return self.component_manager.devices_hw_version

    @attribute(
        dtype=("DevString",),
        max_dim_x=16,
        label="devicesFwVersion",
        doc="List of Fsps firmware version.",
    )
    def devicesFwVersion(self: LowCspCapabilityFsp) -> List[str]:
        """Read sist of Fsps firmware version."""
        return self.component_manager.devices_fw_version

    @attribute(
        dtype=("DevString",),
        max_dim_x=16,
        label="devicesState",
        doc="List of operational state for all Fsps.",
    )
    def devicesState(self: LowCspCapabilityFsp) -> List[DevState]:
        """Read list of state for FSPs"""
        return self.component_manager.devices_state

    @attribute(
        dtype=("DevString",),
        max_dim_x=16,
        label="devicesHealthState",
        doc="List of HealthState for all FSPs",
    )
    def devicesHealthState(self: LowCspCapabilityFsp) -> List[str]:
        """Read list of health state for FSPs"""
        return self.component_manager.devices_health_state

    @attribute(
        dtype=("DevString",),
        max_dim_x=16,
        label="devicesAdminMode",
        doc="List of administration mode for all FSPs.",
    )
    def devicesAdminMode(self: LowCspCapabilityFsp) -> List[str]:
        """Read list of admin mode for FSPs"""
        return self.component_manager.devices_admin_mode

    # @attribute(
    #     dtype=("str",),
    #     max_dim_x=16,
    #     label="FSPSimulationMode",
    #     doc="List of simulation mode for all FSPs.",
    # )
    # def devicesSimulationMode(self: LowCspCapabilityFsp) -> List[str]:
    #     """Read list of simulation mode for FSPs"""
    #     return self.component_manager.devices_simulation_mode

    @attribute(
        dtype="uint16",
        label="devicesDeployed",
        doc="Number of FSP deployed",
    )
    def devicesDeployed(self: LowCspCapabilityFsp) -> int:
        """Read number of deployed FSP"""
        return self.component_manager.devices_deployed

    @attribute(
        dtype=("uint16",),
        label="devicesId",
        max_dim_x=16,
        doc="List of the FSP id deployed",
    )
    def devicesId(self: LowCspCapabilityFsp) -> list[int]:
        """Read ids list of the deployed FSP"""
        return self.component_manager.devices_id

    @attribute(
        dtype=("DevString",),
        label="devicesFqdn",
        max_dim_x=16,
        doc="List of the FSPs fqdn",
    )
    def devicesFqdn(self: LowCspCapabilityFsp) -> list[int]:
        """Read list of the deployed FSP fqdn"""
        return self.component_manager.devices_fqdn

    @attribute(
        dtype="DevString",
        label="devicesJson",
        doc="Json containing the aggregated data for all FSPs",
    )
    def devicesJson(self: LowCspCapabilityFsp) -> str:
        """Read json which contain raw FSP  information."""
        # See example below:
        # {
        #   "devices_deployed": 2,
        #   "fsp": [
        #     {
        #       "dev_id": 1,
        #       "fqdn": "low-cbf/processor/0.0.0",
        #       "state": "ON",
        #       "health_state": "OK",
        #       "admin_mode": "ONLINE",
        #       "serialnumber": "XRE879879GHT",
        #       "ready": "false",
        #       "firmware": "",
        #       "pkts_in": 0,
        #       "pkts_out" : 0,
        #       "subarray_ids" : []
        #     },
        #     {
        #       "dev_id": 2,
        #       "fqdn": "low-cbf/processor/0.0.1",
        #       "state": "ON",
        #       "health_state": "OK",
        #       "admin_mode": "ONLINE",
        #       "serialnumber": "XRJH7892842T",
        #       "ready": "true",
        #       "firmware": "vis0.1.1",
        #       "pkts_in": 1234567,
        #       "pkts_out" : 23456,
        #       "subarray_ids" : ["1"]
        #     },
        #   ]
        # }
        return self.component_manager.devices_json

    # NOTE: attributes ABOVE can be part of the common Capability Device,
    # attributes BELOW are specific of this capability

    @attribute(
        dtype=("DevString",),
        max_dim_x=16,
        label="devicesSerialNumber",
        doc="List of the serialnumber of the FSPs deployed",
    )
    def devicesSerialNumber(self: LowCspCapabilityFsp) -> list[str]:
        """Read the list of the serialnumber of the FSPs deployed"""
        return self.component_manager.devices_serialnumber

    @attribute(
        dtype=("DevString",),
        max_dim_x=16,
        label="devicesReady",
        doc="List of the readiness of the FSPs deployed",
    )
    def devicesReady(self: LowCspCapabilityFsp) -> list[str]:
        """Read the list of the readiness of the FSPs deployed"""
        return self.component_manager.devices_ready

    @attribute(
        dtype=("DevString",),
        max_dim_x=16,
        label="devicesFirmware",
        doc="List of the firmware uploaded on the FSPs deployed",
    )
    def devicesFirmware(self: LowCspCapabilityFsp) -> list[str]:
        """Read the list of the readiness of the FSPs deployed"""
        return self.component_manager.devices_firmware

    @attribute(
        dtype=("uint16",),
        max_dim_x=16,
        label="devicesPktsIn",
        doc="List of the total number of packets coming IN the FSPs deployed",
    )
    def devicesPktsIn(self: LowCspCapabilityFsp) -> list[str]:
        """Read the list of the total number of packets
        coming IN the FSPs deployed"""
        return self.component_manager.devices_pkts_in

    @attribute(
        dtype=("uint16",),
        max_dim_x=16,
        label="devicesPktsOut",
        doc="List of the total number of packets coming IN the FSPs deployed",
    )
    def devicesPktsOut(self: LowCspCapabilityFsp) -> list[str]:
        """Read the list of the total number of packets
        coming OUT the FSPs deployed"""
        return self.component_manager.devices_pkts_out

    @attribute(
        dtype=("DevString",),
        max_dim_x=16,
        label="devicesSubarrayIds",
        doc="List of the total number of packets coming OUT the FSPs deployed",
    )
    def devicesSubarrayIds(self: LowCspCapabilityFsp) -> list[str]:
        """Read the list of the total number of packets
        coming OUT the FSPs deployed"""
        return self.component_manager.devices_subarray_ids


def main(args=None, **kwargs):
    """.
    Launch an SKABaseDevice device.

    :param args: positional args to tango.server.run
    :param kwargs: named args to tango.server.run
    """
    return run((LowCspCapabilityFsp,), args=args, **kwargs)


if __name__ == "__main__":
    main()
