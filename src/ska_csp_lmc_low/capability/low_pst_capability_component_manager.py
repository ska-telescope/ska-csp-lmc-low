"""pst Capability Device Component Manager Class."""

from __future__ import annotations  # allow forward references in type hints

import json
import logging
from typing import Callable, List, Optional

from ska_csp_lmc_common.capability.capability_component_manager import (
    CapabilityComponentManager,
)
from ska_csp_lmc_common.manager.manager_configuration import (
    ComponentManagerConfiguration,
)

from ska_csp_lmc_low import release
from ska_csp_lmc_low.capability.low_pst_capability_data_handler import (
    PstDataHandler,
)

module_logger = logging.getLogger(__name__)

"""
Json with the pst information

    {
        "dev_id" : [1, 2]
        "devices_deployed": 2,
        "pst": [
        {
            "dev_id": 1,
            "fqdn": "low-pst/beam/01",
            *"simulation_mode": True,
            "state": "OFF",
            "health_state": "OK",
            "admin_mode": "ONLINE",
            * "subarray_membership": [3],
            "data_received": 0,
            "data_receiveRate": 0.0,
            "data_dropped": 0,
            "data_drop_rate": 0.0,
            "processing_mode" : "IDLE"
        },
        {
            "dev_id": 2,
            "fqdn": "low-pst/beam/02",
            * "simulation_mode": True,
            "state": "ON",
            "health_state": "OK",
            "admin_mode": "ONLINE",
            * "subarray_membership": [5],
            "data_received": 0,
            "data_receive_rate": 0.0,
            "data_dropped": 0,
            "data_drop_rate": 0.0,
            "processing_mode" : "IDLE"
        },
        ]
    }
"""
# pylint: disable=unused-argument
# pylint: disable=too-many-arguments
# pylint: disable=broad-except
# pylint: disable=logging-fstring-interpolation
# pylint: disable=too-many-instance-attributes
# pylint: disable=too-many-public-methods
# pylint: disable=attribute-defined-outside-init
# pylint: disable=no-member


class LowPstCapabilityComponentManager(CapabilityComponentManager):
    """Class for Low Csp pst Capability Component Manager."""

    def __init__(
        self: LowPstCapabilityComponentManager,
        properties: ComponentManagerConfiguration,
        update_device_property_cbk: Optional[Callable] = None,
        logger: Optional[logging.Logger] = None,
    ) -> None:

        super().__init__(
            properties=properties,
            update_device_property_cbk=update_device_property_cbk,
            logger=logger,
        )

        self._init_version_info(release)

    @property
    def devices_obs_state(self: LowPstCapabilityComponentManager) -> List[str]:
        """
        Return the list of the online PSTs' obsstate.
        """
        return self._data_handler.get_device_dict_entry("obs_state")

    @property
    def devices_processing_mode(
        self: LowPstCapabilityComponentManager,
    ) -> List[str]:
        """
        Return the list of the online PSTs' observation mode.
        """
        return self._data_handler.get_device_dict_entry("processing_mode")

    @property
    def devices_data_received(
        self: LowPstCapabilityComponentManager,
    ) -> List[float]:
        """
        Return the list of the online PSTs' data received.
        """
        return self._data_handler.get_device_dict_entry("data_received")

    @property
    def devices_data_receive_rate(
        self: LowPstCapabilityComponentManager,
    ) -> List[int]:
        """
        Return the list of the online PSTs' data receive rate.
        """
        return self._data_handler.get_device_dict_entry("data_receive_rate")

    @property
    def devices_data_dropped(
        self: LowPstCapabilityComponentManager,
    ) -> List[int]:
        """
        Return the list of the online PSTs' data dropped.
        """
        return self._data_handler.get_device_dict_entry("data_dropped")

    @property
    def devices_data_drop_rate(
        self: LowPstCapabilityComponentManager,
    ) -> List[float]:
        """
        RReturn the list of the online PSTs' data drop rate.
        """
        return self._data_handler.get_device_dict_entry("data_drop_rate")

    @property
    def devices_data_receive_rate_avg(
        self: LowPstCapabilityComponentManager,
    ) -> List[float]:
        """
        Return the list of the online PSTs' data receive rate.
        """
        return self._data_handler.data_receive_rate_avg

    @property
    def devices_data_drop_rate_avg(
        self: LowPstCapabilityComponentManager,
    ) -> List[float]:
        """
        RReturn the list of the online PSTs' data drop rate.
        """
        return self._data_handler.data_drop_rate_avg

    @property
    def devices_data_receive_rate_stddev(
        self: LowPstCapabilityComponentManager,
    ) -> List[float]:
        """
        Return the list of the online PSTs' data receive rate.
        """
        return self._data_handler.data_receive_rate_stddev

    @property
    def devices_data_drop_rate_stddev(
        self: LowPstCapabilityComponentManager,
    ) -> List[float]:
        """
        Return the list of the online PSTs' data drop rate.
        """
        return self._data_handler.data_drop_rate_stddev

    @property
    def devices_data_receive_rate_monitoring(
        self: LowPstCapabilityComponentManager,
    ) -> str:
        """
        Return the list of the online PSTs' data drop rate.
        """
        return json.dumps(self._data_handler.data_receive_rate_monitoring_json)

    @property
    def devices_data_drop_rate_monitoring(
        self: LowPstCapabilityComponentManager,
    ) -> str:
        """
        Return the list of the online PSTs' data drop rate.
        """
        return json.dumps(self._data_handler.data_drop_rate_monitoring_json)

    #
    # Protected Methods
    #

    def _instantiate_data_handler(
        self: LowPstCapabilityComponentManager,
        logger: Optional[logging.Logger] = None,
    ):
        """Instantiate the specific data handler that is meant to
        update the capability attributes"""
        return PstDataHandler(
            "pst", self._update_device_property_cbk, self.logger
        )

    def _populate_capability_specifics(
        self: LowPstCapabilityComponentManager,
    ) -> None:
        """Specify the specific variables of the Capability"""

        self.master_device_name = "csp-controller"
        self.property_master_fqdn = "CspController"
        self.master_attribute_name = "pstBeamsAddresses"
        self.subsystem_name = "pst-"
        self.attributes_to_be_subscribed.extend(
            [
                "obsState",
                "dataReceived",
                "dataReceiveRate",
                "dataDropped",
                "dataDropRate",
                "pstProcessingMode",
                # "subarraymembership"
            ]
        )

    def _read_device_id(
        self: LowPstCapabilityComponentManager, device_online
    ) -> None:
        """Retrieve the ID of an online device.

        :param device_online: one of the online pst component
        """
        # dev_id = -1
        try:
            # dev_id = device_online.read_attr("deviceID")
            # self._device_id.append(dev_id)
            for fqdn in device_online:
                beam_id = int(fqdn[-2:])

                self._device_id.append(str(beam_id))

        except Exception:
            self.logger.warning(
                f"pst {device_online} has not the deviceID attribute"
            )

    def _read_device_version(
        self: LowPstCapabilityComponentManager, device_online
    ) -> None:
        """Read the Software, firmware and hardware version attributes
          from an online devices.

        :param device_online: one of the online pst component.
        """
        sw_version = "NA"
        hw_version = "NA"
        fw_version = "NA"
        try:
            sw_version = device_online.read_attr("versionId")
            # sw_version = device_online.proxy.versionId
        except Exception:
            self.logger.warning(
                f"pst {device_online} has not the version attribute"
            )
        try:
            fw_version = device_online.read_attr("fwVersion")
        except Exception:
            self.logger.warning(
                f"pst {device_online} has not the fw_version attribute"
            )
        try:
            hw_version = device_online.read_attr("hwVersion")
        except Exception:
            self.logger.warning(
                f"pst {device_online} has not the hw_version attribute"
            )
        self._sw_version.append(sw_version)
        self._hw_version.append(hw_version)
        self._fw_version.append(fw_version)
