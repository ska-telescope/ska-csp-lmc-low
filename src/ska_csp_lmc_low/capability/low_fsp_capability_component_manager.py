"""pst Capability Device Component Manager Class."""

from __future__ import annotations  # allow forward references in type hints

import json
import logging
import threading
import traceback
from queue import Queue
from typing import List, Optional

from ska_csp_lmc_common.capability.capability_component_manager import (
    CapabilityComponentManager,
)
from tango import EventType

from ska_csp_lmc_low import release
from ska_csp_lmc_low.capability.low_fsp_capability_data_handler import (
    LowFspDataHandler,
)

module_logger = logging.getLogger(__name__)

"""
Json with the fsp information

  "device_deployed": 3,
  "fsp": [
    {
      "dev_id": 1,
      "fqdn": "xxx.yyy.zzz",
      "state": "ON",
      "health_state": "OK",
      "admin_mode": "ONLINE",
      "serialnumber": "XFDKS19292",
      "ready": "false",
      "firmware": "vis0.1.1",
      "pkts_in": 0,
      "pkts_out": 0,
      "subarray_ids": [1]
"""

# pylint: disable=too-many-instance-attributes
# pylint: disable=broad-exception-caught
# pylint: disable=too-many-nested-blocks
# pylint: disable=logging-fstring-interpolation
# pylint: disable=no-member


class LowFspCapabilityComponentManager(CapabilityComponentManager):
    """Class for Low Csp pst Capability Component Manager."""

    def __init__(
        self,
        properties,
        update_device_property_cbk=None,
        logger=None,
    ) -> None:
        """Initialize the Capability Component Manager Class

        :param properties: A class instance whose properties are the
            device properties.
        :param update_device_property_cbk: The device method invoked
            to update the attributes. Defaults to None.
        :param logger: The device or python logger if default is None.
        """

        super().__init__(
            properties=properties,
            update_device_property_cbk=update_device_property_cbk,
            logger=logger,
        )

        self._event_queue = Queue(100)
        self.connected_devices_fqdn = []
        self.new_connected_devices_fqdn = []

        thread = threading.Thread(
            target=self._process_allocator_event, daemon=True
        )

        thread.start()
        self._init_version_info(release)

    @property
    def devices_id(self: CapabilityComponentManager) -> list[int]:
        """
        Return the list of the online subsystems id.
        """
        return self._data_handler.get_device_dict_entry("dev_id")

    @property
    def devices_serialnumber(
        self: LowFspCapabilityComponentManager,
    ) -> List[str]:
        """
        Return the list of FSPs serialnumber.
        """
        return self._data_handler.get_device_dict_entry("serialnumber")

    @property
    def devices_ready(
        self: LowFspCapabilityComponentManager,
    ) -> List[float]:
        """
        Return the list of FSPs ready attribute.
        """
        return self._data_handler.get_device_dict_entry("ready")

    @property
    def devices_firmware(
        self: LowFspCapabilityComponentManager,
    ) -> List[int]:
        """
        Return the list of FSPs firmware.
        """
        return self._data_handler.get_device_dict_entry("firmware")

    @property
    def devices_pkts_in(
        self: LowFspCapabilityComponentManager,
    ) -> List[float]:
        """
        Return the list of FSPs packets in.
        """
        return self._data_handler.get_device_dict_entry("pkts_in")

    @property
    def devices_pkts_out(
        self: LowFspCapabilityComponentManager,
    ) -> List[int]:
        """
        Return the list of FSPs packets out.
        """
        return self._data_handler.get_device_dict_entry("pkts_out")

    @property
    def devices_subarray_ids(
        self: LowFspCapabilityComponentManager,
    ) -> List[str]:
        """
        Return the list of FSPs subarray ids associated.
        """
        return self._data_handler.get_device_dict_entry("subarray_ids")

    #
    # Protected Methods
    #

    def _instantiate_data_handler(
        self: LowFspCapabilityComponentManager,
        logger: Optional[logging.Logger] = None,
    ):
        """Instantiate the specific data handler that is meant to
        update the capability attributes"""
        return LowFspDataHandler(
            "fsp", self._update_device_property_cbk, self.logger
        )

    def _populate_capability_specifics(
        self: LowFspCapabilityComponentManager,
    ) -> None:
        """Specify the specific variables of the Capability"""

        self.master_device_name = "cbf-allocator"
        self.master_attribute_name = "procDevFqdn"
        self.property_master_fqdn = "CbfAllocator"
        self.subsystem_name = "fsp-"
        self.attributes_to_be_subscribed.extend(
            [
                "stats_mode",
                "stats_io",
                "subarrayIds",
            ]
        )

    def _read_device_id(
        self: LowFspCapabilityComponentManager, device_online
    ) -> None:
        """Method skipped
        There is no Device id attribute on CBF processor.
        The device_id list will be retrieved from json dict

        :param device_online: one of the online pst
        """
        self.logger.warning(
            "Device ID list is not retrieved directly on subsystem."
            "It is calculated from fqdn"
        )

    def _read_device_version(
        self: LowFspCapabilityComponentManager, device_online
    ) -> None:
        """Read the Software, firmware and hardware version attributes
          from an online devices.

        :param device_online: one of the online pst
        """
        sw_version = "NA"
        hw_version = "NA"
        fw_version = "NA"
        # try:
        #     sw_version = device_online.read_attr("versionId")
        #     # sw_version = device_online.proxy.versionId
        # except Exception:
        #     self.logger.warning(
        #         f"pst {device_online} has not the version attribute"
        #     )
        # try:
        #     # fw_version = device_online.proxy.fw_version
        #     fw_version = device_online.read_attr("fwVersion")
        # except Exception:
        #     self.logger.warning(
        #         f"pst {device_online} has not the fw_version attribute"
        #     )
        # try:
        #     # fw_version = device_online.proxy.fw_version
        #     hw_version = device_online.read_attr("hwVersion")
        # except Exception:
        #     self.logger.warning(
        #         f"pst {device_online} has not the hw_version attribute"
        #     )
        self._sw_version.append(sw_version)
        self._hw_version.append(hw_version)
        self._fw_version.append(fw_version)

    def _process_allocator_event(self):
        """Method that runs in a thread, looking  for events
        generated by CBFAllocator.
        When events are reported for both fsps and procdevfqdn
        it tries to retrieve the processor fqdn. When processor fqdn
        are available it starts to connect to devices."""

        thread = threading.current_thread()
        self.logger.info(
            f"Running process_allocator-event in thread {thread.name}"
        )
        try:
            fsps_events = []
            procdevfqdn_events = []

            while True:
                evt = self._event_queue.get(block=True, timeout=None)
                if evt.attr_name == "fsps":
                    fsps_events.append(evt.value)
                elif evt.attr_name == "procdevfqdn":
                    procdevfqdn_events.append(evt.value)

                if fsps_events and procdevfqdn_events:
                    fsps_dict = json.loads(fsps_events[-1])
                    procdevfqdn_dict = json.loads(procdevfqdn_events[-1])
                    if fsps_dict and procdevfqdn_dict:
                        if self._retrieve_processors_fqdn(
                            fsps_dict, procdevfqdn_dict
                        ):
                            fsps_events = []
                            procdevfqdn_events = []
                            self._connect_to_devices()
                            for serialnumber in procdevfqdn_dict:
                                self._data_handler.update(
                                    procdevfqdn_dict[serialnumber],
                                    "serialnumber",
                                    serialnumber,
                                )
                                # command below update dev_id with
                                # correspondant fsp_id overwriting the value
                                # set by init of data_handler (common)
                                for key, serial_list in fsps_dict.items():
                                    if serialnumber in serial_list:
                                        dev_id = int(key[-2:])
                                self._data_handler.update(
                                    procdevfqdn_dict[serialnumber],
                                    "dev_id",
                                    dev_id,
                                )

        except Exception:
            self.logger.error("Error in processing the CBF Allocator event")
            self.logger.error(traceback.format_exc())

    def _retrieve_processors_fqdn(
        self: LowFspCapabilityComponentManager,
        fsps_dict: dict,
        procdevfqdn_dict: dict,
    ) -> None:
        """Retrieve the FQDNs of the processor devices to be connected
        The fqdn list is retrieved by comparing "fsps" and "procDevFqdn"
        attributes of the CBF Allocator. Since this method is called at
        each processor registration, it returns only the list of the new
        processor registered.

        return: the list of the new processr FQDN deployed"""

        devices_fqdn = []

        for fsp in fsps_dict:
            serialnumber = fsps_dict[fsp][0]
            # note: we are assuming only one processor per fsp,
            # that's why index 0
            devices_fqdn.append(procdevfqdn_dict[serialnumber])

        for fqdn in devices_fqdn:
            if fqdn not in self.connected_devices_fqdn:
                self.new_connected_devices_fqdn.append(fqdn)
                self.logger.info(f"New processor connected: {fqdn}")
                # self._data_handler.update(fqdn,"serialnumber", serialnumber)

        if self.new_connected_devices_fqdn:
            self.connected_devices_fqdn += self.new_connected_devices_fqdn
            self.logger.info(
                "Capability will connect to processors: "
                f"{self.new_connected_devices_fqdn}"
            )
            return True

        return False

    def _subscribe_allocator_attributes(
        self: LowFspCapabilityComponentManager,
    ):
        """Subscribe to necessary attributes on the CBF allocator"""

        def evt_callback(evt):
            """Callback to enqueue received events."""
            self._event_queue.put(evt)

        attributes = ["procDevFqdn", "fsps"]
        for attr in attributes:
            self.master_dev.subscribe_attribute(
                attr, EventType.CHANGE_EVENT, evt_callback
            )

    def _connection_helper(self: LowFspCapabilityComponentManager):
        """
        Implement a custom version of the base method from the parent class,
        `CapabilityComponentManager`.
        """
        self._subscribe_allocator_attributes()

    def _retrieve_devices_fqdn(self: LowFspCapabilityComponentManager) -> None:
        """Retrieve the FQDNs of the processor devices to be connected
        The fqdn list is retrieved by comparing "fsps" and "procDevFqdn"
        attributes of the CBF Allocator. Since this method is called at
        each processor registration, it returns only the list of the new
        processor registered.

        return: the list of the new processr FQDN deployed"""
        new_connected_devices_fqdn = self.new_connected_devices_fqdn
        self.new_connected_devices_fqdn = []
        self.logger.info(
            f"Subscribing attributes of {new_connected_devices_fqdn}"
        )
        return new_connected_devices_fqdn
