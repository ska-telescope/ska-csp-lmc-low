########################
CSP LMC Troubleshooting
########################

Since the CSP LMC is the entry point of all the CSP sub-elements, the State, ObsState and HealthState of the CSP LMC have to
reflect also the State, ObsState and HealthState of all the CSP subsystems.
In order to identify the cause of a particular condition of the CSP LMC, the starting point is to understand the subsystems status. 
For doing that, we have defined some tables to link the status of the CSP LMC and the one of the subsystems.

- `State aggregation table <https://confluence.skatelescope.org/display/SE/CSP+LMC+State+aggregation>`_
- `HealthState aggregation table <https://confluence.skatelescope.org/display/SE/CSP+LMC+healthState>`_
- `ObsState aggregation table <https://confluence.skatelescope.org/display/SE/CSP+LMC+ObsState+aggregation>`_

.. _csp-lmc-troubleshooting:

*******************************
Recover from a FAULT obsState
*******************************

If something goes wrong with the observation, the *obsState* of the Low CSP.LMC Subarray could transition to FAULT.
Note that this condition differs from the one where the State of a TANGO Device is in FAULT (see below).
To recover from this situation, issue the *Restart* command.
This command will release also all the resources, bringing the subarray into an EMPTY *obsState*. 

The sequence of operations and responses is::

    csp_sub1.obsstate -> FAULT
    
    csp_sub1.restart()

    csp_sub1.commandResult -> ('restart', '1')
    csp_sub1.obsstate -> RESTARTING
    csp_sub1.commandResult -> ('restart', '0')
    csp_sub1.obsstate -> EMPTY

.. _csp-lmc-reset:

****************************
Recover from a FAULT State
****************************

When an internal error occours in the Low CSP.LMC Subarray or in the Controller, the *DevState* (State) of the device can transition to FAULT.
To recover from this condition, the *Reset* command needs to be issued.
If executed successfully, this command will restore the device to a state determined by the aggregation of the states of
its subordinate systems. The same applies to the observing mode for the CSP Subarray.

Please be aware that when the Low CBF device state is DISABLED, the state of the corresponding CSP LMC device is
indicated as FAULT.
Even after executing the *Reset* command, the CSP LMC device remains in the FAULT state.

In such a scenario, it's advisable to first set the CSP LMC device's *AdminMode* to OFFLINE, then switch it back to
ONLINE, and verify whether the state of both the Low CBF device and the CSP LMC device has changed.
