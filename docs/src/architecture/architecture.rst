########################
Low CSP.LMC Architecture
########################

The architecture of the CSP.LMC software is the same for Low and Mid Telescope. 
Please refer to `common documentation <https://developer.skatelescope.org/projects/ska-csp-lmc-common/en/latest/architecture/index.html>`_