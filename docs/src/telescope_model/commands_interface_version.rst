###################################
Telescope model interfaces version
###################################

The Telescope model version used is the *1.19.2*

The table below contains the relationship between the available commands CSP interface version and the CSP subsystems interface version.
More details about the interface content can be found in the `telescope moder documentation <https://developer.skao.int/projects/ska-telmodel/en/1.18.1/schemas/csp/ska-low-csp.html>`_

Note that the CSP LMC itself has no limitation about the command interface used. The constrains are due to subsystems interface version.

**********************************
Assign resources command interface
**********************************

.. list-table::
   :widths: 15 15 15 15 40
   :header-rows: 1

   * - CSP
     - CBF
     - PST
     - PSS
     - Note
   * - 3.2
     - 0.2
     - NA
     - 2.0
     - 
   * - 3.0
     - 0.2
     - NA
     - 2.0
     - 
   * - 2.0
     - 0.1
     - NA
     - 2.0
     - 

*****************************
Configure command interface
*****************************

.. list-table::
   :widths: 15 15 15 15 40
   :header-rows: 1

   * - CSP
     - CBF
     - PST
     - PSS
     - Note
   * - 4.0
     - 1.0
     - 2.5
     - 2.0
     - This CSP version uses a CBF interface version (1.0) that is not yet compatible with CBF device version 0.7.0
   * - 3.2
     - 0.2
     - 2.5
     - 2.0
     - 
   * - 3.1
     - 0.2
     - 2.5
     - 2.0
     - 
   * - 3.0
     - 0.2
     - 2.4
     - 2.0
     - 
   * - 2.0
     - 0.1
     - 2.4
     - 2.0
     - 

************************
Scan command interface
************************

.. list-table::
   :widths: 15 15 15 15 40
   :header-rows: 1

   * - CSP
     - CBF
     - PST
     - PSS
     - Note
   * - 4.0
     - 0.2
     - 2.5
     - 2.0
     - This CSP version uses a CBF interface version (0.2) that is not yet compatible with CBF device version 0.7.0
   * - 3.2
     - 0.1
     - 2.4
     - 2.0
     - 
   * - 2.0
     - 0.1
     - 2.4
     - 2.0
     - 

**************************************
Release resources command interface
**************************************

.. list-table::
   :widths: 15 15 15 15 40
   :header-rows: 1

   * - CSP
     - CBF
     - PST
     - PSS
     - Note
   * - 3.2
     - 0.2
     - NA
     - 2.0
     - 
   * - 3.0
     - 0.2
     - NA
     - 2.0
     - 
   * - 2.0
     - 0.1
     - NA
     - 2.0
     - 
