#################################
Low CSP.LMC Controller Attributes
#################################

The following table contains information about the Low CSP LMC attributes of the controller Tango device. 


.. list-table:: 
   :widths: 15 25 15 5 5 5 25 
   :header-rows: 1

   * - Name
     - Description
     - Type
     - R/W 
     - Change event 
     - Archived
     - Example
   * - State
     - State
     - DevState
     - R
     - yes
     - yes
     - tango._tango.DevState.ON
   * - Status
     - Status
     - DevString
     - R
     - yes
     - yes
     - The device is in DISABLE state.
   * - adminMode
     - The admin mode reported for this device. It may interpret the current device condition and condition of all managed devices to set this. Most possibly an aggregate attribute.
     - Enum (AdminMode)
     - R/W
     - yes
     - yes
     - adminMode.ONLINE: 0
   * - healthState
     - Read the Health State of the device. It interprets the current device condition and condition of all managed devices to set this. Most possibly an aggregate attribute.
     - Enum (HealthState)
     - R
     - yes
     - yes
     - healthState.UNKNOWN: 3
   * - versionId
     - Read the Version Id of the device.
     - String
     - R
     - no
     - no
     - '0.17.2'
   * - buildState
     - Build state of the device.
     - String
     - R
     - no
     - no
     - :ref:`example <low-controller-buildstate>`
   * - controlMode
     - Read the Control Mode of the device. The control mode of the device are REMOTE, LOCAL Tango Device accepts only from a ‘local’ client and ignores commands and queries received from TM or any other ‘remote’ clients. The Local clients has to release LOCAL control before REMOTE clients can take control again.
     - Enum (ControlMode)
     - R/W
     - yes
     - yes
     - controlMode.REMOTE: 0
   * - loggingLevel
     - Read the logging level of the device. Initialises to LoggingLevelDefault on startup.See :py:class:`~ska_control_model.LoggingLevel`.
     - Enum (LoggingLevel)
     - R/W
     - 
     -
     - loggingLevel.INFO: 4
   * - simulationMode
     - Read the Simulation Mode of the device. Some devices may implement both modes, while others will have simulators that set simulationMode to True while the real devices always set simulationMode to False.
     - Enum (ControlMode)
     - R/W
     - yes
     - yes
     - simulationMode.FALSE: 0
   * - testMode
     - Read the Test Mode of the device. Either no test mode or an indication of the test mode.
     - Enum (TestMode)
     - R/W
     - yes
     - yes
     - testMode.NONE: 0
   * - loggingTargets
     - Read the additional logging targets of the device. Note that this excludes the handlers provided by the ska_ser_logging library defaults - initialises to LoggingTargetsDefault on startup. 
     - String
     - R/W
     -
     -
     - ["tango::logger"]
   * - longRunningCommandIDsInQueue
     - Read the IDs of the long running commands in the queue. Every client that executes a command will receive a command ID as response. Keep track of IDs in the queue. Pop off from front as they complete.
     - list of Strings
     - R
     - yes
     - yes
     - 
   * - longRunningCommandProgress
     - Read the progress of the currently executing long running command. ID, progress of the currently executing command. Clients can subscribe to on_change event and wait for the ID they are interested in.
     - list of Strings
     - R
     - yes
     - yes
     - 
   * - longRunningCommandResult
     - Read the result of the completed long running command. Reports unique_id, json-encoded result. Clients can subscribe to on_change event and wait for the ID they are interested in.
     - Tuple [str, str]
     - R
     - yes
     - yes
     - :ref:`example <low-controller-longrunningcommandresult>`
   * - longRunningCommandStatus
     - Read the status of the currently executing long runnin ID, status pair of the currently executing command. Clients can subscribe to on_change event and wait for the ID they are interested in.
     - list of Strings
     - R
     - yes
     - yes
     - :ref:`example <low-controller-longrunningcommandstatus>`
   * - longRunningCommandsInQueue
     - Read the long running commands in the queue. Keep track of which commands are in the queue. Pop off from front as they complete. 
     - list of Strings
     - R
     - yes
     - yes
     - 
   * - elementAlarmAddress
     - FQDN of Element Alarm Handlers
     - String
     - R
     - no
     - no
     - NOTE: not available
   * - elementDatabaseAddress
     - FQDN of Element Database device
     - String
     - R
     - no
     - no
     - NOTE: not available
   * - elementLoggerAddress
     - Read FQDN of Element Logger device.
     - String
     - R
     - no
     - no
     - NOTE: not available
   * - elementTelStateAddress
     - FQDN of Element TelState device
     - String
     - R
     - no
     - no
     - NOTE: not available
   * - maxCapabilities
     - Maximum number of instances of each capability type.
     - list of Strings
     - R
     - no
     - no
     - NOTE: not available
   * - cspCbfState
     - The CBF sub-system Device State.
     - Enum (DevState)
     - R
     - yes
     - yes
     - tango._tango.DevState(0)
   * - cspPssState
     - The PSS sub-sstem Device State
     - Enum (DevState)
     - R
     - yes
     - yes
     - tango._tango.DevState(0)
   * - cspPstBeamsState
     - Report the state of the deployed PST Beams as a JSON formatted string
     - DevString of DevState
     - R
     - yes
     - yes
     - {"low-pst/beam/01":"DISABLE"}
   * - cspCbfAdminMode
     - The CBF subsystem adminMode
     - DevEnum (AdminMode)
     - R/W
     - yes 
     - yes
     - cspCbfAdminMode.ON-LINE: 0
   * - cspPssAdminMode
     - The PSS sub-system adminMode
     - DevEnum (AdminMode)
     - R/W
     - yes 
     - yes
     - :ref:`example <low-controller-cspPssAdminMode>`
   * - cspPstBeamsAdminMode
     - Report the administration mode of the PST Beams as JSON formatted string
     - DevString of AdminMode
     - R/W
     - yes
     - yes
     - {"low-pst/beam/01":"OFFLINE"}
   * - cspCbfHealthState
     - The CBF sub-system healthState.
     - DevEnum (HealthState)
     - R
     - yes
     - yes
     - cspCbfHealthState.UNKNOWN: 3
   * - cspPssHealthState
     - The PSS sub-system healthState
     - DevEnum (HealthState)
     - R
     - yes
     - yes
     - cspPssHealthState.UNKNOWN: 3
   * - cspPstBeamsHealthState
     - Health state of the PST Beams as a JSON formatted string
     - DevEnum (HealthState)
     - R
     - yes
     - yes
     - '{"low-pst/beam/01": "OK"}'
   * - cbfControllerAddress
     - TANGO FQDN of the CBF Controller device
     - DevString
     - R
     - no
     - no
     - low-cbf/control/0
   * - cbfVersion
     - SW version of the CBF Controller
     - String
     - R
     - no
     - no
     - '0.7.0'
   * - commandTimeout
     - Duration of command timeout in seconds. On set: Configure the timeout applied to each command. The specified value shall be \> 0. Negative values raise a *TypeError* exception. This is the expected behavior because the attribute has been defined as DevUShort. A zero value raises a *ValueError* exception.
     - DevUShort (0,100]
     - R/W
     - yes
     - yes
     - 30
   * - isCommunicating
     - Whether the CSP Controller TANGO Device is communicating with the System Under Control (SUT). The SUT is composed of: the Controller devices of the CSP sub-systems, all the deployed PST Beams of the PST sub-systems, the deployed CSPSubarray devices. This flag is reported as False when the CSP.LMC Controller is not able to communicate with the corresponding CBF device.
     - Bool
     - R
     - no
     - no
     - true
   * - jsonComponentVersions
     - Return a JSON formatted string with the SW versions of the CSP.LMC Controller subordinate devices. 
     - String
     - R
     - no
     - no
     - :ref:`example <low-controller-jsoncomponentversions>`
   * - numOfDevCompletedTask
     - Number of devices that completed the task.
     - DevUShort
     - R
     - no
     - no
     - NOTE: not implemented
   * - offCmdDurationExpected
     - Expected duration (sec) of the Off command. Set/Report the duration expected (in sec.) for the Off command execution.
     - DevUShort
     - R/W
     - no 
     - yes
     - NOTE: deprecated
   * - offCmdDurationMeasured
     - Measured duration (sec) of the Off command execution. Report the measured duration (in sec.) of the Off command execution 
     - DevUShort
     - R
     - no 
     - no
     - NOTE: not available
   * - offCmdFailure
     - Off execution failure flag. Alarm flag set when the Off command fails with error(s).
     - DevBoolean
     - R
     - no
     - no
     - NOTE: deprecated
   * - offCmdTimeoutExpired
     - PSS command timeout flag. Signal the occurence of a timeout during the execution off commands on PSS Sub-system.
     - DevBoolean
     - R
     - no
     - no
     - NOTE: deprecated
   * - offCommandProgress
     - Progress percentage for the Off command. Percentage progress implemented for commands that result in state/mode transitions for a large number of components and/or are executed in stages (e.g power up, power down)
     - DevUShort [0,100]
     - R
     - no
     - no
     - NOTE: deprecated
   * - offFailureMessage
     - Off execution failure message. Alarm message when the Off command fails with error(s).
     - DevString
     - R
     - no
     - no
     - NOTE: deprecated
   * - onCmdDurationExpected
     - Expected duration (sec) of the On command. Set/Report the duration expected (in sec.) for the Oncommand execution
     - DevUShort [0,100]  
     - R/W
     - no
     - yes
     - NOTE: deprecated
   * - onCmdDurationMeasured
     - Measured duration (sec) of the On command execution. Report the measured duration (in sec.) of the On command execution
     - DevUShort [0,100]
     - R/W
     - no
     - no
     - NOTE: not available
   * - onCmdFailure
     - CBF command failure flag. Alarm flag set when the On command fails with error(s).
     - DevBoolean
     - R
     - no
     - no
     - NOTE: deprecated
   * - onCmdTimeoutExpired
     - CBF command timeout flag. Signal the occurence of a timeout during the execution of commands on CBF sub-system.
     - DevBoolean
     - R
     - no
     - no
     - NOTE: deprecated
   * - onCommandProgress
     - Progress percentage for the On command. Percentage progress implemented for commands that result in state/modetransitions for a large number of components and/or are executed in stages (e.g power up, power down)
     - DevUShort [0,100] 
     - R
     - no
     - no
     - NOTE: deprecated
   * - onFailureMessage
     - On execution failure message. Alarm message when the On command fails with error(s). 
     - DevString
     - R
     - no
     - no
     - NOTE: deprecated
   * - pssControllerAddress
     - TANGO FQDN of the PSS sub-system Controller.
     - DevString
     - R
     - no
     - no
     - NOTE: not available
   * - pssVersion
     - The SW version of the PSS Controller.
     - String
     - R
     - no
     - no
     - ''
   * - standbyCmdDurationExpected
     - Expected duration (sec) of the Standby command. Set/Report the duration expected (in sec.) for the Standby command execution. 
     - DevUShort [0,100] 
     - R/W
     - no
     - yes
     - NOTE: deprecated
   * - standbyCmdDurationMeasured
     - Measured duration (sec) of the Standby command execution. Report the measured duration (in sec.) of the Standby command execution.
     - DevUShort [0,100] 
     - R
     - no
     - no
     - NOTE: not available
   * - standbyFailureMessage
     - Standby execution failure message. Alarm message when the Standby command fails with error(s).
     - DevString
     - R
     - no
     - no
     - NOTE: deprecated
   * - standbyCmdTimeoutExpired
     - PST command timeout flag. Signal the occurence of a timeout during the execution of commands on PST sub-system.
     - DevBoolean
     - R
     - no
     - no
     - NOTE: deprecated
   * - standbyCommandProgress
     - Progress percentage for the Standby command. Percentage progress implemented for commands that result in state/mode transitions for a large number of components and/or are executed in stages (e.g power up, power down).
     - DevUShort [0,100]  
     - R
     - no
     - no
     - NOTE: deprecated
   * - standbyCmdFailure
     - Standby execution failure message. Alarm flag set when the Standby command fails with error(s).
     - DevBoolean
     - R
     - no
     - no
     - NOTE: deprecated
   * - commandResult
     - A tuple with the name and the result code of latest command execution: (commandName, resultCode)
     - String
     - R  
     - yes
     - yes
     - ('on', '0')
   * - pstBeamsAddresses
     - TANGO FQDNs of the PST Beams
     - list of Strings, max_dim=16
     - R
     - no
     - no
     - ["low-pst/beam/01"]
   * - pstVersion
     - Return the SW of the PST Beams
     - list of Strings, max_dim=16
     - R
     - no
     - no
     - ('0.7.1',)
   * - searchBeamsAddresses
     - The SearchBeam Capabilities FQDNs
     - list of DevStrings, max_dim=1500
     - R  
     - no
     - no
     - NOTE: not available
   * - timingBeamsAddresses
     - The list of Timing Beam Capabilities FQDNs
     - list of DevStrings, max_dim=16
     - R  
     - no
     - no
     - NOTE: not implemented
   * - testAlarm
     - flag to test alarm
     - DevBoolean
     - R/W
     - no
     - no
     - false
   * - vlbiBeamsAddresses
     - The list of VlbiBeam Capabilities FQDNs
     - list of DevStrings, max_dim=20
     - R  
     - no
     - no
     - NOTE: not implemented
   * - stationList
     - The list of all stations
     - ("DevUShort",)
     - R
     - no
     - no
     - [1, 2, 3, 4]
   * - availableCapabilities
     - A list of available number of instances of each capability
     - DevString
     - R
     - no
     - no
     - NOTE: not implemented
   * - resourceTableFSP
     - The FSP resource table as JSON string.
     - ("DevString",),
     - R
     - no
     - no
     - NOTE: not implemented :ref:`example <low-controller-resourceTableFSP>`
   * - resourceTableP4
     - The P4 resource table as JSON string.
     - DevString
     - R
     - no
     - no
     - :ref:`example <low-controller-resourceTableP4>`

.. example
   * -
     -
     -
     -
     -
     -
     -
     -
     -
     -
     -
     - 