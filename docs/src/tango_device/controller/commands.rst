####################
Controller Commands
####################

The following table contains information about the Low CSP LMC commands of the controller Tango device.
A more extensive documentation about command parameters can be found in the `Telescope Model - Low.CSP schemas <https://developer.skao.int/projects/ska-telmodel/en/latest/schemas/csp/ska-low-csp.html>`_


.. list-table::
   :widths: 15 25 15 10 5 10
   :header-rows: 1

   * - Command name
     - Description
     - Parameters
     - Example
     - Sequence diagram
     - Note
   * - *On*
     - The command has an argument list as input that contains the devices FQDN that have to be transition to ON.
     - List that contains the devices FQDN. when the list is empty, the commmand is propagated to all the subsystem devices
     - :ref:`On example <Power-on (off) the Low.CSP>`
     - `On diagram <https://confluence.skatelescope.org/display/SE/On>`_
     - 
   * - *Off*
     - The Off command disables any signal processing capability of a device,
     - List that contains the devices FQDN. when the list is empty, the commmand is propagated to all the subsystem devices
     - :ref:`Off example <Power-on (off) the Low.CSP>`
     - `Off diagram <https://confluence.skatelescope.org/display/SE/Off>`_
     - 
   * - *Standby*
     - Transition the CSP to a low power consumption mode. The CSP LMC controller is responsible to propagate the transition to the subsystems
     - List that contains the devices FQDN. when the list is empty, the commmand is propagated to all the subsystem devices
     - :ref:`Standby example <Power-on (off) the Low.CSP>`
     - `Standby diagram <https://confluence.skatelescope.org/display/SE/Standby>`_
     - The standby command is rejected sin the CSP subsystems do not support it (Low CBF, PST)
   * - *Reset*
     - Recover from a FAULT State. The controller will be move in STANDBY obsState
     - No input
     - :ref:`Reset example <csp-lmc-reset>`
     - `Reset diagram <https://confluence.skatelescope.org/display/SE/Reset>`_
     - The command is propagated to subarrays
