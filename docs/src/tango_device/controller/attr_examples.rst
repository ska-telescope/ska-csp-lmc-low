:orphan:

##########################################
Low CSP.LMC Controller Attributes Examples
##########################################

* buildState

.. _low-controller-buildstate:

::

    'ska-csp-lmc-low, 0.9.0, SKA LOW CSP.LMC'

* longrunningcommandresult

.. _low-controller-longrunningcommandresult:

::

    ('1708358750.7680037_31446241612572_On', '[0, "on completed  1/1"]')

* longrunningcommandstatus

.. _low-controller-longrunningcommandstatus:

::

    ('1708442895.8833039_185769114989020_On', 'COMPLETED')

* cspPssAdminMode

.. _low-controller-cspPssAdminMode:

::

    cspPssAdminMode.NOT-FITTED: 3; write NOTE: not implemented

* jsoncomponentyersions

.. _low-controller-jsoncomponentversions:

::

    '{"low-cbf/control/0": "0.7.0", "low-pst/beam/01": "0.7.1", "low-csp/subarray/01": "0.11.1", "low-csp/subarray/02": "0.11.1", "low-csp/subarray/03": "0.11.1", "low-csp/subarray/04": "0.11.1"}'

* resourceTableFSP

.. _low-controller-resourceTableFSP:

::

    "['CORRELATOR:512', 'PSS-BEAMS:4']"

* resourceTableP4

.. _low-controller-resourceTableP4:

::

    '{"p4_01": {"health": null, "admin_mode": null, "op_state": null, "fw_image": null, "fw_mode": null, "links": ["alveo_001", "alveo_002", "alveo_003", "alveo_004"]}}'
