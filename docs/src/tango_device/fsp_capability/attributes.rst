#####################################
Low CSP.LMC Capability Fsp Attributes
#####################################

The following table contains information about the Low CSP LMC attributes of the Fsp capability Tango device.


.. list-table::
   :widths: 15 25 15 5 5 5 25
   :header-rows: 1

   * - Name
     - Description
     - Type
     - R/W 
     - Change event
     - Archived
     - Example
   * - State
     - State
     - DevState
     - R
     - yes
     - yes
     - tango._tango.DevState.ON
   * - Status
     - Status
     - DevString
     - R
     - yes
     - yes
     - The device is in DISABLE state.
   * - adminMode
     - The admin mode reported for this device. It may interpret the current device condition and condition of all managed devices to set this. Most possibly an aggregate attribute.
     - Enum (AdminMode)
     - R/W
     - yes
     - yes
     - adminMode.ONLINE: 0
   * - healthState
     - Read the Health State of the device. It interprets the current device condition and condition of all managed devices to set this. Most possibly an aggregate attribute.
     - Enum (HealthState)
     - R
     - yes
     - yes
     - healthState.UNKNOWN: 3
   * - versionId
     - Read the Version Id of the device.
     - String
     - R
     - no
     - no
     - '0.11.1'
   * - isCommunicating
     - Whether the Fsp TANGO Device is communicating with the System Under Control (SUT). The SUT is composed of the CBF Processor devices registered in the CBFAllocator as FSP. This flag is reported as False when the FSP Capability is not able to communicate with the corresponding CBF devices or no Processors are allocated as FSPs.
     - Bool
     - R
     - no
     - no
     - True
   * - devicesState
     - Report the allocated CBF processors attribute State aggregated in a list.
     - (DevString, ), max_dim = 16
     - R
     - yes
     - no
     - ('DISABLE',)
   * - devicesHealthState
     - Report the allocated CBF processors attribute AdminMode aggregated in a list.
     - (String,), max_dim = 16
     - R
     - yes
     - no
     - ("UNKNOWN",)
   * - devicesAdminMode
     - Report the allocated CBF processors attribute AdminMode aggregated in a list.
     - (String, ), max_dim = 16
     - R
     - yes
     - yes
     - ("ONLINE", )
   * - devicesDeployed
     - Report the number of the CBF processors allocated
     - Int
     - R
     - no
     - no
     - 2
   * - devicesId
     - Report the allocated CBF processors correspondent FSP number aggregated in a list
     - (Int, ), max_dim = 16
     - R
     - no
     - no
     - (1, 2)
   * - devicesFqdn
     - Report the allocated CBF processors fqdn aggregated in a list.
     - (string,), max_dim = 16
     - R 
     - no
     - no
     - ('low-cbf/processor/0.0.0','low-cbf/processor/0.0.1')
   * - devicesJson
     - Report allocated CBF processors information aggregated in a json formatted string.
     - String
     - R
     - yes
     - no
     - :ref:`example <low-fspcapability-devicesJson>`
   * - devicesSerialNumber
     - Report the allocated CBF processors serialnumbers aggregated in a list.
     - (string, ), max_dim = 16
     - R
     - yes
     - no
     - ('XFL1RCFEG244', 'XFL1E35JVJTQ')
   * - devicesReady
     - Report the allocated CBF processors "readiness" aggregated in a list.
     - (string,), max_dim = 16
     - R
     - yes
     - no
     - ("true", "false")
   * - devicesFirmware
     - Report the allocated CBF processors loaded firmware aggregated in a list.
     - (string, ), max_dim = 16
     - R
     - yes
     - no
     - ('vis0.1.0', 'pst0.1.0')
   * - devicesPktsIn
     - Report the allocated CBF processors total packets received in input aggregated in a list.
     - (int,), max_dim = 16
     - R
     - yes
     - no
     - (15548, 13927)
   * - devicesPktsOut
     - Report the allocated CBF processors total packets trasmitted in output aggregated in a list.
     - (int,), max_dim = 16
     - R
     - yes
     - no
     - (13927, 15548)
   * - devicesSubarrayIds
     - Report the allocated CBF processors subarray allocation aggregated in a list.
     - (Float,), max_dim = 16
     - R
     - yes
     - no
     - (1, 1)