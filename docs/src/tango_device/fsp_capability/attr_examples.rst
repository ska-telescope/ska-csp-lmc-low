:orphan:

##############################################
Low CSP.LMC Capability FSP Attributes Examples
##############################################


* devicesJson

.. _low-fspcapability-devicesjson:

::

    '{
        "devices_deployed": 2,
        "fsp": [
            {
                "dev_id": 1,
                "fqdn": "low-cbf/processor/0.0.0",
                "state": "ON",
                "health_state": "OK",
                "admin_mode": "ONLINE",
                "serialnumber": "XRE879879GHT",
                "ready": "false",
                "firmware": "",
                "pkts_in": 0,
                "pkts_out" : 0,
                "subarray_ids" : []
            },
            {
                "dev_id": 2,
                "fqdn": "low-cbf/processor/0.0.1",
                "state": "ON",
                "health_state": "OK",
                "admin_mode": "ONLINE",
                "serialnumber": "XRJH7892842T",
                "ready": "true",
                "firmware": "vis0.1.1",
                "pkts_in": 1234567,
                "pkts_out" : 23456,
                "subarray_ids" : ["1"]
            },
        ]
    }'