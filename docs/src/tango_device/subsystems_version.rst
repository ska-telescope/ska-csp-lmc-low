####################
Subsystems version
####################

The following table contains the supported version of the Low CSP LMC subsystems that are verified with our integration tests.

.. list-table::
   :widths: 30 30 40
   :header-rows: 1

   * - Name
     - Version
     - Note
   * - CBF
     - 0.12.0
     - 
   * - PST
     - 1.0.1
     -
   * - PSS
     - NA
     -
