:orphan:

##########################################
Low CSP.LMC Subarray Attributes Examples
##########################################

* buildState

.. _low-subarray-buildstate:

::

    'ska-csp-lmc-low, 0.11.1, SKA LOW CSP.LMC'

* longrunningcommandresult

.. _low-subarray-longrunningcommandresult:

::

    ('1708358750.7680037_31446241612572_On', '[0, "on completed  1/1"]')

* longrunningcommandstatus

.. _low-subarray-longrunningcommandstatus:

::

    ('1708358750.7680037_31446241612572_On', 'COMPLETED')

* configuredCapabilities

.. _low-subarray-configuredCapabilities:

::

    ('Correlators:512, PssBeams:4 PstBeams:4, VlbiBeams:0')


* cbfSubarrayAdminMode

.. _low-subarray-cbfSubarrayAdminMode:

::

    cbfSubarrayAdminMode.ON-LINE: 0

* cbfSubarrayHealthState

.. _low-subarray-cbfSubarrayHealthState:

::

    cbfSubarrayHealthState.UNKNOWN: 3

* pssSubarrayHealthState

.. _low-subarray-pssSubarrayHealthState:

::

    pssSubarrayHealthState.UNKNOWN: 3

* validScanConfiguration

.. _low-subarray-validScanConfiguration:

::

    '{"interface": "https://schema.skao.int/ska-low-csp-configurescan/0.0", .. ,"transaction_id": "txn-local-20240219-197430219"}'
