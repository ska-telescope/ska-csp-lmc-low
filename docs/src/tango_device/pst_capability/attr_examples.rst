:orphan:

##############################################
Low CSP.LMC Capability PST Attributes Examples
##############################################

* buildState

.. _low-pstcapability-buildstate:

::

    'ska-csp-lmc-low, 0.9.0, SKA LOW CSP.LMC'

* longrunningcommandresult

.. _low-pstcapability-longrunningcommandresult:

::

    ('1708358750.7680037_31446241612572_On', '[0, "on completed  1/1"]')

* longrunningcommandstatus

.. _low-pstcapability-longrunningcommandstatus:

::

    ('1708442895.8833039_185769114989020_On', 'COMPLETED')

* devicesJson

.. _low-pstcapability-devicesjson:

::

    '{
        "devices_id": [1]
        "devices_deployed": 1,
        "pst": 
        [
            {
                "dev_id": 1,
                "fqdn": "low-pst/beam/01",
                "state": "DISABLE",
                "health_state": "UNKNOWN",
                "admin_mode": "OFFLINE",
                "data_received": 0,
                "data_receive_rate": 0.0,
                "data_dropped": 0,
                "data_drop_rate": 0.0,
                "processing_mode": "IDLE",
                "obs_state": "IDLE"
            }
        ]
    }'

* devicesDataDropRateMonitoring

.. _low-pstcapability-devicesDataDropRateMonitoring:

::

    '{
        "low-pst/beam/01": {
            "value": 0.0,
            "avg_value": 0.0,
            "stddev_value": 0.0,
            "avg_buffer": [],
            "stddev_buffer": [],
        },
        "low-pst/beam/02": {
            "value": 0.0,
            "avg_value": 0.0,
            "stddev_value": 0.0,
            "avg_buffer": [],
            "stddev_buffer": [],
        },
        "low-pst/beam/03": {
            "value": 0.0,
            "avg_value": 0.0,
            "stddev_value": 0.0,
            "avg_buffer": [],
            "stddev_buffer": [],
        },
    }'



* devicesDataReceiveRateMonitoring

.. _low-pstcapability-devicesDataReceiveRateMonitoring:

::

    '{
        "low-pst/beam/01": {
            "value": 0.0,
            "avg_value": 0.0,
            "stddev_value": 0.0,
            "avg_buffer": [],
            "stddev_buffer": [],
        },
        "low-pst/beam/02": {
            "value": 0.0,
            "avg_value": 0.0,
            "stddev_value": 0.0,
            "avg_buffer": [],
            "stddev_buffer": [],
        },
        "low-pst/beam/03": {
            "value": 0.0,
            "avg_value": 0.0,
            "stddev_value": 0.0,
            "avg_buffer": [],
            "stddev_buffer": [],
        },
    }'
