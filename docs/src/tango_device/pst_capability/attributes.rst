#####################################
Low CSP.LMC Capability Pst Attributes
#####################################

The following table contains information about the Low CSP LMC attributes of the Pst capability Tango device.


.. list-table::
   :widths: 15 25 15 5 5 5 25
   :header-rows: 1

   * - Name
     - Description
     - Type
     - R/W 
     - Change event
     - Archived
     - Example
   * - State
     - State
     - DevState
     - R
     - yes
     - yes
     - tango._tango.DevState.ON
   * - Status
     - Status
     - DevString
     - R
     - yes
     - yes
     - The device is in DISABLE state.
   * - adminMode
     - The admin mode reported for this device. It may interpret the current device condition and condition of all managed devices to set this. Most possibly an aggregate attribute.
     - Enum (AdminMode)
     - R/W
     - yes
     - yes
     - adminMode.ONLINE: 0
   * - healthState
     - Read the Health State of the device. It interprets the current device condition and condition of all managed devices to set this. Most possibly an aggregate attribute.
     - Enum (HealthState)
     - R
     - yes
     - yes
     - healthState.UNKNOWN: 3
   * - versionId
     - Read the Version Id of the device.
     - String
     - R
     - no
     - no
     - '0.11.1'
   * - buildState
     - Build state of the device.
     - String
     - R
     - no
     - no
     - :ref:`example <low-pstcapability-buildstate>`
   * - controlMode
     - Read the Control Mode of the device. The control mode of the device are REMOTE, LOCAL Tango Device accepts only from a ‘local’ client and ignores commands and queries received from TM or any other ‘remote’ clients. The Local clients has to release LOCAL control before REMOTE clients can take control again.
     - Enum (ControlMode)
     - R/W
     - yes
     - yes
     - controlMode.REMOTE: 0
   * - loggingLevel
     - Read the logging level of the device. Initialises to LoggingLevelDefault on startup.See :py:class:`~ska_control_model.LoggingLevel`.
     - Enum (LoggingLevel)
     - R/W
     - 
     -
     - loggingLevel.INFO: 4
   * - simulationMode
     - Read the Simulation Mode of the device. Some devices may implement both modes, while others will have simulators that set simulationMode to True while the real devices always set simulationMode to False.
     - Enum (ControlMode)
     - R/W
     - yes
     - yes
     - simulationMode.FALSE: 0
   * - testMode
     - Read the Test Mode of the device. Either no test mode or an indication of the test mode.
     - Enum (TestMode)
     - R/W
     - yes
     - yes
     - testMode.NONE: 0
   * - loggingTargets
     - Read the additional logging targets of the device. Note that this excludes the handlers provided by the ska_ser_logging library defaults - initialises to LoggingTargetsDefault on startup. 
     - String
     - R/W
     -
     -
     - ['tango::logger',]
   * - longRunningCommandIDsInQueue
     - Read the IDs of the long running commands in the queue. Every client that executes a command will receive a command ID as response. Keep track of IDs in the queue. Pop off from front as they complete.
     - (String,)
     - R
     - yes
     - yes
     - 
   * - longRunningCommandProgress
     - Read the progress of the currently executing long running command. ID, progress of the currently executing command. Clients can subscribe to on_change event and wait for the ID they are interested in.
     - (String,)
     - R
     - yes
     - yes
     - 
   * - longRunningCommandResult
     - Read the result of the completed long running command. Reports unique_id, json-encoded result. Clients can subscribe to on_change event and wait for the ID they are interested in.
     - Tuple [str, str]
     - R
     - yes
     - yes
     - :ref:`example <low-pstcapability-longrunningcommandresult>`
   * - longRunningCommandStatus
     - Read the status of the currently executing long runnin ID, status pair of the currently executing command. Clients can subscribe to on_change event and wait for the ID they are interested in.
     - (String, ),
     - R
     - yes
     - yes
     - :ref:`example <low-pstcapability-longrunningcommandstatus>`
   * - longRunningCommandsInQueue
     - Read the long running commands in the queue. Keep track of which commands are in the queue. Pop off from front as they complete. 
     - (String, ),
     - R
     - yes
     - yes
     - 
   * - isCommunicating
     - Whether the CSP.LMC PstCapability TANGO Device is communicating with the deployed PST Beams of the PST sub-systems. This flag is reported as False when the CSP.LMC PstCapability is not able to communicate with the deployed PST Beams.
     - Bool
     - R
     - no
     - no
     - True
   * - devicesSwVersion
     - Report the deployed Pst beams attribute versionId aggregated in a list.
     - (String, ), max_dim = 16
     - R
     - no
     - no
     - ('0.11.1',)
   * - devicesHwVersion
     - Report the deployed Pst beams attribute hardware version aggregated in a list.
     - (String, ), max_dim = 16
     - R
     - no
     - no
     - ('0.11.1',) / NOTE: not available in the Pst device
   * - devicesFwVersion
     - Report the deployed Pst beams attribute firmware version aggregated in a list.
     - (String,), max_dim = 16
     - R
     - no
     - no
     - ('0.11.1',) / NOTE: not available in the Pst device
   * - devicesState
     - Report the deployed Pst beams attribute State aggregated in a list.
     - (DevString, ), max_dim = 16
     - R
     - yes
     - no
     - ('DISABLE',)
   * - devicesHealthState
     - Report the deployed Pst beams attribute AdminMode aggregated in a list.
     - (String,), max_dim = 16
     - R
     - yes
     - no
     - ("UNKNOWN",)
   * - devicesAdminMode
     - Report the deployed Pst beams attribute AdminMode aggregated in a list.
     - (String, ), max_dim = 16
     - R
     - yes
     - yes
     - ("ONLINE", )
   * - devicesDeployed
     - Report the number of the Pst beams deployed
     - Int
     - R
     - no
     - no
     - 2
   * - devicesId
     - Report the deployed Pst beams attribute deviceID aggregated in a list
     - (Int, ), max_dim = 16
     - R
     - no
     - no
     - (1, 2)
   * - devicesPstProcessingMode
     - Report the deployed Pst beams attribute pstProcessingMode aggregated in a list.
     - (String, ), max_dim = 16
     - R
     - yes
     - no
     - ('IDLE',)
   * - devicesObsState
     - Report the deployed Pst beams attribute obsState aggregated in a list.
     - (String, ), max_dim = 16
     - R
     - yes
     - no
     - ("EMPTY", )
   * - devicesFqdn
     - Report the deployed Pst beams fqdn aggregated in a list.
     - (string,), max_dim = 16
     - R 
     - no
     - no
     - ('low-pst/beam/01','low-pst/beam/03',)
   * - devicesJson
     - Report some Pst beams attributes aggregated in a json formatted string.
     - String
     - R
     - yes
     - no
     - :ref:`example <low-pstcapability-devicesJson>`
   * - devicesDataReceived
     - Report the deployed Pst beams attribute dataReceived aggregated in a list.
     - (int, ), max_dim = 16
     - R
     - yes
     - no
     - (0, )
   * - devicesDataReceiveRate
     - Report the deployed Pst beams attribute dataReceiveRate aggregated in a list.
     - (Float,), max_dim = 16
     - R
     - yes
     - no
     - (0.0, )
   * - devicesDataDropped
     - Report the deployed Pst beams attribute dataDropped aggregated in a list.
     - (int, ), max_dim = 16
     - R
     - yes
     - no
     - (0, )
   * - devicesDataDropRate
     - Report the deployed Pst beams attribute dataDropRate aggregated in a list.
     - (Float,), max_dim = 16
     - R
     - yes
     - no
     - (0.0, )
   * - devicesDataDropRateAvg
     - Report the data drop rate average value of the deployed Pst beams aggregated in a list.
     - (Float,), max_dim = 16
     - R
     - yes
     - no
     - (0.0, )
   * - devicesDataDropRateStddev
     - Report the data drop rate standard deviation value of the deployed Pst beams aggregated in a list.
     - (Float,), max_dim = 16
     - R
     - yes
     - no
     - (0.0, )
   * - devicesDataReceiveRateAvg
     - Report the data receive rate average value of the deployed Pst beams aggregated in a list.
     - (Float,), max_dim = 16
     - R
     - yes
     - no
     - (0.0, )
   * - devicesDataReceiveRateStddev
     - Report the data receive rate standard deviation value of the deployed Pst beams aggregated in a list.
     - (Float,), max_dim = 16
     - R
     - yes
     - no
     - (0.0, )
   * - devicesDataDropRateMonitoring
     - Report the data drop rate monitoring values of the deployed Pst beams aggregated in a Json formatted string.
     - String
     - R
     - yes
     - no
     - :ref:`example <low-pstcapability-devicesDataDropRateMonitoring>`
   * - devicesDataReceiveRateMonitoring
     - Report the data receive rate monitoring values of the deployed Pst beams aggregated in a Json formatted string.
     - String
     - R
     - yes
     - no
     - :ref:`example <low-pstcapability-devicesDataReceiveRateMonitoring>`