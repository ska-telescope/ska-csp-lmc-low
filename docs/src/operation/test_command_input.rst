###################
Json Command Input
###################

The following Json string are used as command input in our integration tests to vallidate the Low CSP.LMC behavior.

Assign Resources without PST
############################

:: 

    {
        "interface": "https://schema.skao.int/ska-low-csp-assignresources/3.1",
        "common": {
            "subarray_id": 1
        },
        "lowcbf":{}
    }

Assign Resources with PST
############################

::

    {
        "interface": "https://schema.skao.int/ska-low-csp-assignresources/3.1",
        "common": {
            "subarray_id": 1
        },
        "lowcbf":{
        },
        "pst":{"beams_id":[1]}
    }


Release Resources
#################

::

    {
        "interface": "https://schema.skao.int/ska-low-csp-releaseresources/3.0",
        "common": {
            "subarray_id": 1
        },
        "lowcbf": {},
        "pst": {
            "beams_id": [1]
        },
    }

Configure without PST
#####################
    
::    

    {
        "interface": "https://schema.skao.int/ska-low-csp-configure/3.1",
        "subarray": {
            "subarray_name": "science period 23"
        },
        "common": {
            "config_id": "sbi-mvp01-20200325-00001-science_A",
            "subarray_id": 1,
            "frequency_band": "low"
        },         
        "lowcbf": {
            "stations": {
                "stns": [[1, 1], [2, 1], [3, 1], [4, 1], [5, 1], [6, 1]],
                "stn_beams": [
                    {
                        "beam_id": 1,
                        "freq_ids": [400],
                        "delay_poly": "tango://delays.skao.int/low/stn-beam/1"
                    }
                ]
            },
            "vis": {
                "fsp": {"firmware": "vis", "fsp_ids": [1]},
                "stn_beams": [
                    {
                        "stn_beam_id": 1,
                        "host": [[0, "192.168.0.1"]],
                        "port": [[0, 9000, 1]],
                        "mac": [[0, "02-03-04-0a-0b-0c"]],
                        "integration_ms": 849
                    }
                ]
            }
        }
    }   


Configure with PST
##################

::
    
    {
        "interface": "https://schema.skao.int/ska-low-csp-configure/3.1",
        "subarray": {
        "subarray_name": "science period 23"
        },
        "common": {
            "config_id": "sbi-mvp01-20200325-00001-science_A",
            "subarray_id": 1,
            "eb_id": "eb-x449-20231105-34696"
        },         
        "lowcbf": {
            "stations": {
                "stns": [[1, 1], [2, 1], [3, 1], [4, 1], [5, 1], [6, 1]],
                "stn_beams": [
                    {
                        "beam_id": 1,
                        "freq_ids": [400],
                        "delay_poly": "tango://delays.skao.int/low/stn-beam/1"
                    }
                ]
            },
            "vis": {
                "fsp": {"firmware": "vis", "fsp_ids": [1]},
                "stn_beams": [
                    {
                        "stn_beam_id": 1,
                        "host": [[0, "192.168.0.1"]],
                        "port": [[0, 9000, 1]],
                        "mac": [[0, "02-03-04-0a-0b-0c"]],
                        "integration_ms": 849
                    }
                ]
            },
            "timing_beams": {
                "fsp": {"firmware": "pst", "fsp_ids": [2]},
                "beams": [
                    {
                        "pst_beam_id": 1,
                        "stn_beam_id": 1,
                        "stn_weights": [0.9, 1.0, 1.0, 1.0, 0.9, 1.0],
                        "delay_poly": "tango://delays.skao.int/low/stn-beam/1",
                        "jones": "tango://jones.skao.int/low/stn-beam/1",
                    }
                ]
            }
        }, 
        "pst": {
            "beams" : [
                {
                "beam_id": 1,
                "scan": {
                    "activation_time": "2022-01-19T23:07:45Z",
                    "bits_per_sample": 32,
                    "num_of_polarizations": 2,
                    "udp_nsamp": 32,
                    "wt_nsamp": 32,
                    "udp_nchan": 24,
                    "num_frequency_channels": 432,
                    "centre_frequency": 1000000000.0,
                    "total_bandwidth": 1562500.0,
                    "observation_mode": "VOLTAGE_RECORDER",
                    "observer_id": "jdoe",
                    "project_id": "project1",
                    "pointing_id": "pointing1",
                    "source": "J1921+2153",
                    "itrf": [
                        5109360.133,
                        2006852.586,
                        -3238948.127
                        ],
                    "receiver_id": "receiver3",
                    "feed_polarization": "CIRC",
                    "feed_handedness": 1,
                    "feed_angle": 10.0,
                    "feed_tracking_mode": "FA",
                    "feed_position_angle": 0.0,
                    "oversampling_ratio": [ 4, 3 ],
                        "coordinates": {
                        "ra": "19:21:44.815",
                        "dec": "21.884"
                        },
                    "max_scan_length": 300.0,
                    "subint_duration": 30.0,
                    "receptors": [ "MKT000" ],
                    "receptor_weights": [ 1.0 ],
                    "num_rfi_frequency_masks": 0,
                    "rfi_frequency_masks": [],
                    "destination_address": ["192.168.178.26", 9021],
                    "test_vector_id": "test_vector_id",
                    "num_channelization_stages": 1,
                    "channelization_stages": [
                            {
                            "num_filter_taps": 1,
                            "filter_coefficients": [ 1.0 ],
                            "num_frequency_channels": 10,
                            "oversampling_ratio": [ 4, 3 ]
                            }
                        ]
                    }
                }
            ]
        }
    }

Scan
####

::

    {
        "interface": "https://schema.skao.int/ska-low-csp-scan/3.1",
        "common": {
            "subarray_id": 1
        },
        "lowcbf": {
            "scan_id":987654321
        }
    }

