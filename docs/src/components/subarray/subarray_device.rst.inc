The core CSP functionality, configuration and execution of signal processing, is configured, controlled 
and monitored via subarrays.

CSP Subarray makes provision to TM to configure a subarray, select Processing Mode and related parameters, 
specify when to start/stop signal processing and/or generation of output products.
TM accesses directly a CSP Subarray to:

* Assign resources
* Configure a scan
* Control and monitor states/operations

Resources assignment
--------------------

The assignment of Capabilities to a subarray (*subarray composition*) is performed 
in advance of a scan configuration. 
Assignable Capabilities for LOW CSP.LMC Subarrays are:

* stations 
* station beams
* tied-array beams: Search Beams, Timing Beams and Vlbi Beams.

In general resource assignment to a subarray is exclusive, but in some cases the same Capability instance
may be used in shared manner by more then one subarray.

Inherent Capabilities
---------------------

Each CSP subarray has also five permanently assigned *inherent Capabilities*: 

* Station Beam
* Correlation
* PSS
* PST
* VLBI

An inherent Capability can be enabled or disabled, but cannot assigned or removed to/from a subarray. 
They correspond to the CSP Low Processing Modes and are configured via a scan configuration.

Scan configuration
------------------

TM provides a complete scan configuration to a subarray via an ASCII JSON encoded string.
Parameters specified via a JSON string are implemented as TANGO Device attributes 
and can be accessed and modified directly using the buil-in TANGO method *write_attribute*.
When a complete and coherent scan configuration is received and the subarray configuration 
(or re-configuration) completed,  the subarray it's ready to observe.


Control and Monitoring
----------------------

Each Low CSP.LMC Subarray maintains and reports the status and state transitions for the 
Low CSP subarray as a whole and for the individual assigned resources.

In addition to pre-configured status reporting, a Low CSP Subarray makes provision for the TM and any authorized client, 
to obtain the value of any subarray attribute.

Low.CSP Subarray TANGO Device name
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
The Low.CSP Subarray TANGO Device name is defined in the document “SKA1 TANGO Naming Conventions”::

        low-csp/subarray/XY

where XY is a two digit number in range [01,..,16].

Low.CSP Subarray operational state
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Low.CSP Subarray intelligently rolls‐up the operational state of all components used by the sub‐array and reports the overall 
operational state for the sub‐array.

The Low.CSP Subarray supports the following sub-set of the TANGO Device states:

* UNKNOWN:  Low.CSP sub-array is unresponsive, e.g. due to communication loss.
* OFF: The sub-array is not enabled to perform signal processing. The sub‐array is ‘empty’.
* INIT: Initialization of the monitor and control network,equipment and functionality is being performed.
* DISABLE: Low.CSP sub-array is administratively disabled, basic monitor and control functionality is available but signal
  processing functionality is not available.
* ON:  The sub-array is enabled to perform signal processing. The sub‐array observing state is EMPTY if receptors have not 
  been assigned to the sub‐array, yet.
* ALARM:  Quality Factor for at least one attribute crossed the ALARM threshold. Part of functionality may be unavailable.
* FAULT: Unrecoverable fault has been detected. The sub‐array is not available for use and maintainer/operator intervention 
  might be required.

Low.CSP Subarray observing state
--------------------------------
The sub‐array Observing State indicates status related to scan configuration and
execution.

The Low.CSP Subarray observing state adheres to the State Machine defined by ADR-8.

Low.CSP Subarray TANGO Device Commands
--------------------------------------
Low.CSP Subarray implements the standard set of commands as specified in:

* Standard set of TANGO Device commands as defined in TANGO User Manual
* Standard set of SKA TANGO Device commands
* Command specific to Low.CSP Subarray as described in API section.

Low.CSP makes provision for TM to request state transitions for individual sub‐systems and/or Capabilities.

Low.CSP Subarray TANGO Device Attributes
----------------------------------------
Low.CSP Subarray implements the standard set of attributes as specified in:

* Standard set of TANGO Device attributes as defined in TANGO User Manual
* The standard set of SKA TANGO Device attributes as defined for the SKA Subarray TANGO Device.
* Attributes to Low.CSP Subarray as described in API section.

Virtually all parameters provided in scan configuration are exposed as attributes either by
Low.CSP Subarray or by individual Capabilities.
