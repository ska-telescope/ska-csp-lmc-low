Low.CSP Controller is the top‐level TANGO Device and the primary point of contact for monitor and control
of the Low.CSP Sub-syste.

The Low.CSP Controller represents Low.CSP Sub-system as a unit for control and monitoring for
general operations. 

Low.CSP Controller main roles are:

* To be the central control node for Low.CSP. The Controller provides a single point of access 
  for control of the Low.CSP as a whole, this includes provision for housekeeping and supervisory 
  commands including: power‐up, power‐down, power management, restart (re‐initialize), support 
  for firmware and software upgrades, etc.
* To provide rolled‐up reporting for the overall Low.CSP status. Low.CSP Controller  monitors and 
  intelligently rolls‐up status reported by Low.CSP equipment, sub‐arrays and capabilities and 
  maintains a standard set of states and modes, as defined in the document “SKA1 Control System Guidelines”.
  State transitions are reported using standard TANGO mechanism.  
* To implement a set of attributes that represent the status and configuration of the Low.CSP as a whole and, 
  where required, report availability, status and configuration of the Low.CSP equipment, components and 
  capabilities (in the form of lists or tables or JSON string).  
* To maintains the pool of resources (VCCs, FSPs, Search/Timing/VLBI beams), keep track of allocation to sub‐arrays and provide 
  reports on resource availability, allocation, and more.

Low.CSP Controller implementation is based on (derived from) the standard SKA1 TANGO Device Controller;
Low.CSP Controller  implements the standard TANGO API, aligned with the document “SKA1 Control System Guidelines”.

The interface is between a TANGO client and a TANGO Device. The TANGO Device exposes attributes and
commands to clients.

The roles of the interfacing systems are:

* TANGO Clients: Low.TMC sub-system TANGO client(s).
* TANGO Device: Low.CSP sub-system implements Low.CSP Controller.

The clients use requests to obtain read and/or write access to TANGO device attributes, and to invoke TANGO
device commands.

Low.CSP Controller TANGO Device name
------------------------------------

The Controller TANGO Device name is defined in the document “SKA1 TANGO Naming Conventions”::

        low-csp/control/0

Low.CSP Controller TANGO Device Properties
------------------------------------------

The Low.CSP Controller device has a standard set of properties inherited from the SKA Controller TANGO Device and a number of specific 
properties documented in the Controller API section.

Low.CSP Controller TANGO Device States and Modes
------------------------------------------------

Low.CSP Controller implements the standard set of state and mode attributes defined by the SKA Control Model.

Low.CSP Controller reports on behalf of the Low.CSP Sub-system – unless explicitly stated otherwise, the state
and mode attributes implemented by the Low.CSP Controller represent the status of the Low.CSP as a whole, not
the status of the Low.CSP Controller itself.

Low.CSP Controller operational state
------------------------------------

The Low.CSP Controller supports the following sub-set of the TANGO Device states:

* UNKNOWN:  Low.CSP is unresponsive, e.g. due to communication loss. This state cannot be reported by CSP itself.
* OFF: power is disconnected. This state cannot be reported by CSP itself
* INIT: Initialization of the monitor and control network,equipment and functionality is being performed. During initialization 
  commands that request state transition to OFF (power-down) or re‐start initialization are accepted.
* DISABLE: Low.CSP is administratively disabled, either by setting adminMode=OFFLINE or NOT-FITTED.
  Basic monitor and control functionality is available but signal processing functionality and related commands are not available. All
  sub‐arrays are empty (OFF) and IDLE; all resources (receptors, tide‐array beams) are placed in the pool of unused resources.)
* STANDBY: Low‐power state, Low.CSP uses < 5% of nominal power. Basic monitor and control functionality is available, including the commands to
  request state transition to ON, OFF, DISABLE, or INIT. Signal processing functionality and related commands are not available.
  All sub‐arrays are empty (OFF) and IDLE; all resources (receptors, tide‐array beams) are placed in the pool of unused resources.).
* ON:  At least a minimum of CSP signal processing capability is available; at least one receptor and one sub‐array can be used 
  for observing (either for scientific observations or for testing andmaintenance). Low.CSP is in normal operational state,
  all commands, including commands to increase/decrease functional availability and power consumption are available.
* ALARM:  Quality Factor for at least one attribute crossed the ALARM threshold. Part of Low.CSP functionality may be unavailable.
* FAULT: Unrecoverable fault has been detected, Low.CSP is not available for use at all, maintainer/operator intervention is required in 
  order to return to ON, STANDBY, or DISABLE. Depending on the extent of failure commands restart and init, as well as status reporting 
  may be available.

Low.CSP Controller TANGO Device Commands
-----------------------------------------

Low.CSP Controller implements the standard set of commands as specified in:

* Standard set of TANGO Device commands as defined in TANGO User Manual
* Standard set of SKA TANGO Device commands
* Command specific to Low.CSP Controller as described in API section.

Low.CSP makes provision for TM to request state transitions for individual sub‐systems and/or Capabilities.

Low.CSP Controller TANGO Device Attributes
------------------------------------------

Low.CSP Controller implements the standard set of attributes as specified in:

* Standard set of TANGO Device attributes as defined in TANGO User Manual
* The standard set of SKA TANGO Device attributes as defined for the SKA Controller TANGO Device.
* Attributes to Low.CSP Controller as described in API section.

The Low.CSP Controller maintains the ‘pool of resources’ and is able to provide information regarding sub‐array
membership, status and usage.
