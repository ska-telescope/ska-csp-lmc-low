#########################
Software Components
#########################

The top-level software components provided by Low.CSP LMC API are:

* :ref:`Low.CSP LMC Controller <Low.CSP LMC Controller>`
* :ref:`Low.CSP LMC Subarray <Low.CSP LMC Subarray>`

Components listed above are implemented as TANGO devices, i.e. classes that implement standard TANGO API.
The CSP.LMC TANGO devices are based on the standard SKA1 TANGO Element Devices provided via the *SKA Base Classes*.

**********************
Low.CSP LMC Controller
**********************

.. include:: controller/controller_device.rst.inc

********************
Low.CSP LMC Subarray
********************

.. include:: subarray/subarray_device.rst.inc

